<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Passion_controller extends CI_Controller 
{   
    public function __construct()
    {
        parent::__construct();
        $this->load->model('passion_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('test_helper');
    }
    public function index()
    {
        $this->load->view('index');
    }
    public function loadPages()
    {
        $page=$this->uri->segment('1');
        $this->load->view($page);
    }
    public function register()
    {
       $data['cand_id']=$this->passion_model->get_id(); 
       $this->load->view('register',$data);
    }
    public function candidate_Registration()
    {
        
        $cand_registration=$this->passion_model->candidate_Registration();
        if($cand_registration)
        {
            $this->session->set_flashdata('candidateregistration','You Are Registered Successfully');
            redirect('login');
        }
        else
        {
            $this->session->set_flashdata('candidatregistrationerror','Failed.... Please try again...');
            redirect('register');
        }
        
    }
    
     public function login()
    {
        $login=$this->passion_model->login();

        if($login)
        {
            $this->session->set_userdata('cand_data',$login);
            $status=$this->session->userdata['cand_data']['status'];

            if($status=='1') //approved candidate
            {
                redirect('register_final');
            }
            if($status=='0')
            {
                $this->session->set_flashdata('loginerror','Username or Password Is Incorrect');
                redirect('login');
            }
        }
        else
		{
			$this->session->set_flashdata('loginerror','Username or Password Is Incorrect');
			redirect('login');
		}
    }
    
    public function newsLetter()
    {
        $newsletter=$this->passion_model->newsLetter();
        if($newsletter)
        {
            $this->session->set_flashdata('newsletter','You Are Subscribe Successfully');
            redirect(base_url());
        }
        else
        {
            $this->session->set_flashdata('$newslettererror','Failed.... Please try again...');
            redirect(base_url());
        }
    }
     public function get_question_list($cand_level='',$topic_id='')
    {
        $data['general_question'] = $this->passion_model->get_general_question();
            
        $this->load->view('aptitude',$data);   
    }

    public function aptitude_submission()
    {
        $aptitude_result=$this->passion_model->save_aptitude_result();

        redirect('report');

    }
     public function report()
    {
    
        $cand_id              = 1;
        $report['result']     = $this->passion_model->get_report($cand_id);
        
        if($report)
        {
            $report['cand_id']    = $cand_id;            
           
            $this->load->view('report',$report);
            
        }
        else
        {
            redirect('logout');
        }
    }
    
     public function second_register()
    {

        $cand_id = $data['cand_id'] = $this->session->userdata['cand_data']['cand_id'];
      // $cand_id = $this->session->userdata('cand_id');
    //   $data['name'] = $this->session->userdata('name');
       $name =  $data['name'] = $this->session->userdata['cand_data']['name'];

       $data['cand_details']=$this->passion_model->get_cand_details($cand_id); 
       $this->load->view('register_final', $data);
    }

    public function do_upload_profile_image() 
    {
        $config['upload_path'] = './upload/profile_images';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '300000';
        $config['max_width'] = '150';
        $config['max_height'] = '200';
        $config['overwrite'] = TRUE;
        $profileimage = time().$_FILES['profile_img']['name'];
        $config['file_name'] = $profileimage;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('profile_img')) 
        {
            // echo "string";exit();
            $error = array('error' => $this->upload->display_errors()); 
            print_r($error);
            return FALSE;
        } 
        else 
        {
            $data = array('upload_data' => $this->upload->data());
            return $profileimage;
        }
    }
    public function do_upload_signature() 
    {
        $config['upload_path'] = './upload/signature';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '300000';
        $config['max_width'] = '150';
        $config['max_height'] = '200';
        $config['overwrite'] = TRUE;
        $signature = time().$_FILES['sign']['name']; 
        $config['file_name'] = $signature;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('sign')) 
        {
            $error = array('error' => $this->upload->display_errors()); 
            print_r($error);
            return FALSE;
        } 
        else 
        {
            $data = array('upload_data' => $this->upload->data());
            return $signature;
        }
    }

    public function profile_registration()
    {
        // $data['cand_id'] = $this->session->userdata('cand_id');
        // $cand_id = $this->session->userdata('cand_id');
        $image=$this->do_upload_profile_image();
        if(!$image)
        {
            $this->session->set_flashdata('imageerr','Failed...Image Size or Format Mismatches!');
            redirect('register_final');
        }
        $signature=$this->do_upload_signature();
        if(!$signature)
        {
            $this->session->set_flashdata('signatureerr','Failed...Signature Size or Format Mismatches!');
            redirect('register_final');
        }
        $cand_registration=$this->passion_model->profile_registration($image,$signature);
        if($cand_registration)
        {
            $this->session->set_flashdata('profileregistration','You Are Registered Successfully');
            redirect('login');
        }
        else
        {
            $this->session->set_flashdata('profileregistrationerror','Failed.... Please try again...');
            redirect('register_final');
        }
        
    }

}
?>
