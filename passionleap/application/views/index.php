<!doctype html>
<?php include('include/header.php');?>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner">
        <div class="container">
            <div class="banner-content wow fadeInUp" data-wow-delay="0.1s">
                <h4>Right Opportunities for </h4>
                <h2>the Right Talent</h2>
                <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one way or other but one may not be aware of what one can be.To create awareness in ones passion is our passion. PassionLeap helps the candidates to follow their passion by getting trained in various professional courses,which have yielded many a candidate to carve a niche in their domains of passion. Our professional courses are framed based on expert research to cater the basic foundation of the candidate and to build the efficiency of the candidate at various levels of advancement to engross professional outlook and compete in the contemporary opportunities. </p>
                <span class="mouse-icon">
                    <div class="mouse-scroller"></div>
                </span>
            </div>
        </div>
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
                 <?php
                    if($this->session->flashdata('newsletter'))
                    {
                      echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('newsletter').'</div>';   
                    }
                    if($this->session->flashdata('newslettererror'))
                    {
                      echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('newslettererror').'</div>';   
                    }
                    ?>
                <div class="row">
                    <div class="col-md-5 span3 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/future.png" alt="" />
                        </div>
                       
                    </div>
                     
                    <div class="col-md-7 wow fadeInUp" data-wow-delay="0.3s">
                        <h4>Passionate About</h4>
                        <h2>Creating Future</h2>
                        <p>Passion Leap is a movement to raise the spirits of young and dynamic youth in Kerala who aim for a passionate yet successful life ahead. In the present days, when Academics has become only a mere eligibility and a qualification to compete for career building, the responsibility lies with the candidate to carve the career independently, sooner or later upon achieving a qualification of their choice. To make the candidate ready to leap into a better career prospects is the objective of PassionLeap. Come, join our professional courses where your passion lies. Because, to build your career is our passion.</p>
                    </div>
                </div>
            </div>
        </section>

        <section class="site-section section-two">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 wow fadeInUp" data-wow-delay="0.1s"><br><br>
                        <h2>About <span style="color:#0a0808">Passion Leap</span></h2>
                        <p>Passion Leap is a movement to raise the spirits of young and dynamic youth in Kerala who aim for a passionate yet successful life ahead. In the present days, when Academics has become only a mere eligibility and a qualification to compete for career building, the responsibility lies with the candidateto carve the career independently, sooner or later upon achieving a qualification of their choice. To make the candidate ready to leap into a better career prospects is the objective of PassionLeap. Come, join our professional courses where your passion lies. Because, to build your career is our passion.</p>
                    </div>
                    <div class="col-md-5 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/about.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="site-section section-three">
            <div class="container">
                <div class="slider-holder wow fadeInUp" data-wow-delay="0.1s">

                    <div class="owl-carousel owl-theme" id="txtSlide">
                        <div class="item">
                            <h2>Have you met your <span>Passion</span></h2>
                        </div>

                        <div class="item">
                            <h2>How to Identify your<span>Passion</span></h2>
                        </div>

                        <div class="item">
                            <h2>How to turn your Passion into <br><span>Profession</span></h2>
                        </div>

                        <div class="item">
                            <h2>Discover your Passion <span>now</span></h2>
                        </div>

                        <div class="item">
                            <h2>Share Your <span>Passion</span></h2>
                        </div>
                    </div>

                    <div class="owl-carousel owl-carousel-sub" id="mainSlide">
                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide1.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>Being Clever is to be clear<br> in identifying
                                        <span>Your Passion</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>


                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide2.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>Don’t go for a job just because <br>someone saidits good for
                                        <span>you</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide3.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>Dreams Really do come true.. <br>when passion awakes inside of
                                        <span>you</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide4.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>Your Energy in Action is the <br>Passion in
                                        <span>you</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide5.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>Follow your Passion and Success <br>will follow
                                        <span>you</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide6.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>Aptitude + Interest = <span>PASSION</span><br> Energy + Action = <span>SUCCESS</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide7.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>
                                        Allow your Passion, to become your purpose, and it will one day become your <span>profession</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide8.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>


                                        Check your Passion <span>Grit</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                    <a href="#">Take a Test</a>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide10.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>
                                        Explore <span>Courses</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>


                        <div class="item">
                            <div class="row">
                                <div class="col-md-5" data-animation-in="fadeInLeft">
                                    <div class="item-holder">
                                        <img src="<?php echo base_url();?>assets/img/slide9.png" alt="" />
                                    </div>
                                </div>
                                <div class="col-md-7" data-animation-in="fadeInRight">
                                    <h4>
                                        Register<span>Now</span></h4>
                                    <p>Passion Leap is a visionary that looks forward to mould the right candidate for the right job. In order to mould a right professional,we look for the best of ability and passion in the candidate. Everyone is talented in one.</p>
                                    <a href="#">Click Here</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>

        </section>
    </div>

    <!-- Content End -->

    <!-- Footer -->
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script>
        new WOW().init();

    </script>

    <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });




            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
</body>

</html>
