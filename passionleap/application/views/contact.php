<!doctype html>
<?php include('include/header.php');?>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
                 <?php
        if($this->session->flashdata('candidateregistration'))
        {
          echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('candidateregistration').'</div>';   
        }
        if($this->session->flashdata('candidateregisrationerror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('candidateregisrationerror').'</div>';   
        }
        ?>
                <form action="<?php echo base_url();?>passion_controller/candidate_Registration" method="post">
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="name">Name</label>
      <input type="name" class="form-control" id="name" name="name" placeholder="Name">
    </div>
    <div class="form-group col-md-6">
      <label for="email">Email Address</label>
      <input type="email" class="form-control" id="email" name="email" placeholder="Email Address">
    </div>
  </div>
<div class="form-row">
    <div class="form-group col-md-6">
<div class="form-check form-check-inline">
  <label>Gender</label>
</div>
	 <div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="gender" id="gender" value="Male">
  <label class="form-check-label" for="gender">Male</label>
</div>
<div class="form-check form-check-inline">
  <input class="form-check-input" type="checkbox" name="gender" id="gender" value="Female">
  <label class="form-check-label" for="gender">Female</label>
</div>
</div>
    <div class="form-group col-md-3">
   <!--<form action="/action_page.php">-->
  DOB:
  <input type="date" name="dob" id="dob">
<!--</form>-->
</div>
	<div class="form-group col-md-3">
      <input type="text" class="form-control" name="age" id="age" placeholder="Age">
</div>
  </div>
	<div class="form-row">
    <div class="form-group col-md-6">
      <label for="contactnumber">Contact Number</label>
      <input type="contact" class="form-control" name="contactnumber" id="contactnumber" placeholder="Contact Number" onkeypress="return isNumberKey(event)"  maxlength="15" >
    </div>
    <div class="form-group col-md-6">
      <label for="inputPassword4">WhatsApp Number</label>
      <input type="whatsap" class="form-control" name="whatsappnumber" id="whatsappnumber" placeholder="WhatsApp Number" onkeypress="return isNumberKey(event)"  maxlength="15" >
    </div>
  </div>				
  <div class="form-group">
    <label for="address">Address</label>
    <input type="text" class="form-control" name="address" id="address" placeholder="1234 Main St">
  </div>
<div class="form-row">
    <div class="form-group col-md-6">
      <label for="father_name">Father's Name</label>
      <input type="name" class="form-control" name="father_name" id="father_name" placeholder="Father's Name">
    </div>
    <div class="form-group col-md-6">
      <label for="father_occupation">Father's Occupation</label>
      <input type="name" class="form-control" name="father_occupation" id="father_occupation" placeholder="Father's Occupation">
    </div>
  </div>
	<div class="form-row">
    <div class="form-group col-md-6">
      <label for="mother_name">Mother's Name</label>
      <input type="name" class="form-control" name="mother_name" id="mother_name" placeholder="Mother's Name">
    </div>
    <div class="form-group col-md-6">
      <label for="mother_occupation">Mother's Occupation</label>
      <input type="name" class="form-control" name="mother_occupation" id="mother_occupation" placeholder="Mother's Occupation">
    </div>
  </div>
 <div class="form-row">
    <div class="form-group col-md-6">
      <label for="altnumber">Alternative Number</label>
      <input type="name" class="form-control" name="altnumber" id="altnumber" placeholder="Alternative Number" onkeypress="return isNumberKey(event)"  maxlength="15" >
    </div>
    <div class="form-group col-md-6">
      <label for="qualification">Educational Qualification</label>
      <input type="name" class="form-control" name="qualification" id="qualification" placeholder="Educational Qualification">
    </div>
  </div>
  <div class="form-row">
    <div class="form-group col-md-3">
      <label for="course_1">Post Graduation</label>
      <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_1" value="PG"  readonly="readonly" />
      <input type="text" class="form-control" name="course[]" id="course_1">
    </div>
    <div class="form-group col-md-3">
      <label for="subject_1">Subject</label>
      <input type="text" class="form-control"  name="subject[]" id="subject_1">
    </div>
	  <div class="form-group col-md-3">
      <label for="pass_year_1">Year Of Passing</label>
      <select name="pass_year[]" id="pass_year_1" class="form-control">
        <option selected>Choose...</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="mark_1">Mark</label>
      <input type="text" class="form-control" name="mark[]" id="mark_1" onkeypress="return isNumberKey(event)">
    </div>
  </div>
	<div class="form-row">
    <div class="form-group col-md-3">
      <label for="course_2">Graduation</label>
      <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_2" value="UG"  readonly="readonly" />
      <input type="text" class="form-control" name="course[]" id="course_2">
    </div>
    <div class="form-group col-md-3">
      <label for="subject_2">Subject</label>
      <input type="text" class="form-control" name="subject[]" id="subject_2">
    </div>
	  <div class="form-group col-md-3">
      <label for="pass_year_2">Year Of Passing</label>
      <select name="pass_year[]" id="pass_year_2" class="form-control">
        <option selected>Choose...</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="mark_2">Mark</label>
      <input type="text" class="form-control" name="mark[]" id="mark_2" onkeypress="return isNumberKey(event)">
    </div>
  </div>
	<div class="form-row">
    <div class="form-group col-md-3">
      <label for="course_3">Graduation</label>
      <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_3" value="HSE"  readonly="readonly" />
      <input type="text" class="form-control" name="course[]" id="course_3">
    </div>
    <div class="form-group col-md-3">
      <label for="subject_3">Subject</label>
      <input type="text" class="form-control" name="subject[]" id="subject_3">
    </div>
	  <div class="form-group col-md-3">
      <label for="pass_year_3">Year Of Passing</label>
      <select name="pass_year[]" id="pass_year_3" class="form-control">
        <option selected>Choose...</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="mark_3">Mark</label>
      <input type="text" class="form-control" name="mark[]" id="mark_3" onkeypress="return isNumberKey(event)">
    </div>
  </div>
	<div class="form-row">
    <div class="form-group col-md-3">
      <label for="course_4">Graduation</label>
      <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_4" value="SSLC"  readonly="readonly" />
      <input type="text" class="form-control" name="course[]" id="course_4">
    </div>
    <div class="form-group col-md-3">
      <label for="subject_4">Subject</label>
      <input type="text" class="form-control" name="subject[]" id="subject_4">
    </div>
	  <div class="form-group col-md-3">
      <label for="pass_year_4">Year Of Passing</label>
      <select name="pass_year[]" id="pass_year_4" class="form-control">
        <option selected>Choose...</option>
        <option value="2016">2016</option>
        <option value="2017">2017</option>
        <option value="2018">2018</option>
        <option value="2019">2019</option>
        <option value="2020">2020</option>
      </select>
    </div>
    <div class="form-group col-md-3">
      <label for="mark_4">Mark</label>
      <input type="text" class="form-control" name="mark[]" id="mark_4" onkeypress="return isNumberKey(event)">
    </div>
  </div>				
  <div class="form-row">
    <div class="form-group col-md-6">
      <label for="work">Work Experience</label>
      <input type="name" class="form-control" name="work" id="work" placeholder="Work Experience">
    </div>
    <div class="form-group col-md-6">
      <label for="other">Other</label>
      <input type="name" class="form-control" name="other" id="other" placeholder="Other">
    </div>
  </div>
 <label for="message">Message</label>
    <textarea class="form-control is-invalid" name="message" id="message" placeholder="Drop Your Words" required></textarea>
  <div class="form-group">
    <div class="form-check">
      <input class="form-check-input" type="checkbox" id="gridCheck">
      <label class="form-check-label" for="gridCheck">
        Check me out
      </label>
    </div>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
            </div>
        </section>
    </div>
    <!-- Content End -->

    <!-- Footer -->
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script>
        new WOW().init();

    </script>

    <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });




            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });
        
        function isNumberKey(evt)
        {
           var charCode = (evt.which) ? evt.which : event.keyCode
                 if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
        
                 return true;
        }

    </script>
</body>

</html>
