<html lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <title>Passion Leap</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#f76448">
    <link rel="icon" href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon" sizes="16x16">
    <!-- Styles -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/style.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/owl.carousel.min.css">
    <link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
   
    <link href="<?php echo base_url();?>assets/css/style-change.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/intlTelInput.css">
    
</head>

<body>
    <!-- Header -->
    <header class="site-header">
        <div class="container">
            <div class="logo-holder">
                <img src="<?php echo base_url();?>assets/img/logo.svg" alt="" />
            </div>
            <div class="right-holder">
                <div class="search-bar">
                    <form>
                        <input type="text" placeholder="Search..">
                        <button type="submit"><span class="search-icon"></span></button>
                    </form>
                </div>
                <div class="main-menu">
                    <nav class="navbar navbar-expand-md navbar-dark">
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainMenu" aria-controls="mainMenu" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="menu-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="mainMenu">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>">Home</a></li>
                                <li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>service">Services</a></li>
                                <li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>register">Registration</a></li>
                                    <!--<li class="nav-item">
                                    <a class="nav-link hvr-underline-from-center" href="<?php echo base_url();?>contact">Contact</a>
                                    </li>-->
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>