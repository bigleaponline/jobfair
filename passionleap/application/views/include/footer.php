<footer class="site-footer">
        <div class="footer-content">
            <div class="container wow fadeInUp" data-wow-delay="0.1s">
                <h6>Suscribe our Newsletter</h6>

                <div class="newsletter">
                    <form action="<?php echo base_url();?>Passion_controller/newsLetter" method="post">
                        <input type="text" placeholder="Enter your Email" name="email">
                        <button type="submit"><span class="send-icon"></span></button>
                    </form>
                </div>
                <address>
                    4th Floor, Markaz Complex, Mavoor Road,<br> Calicut, 673004,
                    Email: info@passionleap.in,<br>
                    Contact: +91 9744 642 225
                </address>
                <ul class="social-media">
                    <li><a href="#"><img src="<?php echo base_url();?>assets/img/fb.png" alt="" /></a></li>
                    <li><a href="#"><img src="<?php echo base_url();?>assets/img/tw.png" alt="" /></a></li>
                    <li><a href="#"><img src="<?php echo base_url();?>assets/img/in.png" alt="" /></a></li>
                </ul>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <p>Copyright 2019 all rights reserved for Passion Leap</p>
            </div>
        </div>
    </footer>