
<?php 
// if($this->session->userdata('cand_data'))
//     $cand_id=$this->session->userdata['cand_data']['cand_id'];
$cand_id=1;
?>
<!doctype html>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
var submitted=false;
    $(document).ready(function(){  
        var user = '<?php echo $cand_id; ?>';
        if (typeof(Storage) !== "undefined") 
        {
            if (localStorage.user) 
            {
                users = localStorage.user;
            } 
            else 
            {
                localStorage.setItem("user", user);
            }
        }
        if (typeof(Storage) !== "undefined") 
        {
            if (localStorage.user) 
            {
                users = localStorage.user;
                console.log(users);
            }
        }
        if(users == user)
        {
        }
        else 
        {
            localStorage.removeItem("seconds");
            if (typeof(Storage) !== "undefined") 
            {
                localStorage.setItem("user", user);
            }
        }
        var seconds = 1201; 
        if (typeof(Storage) !== "undefined") 
        {
            if (localStorage.seconds) 
            {
                seconds = localStorage.seconds;
            }
        }
        function secondPassed() 
        {
            seconds--;
            var hours = Math.floor(seconds / 3600);
            var minutes = Math.floor((seconds - (hours*3600)) / 60);
            var remainingSeconds = Math.floor(seconds % 60);
            if (remainingSeconds < 10) 
            {
                remainingSeconds = "0" + remainingSeconds;
            }
            if (typeof(Storage) !== "undefined") 
            {
                localStorage.setItem("seconds", seconds);
            }
            document.getElementById('countdown').innerHTML = hours + ":" + minutes + ":" + remainingSeconds;
            if (seconds == 0) 
            {
                submitted = true;
                clearInterval(myVar);
                document.getElementById('countdown').innerHTML = "Time Out";
                if (typeof(Storage) !== "undefined") 
                {
                    localStorage.removeItem("seconds");
                }
                alert("Time Out....")
                document.getElementById("my_formm").submit();
            }
            else 
            {
                console.log(seconds);
            }
        }
    
    var myVar = setInterval(secondPassed, 1000);
     $("form").submit(function() { 
            submitted = true;
        }); 
        window.onbeforeunload = function() {
            if (!submitted)
            {
                return "Dude, are you sure you want to leave? Think of the kittens!";
             }
        }
  });
</script>
<?php include('include/header.php');?>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
               <div class="timer_div" style="float: right;">
                  Time Left :  <span id='countdown'></span>
               </div>
                 
                <form action="<?php echo base_url() .'passion_controller/aptitude_submission/'?>" method="post" id="my_formm" name="my_formm" onsubmit="return confirm('Are you sure you want to submit?');">
       <?php 
         $count=1;
         $ccount = count($general_question);?>
             <?php foreach($general_question as $general_question){ ?>
                <?php
                $id = $general_question['id'];

                $option_view = get_question_option($id,$count);
                foreach ($option_view as $option_view) {
                $op1=$option_view['op1'];
                $op2=$option_view['op2'];
                $op3=$option_view['op3'];
                $op4=$option_view['op4'];
                }
            ?>
      <div class="form-group f-r">
        <label class="q_no" style="float: left;padding-right: 1%;"><span><?php echo $count;?>.</span></label>   <?Php echo $general_question['ques_name'];?>
        <input type="hidden" name="question_id[]" id="question_id" value="<?php echo $general_question['id'];?>">
        <input type="hidden" name="question_type[]" id="question_type" value="<?php echo $general_question['question_type'];?>">
        <p class="radio-items" style="padding-right: 1%;">
        <div class="custom-radio">
          <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op1;?>">
          <label><?php echo $op1;?></label>
        </div>
        <div class="custom-radio">
          <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op2;?>">
          <label><?php echo $op2;?></label>
        </div>
        <div class="custom-radio">
          <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op3;?>">
          <label><?php echo $op3;?></label>
        </div>
        <div class="custom-radio">
          <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op4;?>">
          <label><?php echo $op4;?></label>
        </div>
        </p>
      </div>
      <?php
       $count++; 
      }
      ?>
      <div class="form-group sbmt_test" style="text-align: right;padding: 30px;">
        <button type="submit" name="submit_test"  class="btn btn-warning" id="submitBtn" style="width:150px;font-weight: bold;font-family:Times New Roman;font-size: 18px;" >Submit</button>
      </div>
     </form>
            </div>
        </section>
    </div>
    <!-- Content End -->

    <!-- Footer -->
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
        new WOW().init();

    </script>

    <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });
    </script>
    
</body>

</html>
