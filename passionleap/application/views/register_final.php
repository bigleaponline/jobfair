<!DOCTYPE html>
<?php include('include/header.php');?>
<div class="site-banner">
  <div class="container">
    <div class="banner-content wow fadeInUp" data-wow-delay="0.1s"> 
    </div>
  </div>
</div>
<!--Header Area End-->
<?php
foreach ($cand_details as $details) {
   $name = $details['name'];
   $email = $details['email'];
   $cand_id = $details['cand_id'];
   $contactnumber = $details['contactnumber'];
 } 
?>
<div class="register-page page-wrapper s-pd100 register-02">
  <h2 class="section-heading text-capitalize wow fadeInUp">Complete The Following Details</h2>
  <?php 
        if($this->session->flashdata('imageerr'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('imageerr').'</div>';   
        }
        if($this->session->flashdata('signatureerr'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('signatureerr').'</div>';   
        }
        if($this->session->flashdata('profileregistrationerror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('profileregistrationerror').'</div>';   
        }
    ?>
  <form action="<?php echo base_url();?>passion_controller/profile_registration" method="post" enctype="multipart/form-data" name="reg-form">
    <input class="form-control wow fadeInUp" type="hidden" name="cand_id" id="cand_id" value="<?php echo $cand_id;?>">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">First Name</label>
              <input class="form-control wow fadeInUp" type="text" name="fname" id="fname" value="<?php echo $name;?>" readonly>
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Last Name</label>
              <input class="form-control wow fadeInUp" type="text" name="lname" id="lname" placeholder="Enter Your Last Name">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Email Address</label>
              <input class="form-control wow fadeInUp" type="email" name="email" id="email" value="<?php echo $email;?>" readonly>
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area clearfix">
            <p>
              <label class="reglab wow fadeInUp reglabradio">Gender</label>
            <div class="radio wow fadeInUp ">
              <input name="gender" id="gender" value="Male" type="radio" checked>
              <label for="radio-1" class="radio-label">Male</label>
            </div>
            <div class="radio wow fadeInUp ">
              <input name="gender" id="gender" value="Female" type="radio">
              <label  for="radio-2" class="radio-label">Female</label>
            </div>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Contact Number</label>
              <input class="form-control wow fadeInUp" type="text" name="number1" id="number1" value="<?php echo $contactnumber;?>" onkeypress="return isNumberKey(event)"  maxlength="15" readonly>
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Alternate Number</label>
              <input class="form-control wow fadeInUp" type="text" name="number2" id="number2" placeholder="Enter Your Number" onkeypress="return isNumberKey(event)"  maxlength="15">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Enter Your Address</label>
              <textarea class="form-control wow fadeInUp txtarea" placeholder="Address" name="address" id="address"></textarea>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Father's Name</label>
              <input class="form-control wow fadeInUp" type="text" name="father_name" id="father_name" placeholder="Enter Your Father Name">
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Father's Occupation</label>
              <input class="form-control wow fadeInUp" type="text"  name="father_occupation" id="father_occupation" placeholder="Enter Your Father Occupation">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Mother's Name</label>
              <input class="form-control wow fadeInUp" type="text" name="mother_name" id="mother_name" placeholder="Enter Your Mother Name">
            </p>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Mother's Occupation</label>
              <input class="form-control wow fadeInUp" type="text" name="mother_occupation" id="mother_occupation" placeholder="Enter Your Mother Occupation">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-6 col-md-12 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Course Applied For</label>
              <select id="course_applied" name="course_applied" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Course --</option>
                <option value="Digital">Digital Mareketing</option>
                <option value="SEO">SEO</option>
                <option value="DESIGN">DESIGN</option>
              </select>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Post Graduation</label>
               <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_1" value="PG"  readonly="readonly" />
              <input class="form-control wow fadeInUp" type="text" name="course[]" id="course_1" placeholder="Enter Your Graduation">
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Stream</label>
              <select name="subject[]" id="subject_1" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Stream --</option>
                <option value="CSE">CSE</option>
                <option value="MECHANICAL">MECHANICAL</option>
                <option value="EEE">EEE</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">University/College</label>
              <input class="form-control wow fadeInUp" type="text" name="college[]" id="college_1" placeholder="Enter Your College">
            </p>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Year Of Passing</label>
              <select name="pass_year[]" id="pass_year_1" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Passing Year --</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-1 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Marks</label>
              <input class="form-control wow fadeInUp" type="text" name="mark[]" id="mark_1" placeholder="50%" onkeypress="return isNumberKey(event)"  maxlength="2">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Graduation</label>
              <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_2" value="UG"  readonly="readonly" />
              <input class="form-control wow fadeInUp" type="text" name="course[]" id="course_2" placeholder="Enter Your Graduation">
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Stream</label>
              <select name="subject[]" id="subject_2" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Stream --</option>
                <option value="CSE">CSE</option>
                <option value="MECHANICAL">MECHANICAL</option>
                <option value="EEE">EEE</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">University/College</label>
              <input class="form-control wow fadeInUp" type="text" name="college[]" id="college_2" placeholder="Enter Your College">
            </p>
          </div>
        </div>
        <div class="col-lg-2 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Year Of Passing</label>
              <select name="pass_year[]" id="pass_year_2" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Passing Year --</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-1 col-md-4 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Marks</label>
              <input class="form-control wow fadeInUp" type="text" name="mark[]" id="mark_2" placeholder="50%" onkeypress="return isNumberKey(event)"  maxlength="2">
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">PlusTwo/ITI/SSLC</label>
              <input class="form-control" type="hidden" name="qualfcn_catgry[]" id="qualfcn_catgry_3" value="HSE"  readonly="readonly" />
              <input class="form-control wow fadeInUp" type="text" name="course[]" id="course_3" placeholder="Enter Your Graduation">
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Board</label>
              <select name="subject[]" id="subject_3" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Board --</option>
                <option value="Kerala">Kerala</option>
                <option value="CBSE">CBSE</option>
                <option value="NCRT">NCRT</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Year Of Passing</label>
              <select name="pass_year[]" id="pass_year_3" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Passing Year --</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Marks</label>
              <input class="form-control wow fadeInUp" type="text" name="mark[]" id="mark_3" placeholder="50%" onkeypress="return isNumberKey(event)"  maxlength="2">
            </p>
          </div>
        </div>
      </div>
      <!-- <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">SSLC</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="Enter Your Graduation">
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Board</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Board --</option>
                <option value="Kerala">Kerala</option>
                <option value="CBSE">CBSE</option>
                <option value="NCRT">NCRT</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Year Of Passing</label>
              <select id="year" class="yearchoose stream wow fadeInUp">
                <option value="hide">-- Passing Year --</option>
                <option value="2010">2010</option>
                <option value="2011">2011</option>
                <option value="2012">2012</option>
              </select>
            </p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Marks</label>
              <input class="form-control wow fadeInUp" type="text" name="log" id="user_login" placeholder="50%">
            </p>
          </div>
        </div>
      </div> -->
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp"  for="userfile">Upload Profile Image</label>
            </p>
            <div class="login-form-area ">
              <div class="file-upload wow fadeInUp">
                <div class="file-select">
                  <div class="file-select-button" id="fileName">Choose File</div>
                  <!--<div class="file-select-name" id="noFile">No file chosen...</div>-->
                  <input type="file" id="profile_img" name="profile_img" size="20" required >
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp"  for="userfile">Upload Signature</label>
            <div class="login-form-area ">
              <div class="file-upload wow fadeInUp">
                <div class="file-select">
                  <div class="file-select-button" id="fileName2">Choose File</div>
                  <!--<div class="file-select-name" id="noFile2">No file chosen...</div>-->
                  <input type="file" id="sign" name="sign" size="20" required>
                </div>
              </div>
            </div>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
          <div class="login-form-area">
            <p>
              <label class="reglab wow fadeInUp">Declaration</label>
              <textarea class="form-control wow fadeInUp txtarea" placeholder="Drop Your Words" id="declaration" name="declaration"></textarea>
            </p>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="login-form-area wow fadeInUp">
          <p>
            <input class="styled-checkbox" id="styled-checkbox-1" type="checkbox" value="value1">
            <label for="styled-checkbox-1">I Agree</label>
          </p>
        </div>
      </div>
      <p>
        <button class="btn btn-default btn-primary wow fadeInUp" type="submit">SUBMIT</button>
      </p>
      <div class="login-form-register-now wow fadeInUp">
        <p>Already Registerd ? <a href="login.html">CLICK HERE</a></p>
      </div>
    </div>
  </form>
</div>

<!--Start Footer Area -->
<?php include('include/footer.php');?>
<!--End Footer Area --> 

<!-- All JS files are included here.-->
<script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-v3.2.1.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/wow.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url();?>assets/js/dropdown-1.js"></script> 
<script>
jQuery(document).ready(function( $ ) {
  // Initiate the wowjs animation library
  new WOW().init();
  });
</script> 
<script>
   $('#chooseFile').bind('change', function () {
  var filename = $("#chooseFile").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile").text("No file chosen..."); 
  }
  else {
    $(".file-upload").addClass('active');
    $("#noFile").text(filename.replace("C:\\fakepath\\", "")); 
  }
});
   $('#chooseFile2').bind('change', function () {
  var filename = $("#chooseFile2").val();
  if (/^\s*$/.test(filename)) {
    $(".file-upload").removeClass('active');
    $("#noFile2").text("No file chosen..."); 
  }
  else {
    $(".file-upload").addClass('active');
    $("#noFile2").text(filename.replace("C:\\fakepath\\", "")); 
  }
});
</script> 
<script type="text/javascript">
  function isNumberKey(evt)
  {
     var charCode = (evt.which) ? evt.which : event.keyCode
           if (charCode > 31 && (charCode < 48 || charCode > 57))
              return false;
  
           return true;
  }
</script>
</body>
</html>