<!doctype html>
<?php include('include/header.php');?>
<style>
    #myImg,#myImg1,#myImg2 {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover,#myImg1:hover,#myImg2:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Caption of Modal Image */
#caption {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
  text-align: center;
  color: #ccc;
  padding: 10px 0;
  height: 150px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 wow fadeInUp" data-wow-delay="0.1s">
                        <h2>Learn and Lead the World!</h2>
                        <p>PassionLeap offers an array of enriching courses specially designed by academic and industry experts together to offer students the best of academic and industry experience. From curated courses for those seeking to increase their knowledge in data science tools and techniques to value-added cybersecurity and ethical hacking courses, which open up professional possibilities as Ethical Hacker, Forensic Investigator, Information System Security Expert, Mobile Pentester and such, our certification courses in Data Science and IT fields are prospective enough to fetch demanding career opportunities. </p>
						<blockquote class="blockquote">
  
  <footer class="blockquote-footer">Teaching aspirants about surviving and leading capital markets and the fundamental financial area would be our certified course on Capital Market. Besides these, our well-crafted Integrated Career Enrichment Program prepare aspirants for prospective career possibilities, accelerate career growth and help them stand out among the crowd. 
</footer>
</blockquote>
                    </div>
                    <div class="col-md-5 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="item-holder">
                            <img src="<?php echo base_url();?>assets/img/about.png" alt="" />
                        </div>
                    </div>
                </div>
				<!--<embed src="../../assets/img/pdf.pdf" width="800px" height="2100px" />-->
				 
				<img src="<?php echo base_url();?>assets/img/download1.jpg" alt="" id="myImg"/>&nbsp;&nbsp;
	&nbsp;<img src="<?php echo base_url();?>assets/img/download2.jpg" alt=""  id="myImg1"/>&nbsp;&nbsp;&nbsp;
				<img src="<?php echo base_url();?>assets/img/download3.jpg" alt="" id="myImg2"/>
				
				<div id="myModal" class="modal">
  <span class="close">&times;</span>
  <img class="modal-content" id="img01">
  <div id="caption"></div>
</div>
            </div>
        </section>
    </div>

    <!-- Content End -->

    <!-- Footer -->
    
   
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script>
        new WOW().init();

    </script>
    <script>
var modal = document.getElementById("myModal");
var img = document.getElementById("myImg");
var img1 = document.getElementById("myImg1");
var img2 = document.getElementById("myImg2");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = "assets/img/brouchure1_compressed-1.jpg";
  captionText.innerHTML = this.alt;
}
img1.onclick = function(){
  modal.style.display = "block";
  modalImg.src = "assets/img/brouchure3_compressed-1.jpg";
  captionText.innerHTML = this.alt;
}
img2.onclick = function(){
  modal.style.display = "block";
  modalImg.src = "assets/img/brouchure2_compressed-1.jpg";
  captionText.innerHTML = this.alt;
}
var span = document.getElementsByClassName("close")[0];
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
       <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });




            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });

    </script>
</body>

</html>
