
<style type="text/css">
.progress-bar.new1{
  background-color: #428bca;
}
.progress-bar.new2{
  background-color: #d9534f;
}
.col-md-6.skill_title {
    padding: 2% 0 2% 1%;
}
.col-md-6.skill_chart {
    padding: 2% 0 2% 0;
}
 .assmnt-clm label { font-weight:normal; }
</style>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<?php 
// if($this->session->userdata('cand_data'))
//     $cand_id=$this->session->userdata['cand_data']['cand_id'];
$cand_id=1;
?>
<!doctype html>

<?php include('include/header.php');?>
    <!-- Header End  -->

    <!-- Banner -->
    <div class="site-banner">
    </div>
    <!-- Banner End -->

    <!-- Content -->
    <div class="site-content">
        <section class="site-section section-one">
            <div class="container">
              
    <?php
      foreach ($result as $result) {
        
        $score = $result['total_score'];
        $score_percent = $score * 5;
        $wrong = 20 - $score;
        $wrong_percent = $wrong * 5;
      }
    ?>

     <div class="assmnt-clm">
      
      <?php 

      $attributes  = array('id' => 'myform'); 
 ?>
      <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Correct Answers : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $score_percent;?>%;height: 15px;" class="progress-bar new1" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
         <span><?php echo $score;?>/20</span>
       </div>
       <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Wrong Answers : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $wrong_percent;?>%;height: 15px;" class="progress-bar new2" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
         <span><?php echo $wrong;?>/20</span>
       </div>
        <div class="row">
           <div class="col-xs-12 skill_chart">
              <div id="piechart_3d"></div>
              <input type="hidden" name="chrt_img" id="chrt_img">
           </div>
        </div>
    </div>


            </div>
        </section>
    </div>
    <!-- Content End -->

    <!-- Footer -->
    <?php include('include/footer.php');?>
    <!-- Footer End -->

    <script type="text/javascript" src="<?php echo base_url();?>assets/js/jquery-3.3.1.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script>
        new WOW().init();

    </script>

    <script>
        $(document).ready(function() {

            var owl = $('#mainSlide');
            owl.owlCarousel({
                loop: false,
                margin: 0,
                navSpeed: 500,
                nav: true,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 5000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false,
            });


            var owl1 = $('#txtSlide');

            owl1.owlCarousel({
                loop: false,
                margin: 0,
                nav: false,
                autoplay: true,
                rewind: true,
                items: 1,
                loop: true,
                autoplayTimeout: 16000,
                touchDrag: false,
                mouseDrag: false,
                dots: false,
                nav: false
            });

            // add animate.css class(es) to the elements to be animated
            function setAnimation(_elem, _InOut) {
                // Store all animationend event name in a string.
                // cf animate.css documentation
                var animationEndEvent = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';

                _elem.each(function() {
                    var $elem = $(this);
                    var $animationType = 'animated ' + $elem.data('animation-' + _InOut);

                    $elem.addClass($animationType).one(animationEndEvent, function() {
                        $elem.removeClass($animationType); // remove animate.css Class at the end of the animations
                    });
                });
            }

            // Fired after current slide has been changed
            var round = 0;
            owl.on('changed.owl.carousel', function(event) {

                var $currentItem = $('.owl-item', owl).eq(event.item.index);
                var $elemsToanim = $currentItem.find("[data-animation-in]");

                setAnimation($elemsToanim, 'in');
            })

            owl.on('translated.owl.carousel', function(event) {
                console.log(event.item.index, event.page.count);

                if (event.item.index == (event.page.count - 1)) {
                    if (round < 1) {
                        round++
                        console.log(round);
                    } else {
                        owl.trigger('stop.owl.autoplay');
                        var owlData = owl.data('owl.carousel');
                        owlData.settings.autoplay = true; //don't know if both are necessary
                        owlData.options.autoplay = true;
                        owl.trigger('refresh.owl.carousel');
                    }
                }
            });

        });
    </script>


<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
var data = google.visualization.arrayToDataTable([
['Assessment', 'Marks per Topic'],
['Correct Answers',     <?php echo $score;?>],
['Wrong Answers',      <?php echo $wrong;?>]
]);
var options = {
  title: 'My Mark Distribution',
  is3D: true,
};
var chart_area = document.getElementById('piechart_3d');
var chart = new google.visualization.PieChart(chart_area);
google.visualization.events.addListener(chart, 'ready', function(){
chart_area.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
document.getElementById("chrt_img").value =chart.getImageURI();
}); 
chart.draw(data, options);
}
</script>

</body>

</html>
