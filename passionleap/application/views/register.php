<!DOCTYPE html>


<?php include('include/header.php');?>
<style>
    input#stream {
    width: 49%;
    float: right;
    margin-right: 1%;
}
</style>
<div class="site-banner">
  <div class="container">
    <div class="banner-content wow fadeInUp" data-wow-delay="0.1s"> 
    </div>
  </div>
</div>
<!--Header Area End-->

<div class="register-page page-wrapper s-pd100">
    <?php 
        
         if($this->session->flashdata('candidatregistrationerror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('candidatregistrationerror').'</div>';   
        }
    ?>
  <div class="container">
       
    <div class="row">
      <div class="col-lg-7 col-md-12 col-sm-12">
        <div id="home-search-section wow fadeInUp" class="home-search-section-area bg-image home-header-one" style="background-image: url(assets/images/welcome-bg.jpg);">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-sm-12">
                <div class="welcome-text text-center tb">
                  <div class="tb-cell">
                    <h1 class="wow fadeInUp">Choose Your Extraordinary Future</h1>
                    <p class="wow fadeInUp">Explore your options, apply to Exams, Courses &amp; more</p>
                    <form class="product-search-form wow fadeInUp" action="#" method="get">
                      <button class="btn btn-default btn-primary" type="submit">Explore More </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-5 col-md-12 col-sm-12">
        <div class="login-form-area">
          <form action="<?php echo base_url();?>passion_controller/candidate_Registration" method="post" class="reg-form">
            <div class="text-center">
              <h2 class="section-heading text-capitalize wow fadeInUp">Register</h2>
            </div>
            <p>
              <input style="text-transform: capitalize;" class="form-control wow fadeInUp" type="text" name="name" id="user_login" placeholder="Enter Your Name">
            </p>
            <p>
              <input  class="form-control wow fadeInUp" type="email" name="email" id="email-id" placeholder="Email id">
            </p>
            <p class="clearfix">
              <input class="form-control wow fadeInUp datepick fl" type="date" name="dob" id="dateofbirth" placeholder="DD-MM-YY">
              <!--<input class="form-control wow fadeInUp fl numberinpt" type="number" name="num" id="user_number" placeholder="Phone Number">-->
              <input class="form-control wow fadeInUp fl numberinpt" id="mobile-number" type="tel" placeholder="+91 702 123 4567" name="mobile">
            </p>
            
            <p>
              <input class="form-control wow fadeInUp" type="text" name="college" id="user_login" placeholder="Enter Your College Name">
            </p>
            <p class="clearfix">
              <select id="year" class="yearchoose wow fadeInUp" name="year">
                <option value="hide">-- Year of Studying --</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
              <input class="form-control wow fadeInUp" type="stream" name="stream" id="stream" placeholder='Stream'>
            </p>
            <p class="clearfix">
                <?php
                if($cand_id == ''){
                    $id1 = 'PS001'; 
                }
                else{
                    $id2 = $cand_id + 1;
                }
                ?>
                <input class="form-control wow fadeInUp" type="hidden" name="username" id="username" Value="<?php if($cand_id == ''){echo $id1;}else{echo 'PS00' .$id2 ;}?>" readonly>
            </p>
            <!--<p class="clearfix">-->
            <!--    <input class="form-control wow fadeInUp" type="password" name="password" id="password" placeholder='Password'>-->
            <!--</p>-->
            <p>
              <button class="btn btn-default btn-primary wow fadeInUp" type="submit">SUBMIT</button>
            </p>
            <div class="login-form-register-now wow fadeInUp">
              <p>Already Registerd ? <a href="<?php echo base_url();?>login">CLICK HERE</a></p>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Start Footer Area -->
<?php include('include/footer.php');?>
<!--End Footer Area --> 

<!-- All JS files are included here.
<!-- jQuery Latest Version --> 
<script src="<?php echo base_url();?>assets/js/jquery-v3.2.1.min.js"></script> 
<!-- Bootstrap framework JS --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/intlTelInput.js"></script> 
<script src="<?php echo base_url();?>assets/js/dropdown-1.js"></script> 
<script>
jQuery(document).ready(function( $ ) {
  // Initiate the wowjs animation library
  new WOW().init();
  });
</script> 
<script>
   $("#mobile-number").intlTelInput();
</script> 
<!--<script src="js/main.js"></script>-->
</body>
</html>