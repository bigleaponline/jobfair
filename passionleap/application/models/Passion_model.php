<?php
ob_start();
class Passion_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
        $this->db->cache_on();
        $this->load->database();
       
    }
    function candidate_Registration()
    {
        $username = $this->input->post('username');
        $email    = $this->input->post('email');
        $password = $this->input->post('mobile');
        $password = base64_encode(trim(substr($password, strpos($password," "))));
        //$password = base64_encode($this->input->post('mobile'));

        $data=array(
                    'name'=>$this->input->post('name'),
                    'email'=>$email,
                    'dob'=>$this->input->post('dob'),
                    'stream'=>$this->input->post('stream'),
                    'contactnumber'=>$this->input->post('mobile'),
                    'status'=>0,
                    'college'=>$this->input->post('college'),
                    'roll_no'=>$username,
                    'year'=>$this->input->post('year'),
                    'password'=>$password
                    
                    );
        $insert=$this->db->insert('cand_registration',$data);
        //printr($this->db->last_query());exit();
        if($insert)
        {
            // $config = Array(
            //                 'protocol' => 'smtp',
            //                 'smtp_host' => 'ssl://smtp.googlemail.com',
            //                 'smtp_port' => 465,
            //                 'smtp_user' => 'developers@bigleaponline.com',
            //                 'smtp_pass' => 'bglp@123',
            //                 'mailtype'  => 'html', 
            //                 'charset'   => 'iso-8859-1',
            //                 'wordwrap' => TRUE
            //             );
            $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'ssl://smtp.gmail.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'admin@indiamegajobfairs.com',
                            'smtp_pass' => 'admin@bglp',
                            'mailtype'  => 'html', 
                            'charset'   => 'iso-8859-1',
                            'wordwrap' => TRUE
                        );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            $message="Thank you for registering with PassionLeap .Your credentials are given below\r\n UserID:".$username;
            $this->email->from('admin@indiamegajobfairs.com');
            $this->email->to($email); 
            $this->email->subject('PassionLeap');
            $this->email->message($message);  
            $this->email->send();
           /* if($this->email->send())
            {
               print_r("send successfully");exit();
            }
            else
            {
                show_error($this->email->print_debugger());print_r("vk1");exit();
            }*/
            return true;
        }
        else
        {
            return false;
        }
    }
    function login()
    {
        $roll_no=$this->input->post('roll_no');
        $password=base64_encode($this->input->post('password'));
        
       
        $query=$this->db->get_where('cand_registration',array('roll_no'=>$roll_no ,'password'=>$password ,'status'=>1 ));
        
        //  print_r($this->db->last_query());exit();
      
        if($query->num_rows()>0)
        {
            $db_roll_no=$query->row()->roll_no ;
            if($db_roll_no===$roll_no)
            {
               foreach($query->result() as $details)
               
                $cand_data=array(
                                'cand_id'=>$details->cand_id,
                                'name'=>$details->name,
                                'stream'=>$details->stream,                           
                                'status'=>$details->status,                           
                                'roll_no'=>$details->roll_no                            
                            );
                return $cand_data;
            }
            else
            {
                 return false;
            }
        }
        else
        {
            
            return false;
        }
    }
    function newsLetter()
    {
        $data=array('email'=>$this->input->post('email'),
                    'date'=>date('Y-m-d')
                    );
        $members=$this->db->insert('tbl_members',$data);
        if($members)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public function get_general_question()
    {
        $question_type="general";
        $array = array('question_type' => $question_type);
        $this->db->select("*");
        $this->db->from("question_master");
        $this->db->where($array);
        $this->db->order_by("id", "random");
        $this->db->limit(20);   
        $query = $this->db->get()->result_array();
        return $query;
    }
    public function save_aptitude_result()
    {
        $candidate_id   =1;
        $test_date      = date('Y-m-d');
        $ccount = 20;
        $total = 0;
        $data['candidate_id']  = $candidate_id;    
        $data['test_date']     = $test_date;
        $test_master=$this->db->insert('test_master',$data);
        if($test_master)
        {
            $test_id = $this->db->insert_id();
            $question_type_arr   = $this->input->post('question_type');
            $question_id_arr     = $this->input->post('question_id');
            for($i=1;$i<=$ccount;$i++)
            {
                $question_id      =$question_id_arr[$i-1];
                $question_type    =$question_type_arr[$i-1];
                $data1['test_id']  = $test_id;
                $data1['quest_id'] = $question_id;
                $answer           = $this->input->post('quest_optn_'.$i);
                $data1['answer']   = $answer;
                if($answer!=' ')
                {
                    $this->db->select('answer');
                    $query = $this->db->get_where('general_questions',array('question_id' => $question_id));
                    $currect_answer=$query->row()->answer;
                    if($currect_answer == $answer)
                    {            
                        $total = $total +1;
                    }  
                    $this->db->insert('test_answer', $data1);      
                }
            }
            $data['total_score']   = $total;
            $this->db->update('test_master', $data, array('test_id'=>$test_id));
        }
    }
    function get_report($cand_id)
    {
        $this->db->select("*");
        $this->db->from('test_master');
        //$this->db->join('tbl_topic_master B', 'A.topic_id=B.topic_id','left');
        $this->db->where(array('candidate_id' => $cand_id));
        $query = $this->db->get();
        return $query->result_array();
    }
    public function get_id()
    {
        
        $this->db->select_max("cand_id");
        $this->db->from('cand_registration');
        $query = $this->db->get()->result_array();
        $result = $query[0]['cand_id']; 
        //print_r($this->db->last_query());die();
        if($result == '')
        {
            $result = 'PS001';
        }
        else
        {
            return $result;
        }
    }
    
    public function get_cand_details($cand_id)
    {
        $this->db->select("*");
        $this->db->from('cand_registration');
        $this->db->where(array('cand_id' => $cand_id));
        $query = $this->db->get()->result_array();
        //print_r($this->db->last_query());die();
        return $query;
    }

    public function profile_registration($image,$signature)
    {
        $data=array(
                    'cand_id'=>$this->input->post('cand_id'),
                    'first_name'=>$this->input->post('fname'),
                    'last_name'=>$this->input->post('lname'),
                    'email'=>$this->input->post('email'),
                    'gender'=>$this->input->post('gender'),
                    'address'=>$this->input->post('address'),
                    'father_name'=>$this->input->post('father_name'),
                    'father_occupation'=>$this->input->post('father_occupation'),
                    'mother_name'=>$this->input->post('mother_name'),
                    'mother_occupation'=>$this->input->post('mother_occupation'),
                    'course_applied'=>$this->input->post('course_applied'),
                    'profile_image'=>$image,
                    'signature'=>$signature,
                    // 'profile_image'=>$this->input->post('profile_img'),
                    // 'signature'=>$this->input->post('sign'),
                    'declaration'=>$this->input->post('declaration'),
                    'contact_number'=>$this->input->post('number1'),
                    'alternate_number'=>$this->input->post('number2')
                    );
        $insert=$this->db->insert('cand_profile',$data);
        if($insert)
        {
            $cand_id = $this->db->insert_id();
            $row_no  = 3;
            $qualfcn_catgry  = $this->input->post('qualfcn_catgry');
            $course  = $this->input->post('course');
            $subject  = $this->input->post('subject');
            $university  = $this->input->post('college');
            $pass_year  = $this->input->post('pass_year');
            $mark  = $this->input->post('mark');
            for($i=0;$i<$row_no;$i++)
            {
                $data1['qualfcn_catgry'] = $qualfcn_catgry[$i];
                $data1['course'] = $course[$i];
                $data1['subject'] = $subject[$i];
                $data1['university'] = $university[$i];
                $data1['pass_year'] = $pass_year[$i];
                $data1['mark '] = $mark [$i];
                $data1['cand_id'] = $cand_id;
                $candedu=$this->db->insert('candidate_edu_details',$data1);
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    

}
?>