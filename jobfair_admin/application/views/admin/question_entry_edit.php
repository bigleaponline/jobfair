<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script src="//cdn.ckeditor.com/4.10.0/full-all/ckeditor.js"></script>

</head>

<body>
    
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    
    <div id="main-wrapper">
       
        <div class="page-wrapper">
           
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Edit Question</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit Question</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
          
            <div class="container-fluid">
               

            <?php 

            foreach($question_data as $question_data)
            {
                $type = $question_data['question_type'];
                $question = $question_data['ques_name'];
                $weightage = $question_data['weightage'];
            }

            ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <!-- <form class="form-horizontal"> -->
                                <span style="color:red;"><?php echo form_error('qualification');?></span>
                                <?php echo form_open('admin_controller/question_entry_edit/'.$quest_id); ?>
                                    <!-- <h4 class="card-title">Personal Info</h4> -->
                                    <div class="form-group row">
                                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Select Topic</label>
                                        <div class="col-sm-2">
                                            <!-- <input type="text" name="passion" class="form-control" placeholder="Enter Passion Name"  autocomplete="off" /> -->

                                        <select name="topic_id" id="topic_id" class="form-control"  required="">

                                            <option value=" ">Select Topic</option>
                                            <?php foreach($topics_list as $topics_list){ ?>
                                            <option value="<?php echo $topics_list['topic_id']; ?>"  <?php if($topics_list['topic_id']==$question_data['topic_id']){echo "selected='selected'";}?>><?php echo $topics_list['topic_name'] ;?></option>
                                            <?php } ?> 
                                             
                                        </select>

                                        </div>
                                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Weightage</label>
                                        <div class="col-sm-2">

                                        <input type="number" name="weightage" id="weightage"  class="form-group"style="width:60px;text-align: center;" min="1" max="5" value="<?php echo $weightage;?>">

                                        </div>
                                        <label for="fname" class="col-sm-2 text-right control-label col-form-label">Question Type</label>
                                        <div class="col-sm-2">

                                        <select name="question_type" id="question_type" class="form-control">
                                            <!--<option value="<?php echo $type?>"><?php echo $type?></option>-->
                                            <option value="general" <?php if($type=="general"){echo "selected";}?>>General</option>
                                            <option value="passage" <?php if($type=="passage"){echo "selected";}?>>Passage</option>
                                        </select>

                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="lname" class="col-sm-3 text-right control-label col-form-label">Question</label>
                                        <div class="col-sm-9">
                                            <textarea class="ckeditor" id="ques_name" name="ques_name" rows="15"><?php echo $question;?></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                    <?php if($type == "general"){

                                        foreach($general_question_data as $general_question_data)
                                        {

                                            $answer = $general_question_data['answer'];
                                            $option_1 = $general_question_data['option_1'];
                                            $option_2 = $general_question_data['option_2'];
                                            $option_3 = $general_question_data['option_3'];
                                            $option_4 = $general_question_data['option_4'];

                                        }
                                    ?>
                                    <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Answer </label>
                                    <input type="input" id="answer" name="answer"class="form-control"  autocomplete="off"  value="<?php echo $answer;?>"/>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Option 1 </label>
                                    <input type="input" id="option_1" name="option_1"class="form-control"  autocomplete="off" value="<?php echo $option_1;?>" />
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Option 2 </label>
                                    <input type="input" id="option_2" name="option_2"class="form-control"  autocomplete="off" value="<?php echo $option_2;?>" />
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Option 3 </label>
                                    <input type="input" id="option_3" name="option_3"class="form-control"  autocomplete="off" value="<?php echo $option_3;?>" />
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label>Option 4 </label>
                                    <input type="input" id="option_4" name="option_4"class="form-control"  autocomplete="off" value="<?php echo $option_4;?>" />
                                    </div>
                                </div>
                                <?php } else {?>

                                    <div class="form-group row" id="passage_row">
                                    <div class="titles t2" style="border-top:none;">
                                    </div>
                                    <div class="col-md-12 hidden-div" >
                                      <div class="col-md-11">
                                        <input type="hidden" name="row_no" value="1" id="row_no">
                                        <table class="table table-bordered table-responsive" id="myTable">
                                          <thead>
                                            <tr>
                                              <th scope="col">Sl.No</th>
                                              <th scope="col">Question</th>
                                              <th scope="col">Option1</th>
                                              <th scope="col">Option2</th>
                                              <th scope="col">Option3</th>
                                              <th scope="col">Option4</th>
                                              <th scope="col">Answer</th>
                                            </tr>
                                          </thead>
                                          <tbody class="t-body">
                                            <?php

                                            $row_no_edit = count($passage_question_data); 
                                            $counter = 1;
                                            foreach ($passage_question_data as $passage_data) 
                                            {
                                                $ques_id = $passage_data['question_id'];
                                                $ques_name = $passage_data['ques_name'];
                                                $sub_question = $passage_data['question'];
                                                $sub_answer = $passage_data['answer'];
                                                $sub_option_1 = $passage_data['option_1'];
                                                $sub_option_2 = $passage_data['option_2'];
                                                $sub_option_3 = $passage_data['option_3'];
                                                $sub_option_4 = $passage_data['option_4'];
                                                $id = $passage_data['passage_id'];
                                                // $type = $passage_question_data['question_type'];
                                            

                                            ?>
                                            <tr  class="row-periord" id="row1">
                                              <td><span class="btn btn-sm btn-default"><?php echo $counter;?></span><input type="hidden" value="6437" name="count[]"></td>
                                              <td><textarea class="form-control" id="sub_ques_name" name="sub_ques_name[]" value="<?php echo $sub_question;?>"><?php echo $sub_question;?></textarea><input type="hidden" name="q_id" id="q_id" value="<?php echo $id;?>"></td>
                                              <td><textarea class="form-control" id="sub_option1" name="sub_option1[]" value="<?php echo $sub_option_1;?>"><?php echo $sub_option_1;?></textarea></td>
                                              <td><textarea class="form-control" id="sub_option2" name="sub_option2[]" value="<?php echo $sub_option_2;?>"><?php echo $sub_option_2;?></textarea></td>
                                              <td><textarea class="form-control" id="sub_option3" name="sub_option3[]" value="<?php echo $sub_option_3;?>"><?php echo $sub_option_3;?></textarea></td>
                                              <td><textarea class="form-control" id="sub_option4" name="sub_option4[]" value="<?php echo $sub_option_4;?>"><?php echo $sub_option_4;?></textarea></td>
                                              <td><textarea class="form-control" id="sub_answer" name="sub_answer[]" value="<?php echo $sub_answer;?>"><?php echo $sub_answer;?></textarea></td>
                                              
                                            </tr>
                                            <?php $counter++;}?>
                                          </tbody>
                                        </table>
                                        <input type="hidden" name="row_no" id="row_no" value="<?php echo $row_no_edit;?>" />
                                      </div>
                                      <!-- <div class="col-md-1 add-del-btn pull-right">
                                        <div class="add-btn"><i class="fa fa-plus" aria-hidden="true"></i></div>
                                        <div class="del-btn" style="display:none;" id="vk"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
                                      </div> -->
                                      </div>
                                    </div>

                                <?php } ?>
                                </div>
                            </div>
                                <div class="border-top">
                                    <div class="card-body">
                                        <button type="submit" class="btn btn-primary">update</button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>

                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <!-- <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer> -->
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="<?php echo base_url(); ?>assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>dist/js/pages/mask/mask.init.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/quill/dist/quill.min.js"></script>
    <script>
        //***********************************//
        // For select 2
        //***********************************//
        $(".select2").select2();

        /*colorpicker*/
        $('.demo').each(function() {
        //
        // Dear reader, it's actually very easy to initialize MiniColors. For example:
        //
        //  $(selector).minicolors();
        //
        // The way I've done it below is just for the demo, so don't get confused
        // by it. Also, data- attributes aren't supported at this time...they're
        // only used for this demo.
        //
        $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                position: $(this).attr('data-position') || 'bottom left',

                change: function(value, opacity) {
                    if (!value) return;
                    if (opacity) value += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(value);
                    }
                },
                theme: 'bootstrap'
            });

        });
        /*datwpicker*/
        jQuery('.mydatepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        var quill = new Quill('#editor', {
            theme: 'snow'
        });

    </script>
<script type="text/javascript">
    $(function()
    {
        $(".confirmClick").click( function() { 
            if ($(this).attr('title')) {
                var question = '<?php echo $this->lang->line('areyousure');?> ' + $(this).attr('title').toLowerCase() + '?';
            } else {
                var question = '<?php echo $this->lang->line('areyousureaction');?>';
            }
            if ( confirm( question ) ) {
                [removed].href = this.src;
            } else {
                return false;
            }
        });
        
    });
</script>
<!-- <script type="text/javascript">
$(document).ready(function()
  {
    var tbody = $('#myTable').children('tbody');

   var table = tbody.length ? tbody : $('#myTable');

   $(".row-periord:last").find(".title");

   $('.add-btn').on('click', function()
   {

    var row_no = $('#row_no').val();
       
    var rows=$('#myTable tr').length;

    // alert(rows);

    var $sr = ($(".row-periord").length + 1);
    
    var rowid = Math.random();

    if(rows>1)
    {
        $("#vk").show();
    }

    var $html = '<tr class="row-periord" id="' + rowid + '">' +

        '<td><span class="btn btn-sm btn-default">' + $sr + '</span></td>' +

        '<td><textarea class="form-control" id="sub_ques_name'+$sr+'" name="sub_ques_name[]" placeholder="Question"></textarea></td>'+

        '<td><textarea class="form-control" id="sub_option1'+$sr+'" name="sub_option1[]" placeholder="Option1"></textarea></td>'+

        '<td><textarea class="form-control" id="sub_option2'+$sr+'" name="sub_option2[]" placeholder="Option2"></textarea></td>'+

        '<td><textarea class="form-control" id="sub_option3'+$sr+'" name="sub_option3[]" placeholder="Option3"></textarea></td>'+

        '<td><textarea class="form-control" id="sub_option4'+$sr+'" name="sub_option4[]" placeholder="Option4"></textarea></td>'+

        '<td><textarea class="form-control" id="sub_answer'+$sr+'" name="sub_answer[]" placeholder="Answer"></textarea></td>'+
       
        '</tr>';

    $("#myTable").append($html);
    $("#row_no").val($sr);

    return false;

   });

   });

</script>

<script type="text/javascript">
  $('.del-btn').click(function(){
       var rows=$('#myTable tr').length;
       // alert(rows);
        if(rows <= 3)
        {
            $("#vk").hide();
        }
  $( ".row-periord:last").remove();
});
</script> -->
</body>

</html>