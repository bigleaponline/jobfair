<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Candidate Details</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Candidate Details</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">

                <?php 

        foreach($highest_qualification as $highest_qualification)
        {
            $highest_qualification= $highest_qualification['qualification'];

        }

        foreach ($highest_qualification_id as $highest_qualification_id) {

            $highest_qualification_id= $highest_qualification_id['cand_qualification'];
        }

        ?>
        <?php 
        foreach ($cand_qualification_category as $qualification_category) {
           
           $qualfcn_catgry = $qualification_category['qualfcn_catgry'];
        }
        ?>
        <?php 
        foreach ($cand_qualification_percent as $qualification_percent) {
           
           $qualification_percent = $qualification_percent['cand_marks'];
        }
        ?> 

        <?php 
        foreach ($candidate_profile_detail as $candidate_profile_detail) {

        
        }
        ?>
        <?php 
        foreach ($pg_detail as $pg_detail) {
        
        }
        foreach ($ug_detail as $ug_detail) {
        
        }
        foreach ($diploma_detail as $diploma_detail) {

        }
        foreach ($plus_two_detail as $plus_two_detail) {
        
        }
        foreach ($sslc_detail as $sslc_detail) {

        }
        foreach ($candidate_passion as $candidate_passion) {

            $cand_passion = $candidate_passion['passion'];
            $cand_sub_passion = $candidate_passion['sub_passion_name'];

        }
        foreach ($candidate_resume_video as $candidate_resume_video) {
            
            $cand_video1 = $candidate_resume_video['candidate_video'];
            $cand_video2 = $candidate_resume_video['candidate_video1'];
            $candidate_resume = $candidate_resume_video['cand_resume'];
        }
        foreach($candidate_country as $country)
        foreach($candidate_state as $state)
        foreach($candidate_city as $city)
        foreach($candidate_internship as $internship)
        ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <!-- <form class="form-horizontal"> -->
                                <span style="color:red;"><?php echo form_error('qualification');?></span>
                                <div class="card-body">
                                     <div  class="row"> 

        <div class="col-md-12">
        <h4><b>Other Details</b></h4>
        </div>
        <br><br><br>

        <div class="col-md-2">
        <label>Name</label>
        </div>
        <div class="col-md-4">
        <input class="form-control" type="text" name="name" id="name"  value="<?php echo $candidate_profile_detail['name'];?>" readonly="readonly"/>
        <span class="err"><?php echo form_error('name'); ?></span>
        </div>
       

    
        <div class="col-md-2">
            <label> Highest Qualification</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $highest_qualification;?>" readonly="readonly"/>
            <input class="form-control" type="hidden" name="qualification_id" id="qualification_id"  value="<?php echo $highest_qualification_id;?>" readonly="readonly"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-2">
            <label>Phone Number</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_profile_detail['cand_phone'];?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>Whatsapp Number</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_profile_detail['cand_whatsapp'];?>" readonly="readonly"/>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-2">
            <label>Email</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_profile_detail['cand_email'];?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>DOB</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_profile_detail['cand_dob'];?>" readonly="readonly"/>
        </div>

    </div><br>
    <div class="row">
        <div class="col-md-2">
            <label>Course Completed</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php if($candidate_profile_detail['cand_passed']==1){ echo 'completed';}else if($candidate_profile_detail['cand_passed']==2){echo 'not completed';}else{echo 'ongoing';}?>" readonly="readonly"/>
        </div>

        <div class="col-md-2">
            <label>Passout Year</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_profile_detail['cand_passoutyear'];?>" readonly="readonly"/>
        </div>
    </div><br>
    <?php if($candidate_profile_detail['cand_description']!="" || $candidate_profile_detail['cand_backpapers']!=""){?>
    <div class="row">
        <?php if($candidate_profile_detail['cand_backpapers']!="")
        {?>
<div class="col-md-2">
            <label>No of back papers</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_profile_detail['cand_backpapers'];?>" readonly="readonly"/>
        </div>
        <?php }if($candidate_profile_detail['cand_description']!=""){?>
        <div class="col-md-2">
            <label>Description</label>
        </div>
        <div class="col-md-4">
            <textarea class="form-control"  name="qualification" id="qualification"  readonly="readonly"><?php echo $candidate_profile_detail['cand_description'];?></textarea>
        </div>
        <?php }?>
</div><br>
<?php } if($candidate_profile_detail['cand_education_type']!=""){?>
<div class="row">
        <div class="col-md-2">
            <label>Eduction Type</label>
        </div>
        <div class="col-md-4">
            <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $candidate_profile_detail['cand_education_type'];?>" readonly="readonly"/>
        </div>
    </div><br><?php } ?>
      <div  class="row"> 
      
      <div class="col-md-12">
            <h5><b>Educational Details</b></h5>
            </div>
            <br><br>

            <div class="col-md-12"> 
            <div class = col-md-12 >
            <div  class="table-responsive44"  >
            <table class="table table-striped table-bordered table-hover" id="tbl">
            <thead><tr>


            <th width="10%">Qalification</th>
            <th width="20%">Course</th>
            <th width="20%">Stream</th>
            <th width="20%">Board/University</th>
            <th width="10%">Percentage</th>
            <th width="20%">Pass Out Year</th>
            
            </tr>
            </thead>
            <tbody >

            <?php if(count($pg_detail) > 0){?>
            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_1" value="PG"  readonly="readonly" /></td>
            <td> 
            <input class="form-control" type="text" class="form-control" name="course_id[]" id="course_id_1"  value="<?php echo $pg_detail['qualification'];?>" readonly="readonly"/>
            </td>
            <td><input class="form-control" name="stream_id[]" id="stream_id_1" value="<?php echo $pg_detail['stream_name'];?>"></td>
            <td ><input class="form-control" type="input" name="board[]" id="board_1" value="<?php echo $pg_detail['board'];?>"/></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_1" value="<?php echo $pg_detail['percent'];?>"/></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_1" value="<?php echo $pg_detail['pass_out'];?>"/></td>
            </tr>
            <?php } ?>

            <?php if(count($ug_detail) > 0){?>
            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_2" value="UG" readonly="readonly" /></td>                
            <td> 
            <input class="form-control" type="text" class="form-control" name="course_id[]" id="course_id_1"  value="<?php echo $ug_detail['qualification'];?>" readonly="readonly"/>
            </td>
            <td><input class="form-control" name="stream_id[]" id="stream_id_1" value="<?php echo $ug_detail['stream_name'];?>"></td>
            <td ><input class="form-control" type="input" name="board[]" id="board_1" value="<?php echo $ug_detail['board'];?>"/></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_1" value="<?php echo $ug_detail['percent'];?>"/></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_1" value="<?php echo $ug_detail['pass_out'];?>"/></td>
            </tr>
            <?php } ?>

            <?php if(count($diploma_detail) > 0){?>
            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_3" value="Diploma" readonly="readonly"/></td>
            <td> 
            <input class="form-control" type="text" class="form-control" name="course_id[]" id="course_id_1"  value="<?php echo $diploma_detail['qualification'];?>" readonly="readonly"/>
            </td>
            <td><input class="form-control" name="stream_id[]" id="stream_id_1" value="<?php echo $diploma_detail['stream_name'];?>"></td>
            <td ><input class="form-control" type="input" name="board[]" id="board_1" value="<?php echo $diploma_detail['board'];?>"/></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_1" value="<?php echo $diploma_detail['percent'];?>"/></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_1" value="<?php echo $diploma_detail['pass_out'];?>"/></td>
            </tr>
            <?php }?>

            <?php if(count($plus_two_detail) > 0){?>
            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_4" value="Plus Two" readonly="readonly"/></td>
            <td> 
            <input class="form-control" type="text" class="form-control" name="course_id[]" id="course_id_1"  value="<?php echo $plus_two_detail['qualification'];?>" readonly="readonly"/>
            </td>
            <td><input class="form-control" name="stream_id[]" id="stream_id_1" value="<?php echo $plus_two_detail['stream_name'];?>"></td>
            <td ><input class="form-control" type="input" name="board[]" id="board_1" value="<?php echo $plus_two_detail['board'];?>"/></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_1" value="<?php echo $plus_two_detail['percent'];?>"/></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_1" value="<?php echo $plus_two_detail['pass_out'];?>"/></td>
            </tr>
            <?php }?>

            <?php if(count($sslc_detail) > 0){?>
            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_5" value="SSLC" readonly="readonly"/></td>
            <td> 
            <input class="form-control" type="text" class="form-control" name="course_id[]" id="course_id_1"  value="<?php echo $sslc_detail['qualification'];?>" readonly="readonly"/>
            </td>
            <td><input class="form-control" name="stream_id[]" id="stream_id_1" value="<?php echo $sslc_detail['stream_name'];?>"></td>
            <td ><input class="form-control" type="input" name="board[]" id="board_1" value="<?php echo $sslc_detail['board'];?>"/></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_1" value="<?php echo $sslc_detail['percent'];?>"/></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_1" value="<?php echo $sslc_detail['pass_out'];?>"/></td>
            </tr>
            <?php }?>

            </tbody>
            </table>
            </div><!-- <div class="col-md-12"> -->
            </div><!-- <div> -->
            </div>
            <br><br><br>

        </div>
 

        <?php if($candidate_profile_detail['have_experience'] == 'Y'){ ?> 

        <div  class="row"  id="experience_row"> 
            
            <div class="col-md-12">
            <h5><b>Work Experience</b></h5> 
            <div class = col-md-12 >
            <div  class="table-responsive44">
            <table class="table table-striped table-bordered table-hover" id="tb2">
            <thead><tr>


            <th width="5%">Sl.No.</th>
            <th width="25%">Company</th>
            <th width="20%">Position</th>
            <th width="20%">Duration(In Years)</th>
            </tr>
            </thead>
            <tbody id="table-details1">
            <?php 
            $row_no_edit = count($candidate_exp_details);
            $counter = 1;
            foreach($candidate_exp_details as $candidate_exp_details){

                $company_name = $candidate_exp_details['company_name'];
                $job_position = $candidate_exp_details['job_position'];
                $duration = $candidate_exp_details['duration'];

            ?>
            <tr id="row2" class="jdr2">
            <td><span class="btn btn-sm btn-default"><?php echo $counter;?></span><input type="hidden" value="6437" name="count[]"></td>
            <td ><input class="form-control" type="input" name="company_name[]" id="company_name_1" value="<?php echo $company_name;?>"/></td>
            <td ><input class="form-control" type="input" name="job_position[]" id="job_position_1" value="<?php echo $job_position;?>"/></td>
            <td ><input class="form-control" type="input" name="duration[]" id="duration_1" value="<?php echo $duration;?>"/></td>
            </tr>
            <?php $counter++; } ?>
            <tr class="control-group after-add-more">
            </tr>
            </tbody>
            </table> 
            </div><!-- <div class="col-md-12"> -->
            </div><!-- <div> -->
            <input type="hidden" name="row_no1" id="row_no1" value="<?php echo $row_no_edit;?>" />
            </div>
            <br><br><br>

        </div>
        <?php } ?>

        
       
        <div  class="row"> 
             <?php if($candidate_profile_detail['cand_country'] != ''){?>
            <div class="col-md-1">
                <label>Candidate Country</label>
            </div>
            <div class="col-md-3">
                <input class="form-control" type="text" name="cand_location" id="cand_location" value="<?php echo $country['name'];?>" readonly="readonly" />
            </div>
            
             <?php } if($candidate_profile_detail['cand_state'] != ''){?>
            <div class="col-md-1">
                <label>Candidate State</label>
            </div>
            <div class="col-md-3">
                <input class="form-control" type="text" name="cand_location" id="cand_location" value="<?php echo $state['name'];?>" readonly="readonly" />
            </div>
             <?php }if($candidate_profile_detail['cand_city'] != ''){?>
            <div class="col-md-1">
                <label>Candidate City</label>
            </div>
            <div class="col-md-3">
                <input class="form-control" type="text" name="cand_location" id="cand_location" value="<?php echo $city['name'];?>" readonly="readonly" />
            </div>
 <?php } ?>
       </div><br>
       <?php if(count($candidate_passion)>0){?>
       <div  class="row">      
            <div class="col-md-3">
                <label>Area of Passion</label>
            </div>
            <div class="col-md-3">
                <input class="form-control" type="text" name="interest" id="interest" value="<?php echo $cand_passion;?>" readonly="readonly"/>
            </div>
            <div class="col-md-3">
                <label>Area of Interest</label>
            </div>
            <div class="col-md-3">
                <input class="form-control" type="text" name="interest_id" id="interest_id" value="<?php echo $cand_sub_passion;?>" readonly="readonly"/>
            </div>
            </div><br>


       <?php } if($candidate_profile_detail['done_internship'] == 'Y'){?>

        <!--<div  class="row" id="internship_row"> -->

        <!--    <div class="col-md-3">-->
        <!--        <label>Internship Details</label>-->
        <!--    </div>-->
        <!--    <div class="col-md-4">-->
        <!--        <input class="form-control" type="text" name="internship_detail" id="internship_detail" value="<?php echo $candidate_profile_detail['internship_detail'];?>" readonly="readonly"/>-->
        <!--    </div>-->
        <!--</div> <br>-->
        <?php } ?>


        <?php if($candidate_profile_detail['interest_internship'] == 'Y' && $candidate_profile_detail['pref_intern_id'] != ''){?>

        <div  class="row" id="internship_like_row"> 

            <div class="col-md-3">
                <label>Preffered Internship</label>
            </div>
            <div class="col-md-4">
                <input class="form-control" type="text" name="internship_name" id="internship_name" value="<?php echo $internship['internship_name'];?>" readonly="readonly"/>
            </div>
        </div> <br>
        <?php } ?> 

       <?php if($candidate_profile_detail['pref_overseas_country'] !== ''){  ?>

       <div  class="row"> 
            
            <div class="col-md-3">
                <label>Prefered country for overseas career</label>
            </div>
            <div class="col-md-4">
                <input class="form-control" type="text" name="pref_overseas_country" value="<?php echo $candidate_profile_detail['pref_overseas_country'];?>" id="pref_overseas_country" readonly="readonly"/>
                </div>

        </div><br> 
        <?php } ?> 
       

        <div  class="row"> 
            
            <div class="col-md-6">
                <label>Do you want to become a future entrepreneur of make in India program?</label>
            </div>
            <div class="col-md-2">
                <input class="form-control" type="text" name="makein_india_interest" value="<?php if($candidate_profile_detail['makein_india_interest'] == 'Y'){echo 'Yes';}else{echo 'No';}?>" id="makein_india_interest" readonly="readonly" />
            </div>

        </div><br>
       <?php echo form_open_multipart('admin_controller/download/'.$candidate_resume); ?>

        <div class="row">
        <div class="col-md-3">
           <label for="userfile">Resume</label>
        </div>
        <div class="col-md-4">
        <!-- <embed src="<?php echo base_url().'upload/resume/'.$candidate_resume; ?>">        -->
       <!-- <a href="<?=base_url ()?>download/<?php echo $candidate_resume;?>"> Download </a> -->
       <button type="submit">Download</button>
        </div>
        </div>
        </form>
       
        <br><br><br>

        
        <div class="row">
            <?php if($cand_video1!=""){?>
        <div class="col-md-3">
           <label for="userfile">Tell Me Your Self Video</label>
        </div>
        <div class="col-md-4">
        <video width="320" height="240" controls autoplay>   
            <source src="<?= base_url('../upload/video/' . $cand_video1); ?>" type="video/mp4">
      
        </video>
        </div>
        <?php } if($cand_video2!=""){?>
        <div class="col-md-4">
        <video width="320" height="240" controls autoplay>   
            <source src="<?= base_url('../upload/video/' . $cand_video2); ?>" type="video/mp4">
           
        </video>
        </div>
        <?php }?>
        </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="<?php echo base_url(); ?>assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>dist/js/pages/mask/mask.init.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/quill/dist/quill.min.js"></script>
    <script>
        //***********************************//
        // For select 2
        //***********************************//
        $(".select2").select2();

        /*colorpicker*/
        $('.demo').each(function() {
        //
        // Dear reader, it's actually very easy to initialize MiniColors. For example:
        //
        //  $(selector).minicolors();
        //
        // The way I've done it below is just for the demo, so don't get confused
        // by it. Also, data- attributes aren't supported at this time...they're
        // only used for this demo.
        //
        $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                position: $(this).attr('data-position') || 'bottom left',

                change: function(value, opacity) {
                    if (!value) return;
                    if (opacity) value += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(value);
                    }
                },
                theme: 'bootstrap'
            });

        });
        /*datwpicker*/
        jQuery('.mydatepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        var quill = new Quill('#editor', {
            theme: 'snow'
        });

    </script>
</body>

</html>