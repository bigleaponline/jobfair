<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>

table th , table td{
    text-align: center;
}

table tr:nth-child(even){
    background-color: #BEF2F5
}

.pagination li:hover{
    cursor: pointer;
}
ul.pagination span {
    outline-color: black;
    padding: 4px;
    border: 1px solid #000;
}
.pagination li.active {
    background-color: aqua;
}
ul.pagination span.active {
    padding: 4px;
    background-color: red;
}
input {
    padding: 10px 8px;
    width: 300px;
}
</style>
</head>

<body>
  
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
   
    <div id="main-wrapper">
      
        <div class="page-wrapper">
          
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Qualification List</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Qualification List</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
         
            <div class="container-fluid">
                
                <div class="row">
                    <div class="col-12">


                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id= "table-id">
                                        <thead class="my_head">
                                            <tr>
                                                <th>Sl.No</th>
                                                <th>Qualification</th>
                                                <th>Qualification Category</th>
                                                <th>Weightage</th>
                                                <th>Options</th>
                                            </tr>
                                            </thead>
                                            <tbody class="my-list">
                                        <?php
                                        $count=1;
                                        foreach($qualification_list as $u){
                                        ?>
                                            <tr>
                                                <td><?php echo $count; ?></td>
                                                <td><?php echo $u['qualification']; ?></td>
                                                <td><?php echo $u['qualfcn_catgry']; ?></td>
                                                 <td><?php echo $u['weightage']; ?></td>
                                                <td align="center"><a class="btn btn-info btn-xs" title="View" href="<?php echo site_url("admin_controller/qualification_entry_edit/" . $u['qualification_id']); ?>"><i class="fa fa-edit"></i></a>
                                                
                                                <a class="btn btn-danger btn-xs confirmDelete" title="Delete" onClick="return confirm('Are you sure you want delete')" href="<?php echo site_url("admin_controller/delete_qualification/" . $u['qualification_id']); ?>"><i class="fa fa-times-circle"></i></a>
                                                </td>
                                            </tr>
                                        <?php  $count++;} ?>
                                    </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- footer -->
            <!-- ============================================================== -->
            <!-- <footer class="footer text-center">
                All Rights Reserved by Matrix-admin. Designed and Developed by <a href="https://wrappixel.com">WrapPixel</a>.
            </footer> -->
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="<?php echo base_url(); ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>dist/js/custom.min.js"></script>
    <!-- this page js -->
    <script src="<?php echo base_url(); ?>assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script>
$(document).ready(function () {
$('#table-id').DataTable();
$('.dataTables_length').addClass('bs-select');
});
</script>
   

<script>
    $("#my-search-input").keyup(function () {
    var search = $(this).val();
    $(".my-list").children().show();
    $('.noresults').remove();
    if (search) {
        $(".my-list").children().not(":containsNoCase(" + search + ")").hide();
        $(".my-list").each(function () {
            if ($(this).children(':visible').length == 0) $(this).append('<tr class="noresults"><td colspan="9"><em>No Results</em></td></tr>');
        });

    }
});

$.expr[":"].containsNoCase = function (el, i, m) {
    var search = m[3];
    if (!search) return false;
    return new RegExp(search, "i").test($(el).text());
};
</script>

</body>

</html>