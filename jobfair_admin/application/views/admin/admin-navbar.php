
<style type="text/css">
nav.sidebar-nav, footer {
    background-color: #000;
}
.topbar {
    position: relative;
    z-index: 50;
    -webkit-box-shadow: 1px 0px 7px rgba(0, 0, 0, 0.05);
    box-shadow: 1px 0px 7px rgba(0, 0, 0, 0.05);
    background: #000;
    height: 64px;
}
.sidebar-nav ul .sidebar-item.selected>.sidebar-link {
    background: #27a9e3;
    opacity: 1;
}
.btn-primary {
    color: #fff;
    background-color: #27a9e3;
    border-color: #27a9e3;
}
</style>
<header class="topbar" data-navbarbg="skin5">
    <nav class="navbar top-navbar navbar-expand-md navbar-dark">
        <div class="navbar-header" data-logobg="skin5">
            <!-- This is for the sidebar toggle which is visible on mobile only -->
            <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
            <!-- ============================================================== -->
            <!-- Logo -->
            <!-- ============================================================== -->
            <a class="navbar-brand" href="index.html">
                <!-- Logo icon -->
                <b class="logo-icon p-l-10">
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <!-- <img src="<?php echo base_url(); ?>assets/images1/logo-icon.png" alt="homepage" class="light-logo" /> -->
                   
                </b>
                <!--End Logo icon -->
                 <!-- Logo text -->
                <span class="logo-text">
                     <!-- dark Logo text -->
                     <!-- <img src="<?php echo base_url(); ?>assets/images1/logo-text.png" alt="homepage" class="light-logo" /> -->
                    
                </span>
                <!-- Logo icon -->
                <!-- <b class="logo-icon"> -->
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <!-- <img src="assets/images/logo-text.png" alt="homepage" class="light-logo" /> -->
                    
                <!-- </b> -->
                <!--End Logo icon -->
            </a>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Toggle which is visible on mobile only -->
            <!-- ============================================================== -->
            <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav float-left mr-auto">
                <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                <!-- ============================================================== -->
                <!-- create new -->
                <!-- ============================================================== -->
                
                <!-- ============================================================== -->
                <!-- Search -->
                <!-- ============================================================== -->
                
            </ul>
            <!-- ============================================================== -->
            <!-- Right side toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav float-right">
                <!-- ============================================================== -->
                <!-- Comment -->
                <!-- ============================================================== -->
                
                <!-- ============================================================== -->
                <!-- End Comment -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Messages -->
                <!-- ============================================================== -->
                <!--  -->
                <!-- ============================================================== -->
                <!-- End Messages -->
                <!-- ============================================================== -->

                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
                <li class="nav-item dropdown">
                    
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $username;?> <img src="<?php echo base_url(); ?>assets/images1/users/1.jpg" alt="user" class="rounded-circle" width="31"></a>
                    <div class="dropdown-menu dropdown-menu-right user-dd animated">
                        <!-- <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
                        <div class="dropdown-divider"></div> -->
                        <a class="dropdown-item" href="<?php echo base_url('logout'); ?>"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                        <!-- <div class="dropdown-divider"></div>
                        <div class="p-l-30 p-10"><a href="javascript:void(0)" class="btn btn-sm btn-success btn-rounded">View Profile</a></div> -->
                    </div>
                </li>
                <!-- ============================================================== -->
                <!-- User profile and search -->
                <!-- ============================================================== -->
            </ul>
        </div>
    </nav>
</header>
 <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar" data-sidebarbg="skin5">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav" class="p-t-30">
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('dashboard');?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu">Dashboard</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="index.html" aria-expanded="false"><i class="mdi mdi-account-multiple-plus"></i><span class="hide-menu">Candidate List</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="<?php echo base_url('candidate_list');?>" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Candidate List</span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('employer_list');?>" aria-expanded="false"><i class="mdi mdi-checkerboard"></i><span class="hide-menu">Employers List</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo base_url('sponsor_list');?>" aria-expanded="false"><i class="mdi mdi-pencil"></i><span class="hide-menu">Sponsor List</span></a></li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="widgets.html" aria-expanded="false"><i class="mdi mdi-chart-bubble"></i><span class="hide-menu">Topics</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="<?php echo base_url('topic_entry');?>" class="sidebar-link"><i class="mdi mdi-emoticon"></i><span class="hide-menu"> Add Topic </span></a></li>
                                <li class="sidebar-item"><a href="<?php echo base_url('topic_list');?>" class="sidebar-link"><i class="mdi mdi-emoticon-cool"></i><span class="hide-menu"> Topic List </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="tables.html" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu">Questions</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="<?php echo base_url('question_entry');?>" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Add Questions</span></a></li>
                                <li class="sidebar-item"><a href="<?php echo base_url('question_list');?>" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Questions List</span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="grid.html" aria-expanded="false"><i class="mdi mdi-blur-linear"></i><span class="hide-menu">Qualification</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="<?php echo base_url('qualification_entry');?>" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Add Qualification</span></a></li>
                                <li class="sidebar-item"><a href="<?php echo base_url('qualification_list');?>" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Qualification List</span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-receipt"></i><span class="hide-menu">Streams</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="<?php echo base_url('stream_entry');?>" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu"> Add Stream </span></a></li>
                                <li class="sidebar-item"><a href="<?php echo base_url('stream_list');?>" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu"> Stream List </span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="grid.html" aria-expanded="false"><i class="mdi mdi-blur-linear"></i><span class="hide-menu">Passion</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="<?php echo base_url('passion_entry');?>" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Add Passion </span></a></li>
                                <li class="sidebar-item"><a href="<?php echo base_url('passion_list');?>" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Passion List</span></a></li>
                            </ul>
                        </li>
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="grid.html" aria-expanded="false"><i class="mdi mdi-blur-linear"></i><span class="hide-menu">Sub Passion</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="<?php echo base_url('sub_passion_entry');?>" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Add Sub Passion </span></a></li>
                                <li class="sidebar-item"><a href="<?php echo base_url('sub_passion_list');?>" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Sub Passion List</span></a></li>
                            </ul>
                        </li>
                        
                        <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-move-resize-variant"></i><span class="hide-menu">Internship</span></a>
                            <ul aria-expanded="false" class="collapse  first-level">
                                <li class="sidebar-item"><a href="<?php echo base_url('internship_entry');?>" class="sidebar-link"><i class="mdi mdi-note-outline"></i><span class="hide-menu">Add Internship </span></a></li>
                                <li class="sidebar-item"><a href="<?php echo base_url('internship_list');?>" class="sidebar-link"><i class="mdi mdi-note-plus"></i><span class="hide-menu">Internship List</span></a></li>
                            </ul>
                        </li>
<!--                         
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->