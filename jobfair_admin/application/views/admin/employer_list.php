<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>

table th , table td{
    text-align: center;
}

table tr:nth-child(even){
    background-color: #BEF2F5
}

.pagination li:hover{
    cursor: pointer;
}
ul.pagination span {
    outline-color: black;
    padding: 4px;
    border: 1px solid #000;
}
.pagination li.active {
    background-color: aqua;
}
ul.pagination span.active {
    padding: 4px;
    background-color: red;
}
input {
    padding: 10px 8px;
    width: 300px;
}
</style>

</head>

<body>
    
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    
    <div id="main-wrapper">
      
        <div class="page-wrapper">
           
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Employers List</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Employers List</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="container-fluid">
             
                <div class="row">
                    <div class="col-12">


                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table  class="table table-striped table-bordered" id= "table-id">
                                        <thead class="my_head">
                                            <tr>
                                                <th width="2%">Sl.No</th>
                                                <th width="2%">Name</th>
                                                <th width="5%">Contact Number</th>
                                                <th width="5%">Email</th>
                                                <th width="5%">Type of Industry</th>
                                                <th width="5%">Address</th>
                                                <th width="5%">Contact Person</th>
                                                <th width="5%">Designation</th>
                                                <th width="5%">Jobfair Venue</th>
                                                <th width="5%">Date</th>
                                                <th width="5%">Options</th>
                                            </tr>
                                            </thead>
                                            <tbody class="my-list">
                                        <?php
                                        $count=1;
                                        foreach($employer_list as $u){
                                            if($u['emp_venue']==1)
                                            {
                                                $venue='calicut';
                                            }
                                            if($u['emp_venue']==2)
                                            {
                                                $venue='kochi';
                                            }
                                            if($u['emp_venue']==3)
                                            {
                                                $venue='calicut and kochi';
                                            }
                                            if($u['emp_venue']==4)
                                            {
                                                $venue='north banglore';
                                            }
                                            if($u['emp_venue']==5)
                                            {
                                                $venue='hyderbad';
                                            }
                                        ?>
                                            <tr>
                                                <td><?php echo $count; ?></td>
                                                <td><?php echo $u['company_name']; ?></td>
                                                <td><?php echo $u['contact_no']; ?></td>
                                                <td><?php echo $u['email_id']; ?></td>
                                                <td><?php echo $u['industry']; ?></td>
                                                <td><?php echo $u['address']; ?></td>
                                                <td><?php echo $u['contact_person']; ?></td>
                                                <td><?php echo $u['designation']; ?></td>
                                                <td><?php echo $venue; ?></td>
                                                <td><?php echo $u['emp_date']; ?></td>
                                                <td align="center"><a class="btn btn-info btn-xs" title="View" href="<?php echo site_url("admin_controller/employer_view/" . $u['emp_id']); ?>"><i class="fa fa-eye"></i></a>
                                                
                                                <a class="btn btn-danger btn-xs confirmDelete" title="Delete" onClick="return confirm('Are you sure you want to delete')" href="<?php echo site_url("admin_controller/delete_employer/" . $u['emp_id']); ?>"><i class="fa fa-times-circle"></i></a>
                                                </td>
                                            </tr>
                                        <?php $count++;} ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
               
            </div>
           
        </div>
       
    </div>
    
    <script src="<?php echo base_url(); ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>dist/js/custom.min.js"></script>
    <!-- this page js -->
    <script src="<?php echo base_url(); ?>assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/multicheck/jquery.multicheck.js"></script>
    <script>
$(document).ready(function () {
$('#table-id').DataTable();
$('.dataTables_length').addClass('bs-select');
});
</script>
   

<script>
    $("#my-search-input").keyup(function () {
    var search = $(this).val();
    $(".my-list").children().show();
    $('.noresults').remove();
    if (search) {
        $(".my-list").children().not(":containsNoCase(" + search + ")").hide();
        $(".my-list").each(function () {
            if ($(this).children(':visible').length == 0) $(this).append('<tr class="noresults"><td colspan="9"><em>No Results</em></td></tr>');
        });

    }
});

$.expr[":"].containsNoCase = function (el, i, m) {
    var search = m[3];
    if (!search) return false;
    return new RegExp(search, "i").test($(el).text());
};
</script>
</body>

</html>