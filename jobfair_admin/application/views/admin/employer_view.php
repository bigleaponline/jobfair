<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>

</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
   
    <div id="main-wrapper">
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Employer Details</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Employer Details</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">

                <?php 

                foreach($employer_details as $employer_details)
                {
                    $name = $employer_details['company_name'];
                    $number = $employer_details['contact_no'];
                    $email_id = $employer_details['email_id'];
                    $industry = $employer_details['industry'];
                    $address = $employer_details['address'];
                    $contact_person = $employer_details['contact_person'];
                    $designation = $employer_details['designation'];
                }

                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <!-- <form class="form-horizontal"> -->
                                <span style="color:red;"><?php echo form_error('qualification');?></span>
                                <div class="card-body">
                                    <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Company Name: </label>
                                        <input type="text" class="form-control" name="company_name" id="company_name" value="<?php echo $name;?>" readonly>
                                      </div>
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Contact No: </label>
                                        <input type="text" class="form-control" name="contact_no" id="contact_no" value="<?php echo $number;?>" readonly>
                                      </div>
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Email ID: </label>
                                        <input type="email" class="form-control" name="email_id" id="email_id" value="<?php echo $email_id;?>" readonly>
                                      </div>
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Type of Industry: </label>
                                        <input type="text" class="form-control" name="industry" id="industry" value="<?php echo $industry;?>" readonly>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Contact Person: </label>
                                        <input type="text" class="form-control" name="contact_person" id="contact_person" value="<?php echo $contact_person;?>" readonly>
                                      </div>
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Designation: </label>
                                        <input type="text" class="form-control" name="designation" id="designation" value="<?php echo $designation;?>" readonly>
                                      </div>
                                      <div class="form-group">
                                        <label for="exampleInputEmail1">Address: </label>
                                        <textarea class="form-control" name="address" id="address" value="<?php echo $address;?>" readonly></textarea>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="titles t2" style="border-top:none;">
                                      <h4>Job Details</h4>
                                    </div>
                                    <div class="col-md-12 hidden-div" >
                                      <div class="col-md-11">
                                        <input type="hidden" name="row_no" value="1" id="row_no">
                                        <table class="table table-bordered table-responsive" id="myTable">
                                          <thead>
                                            <tr>
                                              <th scope="col">Sl.No</th>
                                              <th scope="col">Job Title</th>
                                              <th scope="col">Experience</th>
                                              <th scope="col">Salary</th>
                                              <th scope="col">Job Description</th>
                                            </tr>
                                          </thead>
                                          
                                          <tbody class="t-body">
                                            <?php

                                            $row_no_edit = count($employer_details); 
                                            $counter = 1;
                                            foreach ($employer_job_details as $employer_job_details) 
                                            {
                                                $job_title = $employer_job_details['job_title'];
                                                $experience = $employer_job_details['eperience'];
                                                $salary = $employer_job_details['salary'];
                                                $job_description = $employer_job_details['job_description'];

                                            ?>
                                            <tr  class="row-periord" id="row1">
                                              <td><span class="btn btn-sm btn-default"><?php echo $counter;?></span><input type="hidden" value="6437" name="count[]"></td>
                                              <td><input type="text" class="form-control title" id="job_title" name="job_title[]" value="<?php echo $job_title;?>"></td>
                                              <td><input type="text" class="form-control experience" id="experience" name="experience[]" value="<?php echo $experience;?>"></td>
                                              <td><input type="text" class="form-control salary" id="salary" name="salary[]" value="<?php echo $salary;?>"></td>
                                              <td class="jb-txtarea"><textarea class="form-control description" id="job_description" name="job_description[]" value="<?php echo $job_description;?>"><?php echo $job_description;?></textarea></td>
                                            </tr>
                                            <?php $counter++;}?>
                                          </tbody>
                                         
                                        </table>
                                        <input type="hidden" name="row_no" id="row_no" value="<?php echo $row_no_edit;?>" />
                                      </div>
                                     
                                      </div>
                                  </div><br>
                                  <?php 
                                  foreach($employer_jd as $empjd) 
                                  {
                                 if($empjd['emp_jd']!="")
                                    {
                                   
                                  $jd=$empjd['emp_jd'];
                                  echo form_open_multipart('admin_controller/downloadJD/'.$jd); ?>

        <div class="row">
        <div class="col-md-3">
           <label for="userfile">JD</label>
        </div>
        <div class="col-md-4">
        <!-- <embed src="<?php echo base_url().'upload/jd/'.$jd; ?>">        -->
       <!-- <a href="<?=base_url ()?>download/<?php echo $candidate_resume;?>"> Download </a> -->
       <button type="submit">Download</button>
        </div>
        </div>
        </form>
       <?php }}?>
                                </div>
                        </div>
                    </div>
                </div>

               
            </div>
            
        </div>
       
    </div>
   
    <script src="<?php echo base_url(); ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>dist/js/custom.min.js"></script>
    <!-- This Page JS -->
    <script src="<?php echo base_url(); ?>assets/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>dist/js/pages/mask/mask.init.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/select2/dist/js/select2.full.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/select2/dist/js/select2.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/quill/dist/quill.min.js"></script>
    <script>
        //***********************************//
        // For select 2
        //***********************************//
        $(".select2").select2();

        /*colorpicker*/
        $('.demo').each(function() {
        //
        // Dear reader, it's actually very easy to initialize MiniColors. For example:
        //
        //  $(selector).minicolors();
        //
        // The way I've done it below is just for the demo, so don't get confused
        // by it. Also, data- attributes aren't supported at this time...they're
        // only used for this demo.
        //
        $(this).minicolors({
                control: $(this).attr('data-control') || 'hue',
                position: $(this).attr('data-position') || 'bottom left',

                change: function(value, opacity) {
                    if (!value) return;
                    if (opacity) value += ', ' + opacity;
                    if (typeof console === 'object') {
                        console.log(value);
                    }
                },
                theme: 'bootstrap'
            });

        });
        /*datwpicker*/
        jQuery('.mydatepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
        var quill = new Quill('#editor', {
            theme: 'snow'
        });

    </script>
</body>

</html>