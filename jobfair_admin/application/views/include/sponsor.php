<!--web-area-end-->
  <div class="col-md-3 col-sm-4 col-xs-12 adds">
    <div class="main-spo hidden-xs">
      <h2>main sponsors</h2>
      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="30000">
        <div class="carousel-inner">
          <div class="item mai-spo-item active">
            <div class="main-spo-clm-1 hidden-xs">
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/oppo.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/vi.jpg"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/lg.jpg"> </div>
              </div>
              <div class="m-logos-right">
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/sam.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/ip.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/voda.png"> </div>
              </div>
            </div>
          </div>
          <div class="item mai-spo-item">
            <div class="main-spo-clm-1">
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/oppo.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/vi.jpg"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/lg.jpg"> </div>
              </div>
              <div class="m-logos-right">
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/sam.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/ip.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/voda.png"> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="co-spo hidden-xs">
      <h2>co sponsors</h2>
      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">
        <div class="carousel-inner">
          <div class="item co-spo-item active">
            <div class="main-spo-clm-1">
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/co-sponsers/1.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/co-sponsers/2.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/co-sponsers/3.png"> </div>
              </div>
              <div class="m-logos-right">
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/co-sponsers/4.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/co-sponsers/5.jpg"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/co-sponsers/6.png"> </div>
              </div>
            </div>
          </div>
          <div class="item co-spo-item">
            <div class="main-spo-clm-1">
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/oppo.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/vi.jpg"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/lg.jpg"> </div>
              </div>
              <div class="m-logos-right">
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/sam.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/ip.png"> </div>
                <div class="m-logos"> <img src="<?= base_url() ?>assets/images/main-sponsers/voda.png"> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!--Pc-view-end-->
    <div class="main-spo hidden-lg hidden-md hidden-sm">
      <h2>main sponsors</h2>
      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="30000">
        <div class="carousel-inner">
          <div class="item mai-spo-item active">
            <div class="main-spo-clm-2">
              <div class="m-logos-left1 row">
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/main-sponsers/oppo.png"> </div>
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/main-sponsers/vi.jpg"> </div>
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/main-sponsers/lg.jpg"> </div>
                </div>
            </div>
          </div>
          <div class="item mai-spo-item">
            <div class="main-spo-clm-2 hidden-lg hidden-md hidden-sm">
              <div class="m-logos-left1 row">
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/main-sponsers/sam.png"> </div>
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/main-sponsers/ip.png"> </div>
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/main-sponsers/voda.png"> </div>
                 </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--main-sponsers-end-->
    <div class="co-spo hidden-lg hidden-md hidden-sm">
      <h2>co sponsors</h2>
      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">
        <div class="carousel-inner">
          <div class="item co-spo-item active">
            <div class="main-spo-clm-1">
              <div class="m-logos-left1 row">
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/co-sponsers/1.png"> </div>
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/co-sponsers/2.png"> </div>
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/co-sponsers/3.png"> </div>
              </div>
            </div>
          </div>
          <div class="item co-spo-item">
            <div class="main-spo-clm-1">
              <div class="m-logos-left1 row">
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/co-sponsers/5.jpg"> </div>
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/co-sponsers/6.png"> </div>
                <div class="m-logos1"> <img src="<?= base_url() ?>asset/images1/co-sponsers/1.png"> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>