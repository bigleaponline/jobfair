<?php
ob_start();
class Admin_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
        $this->db->cache_on();
        $this->load->database();
       
    }
    function login()
    {
        $username=$this->input->post('username');
        $password=base64_encode($this->input->post('pass'));
        $query=$this->db->get_where('tbl_login_users',array('username'=>$username,'password'=>$password,'type'=>'admin'));
        if($query->num_rows()>0)
        {
            foreach($query->result() as $details)
            $cand_data=array(
                            'id'=>$details->id,
                            'username'=>$details->username,
                        );
            $this->session->set_userdata($cand_data);
            return true;
        }
        else
        {
            return false;
        }
    }
    function get_candidate_list()
    {
        $this->db->select('A.*,B.qualification,C.stream_name,E.name AS city');
        $this->db->FROM('tbl_candidate_registration A');
        $this->db->join('tbl_qualificatio_master B', 'A.cand_qualification=B.qualification_id','left');
        $this->db->join('tbl_stream_master C', 'A.cand_stream=C.stream_id','left');
        $this->db->join('tbl_candidate_profile D', 'A.cand_id=D.candidate_id','left');
        $this->db->join('tbl_cities E', 'D.cand_city=E.id','left');
        $this->db->order_by('A.id' ,'ASC');

        $query = $this->db->get()->result_array();
        // print_r($this->db->last_query());die();
        
        return $query;
    }
    function get_qualification_pg()
    {
        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','PG');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_ug()
    {
        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','UG');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_diploma()
    {
        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','diploma');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_plus_two()
    {
        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','plus_two');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_sslc()
    {
        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','SSLC');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_stream($qualification_id='')
    {
        $this->db->select('*');
        $this->db->from('tbl_stream_master');
        $this->db->where('qualification_id', $qualification_id);
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_cand_qualification($cand_id)
    {
        $this->db->select("A.*,B.qualification");
        $this->db->from("tbl_candidate_registration A");
        $this->db->join("tbl_qualificatio_master B", "A.cand_qualification=B.qualification_id","left");
        $this->db-> where(array('A.cand_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    } 
    function get_cand_sub_passion($passion_id='')
    {
        $this->db->select('*');
        $this->db->from('tbl_passion_sub_category');
        $this->db->where('passion_id', $passion_id);
        $query = $this->db->get()->result_array();
        return $query;
    }

    function get_cand_qualification_id($cand_id)
    {
        $this->db->select('cand_qualification');
        $this->db->from('tbl_candidate_registration');
        $this->db-> where('cand_id', $cand_id);
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_cand_qualification_category($cand_id)
    {
        $this->db->select("A.*,B.qualfcn_catgry");
        $this->db->from("tbl_candidate_registration A");
        $this->db->join("tbl_qualificatio_master B", "A.cand_qualification=B.qualification_id","left");
        $this->db-> where(array('A.cand_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_cand_qualification_percent($cand_id)
    {
        $this->db->select("cand_marks");
        $this->db->from("tbl_candidate_registration");
        $this->db->where('cand_id', $cand_id);
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_profile_detail($cand_id)
    {
        $this->db->select('A.*,B.*');
        $this->db->from('tbl_candidate_registration A');
        $this->db->join('tbl_candidate_profile B', 'A.cand_id=B.candidate_id');
        $this->db->where(array('A.cand_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_country($cand_id)
    {
        $this->db->select('B.name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_countries B', 'A.cand_country=B.id');
        $this->db->where(array('A.candidate_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_state($cand_id)
    {
        $this->db->select('B.name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_states B', 'A.cand_state=B.id');
        $this->db->where(array('A.candidate_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_city($cand_id)
    {
        $this->db->select('B.name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_cities B', 'A.cand_city=B.id');
        $this->db->where(array('A.candidate_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_pg_qualification($cand_id)
    {
        $this->db->select('A.profile_id,B.*,C.qualification,D.stream_name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_candidate_edu_details B', 'A.profile_id=B.cand_id',"left");
        $this->db->join('tbl_qualificatio_master C', 'B.course_id=C.qualification_id',"left");
        $this->db->join('tbl_stream_master D', 'B.stream_id=D.stream_id',"left");
        $this->db->where(array('A.candidate_id' =>  $cand_id, 'B.qualfcn_catgry' => 'PG'));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_ug_qualification($cand_id)
    {
        $this->db->select('A.profile_id,B.*,C.qualification,D.stream_name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_candidate_edu_details B', 'A.profile_id=B.cand_id',"left");
        $this->db->join('tbl_qualificatio_master C', 'B.course_id=C.qualification_id',"left");
        $this->db->join('tbl_stream_master D', 'B.stream_id=D.stream_id',"left");
        $this->db->where(array('A.candidate_id' =>  $cand_id, 'B.qualfcn_catgry' => 'UG'));
        $query = $this->db->get()->result_array();
        // print_r($this->db->last_query());die();
        return $query;
    }
    function get_candidate_diploma_qualification($cand_id)
    {
        $this->db->select('A.profile_id,B.*,C.qualification,D.stream_name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_candidate_edu_details B', 'A.profile_id=B.cand_id',"left");
        $this->db->join('tbl_qualificatio_master C', 'B.course_id=C.qualification_id',"left");
        $this->db->join('tbl_stream_master D', 'B.stream_id=D.stream_id',"left");
        $this->db->where(array('A.candidate_id' =>  $cand_id, 'B.qualfcn_catgry' => 'Diploma'));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_plus_two_qualification($cand_id)
    {
        $this->db->select('A.profile_id,B.*,C.qualification,D.stream_name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_candidate_edu_details B', 'A.profile_id=B.cand_id',"left");
        $this->db->join('tbl_qualificatio_master C', 'B.course_id=C.qualification_id',"left");
        $this->db->join('tbl_stream_master D', 'B.stream_id=D.stream_id',"left");
        $this->db->where(array('A.candidate_id' =>  $cand_id, 'B.qualfcn_catgry' => 'Plus Two'));
        $query = $this->db->get()->result_array();
        return $query;
    
    }
    function get_candidate_sslc_qualification($cand_id)
    {
        $this->db->select('A.profile_id,B.*,C.qualification,D.stream_name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_candidate_edu_details B', 'A.profile_id=B.cand_id',"left");
        $this->db->join('tbl_qualificatio_master C', 'B.course_id=C.qualification_id',"left");
        $this->db->join('tbl_stream_master D', 'B.stream_id=D.stream_id',"left");
        $this->db->where(array('A.candidate_id' =>  $cand_id, 'B.qualfcn_catgry' => 'SSLC'));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_experience_details($cand_id)
    {
        $this->db->select('A.profile_id,B.*');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_candidate_experience B', 'A.profile_id=B.candidate_id');
        $this->db->where(array('A.candidate_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_passion_interest($cand_id)
    {
        $this->db->select('A.passion_id,A.interest_id,B.passion,C.sub_passion_name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_passion_master B', 'A.passion_id=B.passion_id');
        $this->db->join('tbl_passion_sub_category C', 'A.interest_id=C.sub_passion_id');
        $this->db->where(array('A.candidate_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_candidate_internship_interest($cand_id)
    {
        $this->db->select('A.pref_intern_id,B.internship_name');
        $this->db->from('tbl_candidate_profile A');
        $this->db->join('tbl_internship_master B', 'A.pref_intern_id=B.internship_id');
        $this->db->where(array('A.candidate_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        // print_r($this->db->last_query());die();
        return $query;
    }
    function get_candidate_video($cand_id)
    {
        $this->db->select('A.cand_id,B.candidate_video,B.candidate_video1,B.cand_resume');
        $this->db->from('tbl_candidate_registration A');
        $this->db->join('tbl_candidate_profile B', 'A.cand_id=B.candidate_id');
        $this->db->where(array('A.cand_id' =>  $cand_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function delete_jobfair_candidate_details($cand_id) 
    {
        $this->db->delete('tbl_candidate_registration', array('cand_id' => $cand_id)); 
        return;
    }
    function get_employer_list()
    {
        $this->db->select('*');
        $this->db->from('tbl_employer_registration');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_employer_details($emp_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_employer_registration');
        $this->db->where('emp_id', $emp_id);
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_employer_job_details($emp_id)
    {
        $this->db->select('B.*,A.emp_id');
        $this->db->FROM('tbl_employer_registration A');
        $this->db->join('tbl_emp_job_details B', 'A.emp_id=B.emp_id','left');
        $this->db->where(array('A.emp_id' =>  $emp_id));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_employer_jd($emp_id)
    {
        $this->db->select('B.*,A.emp_id');
        $this->db->FROM('tbl_employer_registration A');
        $this->db->join('tbl_jd B', 'A.emp_id=B.emp_id','left');
        $this->db->where(array('A.emp_id' =>  $emp_id));
        $query = $this->db->get()->result_array(); 
        return $query;
    }
    function delete_employer($emp_id) 
    {
        $this->db->delete('tbl_employer_registration', array('emp_id' => $emp_id)); 
        return;
    }
    function get_topic_list()
    {
        $this->db->select('*');
        $this->db->from('tbl_topic_master');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function save_topic_entry() 
    {
        $data['topic_id'] = $this->input->post('topic_id');
        $data['topic_name'] = $this->input->post('topic_name');
        $this->db->insert('tbl_topic_master', $data);
    }
    function view_topic($topic_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_topic_master');
        $this->db->where('id',$topic_id);
        $query = $this->db->get()->result_array();
        return $query;
    }
    function topic_update() 
    {
        $topicid=$this->input->post('tid');
        $data['topic_id'] = $this->input->post('topic_id');
        $data['topic_name'] = $this->input->post('topic_name');
        $this->db->where('id',$topicid);
        $this->db->update('tbl_topic_master', $data);
    }
    function delete_topic_entry($topic_id) 
    {
        $this->db->delete('tbl_topic_master', array('id' => $topic_id)); 
        return;
    }
    function check_qualification_dup($q_name)
    {
        $q_catgry  = strtoupper($this->input->post('qualfcn_catgry'));
        $q_name    = strtoupper($q_name);
        $this->db->select("*");
        $this->db->from("tbl_qualificatio_master");
        $this->db->where('upper(qualification)', $q_name);
        $this->db->where('upper(qualfcn_catgry)', $q_catgry);
        $query = $this->db->get();
        if ($query->num_rows() > 0) 
        {
            return FALSE;
        }
    }
    function save_qualification_entry() 
    {
        $data['qualfcn_catgry'] = $this->input->post('qualfcn_catgry');
        $data['qualification'] = $this->input->post('qualification');
        $data['weightage'] = $this->input->post('weightage');
        $this->db->insert('tbl_qualificatio_master', $data);
    }
    function get_qualification_list()
    {
        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function delete_qualification_entry($qualification_id) 
    {
        $this->db->delete('tbl_qualificatio_master', array('qualification_id' => $qualification_id)); 
        return;
    }
    function get_qualification_edit($qualification_id)
    {
        $this->db->select("*");
        $this->db->from("tbl_qualificatio_master");
        $this->db->where("qualification_id",$qualification_id);
        $query = $this->db->get()->result_array();
        return $query;
    }
    function update_qualification_entry($qualification_id)
    {
        $data['qualfcn_catgry'] = $this->input->post('qualfcn_catgry');
        $data['qualification'] = $this->input->post('qualification');
         $data['weightage'] = $this->input->post('weightage');
        $this->db->update('tbl_qualificatio_master', $data, array('qualification_id'=>$qualification_id));
    }
    function check_passion_dup($p_name)
    {
        $p_name = strtoupper($p_name);
        $this->db->select("*");
        $this->db->from("tbl_passion_master");
        $this->db->where("upper(passion) LIKE '%$p_name%'"); 
        $query = $this->db->get();
        if ($query->num_rows() > 0) 
        {
            return FALSE;
        }
    }
    function save_passion_entry() 
    {
        $data['passion'] = $this->input->post('passion');
        $this->db->insert('tbl_passion_master', $data);
    }
    function delete_passion_entry($passion_id) 
    {
        $this->db->delete('tbl_passion_master', array('passion_id' => $passion_id)); 
        return;
    }
    function get_passion_edit($passion_id)
    {
        $this->db->select("*");
        $this->db->from("tbl_passion_master");
        $this->db->where("passion_id",$passion_id);
        $query = $this->db->get()->result_array();
        return $query;
    } 
    function update_passion_entry($passion_id)
    {
        $data['passion'] = $this->input->post('passion');
        $data['passion_id'] = $passion_id;
        $this->db->update('tbl_passion_master', $data, array('passion_id'=>$passion_id));
    }
    function get_passion_list()
    {
        $this->db->select('*');
        $this->db->from('tbl_passion_master');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function save_sub_passion_entry()
    {
        $passion_id  = $this->input->post('passion_id');
        $sub_passion_name = $this->input->post('sub_passion_name');
        $data['passion_id']  = $passion_id;
        $data['sub_passion_name'] = $sub_passion_name;
        $this->db->insert('tbl_passion_sub_category',$data);
    }
    function get_sub_passion()
    {
        $this->db->select("A.*,B.passion");
        $this->db->from("tbl_passion_sub_category A");
        $this->db->join("tbl_passion_master B", "A.passion_id=B.passion_id","left");
        if($this->input->post('passion_id'))
        {
            $passion_id=$this->input->post('passion_id');
            $this->db-> where(array('A.passion_id' =>  $passion_id));
        }
        $this->db->order_by('B.passion' ,'ASC');
        $query = $this->db->get()->result_array();
        return $query;
    } 
    function get_sub_passion_edit($sub_passion_id)
    {
        $this->db->select("*");
        $this->db->from("tbl_passion_sub_category");
        $this->db->where("sub_passion_id",$sub_passion_id);
        $query = $this->db->get()->result_array();
        return $query;
    } 
    function update_sub_passion_entry($sub_passion_id)
    {
        $data['passion_id'] = $this->input->post('passion_id');
        $data['sub_passion_name'] = $this->input->post('sub_passion_name');   
        $this->db->update('tbl_passion_sub_category', $data, array('sub_passion_id'=>$sub_passion_id));
    }
    function delete_sub_passion($sub_passion_id) 
    {
        $this->db->delete('tbl_passion_sub_category', array('sub_passion_id' => $sub_passion_id)); 
        return;
    }
    function get_passion()
    {
        $this->db->select('*');
        $this->db->order_by('passion' ,'ASC');
        $query = $this->db->get('tbl_passion_master')->result_array();
        return $query;
    }
    function get_stream()
    {
        $this->db->select("A.*,B.qualification");
        $this->db->from("tbl_stream_master A");
        $this->db->join("tbl_qualificatio_master B", "A.qualification_id=B.qualification_id","left");
        if($this->input->post('qualification_id'))
        {
            $qualification_id=$this->input->post('qualification_id');
            $this->db-> where(array('A.qualification_id' =>  $qualification_id));
        }
        $this->db->order_by('B.qualification' ,'ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }    
    function save_stream_entry()
    {
        $qualification_id  = $this->input->post('qualification_id');
        $stream_name = $this->input->post('stream_name');
        $data['qualification_id']  = $qualification_id;
        $data['stream_name'] = $stream_name;
        $this->db->insert('tbl_stream_master',$data);
    }
    function delete_stream_entry($stream_id) 
    {
        $this->db->delete('tbl_stream_master', array('stream_id' => $stream_id)); 
        return;
    } 
    function get_stream_edit($stream_id)
    {
        $this->db->select("*");
        $this->db->from("tbl_stream_master");
        $this->db->where("stream_id",$stream_id);
        $query = $this->db->get()->result_array();
        return $query;
    } 
    function update_stream_entry($stream_id)
    {
        $data['qualification_id'] = $this->input->post('qualification');
        $data['stream_name'] = $this->input->post('stream_name');   
        $this->db->update('tbl_stream_master', $data, array('stream_id'=>$stream_id));
    }
    function get_qualification()
    {
        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function save_question_entry()
    {
        $data['topic_id']  = $this->input->post('topic_id');
        $data['ques_name'] = $this->input->post('ques_name');
        $data['weightage']  = $this->input->post('weightage');
        $data['question_type']  = $this->input->post('question_type');
        $this->db->insert('tbl_question_master',$data);
        $question_id = $this->db->insert_id();
        if($data['question_type']  == "passage")
        {
            $row_no = $this->input->post('row_no');
            $sub_ques_name = $this->input->post('sub_ques_name');
            $sub_option1 = $this->input->post('sub_option1');
            $sub_option2 = $this->input->post('sub_option2');
            $sub_option3 = $this->input->post('sub_option3');
            $sub_option4 = $this->input->post('sub_option4');
            $sub_answer = $this->input->post('sub_answer');
            for($i=0;$i<$row_no;$i++)
            {
                $data1['question_id'] = $question_id;
                $data1['question'] = $sub_ques_name[$i];
                $data1['option_1'] = $sub_option1[$i];
                $data1['option_2'] = $sub_option2[$i];
                $data1['option_3'] = $sub_option3[$i];
                $data1['option_4'] = $sub_option4[$i];
                $data1['answer'] = $sub_answer[$i]; 
                $this->db->insert('tbl_passage_questions',$data1);
            }
        }
        else
        {
            $data2['question_id'] = $question_id;
            $data2['option_1'] = $this->input->post('option_1');
            $data2['option_2'] = $this->input->post('option_2');
            $data2['option_3'] = $this->input->post('option_3');
            $data2['option_4'] = $this->input->post('option_4');
            $data2['answer'] = $this->input->post('answer');
            $this->db->insert('tbl_general_questions',$data2);
        }
    }
    function get_question_master_list() 
    {
        $this->db->select("A.*,B.topic_name");
        $this->db->from("tbl_question_master A");
        $this->db->join("tbl_topic_master B", "A.topic_id=B.topic_id","left");
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_cand_elblty_ctgry()
    {
        $this->db->select("*");
        $this->db->from("tbl_eligibility_catgry_master ");
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_question_master_list1() 
    {
        $topic_id = $this->input->post('topic_id');
        $weightage = $this->input->post('weightage');
        $this->db->select("A.*,B.topic_name");
        $this->db->from("tbl_question_master A");
        $this->db->join("tbl_topic_master B", "A.topic_id=B.id","left");
        $this->db->where("A.topic_id", $topic_id);
        if($weightage !=-1)
        { 
            $this->db->where("A.weightage", $weightage);
        }
        $query = $this->db->get();
        return $query->result_array();
    }
    function get_general_question_detail($quest_id)
    {
        $this->db->select('B.*,A.id,A.question_type,A.ques_name');
        $this->db->FROM('tbl_question_master A');
        $this->db->join('tbl_general_questions B', 'A.id=B.question_id','left');
        $this->db->where(array('A.id' =>  $quest_id,'A.question_type' => 'general'));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_question_detail($quest_id) 
    {
        $this->db->select("*");
        $this->db->from("tbl_question_master");
        $this->db->where("id",$quest_id);
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_passage_question_detail($quest_id)
    {
        $this->db->select('B.*,A.id,A.question_type,A.ques_name');
        $this->db->FROM('tbl_question_master A');
        $this->db->join('tbl_passage_questions B', 'A.id=B.question_id','left');
        $this->db->where(array('A.id' =>  $quest_id,'A.question_type' => 'passage'));
        $query = $this->db->get()->result_array();
        return $query;
    }
    function update_question_entry($quest_id,$question_type)
    {
        $question_type = $this->input->post('question_type');
        $this->db->select('*');
        $this->db->from("tbl_question_master");
        $this->db->where("id",$quest_id);
        $query = $this->db->get();
        $result = $query->num_rows();
        if($result > 0)
        {
            if($question_type == 'general')
            {
                $data['question_id'] = $quest_id;
                $data['answer'] = $this->input->post('answer');
                $data['option_1'] = $this->input->post('option_1');
                $data['option_2'] = $this->input->post('option_2');
                $data['option_3'] = $this->input->post('option_3');
                $data['option_4'] = $this->input->post('option_4');
                $this->db->update('tbl_general_questions', $data, array('question_id' =>  $quest_id));
            }
            else
            {
                $row_no = $this->input->post('row_no');
                $sub_ques_name = $this->input->post('sub_ques_name');
                $sub_option1 = $this->input->post('sub_option1');
                $sub_option2 = $this->input->post('sub_option2');
                $sub_option3 = $this->input->post('sub_option3');
                $sub_option4 = $this->input->post('sub_option4');
                $sub_answer = $this->input->post('sub_answer');
                $id = $this->input->post('q_id');
                for($i=0;$i<$row_no;$i++)
                {
                    $data1['question_id'] = $quest_id;
                    $data1['question'] = $sub_ques_name[$i];
                    $data1['option_1'] = $sub_option1[$i];
                    $data1['option_2'] = $sub_option2[$i];
                    $data1['option_3'] = $sub_option3[$i];
                    $data1['option_4'] = $sub_option4[$i];
                    $data1['answer'] = $sub_answer[$i]; 
                    $this->db->update('tbl_passage_questions', $data1, array('question_id' =>  $quest_id, 'passage_id' => $id));
                }
            }
            $data2['topic_id']  = $this->input->post('topic_id');
            $data2['weightage']  = $this->input->post('weightage');
            $data2['question_type']  = $this->input->post('question_type');
            $data2['ques_name'] = $this->input->post('ques_name');
            $this->db->update('tbl_question_master', $data2, array('id' =>  $quest_id));
        }
    }
    function delete_question_entry($quest_id) 
    {
        $this->db->delete('tbl_question_master', array('id' => $quest_id)); 
        return;
    }
    function get_internship()
    {
        $this->db->select('*');
        $this->db->from('tbl_internship_master');
        $this->db->order_by('internship_name','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function check_internship_dup($intern_name)
    {
        $intern_name = strtoupper($intern_name);
        $this->db->select("*");
        $this->db->from("tbl_internship_master");
        $this->db->where("upper(internship_name) LIKE '%$intern_name%'"); 
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return FALSE;
        }
    }
    function save_internship_entry() 
    {
    $data['internship_name'] = $this->input->post('internship_name');
    $this->db->insert('tbl_internship_master', $data);
    }
    function get_internship_edit($internship_id)
    {
        $this->db->select("*");
        $this->db->from("tbl_internship_master");
        $this->db->where("internship_id",$internship_id);
        $query = $this->db->get()->result_array();
        return $query;
    } 
    function update_internship_entry($internship_id)
    {
        $data['internship_name'] = $this->input->post('internship_name');
        $data['internship_id'] = $internship_id;
        $this->db->update('tbl_internship_master', $data, array('internship_id'=>$internship_id));
    }
    function delete_internship_entry($internship_id) 
    {
        $this->db->delete('tbl_internship_master', array('internship_id' => $internship_id)); 
        return;
    }
    function get_sponsor_list()
    {
        $this->db->select('*');
        $this->db->from('tbl_sponsor_registration');
        $this->db->order_by('sponsor_id' ,'ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }   
    function delete_sponsor($spo_id)
    {
        $this->db->delete('tbl_sponsor_registration',array('sponsor_id'=>$spo_id));
        return;
    }
}
?>