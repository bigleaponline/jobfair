<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_controller extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('admin_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('test_helper');
        $this->load->helper('download');
    }
    public function index()
    {
        $this->load->view('admin/login');
    }
    public function loadpage()
	{
		$page=$this->uri->segment('1');
		$this->load->view('admin/'.$page);
	}
    public function dashboard()
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {

        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');

        $data['candidate_count'] = count($this->admin_model->get_candidate_list());
        $data['employer_count'] = count($this->admin_model->get_employer_list());
        $data['sponsor_Count'] = count($this->admin_model->get_sponsor_list());

        $this->load->view('admin/admin-header',$data);
        $this->load->view('admin/admin-navbar',$data);
        $this->load->view('admin/index',$data);
        }
    }
    public function login()
    {

        $login=$this->admin_model->login();
        if($login)
        {
            $this->session->set_userdata('cand_data',$login);
            redirect('dashboard');
        }
        else
        {
            $this->session->set_flashdata('loginerror','Username or Password Is Incorrect');
            redirect('login');
        }
    }
    public function logout()
    {
        session_destroy();

        $this->load->view('admin/login');

    }
    public function get_candidate_list()
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
    	$data['candidate_list'] = $this->admin_model->get_candidate_list();
		$this->load->view('admin/admin-header',$data);
		$this->load->view('admin/admin-navbar',$data);
		$this->load->view('admin/candidate_list',$data);
        }
    }
    public function view_jobfair_candidate_details($cand_id)
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
    	$data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['cand_id'] = $cand_id;
        $passion_id = '';
        $data['qualification_pg'] = $this->admin_model->get_qualification_pg();
        $data['qualification_ug'] = $this->admin_model->get_qualification_ug();
        $data['qualification_diploma'] = $this->admin_model->get_qualification_diploma();
        $data['qualification_plus_two'] = $this->admin_model->get_qualification_plus_two();
        $data['qualification_sslc'] = $this->admin_model->get_qualification_sslc();
        $data['passion_list'] = $this->admin_model->get_passion();
        $data['highest_qualification'] = $this->admin_model->get_cand_qualification($cand_id);
        $data['highest_qualification_id'] = $this->admin_model->get_cand_qualification_id($cand_id);
        $data['sub_passion'] = $this->admin_model->get_cand_sub_passion($passion_id);
        $data['cand_qualification_category'] = $this->admin_model->get_cand_qualification_category($cand_id);
        $data['cand_qualification_percent'] = $this->admin_model->get_cand_qualification_percent($cand_id);
        $data['candidate_profile_detail'] = $this->admin_model->get_candidate_profile_detail($cand_id);
        $data['candidate_country'] = $this->admin_model->get_candidate_country($cand_id);
        $data['candidate_state'] = $this->admin_model->get_candidate_state($cand_id);
        $data['candidate_city'] = $this->admin_model->get_candidate_city($cand_id);
        $data['pg_detail'] = $this->admin_model->get_candidate_pg_qualification($cand_id);
        $data['ug_detail'] = $this->admin_model->get_candidate_ug_qualification($cand_id);
        $data['diploma_detail'] = $this->admin_model->get_candidate_diploma_qualification($cand_id);
        $data['plus_two_detail'] = $this->admin_model->get_candidate_plus_two_qualification($cand_id);
        $data['sslc_detail'] = $this->admin_model->get_candidate_sslc_qualification($cand_id);
        $data['candidate_exp_details'] = $this->admin_model->get_candidate_experience_details($cand_id);
        $data['candidate_passion'] = $this->admin_model->get_candidate_passion_interest($cand_id);
        $data['candidate_internship'] = $this->admin_model->get_candidate_internship_interest($cand_id);
        $data['candidate_resume_video'] = $this->admin_model->get_candidate_video($cand_id);
    	$this->load->view('admin/admin-header',$data);
		$this->load->view('admin/admin-navbar',$data);
		$this->load->view('admin/candidate_view',$data);
        }
    }
    public function download($fileName) 
    {  
        if ($fileName) 
        { 
            $file ='../upload/resume/'.$fileName;
            if (file_exists ( $file )) 
            {
                $data = file_get_contents ($file);
                force_download ( $fileName, $data );
            } 
            else 
            {
               redirect('candidate_list');
            }
        }
    }
    public function delete_jobfair_candidate_details($cand_id)
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $this->admin_model->delete_jobfair_candidate_details($cand_id);
        redirect('candidate_list');
        }
    }
    public function get_employer_list()
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {

        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
    	$data['employer_list'] = $this->admin_model->get_employer_list();
		$this->load->view('admin/admin-header',$data);
		$this->load->view('admin/admin-navbar',$data);
		$this->load->view('admin/employer_list',$data);
        }
    }
    public function employer_view($emp_id)
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['emp_id'] = $emp_id;
        $data['employer_details'] = $this->admin_model->get_employer_details($emp_id);
        $data['employer_job_details'] = $this->admin_model->get_employer_job_details($emp_id);
        $data['employer_jd'] = $this->admin_model->get_employer_jd($emp_id);
        $this->load->view('admin/admin-header',$data);
        $this->load->view('admin/admin-navbar',$data);
        $this->load->view('admin/employer_view',$data);
        }
    }
    public function downloadJD($fileName) 
    {  
        if ($fileName) 
        { 
            $file ='../upload/jd/'.$fileName;
            if (file_exists ( $file )) 
            {
                $data = file_get_contents ($file);
                force_download ( $fileName, $data );
            } 
            else 
            {
               redirect('candidate_list');
            }
        }
    }
    public function delete_employer($emp_id)
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $this->admin_model->delete_employer($emp_id);
        redirect('employer_list');
        }
    }
    public function get_topic_list()
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
    	$data['topic_list'] = $this->admin_model->get_topic_list();
		$this->load->view('admin/admin-header',$data);
		$this->load->view('admin/admin-navbar',$data);
		$this->load->view('admin/topic_list',$data);
        }
    }
    public function topic_entry() 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        }
        else 
        {
            $this->form_validation->set_rules('topic_id', 'Topic Name', 'required');  
            if ($this->form_validation->run() === FALSE) 
            {
                 
            }
            else 
            {
                $this->admin_model->save_topic_entry();
                redirect('topic_list');
            }
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/topic_entry');
        }
    }
    public function view_topic($topic_id)
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $data['topic_view']=$this->admin_model->view_topic($topic_id);
        $this->load->view('admin/admin-header',$data);
        $this->load->view('admin/admin-navbar',$data);
        $this->load->view('admin/topic_view',$data);
        }
    }
    public function topic_update()
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $this->admin_model->topic_update();
        redirect('topic_list');
        }
    }
    public function delete_topic($topic_id)
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $this->admin_model->delete_topic_entry($topic_id);
        redirect('topic_list');
        }
    }
    public function get_qualification_list()
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['qualification_list'] = $this->admin_model->get_qualification_list();
        $this->load->view('admin/admin-header',$data);
        $this->load->view('admin/admin-navbar',$data);
        $this->load->view('admin/qualification_list',$data);
        }
    }
    public function qualification_entry()
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
         if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $this->form_validation->set_rules('qualification', 'Qualification Name', 'required');
        $this->form_validation->set_rules('qualification', 'Qualification Name', 'callback_check_qualification_dup');
        $this->form_validation->set_message('check_qualification_dup','This qualification already exists');   
        if ($this->form_validation->run() === FALSE) 
        {
        } 
        else 
        {
            $this->admin_model->save_qualification_entry();
            redirect('qualification_list');
        }
        $this->load->view('admin/admin-header',$data);
        $this->load->view('admin/admin-navbar',$data);
        $this->load->view('admin/qualification_entry');
        }
    }
    public function check_qualification_dup($q_name)
    {
        return  $this->admin_model->check_qualification_dup($q_name);
    }
    public function delete_qualification($qualification_id)
    {
        if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $this->admin_model->delete_qualification_entry($qualification_id);
        redirect('qualification_list');
        }
    }
    public function qualification_entry_edit($qualification_id = ' ') 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['qualification_id'] = $qualification_id;
        $data['qualification_edit'] = $this->admin_model->get_qualification_edit($qualification_id);
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('qualification', 'Qualification Name', 'required');
            if ($this->form_validation->run() === FALSE)
            {
            } 
            else
            {
                $this->admin_model->update_qualification_entry($qualification_id);
                redirect('qualification_list');
            }
                $this->load->view('admin/admin-header',$data);
                $this->load->view('admin/admin-navbar',$data);
                $this->load->view('admin/qualification_entry_edit',$data);
        }
    }
    public function passion_entry() 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('passion', 'Passion Name', 'required');
            $this->form_validation->set_rules('passion', 'Passion Name', 'callback_check_passion_dup');
            $this->form_validation->set_message('check_passion_dup','This passion already exists'); 
            if ($this->form_validation->run() === FALSE) 
            {
            } 
            else 
            {
                $this->admin_model->save_passion_entry();
            }
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/passion_entry');
        }
    }
    public function get_passion_list()
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
         if ($this->session->userdata('username') == '') {
        redirect('login');
        } else {
        $data['passion_list'] = $this->admin_model->get_passion_list();
        $this->load->view('admin/admin-header',$data);
        $this->load->view('admin/admin-navbar',$data);
        $this->load->view('admin/passion_list',$data);
        }
    }
    public function check_passion_dup($p_name)
    {
       return  $this->admin_model->check_passion_dup($p_name);
    }
    public function passion_entry_edit($passion_id) 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['passion_id'] = $passion_id;
        $data['passion_edit'] = $this->admin_model->get_passion_edit($passion_id);
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        }
        else 
        {
            $this->form_validation->set_rules('passion', 'Passion Name', 'required');
            if ($this->form_validation->run() === FALSE)
            {
            } 
            else 
            {
                $this->admin_model->update_passion_entry($passion_id);
                redirect('passion_list');
            }
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/passion_entry_edit',$data);
        }
    }
    public function delete_passion($passion_id) 
    {
        $data['passion_id'] = $passion_id;
        if ($this->session->userdata('username') == '')
        {
            redirect('login');
        } 
        else 
        {
            $this->admin_model->delete_passion_entry($passion_id);
            redirect('passion_list');
        }
    }
    public function sub_passion_list() 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $data['sub_passion_list'] = $this->admin_model->get_sub_passion();
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/sub_passion_list',$data);
        }
    }
    public function sub_passion_entry() 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        if ($this->session->userdata('username') == '')
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('passion_id', 'Passion', 'required');
            $this->form_validation->set_rules('sub_passion_name', 'Sub Passion Name', 'required');
            if ($this->form_validation->run() === FALSE) 
            {
            } 
            else 
            {
                $this->admin_model->save_sub_passion_entry();
                redirect('sub_passion_list');
            }
            $data['passion_list'] = $this->admin_model->get_passion();
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/sub_passion_entry',$data);
        }
    }
    public function sub_passion_entry_edit($sub_passion_id) 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['sub_passion_id'] = $sub_passion_id;
        $data['sub_passion_edit'] = $this->admin_model->get_sub_passion_edit($sub_passion_id);
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('passion_id', 'Passion Name', 'required');
            $this->form_validation->set_rules('sub_passion_name', 'Sub Passion Name', 'required');
            if ($this->form_validation->run() === FALSE)
            {
            } 
            else 
            {
                $this->admin_model->update_sub_passion_entry($sub_passion_id);
                redirect('sub_passion_list');
            }
            $data['passion_list'] = $this->admin_model->get_passion();  
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/sub_passion_entry_edit',$data);
        }
    }
    public function delete_sub_passion($sub_passion_id) 
    {
        $data['sub_passion_id'] = $sub_passion_id;
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->admin_model->delete_sub_passion($sub_passion_id);
            redirect('sub_passion_list');
        }
    }
    public function stream_list() 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $data['stream_list'] = $this->admin_model->get_stream();
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/stream_list',$data);
        }
    }
    public function stream_entry() 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('qualification_id', 'Qualification Id', 'required');  
            if ($this->form_validation->run() === FALSE) 
            {
            } 
            else 
            {
                $this->admin_model->save_stream_entry();
                redirect('stream_list');
            }
            $data['qualification_list'] = $this->admin_model->get_qualification();
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/stream_entry',$data);
        }
    }
    public function stream_entry_edit($stream_id) 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['stream_id'] = $stream_id;
        $data['stream_edit'] = $this->admin_model->get_stream_edit($stream_id);
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('qualification', 'Qualification Name', 'required');
            if ($this->form_validation->run() === FALSE) 
            {
            } 
            else 
            {
                $this->admin_model->update_stream_entry($stream_id);
                redirect('stream_list');
            }
            $data['qualification_list'] = $this->admin_model->get_qualification();    
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/stream_entry_edit',$data);
        }
    }
    public function delete_stream($stream_id) 
    {
        $data['stream_id'] = $stream_id;
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        }
        else 
        {
            $this->admin_model->delete_stream_entry($stream_id);
            redirect('stream_list');
        }
    }
    public function question_entry() 
    {

        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('topic_id', 'Topic Id', 'required');  
            if ($this->form_validation->run() === FALSE)
            {

            } 
            else 
            {

                $this->admin_model->save_question_entry();
            }
            $data['topics_list'] = $this->admin_model->get_topic_list();
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/question_entry',$data);

        }
    }
    public function question_list() 
    {  
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['question_list'] = $this->admin_model->get_question_master_list();
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/question_list', $data);
        }
    }
    public function question_entry_edit($quest_id)
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['id'] = $quest_id;
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('weightage', 'weightage', 'required');  
            if ($this->form_validation->run() === FALSE)
            {
                $data['question_data'] = $this->admin_model->get_question_detail($quest_id);
                $data['topics_list'] = $this->admin_model->get_topic_list();
        
            } 
            else 
            {
                $question_type = $this->input->post('question_type');
                $this->admin_model->update_question_entry($quest_id,$question_type);
                redirect('question_list');
            }
            $data['question_data'] = $this->admin_model->get_question_detail($quest_id);
            $data['general_question_data'] = $this->admin_model->get_general_question_detail($quest_id);
            $data['passage_question_data'] = $this->admin_model->get_passage_question_detail($quest_id);
            $data['topics_list'] = $this->admin_model->get_topic_list();
            $data['quest_id'] = $quest_id;
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/question_entry_edit', $data);
        }
    }
    public function delete_question_entry($quest_id) 
    {
        $data['id'] = $quest_id;
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        }
        else 
        {
            $this->admin_model->delete_question_entry($quest_id);
            redirect('question_list');
        }
    }
    public function internship_entry()
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        if ($this->session->userdata('username') == '')
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('internship_name', 'Internship Name', 'required');
            $this->form_validation->set_rules('internship_name', 'Internship Name', 'callback_check_internship_dup');
            $this->form_validation->set_message('check_internship_dup','Internship Name already exists');   
            if ($this->form_validation->run() === FALSE) 
            {
    
            } 
            else 
            {
        
                $this->admin_model->save_internship_entry();
                redirect('internship_list');
            }
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/internship_entry');
        }
    }
    public function internship_list()
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['internship_list'] = $this->admin_model->get_internship();
        if ($this->session->userdata('username') == '')
        {
            redirect('login');
        } 
        else
        {
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/internship_list',$data);
        }
    }
    public function check_internship_dup($intern_name)
    {
       return  $this->admin_model->check_internship_dup($intern_name);
    }
    public function delete_internship($internship_id) 
    {
        $data['internship_id'] = $internship_id;
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->admin_model->delete_internship_entry($internship_id);
            redirect('internship_list');
        }
    }
    public function internship_entry_edit($internship_id) 
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
        $data['internship_id'] = $internship_id;
        $data['internship_edit'] = $this->admin_model->get_internship_edit($internship_id);
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->form_validation->set_rules('internship_name', 'Internship Name', 'required');
            if ($this->form_validation->run() === FALSE)
            {
            } 
            else
            {
                $this->admin_model->update_internship_entry($internship_id);
                redirect('internship_list');
            }
            $this->load->view('admin/admin-header',$data);
            $this->load->view('admin/admin-navbar',$data);
            $this->load->view('admin/internship_entry_edit',$data);
        }
    }
    public function get_sponsor_list()
    {
        $data['id'] = $this->session->userdata('id');
        $id = $this->session->userdata('id');
        $data['username'] = $this->session->userdata('username');
        $username = $this->session->userdata('username');
    	$data['sponsor_list'] = $this->admin_model->get_sponsor_list();
		$this->load->view('admin/admin-header',$data);
		$this->load->view('admin/admin-navbar',$data);
		$this->load->view('admin/sponsor_list',$data);
    }
    public function delete_sponsor($spo_id)
    {
        if ($this->session->userdata('username') == '') 
        {
            redirect('login');
        } 
        else 
        {
            $this->admin_model->delete_sponsor($spo_id);
            redirect('sponsor_list');
        }
    }
}
?>
