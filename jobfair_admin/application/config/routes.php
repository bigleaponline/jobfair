<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'admin_controller';
// $route['sign-in']='jobfair_controller/loadpage/sign-in';
// $route['login']='jobfair_controller/loadpage/login';
// $route['logout']='jobfair_controller/logout';
// $route['job1']='jobfair_controller/loadpage/job1';
// $route['job2']='jobfair_controller/loadpage/job2';
// $route['employ-registration'] = 'jobfair_controller/loadpage/employ-registration';
// $route['index']='jobfair_controller/loadpage/index';
// $route['other-details']='jobfair_controller/other_details';
// $route['assesment']='jobfair_controller/loadpage/assesment';
// $route['assesment2']='jobfair_controller/loadpage/assesment2';
// $route['welcome']='jobfair_controller/loadpage/welcome';
// $route['sponsor-registration'] = 'Jobfair_controller/loadpage/sponsor-registration';


// $route['company-list']='Jobfair_controller/loadpage/company-list';
$route['login']='admin_controller/loadpage/login';
$route['logout']='admin_controller/logout';
$route['dashboard']='admin_controller/dashboard';
$route['candidate_list']='admin_controller/get_candidate_list';
$route['employer_list']='admin_controller/get_employer_list';
$route['topic_list']='admin_controller/get_topic_list';
$route['topic_entry']='admin_controller/topic_entry';
$route['topic_view']='admin_controller/view_topic';
$route['qualification_entry']='admin_controller/qualification_entry';
$route['qualification_list']='admin_controller/get_qualification_list';
$route['qualification_entry_edit']='admin_controller/qualification_entry_edit';
$route['passion_entry']='admin_controller/passion_entry';
$route['passion_list']='admin_controller/get_passion_list';
$route['passion_entry_edit']='admin_controller/passion_entry_edit';
$route['sub_passion_entry']='admin_controller/sub_passion_entry';
$route['sub_passion_list']='admin_controller/sub_passion_list';
$route['sub_passion_entry_edit']='admin_controller/sub_passion_entry_edit';
$route['stream_entry']='admin_controller/stream_entry';
$route['stream_list']='admin_controller/stream_list';
$route['stream_entry_edit']='admin_controller/stream_entry_edit';
$route['question_entry']='admin_controller/question_entry';
$route['question_list']='admin_controller/question_list';
$route['question_entry_edit']='admin_controller/question_entry_edit';
$route['internship_entry']='admin_controller/internship_entry';
$route['internship_list']='admin_controller/internship_list';
$route['internship_entry_edit']='admin_controller/internship_entry_edit';
$route['sponsor_list']='admin_controller/get_sponsor_list';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
