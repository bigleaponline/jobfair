<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Jobfair_controller';
$route['index'] = 'Jobfair_controller';
$route['login']='Jobfair_controller/loadpage/login';
$route['sign-in']='Jobfair_controller/signin';
$route['sign-up']='Jobfair_controller/signin';
$route['logout']='Jobfair_controller/logout';
$route['job1']='jobfair_controller/loadpage/job1';
$route['job2']='jobfair_controller/loadpage/job2';
$route['faq']='jobfair_controller/loadpage/faq';
$route['employ-registration'] = 'jobfair_controller/loadpage/employ-registration';
$route['(:any)/jd/(:any)'] = 'jobfair_controller/jobdetails/jd/$1';
$route['(:any)/company-list']='Jobfair_controller/companylist';
$route['sponsor-registration'] = 'jobfair_controller/loadpage/sponsor-registration';
$route['thrissur']='Jobfair_controller/calicut';
$route['welcome']='jobfair_controller/loadpage/welcome';
$route['other-details']='jobfair_controller/other_details';
// $route['other-details']='jobfair_controller/other_details';
$route['thrissur/sign-in']='calicut_jobfair_controller/signin';
$route['thrissur/employ-registration'] = 'calicut_jobfair_controller/loadpage/employ-registration';
$route['thrissur/sponsor-registration'] = 'calicut_jobfair_controller/loadpage/sponsor-registration';
$route['thrissur/job1']='calicut_jobfair_controller/loadpage/job1';
$route['thrissur/job2']='calicut_jobfair_controller/loadpage/job2';
$route['thrissur/faq']='calicut_jobfair_controller/loadpage/faq';
$route['thrissur/welcome']='calicut_jobfair_controller/loadpage/welcome';
$route['thrissur/other-details']='jobfair_controller/other_details';

// $route['calicut/assesment']='Calicut_jobfair_controller/loadpage/assesment';
// $route['calicut/assesment2']='Calicut_jobfair_controller/loadpage/assesment2';
$route['thrissur/instructions']='calicut_jobfair_controller/loadpage/instructions';
$route['thrissur/assessment']='calicut_jobfair_controller/assesment_test';

$route['thrissur/technical_assesment']='calicut_jobfair_controller/technical_assesment';
$route['thrissur/tech_assesment/2']='calicut_jobfair_controller/technical_assesment/2';
$route['thrissur/technical_assesment/3']='calicut_jobfair_controller/technical_assesment/3';
$route['thrissur/technical_assesment/4']='calicut_jobfair_controller/technical_assesment/4';
$route['thrissur/report']='calicut_jobfair_controller/report';
$route['thrissur/technical_report']='calicut_jobfair_controller/technical_report';
$route['thrissur/technical_report_pdf']='calicut_jobfair_controller/technical_report_pdf';
$route['thrissur/hall_ticket']='calicut_jobfair_controller/hall_ticket';
$route['thrissur/hall_ticket_pdf']='calicut_jobfair_controller/hall_ticket_pdf';
$route['thrissur/hall_ticket_print']='calicut_jobfair_controller/hall_ticket_print';
$route['thrissur/technical_report_pdf']='calicut_jobfair_controller/technical_report_pdf';

$route['thrissur/aptitude_test']='assesment_controller/technical_assesment_test';
$route['thrissur/aptitude_report']='assesment_controller/aptitude_report';
$route['thrissur/aptitude_report_pdf']='assesment_controller/aptitude_report_pdf';

$route['(:any)/company_list']='Jobfair_controller/company_list';



$route['thrissur/test']='assesment_controller/assessment_test';



$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
