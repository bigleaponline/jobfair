<link href="<?php echo base_url(); ?>/assets/css/material.css" rel="stylesheet">
<style>
    .field-icon {
        float: right;
        margin-left: -25px;
        margin-top: -25px;
        position: relative;
        z-index: 2;
    }

    .container {
        padding-top: 50px;
        margin: auto;
    }

    #errmsg0,
    #errmsg1,
    #passmsg {
        color: red;
    }
</style>
<?php include('include/modi.php');?>
<div class="container-fluid mt right-content">
    <div class="col-md-9 col-sm-8 site">

        <div class="container-fluid">
            <?php 
        if($this->session->flashdata('errorregistration'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('errorregistration').'</div>';   
        }
        if($this->session->flashdata('password'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('password').'</div>';   
        }
        if($this->session->flashdata('imageerr'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('imageerr').'</div>';   
        }
    ?>
            <?php include('include/main-sponsor-slider.php');?>
            <div class="titles til3" style="border-top:none;">
                <h2>Candidate Registration</h2>

            </div>
            <?php $length = 3;
            $randomString = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
            ?>
            <div class="e-r-f regi">
                <form action="<?php echo base_url();?>jobfair_controller/candidateRegistration" method="post" name="myForm" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name : </label>
                                <input type="text" class="form-control" placeholder="Enter Name" name="name" id="name" required style="text-transform: capitalize;">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Date Of Birth : </label>
                                <input class="form-control" type="text" placeholder="Enter Date Of Birth" name="dob" id="dob" required>
                            </div>
                            <div class="form-group">
                                <label for="exampleFormControlSelect1">Gender : </label>
                                <select class="form-control" name="gender" id="gender" required>
                                    <option value="" selected disabled>Select Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Phone No : </label>
                                <input type="text" class="form-control" placeholder="Enter Phone No" name="mobile" id="mobile" onchange="profile(this.value)" maxlength="10">
                                <span id="errmsg0"></span>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Whats App No : </label>
                                <input type="text" class="form-control" placeholder="Enter Whats App No" name="whatsapp" id="whatsapp" maxlength="10">
                                <span id="errmsg1"></span>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Address : </label>
                                <textarea class="form-control" name="address" id="address" ></textarea>
                            </div>
                            <div class="form-group" id="user">
                                <label for="exampleInputEmail1">Username : </label>
                                <input type="text" class="form-control" name="username" id="username" value="<?php echo 'INJBFR00'.$randomString ;?>" readonly>
                            </div>


                            <div class="form-group">
                                <label for="exampleInputEmail1">Email ID : </label>
                                <input type="email" class="form-control" placeholder="Enter Email ID" name="email" id="email" required onkeyup="validate(this.value);">
                                <span  id="error_msg"></span><br />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Job Fair Venue : </label>
                                <select class="form-control" name="venue" id="venue" required>
                                    <option value="" selected disabled>Select Venue</option>
                                    <option <?php if($this->uri->segment('1')=='thrissur'){echo "selected='selected'";} ?> value="1">Thrissur</option>
                                    <option <?php if($this->uri->segment('1')=='kochi'){echo "selected='selected'";} ?> value="2" disabled>Kochi</option>
                                    <option <?php if($this->uri->segment('1')=='northbanglore'){echo "selected='selected'";} ?> value="3" disabled>North Banglore</option>
                                    <option <?php if($this->uri->segment('1')=='hyderbad'){echo "selected='selected'";} ?> value="4" disabled>Hyderabad</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Highest course studied/completed : </label>
                                <select class="form-control" name="qualification" required onchange="getStream(this.value)" id="qualification">
                                    <option value="" selected disabled>Select Higher Education</option>
                                    <?php foreach($qualification as $q)
                  {?>
                                    <option value="<?php echo $q->qualification_id;?>"><?php echo $q->qualification;?></option>

                                    <?php } ?>
                                    <!--<option value="1">Others</option>-->
                                </select>
                                </div>
                                <div class="form-group">
                                <label for="exampleInputEmail1">Stream : </label>
                                <select class="form-control" name="stream" id="stream">
                                    <option value="" selected disabled>Select Stream</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Institution Name : </label>
                                <input type="text" class="form-control" placeholder="Enter Institution Name" name="institution" id="institution" required style="text-transform: capitalize;">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Education Type : </label>
                                <select class="form-control" name="type" id="type" required>
                                    <option value="" selected disabled>Select Education Type</option>
                                    <option value="Full-time">Full-time</option>
                                    <option value="Part-time">Part-time</option>
                                    <option value="Distance">Distance</option>
                                    <option value="Correspondence">Correspondence</option>
                                </select>
                            </div>
                            <div class="form-group" id="course_status" style="display:none;">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="passed" id="vk" value="1" onclick="test(this.value)">
                                    <label class="form-check-label" for="one">Passed</label>
                                </div>

                                <div class="form-check" id="failed_div">
                                    <input class="form-check-input" type="radio" name="passed" id="vk" value="2" onclick="test(this.value)">
                                    <label class="form-check-label" for="two">Not Completed/ Failed</label>
                                </div>

                                <div class="form-check" style="margin-bottom: 5px;">
                                    <input class="form-check-input" type="radio" name="passed" id="vk" value="3" onclick="test(this.value)">
                                    <label class="form-check-label" for="three">On Going</label>
                                </div>

                                <div class="box one">
                                    <div class="row" id="completed" style="display:none;">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleInputEmail1">Marks in %: </label>
                                                <input type="text" class="form-control" name="mark" id="marks" pattern="[+-]?([0-9]*[.])?[0-9]+" placeholder="Enter Marks %">
                                                <!--<input type="text" class="form-control" name="mark" id="marks" pattern="[+-]?([0-9]*[.])?[0-9]+" placeholder="Enter Marks %">-->
                                                
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="inputState">Passout Year:</label>
                                                <select class="form-control" name="passoutyear" id="passoutyear">
                                                    <option selected="" disabled>Choose Year</option>
                                                    <?php foreach($year as $y)
                                                    {?>
                                                    <option value="<?php echo $y['year'];?>"><?php echo $y['year'];?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="box two">
                                    <div class="row" id="failed" style="display:none;">
                                        <div class="col-md-6" >
                                            <div class="form-group">
                                                <label for="inputState">No of back paper</label>
                                                <select id="backpapers1" class="form-control" name="backpapers">
                                                    <option selected="" disabled>Choose No</option>
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">More than 6</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6" >
                                            <div class="form-group">
                                                <label for="inputState">Passout Year</label>
                                                <select id="passoutyear1" class="form-control" name="passoutyear" >
                                                    <option selected="" disabled>Choose Year</option>
                                                    <?php foreach($year as $y)
                                                    {?>
                                                    <option value="<?php echo $y['year'];?>"><?php echo $y['year'];?></option>
                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>
                                        <!--<div class="col-md-12" id="reason">-->
                                        <!--    <div class="form-group">-->
                                        <!--        <label for="inputState">Enter Your</label>-->
                                        <!--        <textarea class="form-control" rows="3" name="description" id="description" ></textarea>-->
                                        <!--    </div>-->
                                        <!--</div>-->
                                    </div>
                                </div>
                                <div class="box three">
                                    <div class="row" id="ongoing" style="display:none;">
                                        <div class="col-md-6" >
                                            <div class="form-group">
                                                <label for="inputState">Passout Year</label>
                                                <select id="passoutyear2" class="form-control" name="passoutyear" >
                                                    <option selected disabled>Choose Year</option>
                                                    <option value="2020">2020</option>
                                                    <option value="2021">2021</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="inputState">No of back paper</label>
                                                <select id="backpapers" class="form-control" name="backpapers" >
                                                    <option selected="" disabled>Choose No</option>
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">More than 6</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="row" id="basic_completed" style="display:none;">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Marks in %: </label>
                                            <input type="text" class="form-control" name="marks" id="marks" pattern="[+-]?([0-9]*[.])?[0-9]+" placeholder="Enter Marks %">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="inputState">Passout Year:</label>
                                            <select class="form-control" name="passoutyear" id="passoutyear3">
                                                <option selected="" disabled>Choose Year</option>
                                                <?php foreach($year as $y)
                                                {?>
                                                <option value="<?php echo $y['year'];?>"><?php echo $y['year'];?></option>
                                                <?php }?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <div class="form-group" id="pass">
                                <label for="exampleInputEmail1">Password : </label>
                                <input type="password" class="form-control" placeholder="Enter Password" name="password" id="password" pattern=".{6,}" onkeyup="checkPass(); return false;" required>
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="myFunction()"></span>
                            </div>

                            <div class="form-group" id="conpass">
                                <label for="exampleInputEmail1">Confirm Password : </label>
                                <input type="password" class="form-control" placeholder="Enter Password" name="confirmpassword" id="password1" pattern=".{6,}" onkeyup="checkPass(); return false;" required>
                                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="myFunction1()"></span>
                            </div>
                            <!-- <div class="g-recaptcha" data-sitekey="6LcTUtEUAAAAACbFZLdIJATeQ-takzcx03amZFqO"></div> -->
                            <div id="error-nwl"></div>
                            <div class="form-group" id="propic" style="display:none;">
                                <label>Upload your Image</label>
                                <!--<span class="btns  btn-file" style="margin-left: 20px;">Browse file-->
                                    <input type="file" name="profile_img" id="profile_image" ><h6>[Maximum Size:3MB Dimension:150W X 200H]</h6>
                                <!--</span>-->
                            </div>
                        </div>
                    </div>
                    <div class="s-btns">
                        <button type="submit" class="sb-btn">Submit</button>
                        <button type="reset" class="sb-btn sb-btn2">Reset</button>
                    </div>
                </form>
                <div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url('login'); ?>">Click here to Log in <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
            </div>
        </div>
        <?php include('include/main-sponsor-slider2.php');?>
        <?php include('include/co-sponsors.php');?>
        <?php include('include/local-sponser.php');?>
    <!--    <div class="fter asft">-->
    <!--        <div class="row">-->
    <!--            <div class="col-xs-4 footer-menu">-->
    <!--                <ul class="footer-menu">-->
    <!--                    <li><a href="#">Home</a></li>-->
    <!--                    <li><a href="#">About Us</a></li>-->
    <!--                    <li><a href="#">Contact Us</a></li>-->
    <!--                </ul>-->
    <!--            </div>-->
    <!--            <div class="col-xs-4 footer-address">-->
    <!--                <p>S I G N (Society for Integrated Growth of the Nation)-->
    <!--                    Nakshatra Garden, Cheranallor P O, Cochin, Kerala - 683544</p>-->
    <!--            </div>-->
    <!--            <div class="col-xs-4 footer-address">-->
    <!--                <p>BigLeap Solutions Pvt Ltd.<br>-->
    <!--                    4th Floor, Markaz Complex, Calicut, Kerala - 673004</p>-->
    <!--            </div>-->
    <!--            <div class="col-xs-12 copyright">-->
    <!--                <p class="copy-txt">Copyright © 2019 All Rights Reserved.</p>-->
    <!--                <ul class="social-media">-->
    <!--                    <li><a href="https://www.facebook.com/indiajobfairs/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>-->
    <!--                    <li><a href="https://www.instagram.com/india_mega_job_fairs/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
    <!--                </ul>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
    <?php include('include/microsite.php');?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src='https://www.google.com/recaptcha/api.js'></script> -->
    <script>
        $(document).ready(function() {
            $("#mobile").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                return true;
            });
            $("#whatsapp").keypress(function(e) {
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    return false;
                }
                return true;
            });
        });

        function profile(val) {
            var mobile = val;
            var name = document.getElementById("name").value;
            var dob = document.getElementById("dob").value;
            if (mobile != "" && name != "" && dob != "") {
                $.post("<?php echo base_url(); ?>jobfair_controller/profile", {
                    mobile: val,
                    name: name,
                    dob: dob
                }, function(result) {
                    var jsonObjArray = $.parseJSON(result);
                    $("#gender").val(jsonObjArray[0].cand_gender);
                    $("#email").val(jsonObjArray[0].cand_email);
                    $("#whatsapp").val(jsonObjArray[0].cand_whatsapp);
                    $("#venue").val(jsonObjArray[0].cand_venue);
                    /*$("#marks").val(jsonObjArray[0].cand_marks);
                    $("#passoutyear").val(jsonObjArray[0].cand_passoutyear);
                    $("#passoutyear1").val(jsonObjArray[0].cand_passoutyear);
                    $("#passoutyear2").val(jsonObjArray[0].cand_passoutyear);
                    $("#backpapers").val(jsonObjArray[0].cand_backpapers);
                    $("#backpapers1").val(jsonObjArray[0].cand_backpapers);*/
                    $("#description").val(jsonObjArray[0].cand_description);
                    $("#passed").val(jsonObjArray[0].cand_passed);
                    $("#type").val(jsonObjArray[0].cand_education_type);
                });
            }
        }
        function getStream(val) {
            $.post("<?php echo base_url(); ?>jobfair_controller/getWeightage", {
                id: val
            }, function(result) {
                var jsonObjArray = $.parseJSON(result);
                var weightage = jsonObjArray[0].weightage;
                if (weightage == 0) {
                    $("#propic").show();
                }else {
                    $("#propic").hide();
                    document.getElementById("profile_image").required = false;
                }
                if(weightage != 0) 
                {
                    if(val == 25)
                    {
                        $("#course_status").show();
                        $("#failed_div").hide();  
                    }
                    else
                    {
                         $("#course_status").show();
                        $("#failed_div").show(); 
                    }
                }
                else
                {
                    $("#course_status").hide();
                    if(val == 26 || val == 39 || val == 25) 
                    {
                        $("#basic_completed").show();
                    }
                    else
                    {
                        $("#basic_completed").hide();
                    }
                }
            });

            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>jobfair_controller/getStream",
                data: 'data=' + val,
                success: function(data) {
                    $("#stream").html(data);
                }
            });
        }

        function myFunction() {
            var x = document.getElementById("password");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        function myFunction1() {
            var x = document.getElementById("password1");
            if (x.type === "password") {
                x.type = "text";
            } else {
                x.type = "password";
            }
        }

        function checkPass() {
            var pass1 = document.getElementById('password');
            var pass2 = document.getElementById('password1');
            var message = document.getElementById('error-nwl');
            var goodColor = "#66cc66";
            var badColor = "#ff6666";

            if (pass1.value.length > 5) {
                pass1.style.backgroundColor = goodColor;
                message.style.color = goodColor;
                message.innerHTML = "character number ok!"
            } else {
                pass1.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = " you have to enter at least 6 digit!"
                return false;
            }

            if (pass1.value == pass2.value) {
                pass2.style.backgroundColor = goodColor;
                message.style.color = goodColor;
                message.innerHTML = "ok!"
            } else {
                pass2.style.backgroundColor = badColor;
                message.style.color = badColor;
                message.innerHTML = " These passwords don't match"
                return false;
            }
        }
        function test(val)
        {
            if(val==1)
            {
                document.getElementById("completed").style.display = "block";
                document.getElementById("failed").style.display = 'none';
                document.getElementById("ongoing").style.display = 'none';
                // document.getElementById("reason").style.display = 'none';
                
                document.getElementById("marks").required=true;
                document.getElementById("passoutyear").required=true;
                document.getElementById("backpapers1").required=false;
                document.getElementById("backpapers").required=false;
                document.getElementById("passoutyear1").required=false;
                document.getElementById("passoutyear2").required=false;
            }
            else if(val==2)
            {
                document.getElementById("completed").style.display = "none";
                document.getElementById("failed").style.display = 'block';
                // document.getElementById("reason").style.display = 'block';
                document.getElementById("ongoing").style.display = 'none';
                
                document.getElementById("marks").required=false;
                document.getElementById("passoutyear1").required=true;
                document.getElementById("backpapers1").required=true;
               /* document.getElementById("backpapers1").name='backpapers';
                document.getElementById("backpapers").name='backpapers1';*/
                document.getElementById("backpapers").required=false;
                document.getElementById("passoutyear").required=false;
                document.getElementById("passoutyear2").required=false;
            }
            else
            {
                document.getElementById("completed").style.display = "none";
                document.getElementById("failed").style.display = 'none';
                // document.getElementById("reason").style.display = 'none';
                document.getElementById("ongoing").style.display = 'block';
                
                document.getElementById("marks").required=false;
                document.getElementById("passoutyear2").required=true;
                document.getElementById("backpapers").required=true;
               /* document.getElementById("backpapers").name='backpapers';
                document.getElementById("backpapers1").name='backpapers1';*/
                document.getElementById("backpapers1").required=false;
                document.getElementById("passoutyear1").required=false;
                document.getElementById("passoutyear").required=false;
            }
            
        }
    </script>
    <script>
        $('#d-picker').dcalendarpicker();
    </script>

    <script>
        var datepicker = new ej.calendars.DatePicker({
            width: "100%"
        });
        datepicker.appendTo('#dob');
    </script>
    
    <script language="JavaScript">
            var ev = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
            var x = document.getElementById("error_msg");
            function validate(email){
            if(!ev.test(email))
                {
                    x.innerHTML	= "Not a valid email";
                    x.style.color = "red"
                }
            else
                {
                    x.innerHTML	= "Ok!";
                    x.style.color = "green"
                }
            }
        </script>
        
        
<script type='text/javascript'>

$(document).ready(function() {
    $('#mobile').keyup(function(e) {
        if (validatePhone('mobile')) {
            $('#errmsg0').html('Valid Mobile Number');
            $('#errmsg0').css('color', 'green');
        }
        else {
            $('#errmsg0').html('Invalid Mobile Number');
            $('#errmsg0').css('color', 'red');
        }
    });
});

function validatePhone(mobile) {
    var a = document.getElementById(mobile).value;
    var filter = /[1-9]{1}[0-9]{9}/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}

$(document).ready(function() {
    $('#whatsapp').keyup(function(e) {
        if (validatePhone('whatsapp')) {
            $('#errmsg1').html('Valid whatsapp Number');
            $('#errmsg1').css('color', 'green');
        }
        else {
            $('#errmsg1').html('Invalid whatsapp Number');
            $('#errmsg1').css('color', 'red');
        }
    });
});

function validatePhone(whatsapp) {
    var a = document.getElementById(whatsapp).value;
    var filter = /[1-9]{1}[0-9]{9}/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}
 

</script>