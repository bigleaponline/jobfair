<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<?php include('include/modi.php');?>
<marquee class="marque-one" direction=”right” onmouseover="stop()" onmouseout="start()">★ Mega Job fair at holy grace engineering campus mala,thrissur on 02-feb-2020 ★</marquee>

<div class="container-fluid mt right-content">
  <div class="col-md-9 col-sm-8 site"> 
    <div class="container-fluid">
        <?php include('include/main-sponsor-slider2.php');?>
  <div class="asmnt">
    <h1>FAQ (Frequently Asked Questions)</h1>
  </div>
  <div class="faq-clm">
      
      
 <div class="accordion">
        <!-- span to target fix closing accordion -->
        <span class="target-fix" id="accordion"></span>
         
        <!-- First Accoridon Option -->
        <div>
            <!-- span to target fix accordion -->
            <span class="target-fix" id="accordion1"></span>
            
            <!-- Link to open accordion, hidden when open --> 
            <a href="#accordion1" id="open-accordion1" title="open">1. Is Mega Job fair- Thrissur is like every other career fair?</a>
            
            <!-- Link to close accordion, hidden when closed -->
            <a href="#accordion" id="close-accordion1" title="close">1. Is Mega Job fair- Thrissur is like every other career fair?</a> 
            
            <!-- Accorion content goes in this div -->
            <div class="accordion-content">
                <p><div class="ans-clm">
      <p><b>Ans:</b> No,Mega Job fair- Thrissur is very different from the typical Job Fairs organised in Kerala.
        Generally, Candidates who attend Job Fairs go directly to the event of Job Fair with resumes,
        photos and certificates and choose from the list of companies to attend the recruitment process.
        But Mega Job fair Thrissur is distinct in many ways as explained below </p>
      <li>All the participating candidates for the Mega Job Fair have to register their details online by
        visiting the Mega Job Fair Website (name)</li>
      <li> All the participating candidates for the Mega Job Fair have to attend Online Assessments </li>
      <!--<li> All the participating candidates for the Mega Job Fair have to attend Online Assessments-->
      <!--  either under Online General Assessment Test OR Online Technical Assessment based on-->
      <!--  their eligibility criteria </li>-->
      <li> All the candidates participating for Online General Assessments can attempt the Online
        <!--Exam between dates----anywhere as per their convenience. But all the candidates-->
        <!--participating for Online Technical Assessment have to attend the test from any of the 3-->
        <!--chosen centres. The date and available slot for attending the test among any of the chosen-->
        <!--centres will be intimated to the candidate at a later date. </li>-->
      <li> Mega Job Fair Thrissur is distinct as its Corporate sponsored event. </li>
      <li> Mega Job Fair Thrissur is distinct because all the Job Aspirants of 12 th pass, Graduates of
        Technical and Non Technical criteria- B.tech, M.tech, BBA, MBA, B.Pharm and M.Pharm BSc
        and MSc BA and MA as well dual professionals like B.Tech and MBA or B.Pharm and MBA
        have respective opportunities with the Recruiting companies visiting the Job Fair </li>
    </div></p>
            </div>
        </div>

        <!-- Second Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion2"></span>
            <a href="#accordion2" id="open-accordion2" title="open">2. What is the number of questions from the syllabus asked in Online General Assessment? </a>
            <a href="#accordion" id="close-accordion2" title="close">2. What is the number of questions from the syllabus asked in Online General Assessment? </a>
            <div class="accordion-content">
                <p> <div class="ans-clm">
      <p><b>Ans:</b> The whole syllabus of Online General Assessment covers Test of English Language including
        Synonyms, Antonyms, Fill in the blanks with appropriate words-Verbs, Prepositions, Spot the
        errors- Tenses, Singular and Plural, Question tags, Jumbled and choose appropriate sentences-
        Active and Passive Voice. The total numbers of questions are 20. All questions are Multiple
        Choice Questions with 4 options. The Test is active for 20 Mins. </p>
    </div></p>       
            </div>
        </div>

        <!-- Third Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion3"></span>
            <a href="#accordion3" id="open-accordion3" title="open">3. What is the next level of process after Online General Assessment? </a>
            <a href="#accordion" id="close-accordion3" title="close">3. What is the next level of process after Online General Assessment? </a>
            <div class="accordion-content">
                <p>
    <div class="ans-clm">
      <p><b>Ans:</b> The scores of Online General Assessment is shared with the eligible companies and the
        candidates are segregated and Hall tickets are generated specifying the types of companies they
        participate. </p>
    </div></p>
            </div>
        </div>
        
        <!-- Fourth Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion4"></span>
            <a href="#accordion4" id="open-accordion4" title="open">4. Who are eligible for the Online General Assessment </a>
            <a href="#accordion" id="close-accordion4" title="close">4. Who are eligible for the Online General Assessment </a>
            <div class="accordion-content">
                <p> <div class="ans-clm">
      <p><b>Ans: Eligibility Criteria of the Candidates</b></p>
      <h6>Category A:</h6>
      <li>Plus Two Passed </li>
      <h6>Category B:</h6>
      <li>ITI/Diploma </li>
      <li>BSc (CS, IT, Maths, Electronics, Statistics) </li>
      <li>BCA -with 50% &amp; above marks </li>
      <li>BCom / MCom / BBA(Finance)- 55% &amp; above marks </li>
      <li>BTech - Electrical/Electronics/Mechanical/Civil [ For Core Jobs] -Below
        60%* marks </li>
      <li>BTech /MCA/MSc( IT,CS, Electronics, Electrical, Mechanical ( optional) - [ For IT Jobs]- Below 60% marks </li>
      <li>Other Graduates </li>
    </div></p>
            </div>
        </div>
         <!-- Five Accoridon Option -->
    <!--    <div>-->
    <!--        <span class="target-fix" id="accordion5"></span>-->
    <!--        <a href="#accordion5" id="open-accordion5" title="open">5. Is B.Tech below 60% not eligible for Online Technical Assessment? </a>-->
    <!--        <a href="#accordion" id="close-accordion5" title="close">5. Is B.Tech below 60% not eligible for Online Technical Assessment? </a>-->
    <!--        <div class="accordion-content">-->
    <!--            <p>-->
    <!--<div class="ans-clm">-->
    <!--  <p><b>Ans:</b> Some companies consider 55% marks as eligibility for technical jobs, in that case-->
    <!--    the candidates are considered under eligibility category C . In this regard the candidates-->
    <!--    will be intimated by SMS/Mail/Whatsapp to opt for nearby centres to attend Preliminary-->
    <!--    exam under Category C of Technical Assessment. </p>-->
    <!--</div></p>-->
    <!--        </div>-->
    <!--    </div>-->
         <!-- Six Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion6"></span>
            <a href="#accordion6" id="open-accordion6" title="open">5. Is there rejection in the Online General Assessment?</a>
            <a href="#accordion" id="close-accordion6" title="close">5. Is there rejection in the Online General Assessment?</a>
            <div class="accordion-content">
                <p>   <div class="ans-clm">
      <p><b>Ans:</b> General Assessment has NO REJECTION of candidate but to identify candidates
        competencies to suit for different job roles and to segregate them to the various
        screening processes of the companies visiting at the Job Fair </p>
    </div></p>
            </div>
        </div>
         <!-- Seven Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion7"></span>
            <a href="#accordion7" id="open-accordion7" title="open">6. Is Online General Assessment Compulsory? </a>
            <a href="#accordion" id="close-accordion7" title="close">6. Is Online General Assessment Compulsory? </a>
            <div class="accordion-content">
                <p>  <div class="ans-clm">
      <p><b>Ans:</b> YES! Online General Assessment is compulsory. Its the passport for attending the Job Fair.
        Registration followed with Online General Assessment is the initial step for participating in the
        Job Fair </p>
    </div></p>
            </div>
        </div>
         <!-- Eight Accoridon Option -->
    <!--    <div>-->
    <!--        <span class="target-fix" id="accordion8"></span>-->
    <!--        <a href="#accordion8" id="open-accordion8" title="open">8. What is the number of questions from the syllabus asked in Online Technical Assessment? </a>-->
    <!--        <a href="#accordion" id="close-accordion8" title="close">8. What is the number of questions from the syllabus asked in Online Technical Assessment? </a>-->
    <!--        <div class="accordion-content">-->
    <!--            <p> <div class="ans-clm">-->
    <!--  <p><b>Ans:</b> Technical Assessment consists of 60 Multiple Choice Questions from the given syllabus-->
    <!--    below. The duration of the test is 60 Mins. </p>-->
    <!--  <h6>Quantitative Aptitude:</h6>-->
    <!--  <li>Percentages </li>-->
    <!--  <li>Profit and Loss </li>-->
    <!--  <li>Simple and Compound interest </li>-->
    <!--  <li>Numbers </li>-->
    <!--  <li>Time and work </li>-->
    <!--  <li>Time and distance </li>-->
    <!--  <li>Boats and Streams, Trains </li>-->
    <!--  <li>Permutations and Combinations </li>-->
    <!--  <li>Probability </li>-->
    <!--  <li>Logarithms </li>-->
    <!--  <h6>Verbal Aptitude:</h6>-->
    <!--  <li>Articles </li>-->
    <!--  <li>Prepositions </li>-->
    <!--  <li>Conjunctions </li>-->
    <!--  <li>Tenses </li>-->
    <!--  <li>Subject Verb Agreement </li>-->
    <!--  <li>Pronoun Antecedent agreement </li>-->
    <!--  <li>Modifiers </li>-->
    <!--  <li> Comparisons</li>-->
    <!--  <li>Sentence Completion </li>-->
    <!--  <li>Reading Comprehension </li>-->
    <!--  <h6>Reasoning:</h6>-->
    <!--  <li>Coding and Decoding </li>-->
    <!--  <li>Classification </li>-->
    <!--  <li>Seating problems </li>-->
    <!--  <li>Blood relations </li>-->
    <!--  <li>Directions </li>-->
    <!--  <li>Calendars and Clock problems </li>-->
    <!--  <li>Statements and Conclusions </li>-->
    <!--  <li>Statements and Assumptions </li>-->
    <!--  <li>Decision Making </li>-->
    <!--  <li> Number series and word series </li>-->
    <!--  <li>Analogies </li>-->
    <!--  <li> Inferences</li>-->
    <!--</div></p>-->
    <!--        </div>-->
    <!--    </div>-->
         <!-- Nine Accoridon Option -->
    <!--    <div>-->
    <!--        <span class="target-fix" id="accordion9"></span>-->
    <!--        <a href="#accordion9" id="open-accordion9" title="open">7. What are the type of jobs and Companies that come under Online Technical Assessment? </a>-->
    <!--        <a href="#accordion" id="close-accordion9" title="close">7. What are the type of jobs and Companies that come under Online Technical Assessment? </a>-->
    <!--        <div class="accordion-content">-->
    <!--            <p>  <div class="ans-clm">-->
    <!--  <p><b>Ans:</b> All Software, IT, Core, MBA-HR, Mktg, Finance jobs come under Online Technical-->
    <!--    Assessments. The Companies are divided under three categories </p>-->
    <!--  <h6><b>1) C1- Type of companies</b></h6>-->
    <!--  <p> Are Top companies hiring directly from the data pool of Technical online assessment-->
    <!--    managed by number one neutral assessment company of India which is organised-->
    <!--    by event officials. Candidates clearing each level will be directly intimated through-->
    <!--    proper channels the schedule of the next rounds. Final rounds will be on the day of-->
    <!--    the Job Fair. Each candidate is free to attend a maximum 3 companies he/she is-->
    <!--    eligible for on the day of the event.</p>-->
    <!--  <h6><b>2) C2- Type of companies</b></h6>-->
    <!--  <p> Are Top companies who does the initial Online Assessments on their own. So, they-->
    <!--    are called Online Company Specific Tests. Prior to the Job Fair at two Engineering-->
    <!--    colleges one in North Kerala and another in South Kerala the candidates are called to-->
    <!--    attend Online Company Specific Test to be conducted by different companies. C2-->
    <!--    Companies short list the candidates based on Candidates Academic Percentage for-->
    <!--    the Online Company Specific Test and the shortlists will be called for the final round-->
    <!--    of Interviews at the Job Fair venue. All candidates who attend the Preliminary Online-->
    <!--    Assessments are eligible to be shortlisted by the C2 Companies irrespective of their-->
    <!--    score in the Preliminary Online Assessment</p>-->
    <!--  <h6><b>3) C3- Type of Companies</b></h6>-->
    <!--  <p> Are renewed companies who do all rounds of Interview at the Job Fair. No initial-->
    <!--    screening is done by these companies. All candidates who attend the Preliminary-->
    <!--    Online Assessment can participate in the screening process of the above companies-->
    <!--    at Job Fair irrespective of their score in the Preliminary Online Assessment.</p>-->
    <!--</div></p>-->
    <!--        </div>-->
    <!--    </div>-->
         <!-- Ten Accoridon Option -->
    <!--    <div>-->
    <!--        <span class="target-fix" id="accordion10"></span>-->
    <!--        <a href="#accordion10" id="open-accordion10" title="open">10. What is the process of Online Technical Assessment? Who are C1 C2 and C3 Candidates? What are the opportunities they have? </a>-->
    <!--        <a href="#accordion" id="close-accordion10" title="close">10. What is the process of Online Technical Assessment? Who are C1 C2 and C3 Candidates? What are the opportunities they have? </a>-->
    <!--        <div class="accordion-content">-->
    <!--            <p> <div class="ans-clm">-->
    <!--  <p><b>Ans:</b> Online Technical Assessment has two levels Preliminary Technical Assessment and-->
    <!--    Main Technical Assessment All eligible candidates will be assessed through Preliminary-->
    <!--    Tehnical Assessments and the average percentage of toppers is shortlisted for Main-->
    <!--    Technical Assessments. Main Technical Assessments are conducted by one of the India’s-->
    <!--    leading Assessments Company and from the shortlists of Main Technical Exam the-->
    <!--    candidates will be called to the final rounds by the recruiters of the Assessments companies-->
    <!--    to the venue of Job Fair. The results of the Preliminary and the Main Technical Exam is-->
    <!--    communicated to the candidates through the mail, WhatsApp and by posting the result in-->
    <!--    the Job Fair site. The candidates who are selected by this process are called as </p>-->
    <!--  <h6><b>1)C1(Category-1) Candidates</b></h6>-->
    <!--  <p> Apart from the above process All candidates who appear for Online Technical Assessment-->
    <!--    irrespective of their score will be called for the Company Specific Tests based on their-->
    <!--    Academic Percentage by Top Companies to any of the Two Engineering Colleges in the North-->
    <!--    and South Kerala. The Short lists will be called for Final Rounds of the above Top companies-->
    <!--    at the Job Fair. The results of the Company Specific Tests is communicated to the candidates-->
    <!--    through the mail, WhatsApp and by posting the result in the Job Fair site. The candidates-->
    <!--    who are shortlisted by this process are called as</p>-->
    <!--  <h6><b>C2(Category-2) Candidates</b></h6>-->
    <!--  <p> Finally, All candidates who appear for Online Technical Assessment irrespective of their-->
    <!--    score will be called by the companies who do all rounds of Interviews at the Job Fair. No-->
    <!--    initial screening is done by these companies. All candidates who attend the Preliminary-->
    <!--    Online Assessment can participate in the screening process of the above. The candidates-->
    <!--    who attend by this process are called as</p>-->
    <!--  <h6><b>C3(Category-3) Candidates</b></h6>-->
    <!--</div></p>-->
    <!--        </div>-->
    <!--    </div>-->
         <!-- Eleven Accoridon Option -->
    <!--    <div>-->
    <!--        <span class="target-fix" id="accordion11"></span>-->
    <!--        <a href="#accordion11" id="open-accordion11" title="open">11. Is there Rejection in the Online Preliminary Technical Assessment? </a>-->
    <!--        <a href="#accordion" id="close-accordion11" title="close">11. Is there Rejection in the Online Preliminary Technical Assessment? </a>-->
    <!--        <div class="accordion-content">-->
    <!--            <p><div class="ans-clm">-->
    <!--  <p><b>Ans:</b> Online Preliminary Technical Assessment has NO REJECTION of candidates but to-->
    <!--    identify candidates’ competencies to suit for different companies screening process as-->
    <!--    per the Main Assessments and Company Specific Assesments and for the direct-->
    <!--    screening process done by the Companies at the Job Fair. Thus Category 1 candidates-->
    <!--    can attend Category-2 and Category-3 Screening Process, Category-2 candidates can-->
    <!--    attend Category-3 Screening Process and the Candidates who are not shortlisted in-->
    <!--    Category 1 or 2 can be part of Category-3 to attend direct screening process at the Job-->
    <!--    Fair. So all candidates have opportunities distributed to participate in the Job Fair. </p>-->
    <!--</div></p>-->
    <!--        </div>-->
    <!--    </div>-->
         <!-- Twelve Accoridon Option -->
    <!--    <div>-->
    <!--        <span class="target-fix" id="accordion12"></span>-->
    <!--        <a href="#accordion12" id="open-accordion12" title="open"> 12. Is Online Preliminary Technical Assessment Compulsory?</a>-->
    <!--        <a href="#accordion" id="close-accordion12" title="close"> 12. Is Online Preliminary Technical Assessment Compulsory?</a>-->
    <!--        <div class="accordion-content">-->
    <!--            <p><div class="ans-clm">-->
    <!--  <p><b>Ans:</b> YES! Online Preliminary Technical Assessment is compulsory. Its the passport for attending-->
    <!--    the Job Fair. Registration followed with Online Preliminary Technical Assessment is the initial-->
    <!--    step for participating in the Job Fair </p>-->
    <!--</div></p>-->
    <!--        </div>-->
    <!--    </div>-->
          <!-- Thirteen Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion13"></span>
            <a href="#accordion13" id="open-accordion13" title="open">7. Is Spot Registration available?</a>
            <a href="#accordion" id="close-accordion13" title="close">7. Is Spot Registration available?</a>
            <div class="accordion-content">
                <p>   <div class="ans-clm">
      <p><b>Ans:</b> No,there will be no spot registration. <b>Every candidate should register online through the website.</b> </p>
    </div></p>
            </div>
        </div>
          <!-- Fouteen Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion14"></span>
            <a href="#accordion14" id="open-accordion14" title="open">8. What are the documents a Candidate should carry?</a>
            <a href="#accordion" id="close-accordion14" title="close">8. What are the documents a Candidate should carry?</a>
            <div class="accordion-content">
                <p> <div class="ans-clm">
      <p><b>Ans:</b> Candidates should bring the following documents along with them to the Job Fair </p>
      <li style="list-style:none">1. Carry at least  15 copies of resume </li>
      <li style="list-style:none">2. Carry plain A4 sheets of papers to jot down things, </li>
      <li style="list-style:none">3.  Have a minimum of 5 passport size color photos of yourself, </li>
      <li style="list-style:none">4. Carry a pen </li>
      <li style="list-style:none">5. Notepad- to use in case of a GD or presentation session. </li>
    </div></p>
            </div>
        </div>
          <!-- Fifteen Accoridon Option -->
    <!--    <div>-->
    <!--        <span class="target-fix" id="accordion15"></span>-->
    <!--        <a href="#accordion15" id="open-accordion15" title="open">10. What is the minimum and maximum age limit for attending the Job Fair?</a>-->
    <!--        <a href="#accordion" id="close-accordion15" title="close">10. What is the minimum and maximum age limit for attending the Job Fair?</a>-->
    <!--        <div class="accordion-content">-->
    <!--            <p><div class="ans-clm">-->
    <!--  <p><b>Ans:</b> Minimum is13yrs </p>-->
    <!--</div></p>-->
    <!--        </div>-->
    <!--    </div>-->
          <!-- Sixteen Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion16"></span>
            <a href="#accordion16" id="open-accordion16" title="open">9. How many companies interview process can a Candidate attend?</a>
            <a href="#accordion" id="close-accordion16" title="close">9. How many companies interview process can a Candidate attend?</a>
            <div class="accordion-content">
                <p> <div class="ans-clm">
      <p><b>Ans:</b> No limit, but maximum companies processes can be attended by the candidate as per
        eligibility criteria and Online Assessments </p>
    </div></p>
            </div>
        </div>
          <!-- Seventeen Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion17"></span>
            <a href="#accordion17" id="open-accordion17" title="open">10. Will I get the offer letter on the same date of Job Fair? </a>
            <a href="#accordion" id="close-accordion17" title="close">10. Will I get the offer letter on the same date of Job Fair? </a>
            <div class="accordion-content">
                <p> <div class="ans-clm">
      <p><b>Ans:</b> It depends up on the Companies recruitment policies. </p>
    </div></p>
            </div>
        </div>
          <!-- Eighteen Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion18"></span>
            <a href="#accordion18" id="open-accordion18" title="open">11. Do I have any further rounds at the company after shortlisted at the Job Fair</a>
            <a href="#accordion" id="close-accordion18" title="close">11. Do I have any further rounds at the company after shortlisted at the Job Fair</a>
            <div class="accordion-content">
                <p><div class="ans-clm">
      <p><b>Ans:</b> Yes! You may have further round for some companies. Some companies call the
        shortlisted candidates to the company for the final round as per the company’s policy </p>
    </div></p>
            </div>
        </div>
          <!-- Nninteen Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion19"></span>
            <a href="#accordion19" id="open-accordion19" title="open">12. What are the Companies participating in the Job Fair? </a>
            <a href="#accordion" id="close-accordion19" title="close">12. What are the Companies participating in the Job Fair? </a>
            <div class="accordion-content">
                <p>    <div class="ans-clm">
      <p><b>Ans:</b> For more details<a href="<?php echo base_url();?>jobfair/company-list" style="color: #337ab7;font-size:14px;padding-left: 0.5%;">click here </a></p>
    </div></p>
            </div>
        </div>
          <!-- Twenty Accoridon Option -->
        <div>
            <span class="target-fix" id="accordion20"></span>
            <a href="#accordion20" id="open-accordion20" title="open">13. Does the Candidate have to pay any amount/fees regarding registration at any stage of
        the Job Fair?</a>
            <a href="#accordion" id="close-accordion20" title="close">13. Does the Candidate have to pay any amount/fees regarding registration at any stage of
        the Job Fair?</a>
            <div class="accordion-content">
                <p><div class="ans-clm">
      <p><b>Ans:  No amount/fees will be charged </b>at any stage of the Job Fair </p>
    </div></p>
            </div>
        </div>
    </div>
      
  </div>

            <div class="buttons">
    <div class="d-flex">
      <div class="path-tosignin path-tosignin2"> 
      <a href="http://www.indiamegajobfairs.com/" class="back-link"> <i class="fa fa-home animated flash infinite" aria-hidden="true"  title="Home"></i></a>
        <a href="<?php if($this->uri->segment('1')=='thrissur'){ echo base_url('thrissur/sign-in');}else {echo base_url('sign-in');}?>" >
          Candidate Registration <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i>
        </a>
         <a href="http://www.indiamegajobfairs.com/thrissur" class="back-linktwo"> <i class="fa fa-undo animated flash infinite" title="Back" aria-hidden="true"></i></a> 

      </div>
    </div>    
</div>    
</div>
 <?php include('include/main-sponsor-slider2.php');?>
 <?php include('include/co-sponsors.php');?>
  <?php include('include/local-sponser.php');?>
