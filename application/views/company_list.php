<?php include('include/modi.php');?>
<div class="container-fluid mt right-content">
<div class="col-md-9 col-sm-8 site">
<div id="height-full">
<div class="container-fluid">
    <?php
     if($this->session->flashdata('employer'))
        {
          echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('employer').'</div>';   
        }
        if($this->session->flashdata('jd'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('jd').'</div>';   
        }
        ?>
        <?php include('include/main-sponsor-slider.php');?>
        <marquee class="marque-one" direction=”right” onmouseover="stop()" onmouseout="start()">Upcoming Event: ★ Mega Job fair at Holy Grace Academy, mala,thrissur on 02-feb-2020 ★</marquee>

  <div class="titles" style="border-top:none;">
  <h2>Company Details</h2>
  </div>
  <div class="e-r-f c-name">
    <div class="table-responsive">
  <table class="table" width="100%">
    <thead>
      <tr>
      <th scope="row">Sl.No.</th>
        <th scope="col">Company Name</th>
        <!-- <th scope="col">REQUIREMENT</th> -->
        <th scope="col">Industry</th>
        <th scope="col">Position</th>
        <!-- <th scope="col">QUALIFICATION</th> -->
        <th scope="col">Job Description</th>
        <th scope="col">Salary</th>
        <!-- <th scope="col">LOCATION</th> -->
        <th scope="col">Apply</th>
       
      </tr>
    </thead>
    <tbody>
     <?php
        $i=0;
        foreach($company as $c)
        {
            $i=$i+1;
        ?>
      <tr>
        <th scope="row" style="width:5%;"><?php echo $i;?></th>
        <a href="<?php echo base_url();?>jd/<?php echo $c['emp_id'];?>"><td style="width:10%;"><?php echo $c['company_name'];?></td></a>
        <td style="width:10%;"><?php echo $c['industry'];?></td>
        <td style="width:10%;"><?php echo $c['job_title'];?></td>
        <td style="width:40%;"><?php echo $c['job_description'];?></td>
        <td style="width:10%;"><?php echo $c['salary'];?></td>
        <td style="width:10%;"><!-- <a href="<?php echo site_url("jobfair_controller/apply_job/" . $c['emp_id'] ."/". $c['job_title'] ); ?>" data-toggle="modal" data-target="#modalRegister">Apply Now</a> -->
          <a href="#modalRegister" class="my_link" data-toggle="modal" data-user_id="<?php echo $c['emp_id']; ?>" data-target="#modalRegister">Apply Now</a>
        </td>
       
      </tr>
      <?php }?>
    </tbody>
  </table>
</div>
  </div>
  
</div>
</div>
   <div class="buttons">

    <div class="d-flex">
      <div class="path-tosignin path-tosignin2">
          <a href="http://www.indiamegajobfairs.com/" class="back-link"> <i class="fa fa-home animated flash infinite" title="Back" aria-hidden="true"></i></a> 
      <a href="http://www.indiamegajobfairs.com/sign-in">
          Candidate Registration <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i>
      </a>
      <a href="http://www.indiamegajobfairs.com/" class="back-linktwo"> <i class="fa fa-undo animated flash infinite" title="Back" aria-hidden="true" action="action" onclick="window.history.go(-1); return false;" value="Cancel"></i></a> 
    

    </div>
    </div>    
</div>


<div class="modal fade" id="modalRegister" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <?php $attributes = array("name" => "contact_form", "id" => "contact_form");
            echo form_open("jobfair_controller/apply_to_job", $attributes);?>
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Job Application</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body mx-3">
        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="orangeForm-name">Name</label>
          <input type="text" id="name" class="form-control validate" name="name">
        </div>
        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="orangeForm-name">Email</label>
          <input type="text" id="email" class="form-control validate" name="email">
        </div>
        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="orangeForm-name">Phone Number</label>
          <input type="text" id="phone" class="form-control validate" name="phone">
        </div>
        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="orangeForm-email">Location</label>
          <input type="email" id="location" class="form-control validate" name="location">
        </div>
        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="orangeForm-email">Position to Apply</label>
          <input type="email" id="position" class="form-control validate" name="position" value="<?php //echo $c['emp_id']; ?>">
        </div>
        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="orangeForm-email">Qualification</label>
          <input type="email" id="qualification" class="form-control validate" name="qualification">
        </div>
        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="orangeForm-email">Experience</label>
          <input type="email" id="experience" class="form-control validate" name="experience">
        </div>
        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="orangeForm-email">Current Salary</label>
          <input type="email" id="c_salary" class="form-control validate" name="c_salary">
        </div>
        <div class="md-form mb-5">
          <label data-error="wrong" data-success="right" for="orangeForm-email">Expected Salary</label>
          <input type="email" id="e_salary" class="form-control validate" name="e_salary">
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <!-- <button class="btn btn-deep-orange">Sign up</button> -->
        <input class="btn btn-default" id="submit" name="submit" type="button" value="Apply" />
      </div>
      <?php echo form_close(); ?>   
    </div>
  </div>
</div>

<script type="text/javascript">

$(document).on("click", ".my_link", function (e) {

  e.preventDefault();

  var _self = $(this);

  var emp_id = _self.data('id');
  $("#position").val(emp_id);

  $(_self.attr('href')).modal('show');
});

$('#submit').click(function() {
    var form_data = {
        name: $('#name').val(),
        email: $('#email').val(),
        phone: $('#phone').val(),
        location: $('#location').val(),
        position: $('#position').val(),
        qualification: $('#qualification').val(),
        experience: $('#experience').val(),
        c_salary: $('#c_salary').val(),
        e_salary: $('#e_salary').val()
    };
    $.ajax({
        url: "<?php echo site_url('jobfair_controller/apply_to_job'); ?>",
        type: 'POST',
        data: form_data,
        success: function(msg) {
          return true;
        }
    });
    return false;
});
</script>
<script>
// For editting data in modal popup with Dynamic ID passing start-------
 
$(document).ready(function()
{    
 $('#modalRegister').on('show.bs.modal', function (e) 
 {        
 var emp_id = $(e.relatedTarget).data('emp_id');      
 $.ajax(
 {            
 type : 'POST',            
 url  : '<?php echo site_url('jobfair_controller/get_job_details'); ?>', //Here you will fetch records      
 data    : 'emp_id='+ emp_id, //Pass $user_id            
 success : function(result) 
 { 
 //alert('success'+result); 
 $('.fetched_user').html(result); //Show fetched data from database
 },
 error   : function(result) 
 { 
 alert('error'+result); 
 }
 });     
 });
});
 // For editting data in modal popup with Dynamic ID passing close----------->
</script>
<?php include('include/main-sponsor-slider2.php');?>
         <?php include('include/co-sponsors.php');?>
          <?php include('include/local-sponser.php');?>
<?php include('include/microsite.php');?>
