<?php include('include/modi.php');?>
<div class="container-fluid mt right-content">
  <div class="col-md-9 col-sm-8 site"> 
    <div class="container-fluid hei">
    <?php 
        if($this->session->flashdata('registration'))
        {
          echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('registration').$this->session->flashdata('message').'</div>';
          echo '<div class="alert alert-info" role="alert">'.$this->session->flashdata('message1').'</div>';
        }
         if($this->session->flashdata('loginerror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('loginerror').'</div>';   
        }
    ?>
    <?php include('include/main-sponsor-slider.php');?>
      <div class="titles til3" style="border-top:none;">
        <h2>Candidate Log In</h2>
      </div>
      <div class="e-r-f e-r-f2 sign-in" id="forgot">
        <form action="<?php echo base_url();?>jobfair_controller/login" method="post">
          <div class="row" >
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">UserName: </label>
                <input type="text" class="form-control" placeholder="Enter Name" name="username">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Password: </label>
                <input type="password" class="form-control" placeholder="Enter Password" name="password">
              </div>
            </div>
            <h6><a href="#" onclick="forgot()">Forget Password ?</a></h6>
          </div>
          <div class="s-btns">
            <button type="submit" class="sb-btn">Login</button>
            <button type="reset" class="sb-btn sb-btn2">Reset</button>
          </div>
        </form>
        <div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url('sign-in');?>">Click here to sign up <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
      </div>
      
      <div class="e-r-f e-r-f2 sign-in" id="login" style="display:none;">
        <form action="<?php echo base_url();?>jobfair_controller/forgotPassword" method="post">
          <div class="row" >
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Your Email ID: </label>
                <input type="text" class="form-control" placeholder="Enter Your Email ID" name="email">
              </div>
            </div>
            
            <h6><a href="#" onclick="login()">Go Back</a></h6>
          </div>
          <div class="s-btns">
            <button type="submit" class="sb-btn">Login</button>
            <button type="reset" class="sb-btn sb-btn2">Reset</button>
          </div>
        </form>
        <div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url('sign-in');?>">Click here to sign up<i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
      </div>
    </div>
    <?php include('include/main-sponsor-slider2.php');?>
 <?php include('include/co-sponsors.php');?>
  <?php include('include/local-sponser.php');?>
<!--     <div class="fter asft">-->
<!--<div class="row">-->
<!--<div class="col-xs-4 footer-menu">-->
<!--<ul class="footer-menu">-->
<!--<li><a href="#">Home</a></li>        -->
<!--<li><a href="#">About Us</a></li>        -->
<!--<li><a href="#">Contact Us</a></li>        -->
<!--</ul>        -->

<!--</div>-->

<!--<div class="col-xs-4 footer-address">-->
<!--<p>S I G N (Society for Integrated Growth of the Nation)-->
<!--Nakshatra Garden, Cheranallor P O, Cochin, Kerala - 683544</p>-->
<!--</div>-->

<!--<div class="col-xs-4 footer-address">-->
<!--<p>BigLeap Solutions Pvt Ltd.<br>-->
<!--4th Floor, Markaz Complex, Calicut, Kerala - 673004</p>-->
<!--</div>-->

<!--<div class="col-xs-12 copyright">-->
<!--<p class="copy-txt">Copyright © 2019 All Rights Reserved.</p> -->
<!--<ul class="social-media">-->
<!--<li><a href="https://www.facebook.com/indiajobfairs/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>-->
<!--<li><a href="https://www.instagram.com/india_mega_job_fairs/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--  </div>-->
<?php include('include/microsite.php');?>
<script>
    function forgot()
    {
        $("#login").show();
        $("#forgot").hide();
    }
    function login()
    {
        $("#login").hide();
        $("#forgot").show();
    }
</script>
<style>
    .alert-success {
    margin-top: 4%;
    color: #3c763d;
    background-color: #dff0d8;
    border-color: #d6e9c6;
}
</style>