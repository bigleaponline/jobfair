<style>
#errmsg0
{
color: red;
}
.disabledbutton {
    pointer-events: none;
    opacity: 0.4;
}
</style>
<?php include('include/modi.php');?>
<div class="container-fluid mt right-content">
<div class="col-md-9 col-sm-8 site">
<div id="height-full">
<!--1e-->
<div class="container-fluid">
    <?php 
        if($this->session->flashdata('email'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('email').'</div>';   
        }
        if($this->session->flashdata('sponsor'))
        {
          echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('sponsor').'</div>';   
        }
        if($this->session->flashdata('sponsorererror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('sponsorererror').'</div>';   
        }
    ?>
    <?php include('include/main-sponsor-slider.php');?>
  <div class="titles til3" style="border-top:none;">
    <h2>Would you like to be our event partner  ?</h2>
    
  </div>
  <div class="e-r-f">
      
      
      <div class="yes-btn">
          <div class="yes-btn1"> <a href="#" onclick="yes()">Yes</a></div>
      <div class="yes-btn2"> <a href="#" onclick="benefits()">Benefits of Partners</a></div>
    </div>
     <?php echo form_open_multipart('jobfair_controller/sponsor_registration/','id="my_formm"'); ?>
      
      <div class="disabledbutton" id="register">
          <p class="event-txt" style="font-size: x-large;">For your company to become an event partner</p>
          
          <div class="yes-btn">
          <div class="yes-btn2"> <a href="#" onclick="register()">Register Now</a></div>
    </div>
    </div>
    <div class="row my-f" id="yes" style="display:none;">
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Name of the Person : </label>
            <input style="text-transform: capitalize;" type="text" class="form-control" name="name" id="brand_name" placeholder="Enter Your Name">
          </div>
          
          <div class="form-group">
            <label for="exampleInputEmail1">Contact No: </label>
            <input type="text" class="form-control"  name="contact" id="contact_no" placeholder="Contact Number" autocomplete="off" onchange="profile(this.value)" maxlength="10">
            <span id="errmsg0"></span>
          </div>
          
         <div class="form-group">
            <label for="exampleInputEmail1">Email: </label>
            <input type="text" class="form-control"  name="email" id="contact" placeholder="Your Email" required onkeyup="validate(this.value);">
            <span  id="error_msg"></span>
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Name of the Organisation: </label>
            <input type="text" class="form-control" name="organisation" placeholder="Your Organisation Name">
          </div>
           <div class="form-group">
                <label for="exampleInputEmail1">Job Fair Venue : </label>
                <select class="form-control" name="venue"  id="venue" required>
                  <option value="" selected disabled>Select Venue</option>
                    <option <?php if($this->uri->segment('1')=='thrissur'){echo "selected='selected'";} ?> value="1">Thrissur</option>
                    <option <?php if($this->uri->segment('1')=='kochi'){echo "selected='selected'";} ?> value="2" disabled>Kochi</option>
                    <option value="3" disabled>Calicut & Kochi</option>
                    <option <?php if($this->uri->segment('1')=='northbanglore'){echo "selected='selected'";} ?> value="4" disabled>North Banglore</option>
                    <option <?php if($this->uri->segment('1')=='hyderbad'){echo "selected='selected'";} ?> value="5" disabled>Hyderabad</option>
                </select>
              </div>
         <div class="form-group">
            <label for="exampleInputEmail1">Designation: </label>
          <select class="form-control" name="designation" required placeholder="Designation">
  <option value="" selected disabled>Select Designation</option>
  <option value="Founder">Founder</option>
  <option value="Director">Director</option>
  <option value="Cxo">Cxo</option>
  <option value="Manager">Manager</option>
  <option value="Other">Other</option>
</select>
          </div>
        </div>
      <div class="col-xs-12 s-btns">
        <button type="submit" class="sb-btn rs-btn">Submit</button>
        </button><div class="sp-txt"><p>You will get call from an offical within 2hrs of office time</p></div>
      </div>
      
      </div>
      
      <div class="sp-txt-2" id="no" style="display:none;">
          <h2>Benefits of Partners</h2>
          <li> Intense Digital Visibilty </li>
          <li>Extensive Media Coverage - National / Local </li>
          <li>Hoarding / Displaying boards </li>
          <li>More direct branding and Marketing Benefits </li>
          
      </div>
    <!--</form>-->
    <?php echo form_close(); ?>
  </div>
</div>
</div>
 <?php include('include/main-sponsor-slider2.php');?>
 <?php include('include/co-sponsors.php');?>
  <?php include('include/local-sponser.php');?>
  <?php include('include/microsite.php');?>
<!-- <div class="fter asft">-->
<!--<div class="row">-->
<!--<div class="col-xs-4 footer-menu">-->
<!--<ul class="footer-menu">-->
<!--<li><a href="#">Home</a></li>        -->
<!--<li><a href="#">About Us</a></li>        -->
<!--<li><a href="#">Contact Us</a></li>        -->
<!--</ul>        -->

<!--</div>-->

<!--<div class="col-xs-4 footer-address">-->
<!--<p>S I G N (Society for Integrated Growth of the Nation)-->
<!--Nakshatra Garden, Cheranallor P O, Cochin, Kerala - 683544</p>-->
<!--</div>-->

<!--<div class="col-xs-4 footer-address">-->
<!--<p>BigLeap Solutions Pvt Ltd.<br>-->
<!--4th Floor, Markaz Complex, Calicut, Kerala - 673004</p>-->
<!--</div>-->

<!--<div class="col-xs-12 copyright">-->
<!--<p class="copy-txt">Copyright © 2019 All Rights Reserved.</p> -->
<!--<ul class="social-media">-->
<!--<li><a href="https://www.facebook.com/indiajobfairs/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>-->
<!--<li><a href="https://www.instagram.com/india_mega_job_fairs/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
function yes()
{
    $("#register").removeClass();
    $("#no").hide();
    $("#yes").hide();
}
function register()
{
    $("#no").hide();
    $("#yes").show();
}
function benefits()
{
    $("#register").addClass('disabledbutton');
    $("#no").show();
    $("#yes").hide();
}
$(document).ready(function()
  {
    $("#contact_no").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
        {
            return false;
        }
        return true;
    });
});
</script>
    
<script language="JavaScript">
    var ev = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
    var x = document.getElementById("error_msg");
    function validate(email){
    if(!ev.test(email))
        {
            x.innerHTML	= "Not a valid email";
            x.style.color = "red"
        }
    else
        {
            x.innerHTML	= "Ok!";
            x.style.color = "green"
        }
    }
</script>
<script type='text/javascript'>

$(document).ready(function() {
    $('#contact_no').keyup(function(e) {
        if (validatePhone('contact_no')) {
            $('#errmsg0').html('Valid Mobile Number');
            $('#errmsg0').css('color', 'green');
        }
        else {
            $('#errmsg0').html('Invalid Mobile Number');
            $('#errmsg0').css('color', 'red');
        }
    });
});

function validatePhone(contact_no) {
    var a = document.getElementById(contact_no).value;
    var filter = /[1-9]{1}[0-9]{9}/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}
</script>
