 <?php
if($this->session->userdata('cand_data'))
{
    $cand_id = $this->session->userdata['cand_data']['cand_id'];
    $cand_name = $this->session->userdata['cand_data']['name'];
    $qualification = $this->session->userdata['cand_data']['qualification'];
?>
<script type="text/javascript">
      function exp_show_fn(val){

        if(val=='Y'){

            $("#experience_row").show();
            $("#arrow_alert").show();
        }
        else{

            $("#experience_row").hide();
            $("#arrow_alert").hide();
        }
    }

      function intership_show_fn(val){

        if(val=='Y'){

            $("#internship_row").show();
            
        }
        else{

            $("#internship_row").hide();
            
        }
    }
    // function intership_like_show_fn(val){

    //     if(val=='Y'){

    //         $("#internship_like_row").show();
            
    //     }
    //     else{

    //         $("#internship_like_row").hide();
            
    //     }
    // }
</script>
<script>
    function getstreamdetails_pg(val)
    {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>jobfair_controller/getStream",
          data:'data='+val,
          success: function(data){

                $('#stream_id_1').html(data);
                
        },
    });
    }
</script>
<script>
    function getstreamdetails_ug(val)
    {
       $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>jobfair_controller/getStream",
          data:'data='+val,
          success: function(data){

                $('#stream_id_2').html(data);
                
        }
    });
    }
</script>

<script>
    function getstreamdetails_diploma(val)
    {
         $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>jobfair_controller/getStream",
          data:'data='+val,
          success: function(data){

                $('#stream_id_3').html(data);

        },
    });
    }
</script>

<script>
    function getstreamdetails_plus_two(val)
    {
         $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>jobfair_controller/getStream",
          data:'data='+val,
          success: function(data){

                $('#stream_id_4').html(data);

        },
    });
    }
</script>

<script>
    function getstreamdetails_sslc(val)
    {
         $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>jobfair_controller/getStream",
          data:'data='+val,
          success: function(data){

                $('#stream_id_5').html(data);

        },
    });
    }
</script>

<script>
    function get_sub_passion(val)
    {
        $.ajax({
          type: "POST",
          url: "<?php echo base_url();?>jobfair_controller/get_sub_passion",
          data:'data='+val,
          success: function(data){

                $('#interest_id').html(data);

        },
    });
    }
</script>
<script>
function onlyOneCheckBox() 
{
	var checkboxgroup = document.getElementById('checkboxgroup').getElementsByTagName("input");
	var limit = 2;
	for (var i = 0; i < checkboxgroup.length; i++) 
	{
		checkboxgroup[i].onclick = function() 
		{
			var checkedcount = 0;
				for (var i = 0; i < checkboxgroup.length; i++) 
				{
				    checkedcount += (checkboxgroup[i].checked) ? 1 : 0;
			    }
			    if (checkedcount > limit)
			    {
    				console.log("You can select maximum of " + limit + " checkbox.");
    				alert("You can select maximum of " + limit + " checkbox.");
    				this.checked = false;
			    }
		}
	}
}
</script>
<!--nav-->
<div class="container-fluid mt  report-page other-details">
  <div class="col-md-9 col-sm-8 site">
         <?php 
         /* foreach($candidate_details as $candidate_details)
        {
            $name= $candidate_details['cand_name'];
            $highest_qualification_id= $candidate_details['cand_qualification'];
            $cand_id= $candidate_details['cand_id'];

        }*/
        foreach($get_cand_qualification as $cand_qualification)
        {
            $cand_qualification= $cand_qualification['qualification'];

        }

      ?>
    <!--1e-->
   <?php include('include/menubar.php');?>
    <div class="container-fluid">
      <div class="titles" style="border-top:none;">
        <h2 class="other-title">Other Details</h2>
      </div>
      <?php 
        if($this->session->flashdata('resumeerr'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('resumeerr').'</div>';   
        }
        if($this->session->flashdata('imageerr'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('imageerr').'</div>';   
        }
        if($this->session->flashdata('otherdetailserr'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('otherdetailserr').'</div>';   
        }
    ?>
      <div class="e-r-f">
        <form action="<?php echo base_url();?>jobfair_controller/candidate_profile_registration" method="post" enctype="multipart/form-data" name="myForm">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="head-label">Name</label>
                <input class="form-control" type="text" name="name" id="name"  value="<?php echo $cand_name;?>" readonly="readonly"/>
                <input class="form-control" type="hidden" name="cand_id" id="cand_id"  value="<?php echo $cand_id;?>" readonly="readonly"/>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label class="head-label">Highest Qualification</label>
                <input class="form-control" type="text" name="qualification" id="qualification"  value="<?php echo $cand_qualification;?>" readonly="readonly"/>
                <input class="form-control" type="hidden" name="qualification_id" id="qualification_id"  value="<?php echo $qualification;?>" readonly="readonly"/>
              </div>
            </div>
          </div>
          <div class="row row-full">
            <div class="col-md-12"> 
            <div  class="table-responsive44">
            <label class="head-label">Educational Details (<span class="imp" style="color: #d0242a;"> </span> </label>
            <br><br>
            <table class="table table-striped table-bordered table-hover" id="tbl">
            <thead><tr>


            <th width="10%">Qalification</th>
            <th width="20%">Course</th>
            <th width="20%">Stream</th>
            <th width="20%">Board/University</th>
            <th width="10%">Percentage</th>
            <th width="20%">Passout (Year)</th>
            
            </tr>
            </thead>
                      <tbody >

            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_1" value="PG"  readonly="readonly" /></td>
            <td>                
                <select class="form-control" name="course_id[]" id="course_id_1" onchange="getstreamdetails_pg(this.value)">
                    <option>Select Course</option>
                    <?php foreach($qualification_pg as $quafctn){?>
                    <option value="<?php echo $quafctn['qualification_id'];?>"><?php echo $quafctn['qualification'];?></option>
                <?php }?>
                    <option value="1">Others</option>
                </select> 
            </td>
            <td>                
                <select class="form-control" name="stream_id_0" id="stream_id_1"  >
                    <option>Select Stream</option>
                </select> 
            </td>
            <td ><input class="form-control" type="input" name="board[]" id="board_1"/></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_1"/></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_1"/></td>
            </tr>

            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_2" value="UG" readonly="readonly" /></td>
           <td>                
                <select class="form-control" name="course_id[]" id="course_id_2" onchange="getstreamdetails_ug(this.value)">
                    <option>Select Course</option>
                    <?php foreach($qualification_ug as $quafctn){?>
                    <option value="<?php echo $quafctn['qualification_id'];?>"><?php echo $quafctn['qualification'];?></option>
                <?php }?>
                    <option value="1">Others</option>
                </select> 
            </td>
            <td>                
                <select class="form-control" name="stream_id_1" id="stream_id_2">
                    <option>Select Stream</option>
                </select> 
            </td>
            <td ><input class="form-control" type="input" name="board[]" id="board_2"/></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_2"/></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_2"/></td>
            </tr>

            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_3" value="Diploma" readonly="readonly"/></td>
            <td>                
                <select class="form-control" name="course_id[]" id="course_id_3" onchange="getstreamdetails_diploma(this.value)">
                    <option>Select Course</option>
                    <?php foreach($qualification_diploma as $quafctn){?>
                    <option value="<?php echo $quafctn['qualification_id'];?>"><?php echo $quafctn['qualification'];?></option>
                <?php }?>
                </select> 
            </td>
            <td>                
                <select class="form-control" name="stream_id_2" id="stream_id_3">
                    <option>Select Stream</option>
                </select> 
            </td>
            <td ><input class="form-control" type="input" name="board[]" id="board_3"/></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_3"/></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_3"/></td>
            </tr>

            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_4" value="Plus Two" readonly="readonly"/></td>
            <td>                
                <select class="form-control" name="course_id[]" id="course_id_4" onchange="getstreamdetails_plus_two(this.value)" >
                    <option>Select Course</option>
                    <?php foreach($qualification_plus_two as $quafctn){?>
                    <option value="<?php echo $quafctn['qualification_id'];?>"><?php echo $quafctn['qualification'];?></option>
                <?php }?>
                </select> 
            </td>
            <td>                
                <select class="form-control" name="stream_id_3" id="stream_id_4"  >
                    <option>Select Stream</option>
                </select> 
            </td>
            <td ><input class="form-control" type="input" name="board[]" id="board_4" /></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_4" /></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_4" /></td>
            </tr>

            <tr>
            <td><input class="form-control" type="input" name="qualfcn_catgry[]" id="qualfcn_catgry_5" value="SSLC" readonly="readonly" /></td>
            <td>                
                <select class="form-control" name="course_id[]" id="course_id_4" onchange="getstreamdetails_sslc(this.value)" >
                    <option>Select Course</option>
                    <?php foreach($qualification_sslc as $quafctn){?>
                    <option value="<?php echo $quafctn['qualification_id'];?>"><?php echo $quafctn['qualification'];?></option>
                <?php }?>
                </select> 
            </td>
            <td>                
                <select class="form-control" name="stream_id_4" id="stream_id_5"  >
                    <option>Select Stream</option>
                </select> 
            </td>
            <td ><input class="form-control" type="input" name="board[]" id="board_5" /></td>
            <td ><input class="form-control" type="input" name="percent[]" id="percent_5" /></td>
            <td ><input class="form-control" type="input" name="pass_out[]" id="pass_out_5" /></td>
            </tr>

            <tr class="control-group after-add-more">
            </tr>
            </tbody>
            </table>
            </div>
            </div>
        </div>

            <div class="col-md-12 m-b">
                <label class="head-label">Candidate Address<span class="imp" style="color: #d0242a;"> </span></label>
            <textarea class="form-control" name="cand_address" id="cand_address" ></textarea>
            </div>
<br>
           <div class="col-md-4">
                <label class="head-label">Candidate Location<span class="imp" style="color: #d0242a;"> </span></label>
                <select class="form-control" id="country" name="country" onChange="getState(this.value);"   >
                    <option value="" selected disabled>Select Country</option>
                    <?php 
                      $query=$this->db->get('tbl_countries')->result();
                      foreach($query as $q1)
                      {
                        echo '<option value='.$q1->id.'>'.$q1->name.'</option>';
                      }
                      ?>

                </select>
            </div>
 <div class="col-md-4">
               <label>&nbsp;</label>
                <select class="form-control"id="state" name="state" onChange="getCity(this.value);"  >
                    <option value="" selected disabled>Select State</option>
                     
                   

                </select>
            </div>
<div class="col-md-4">
               <label>&nbsp;</label>
                <select class="form-control" name="city" id="city" >
                    <option value="" selected disabled>Select City</option>
                </select>
            </div>
            <div class="frm-sb-sections col-md-12">
              <h4>Your area of interest to be Upskilled</h4>
          </div>
            <div class="col-md-6 m-b-15">
            <select class="form-control" name="passion_id" id="passion_id" onchange="get_sub_passion(this.value)" >
                <option>Select Passion</option>
                <?php foreach($passion_list as $passion_list){?>  
                <option value="<?php echo $passion_list['passion_id']; ?>"><?php echo $passion_list['passion']; ?></option>
                <?php } ?>
            </select>
            </div>
            <div class="col-md-6 m-b-15">
                <select class="form-control" name="interest_id" id="interest_id" >
                    <option>Select interest</option>
                </select>
        </div>
            <div class="col-md-6">
                <label class="head-label">Do you have any experience ?</label>
            </div>
            <div class="col-md-6">
               <label><input type="radio" name="have_experience" value="Y" id="have_experience"  onclick="exp_show_fn(this.value);" /> Yes &nbsp;&nbsp;</label>
                <label>
            <input type="radio" name="have_experience" value="N" id="have_experience" onclick="exp_show_fn(this.value);" checked="checked" /> No&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </div>
            <div class="col-md-12 hidden-div" id="experience_row" style="display:none;">
              <div class="col-md-11">
                <input type="hidden" name="row_no" value="1" id="row_no">
                <table class="table table-bordered table-responsive" id="myTable">
                  <thead>
                    <tr>
                      <th scope="col">Company Name</th>
                      <th scope="col">Position</th>
                      <th scope="col">Duration</th>
                    </tr>
                  </thead>
                  <tbody class="t-body">
                    <tr  class="row-periord" id="1">
                      <td><input type="text" class="form-control company" placeholder="Enter Company Name" name="company_name[]" id="company_name_1"></td>
                      <td><input type="text" class="form-control position" placeholder="Enter Position" name="job_position[]" id="job_position_1"></td>
                      <td><input type="text" class="form-control duration" placeholder="Enter Duration" name="duration[]" id="duration_1"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-1 add-del-btn">
                <div class="add-btn"><i class="fa fa-plus" aria-hidden="true"></i></div>
                <div class="del-btn" style="display:none;" id ="vk"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
              </div>
            </div>
            <div class="col-md-6">
                <label class="head-label">Have you done Any Internship?</label>
            </div>
            <div class="col-md-6">
                <label><input type="radio" name="done_internship" value="Y" id="done_internship"  onclick="intership_show_fn(this.value);" /> Yes &nbsp;&nbsp;</label>
                <label>
            <input type="radio" name="done_internship" value="N" id="done_internship"  checked="checked" onclick="intership_show_fn(this.value);"/> No&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </div>
          <div  class="row row-full" id="internship_row" style="display: none;"> 
            <div class="col-md-12">
            <div class="col-md-3">
                <label>Internship Details</label>
            </div>
            <div class="col-md-4">
                <input class="form-control" type="text" name="internship_detail" id="internship_detail" value=""/>
            </div>
            </div>
          </div> <br>
            <div class="col-md-6">
                <label class="head-label">Are you interested to do internship?</label>
            </div>
            <div class="col-md-6">
           <label><input type="radio" name="interest_internship" value="Y" id="interest_internship" /> Yes &nbsp;&nbsp;</label>
                <label>
            <input type="radio" name="interest_internship" value="N" id="interest_internship" checked="checked" /> No&nbsp;&nbsp;&nbsp;&nbsp;</label>
            </div>
        <!--<div  class="row" id="internship_like_row" style="display: none;"> -->
        <!--  <div class="col-md-12">-->
        <!--    <div class="col-md-3">-->
        <!--        <label>Preffered Internship</label>-->
        <!--    </div>-->
        <!--    <div class="col-md-4">-->
        <!--        <select class="form-control" name="pref_intern_id" id="pref_intern_id"  >-->
        <!--            <option value="" selected disabled>Select Internship</option>-->
                     <?php //foreach($internship as $intern)
                     //{?>
        <!--            <option value="<?php //echo $intern['internship_id'];?>"><?php //echo $intern['internship_name'];?></option>-->
        <!--            <?php //}?>-->

        <!--        </select> -->
        <!--    </div>-->
        <!--  </div>-->
        <!--</div> <br>-->

            <div class="col-md-12">
              <div class="form-group f-r" id="checkboxgroup">
                <label class="head-label">For an away career in future,which is your preferred location?</label>
                <p>
                    <div class="row checkboxtwoclms"><div class="col-md-6">
                    <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="Kanpur" onclick="onlyOneCheckBox()">
                    Kanpur </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="mumbai" onclick="onlyOneCheckBox()">
                    Mumbai </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="delhi" onclick="onlyOneCheckBox()">
                    Delhi </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="chennai" onclick="onlyOneCheckBox()">
                    Chennai </label>
                    <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="banglore" onclick="onlyOneCheckBox()">
                    Bangalore </label>
                   
                  <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="other" onclick="onlyOneCheckBox()">
                    Others Outside kerala </label>
                    </div>
                    <div class="col-md-6">
                  <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="canada" onclick="onlyOneCheckBox()">
                    Canada </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="sweden" onclick="onlyOneCheckBox()">
                    Sweden </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="germany" onclick="onlyOneCheckBox()">
                    Germany </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="australia" onclick="onlyOneCheckBox()">
                    Australia </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="azerbaijan" onclick="onlyOneCheckBox()">
                    Azerbaijan </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="pref_overseas_country[]" value="not" onclick="onlyOneCheckBox()">
                    Not Intersted</label>
                    </div>
                    </div>
                </p>
              </div>
            </div>
            <div class="col-md-12">
        
            </div>
<div class="row row-full no-margin">
            <div class="col-md-6">
              <label class="head-label">Do you wish to become an enterpreneur in the future ?</label>
            </div>
            <div class="col-md-6">
                <label><input type="radio" name="makein_india_interest" value="Y" id="makein_india_interest"   /> Yes &nbsp;&nbsp;</label>
                <label>
            <input type="radio" name="makein_india_interest" value="N" id="makein_india_interest" checked="checked" /> No &nbsp;&nbsp;&nbsp;&nbsp;</label>
            </div>
            </div>



            <div class="col-md-6">
               <label for="userfile" class="head-label">Upload Resume <span class="imp" style="color: #d0242a;"> </span>(doc or pdf file.)<br><h6>[Maximum Size : 5MB]</h6></label>
            </div>
            <div class="col-md-6">
                
            <input type="file" id="userfile" name="userfile" size="20"   />
            
            <input type="hidden" id="src" name="src" value="" />
            </div>
            
            <!--<div class="row " style="margin-left:0; float: left;">-->
            <div class="col-md-12">
                <div class="col-md-6 pimg" style="margin-left:0; float: left;">
                <label for="userfile" class="head-label">Upload Image <span class="imp" style="color: #d0242a;"> </span>(jpg or png file.)<br><h6>[Maximum Size:50KB Dimension:200W X 230H]</h6></label>
                </div>
            <!--</div>-->
            <div class="col-md-6" >
            <input type="file" id="profile_img" name="profile_img" size="20"  style="margin-top:-2px;" onchange="validate_image(this)"  />
            </div>
            </div>
            
                
            
                

            <!--<div class="col-md-12 frm-sb-sections">
              <h4>Sample Videos ( to be Added )</h4>

            </div>-->


           <!-- <div class="col-md-6">
               <label for="userfile">Tell about your self (If Video 45 Sec)</label>
            </div>
            <div class="col-md-6">
                
              <input type="file" id="video" name="video" size="20" />
           
            </div> -->



           <!-- <div class="col-md-6">
               <label for="userfile">Upload Tell Me Your Self Video</label>
            </div>
            <div class="col-md-6">
                
            <input type="file" id="video_1" name="video_1" size="20" />
           
            </div> -->
          <!--<div class="row" style="margin-left:0; float:left;">-->
          <!--  <div class="col-md-6">-->
          <!--    <label for="userfile" class="head-label">Upload Profile Image <span class="imp" style="color: #d0242a;"> *</span><br><h6>[Maximum Size:3MB Dimension:150W X 200H]</h6></label>-->
          <!--  </div>-->
          <!--  <div class="col-md-6">   -->
          <!--  <input type="file" id="profile_img" name="profile_img" size="20" required style="margin-top:-2px;"/>-->
          <!--  </div>-->
          <!--</div>-->
          
          
            <div class="col-md-12">
                <label>&emsp;</label><br>
                <button type="submit" class="sb-btn" value="<?php echo $this->session->userdata['cand_data']['cand_id'];?>" onclick="profileRegistration(this.value)">Submit</button>
                <div align="right" style="padding: 0 0 2% 0;"><h6>(<span class="imp" style="color: #d0242a;"> *</span> fields are mandatory )</h6></div>
            </div>
        </form>
      </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script>
    function profileRegistration(val)
{
    	$.ajax({
        	type: "POST",
	        url: "<?php echo base_url();?>jobfair_controller/get_candidate_details",
	        data:'cand_id='+val,
	        success: function(data)
	        {
	            if(data)
	            {
	                alert("You Are Already Registered");
		            return false;
	            }
	        }
	});
}
</script>

<script type="text/javascript"> 
        var _URL = window.URL || window.webkitURL;
        $('#profile_img').on('change', function() { 

// alert("mdfgbkm;");exit();
            var file = this.files[0],img;
            
            var fileExtension = ['jpeg', 'jpg', 'png'];
            
            var imgwidth=0,imgheight=0 ,imgDimen = 0;
  
            const size =  
               (this.files[0].size / 1024 / 1024).toFixed(2); 
              
            var file, img;
        if ((file = this.files[0])) 
        {
            img = new Image();
            var objectUrl = _URL.createObjectURL(file);
            img.onload = function () {
               // alert("width"+this.width + " " + this.height);exit();
                imgwidth=this.width;
                imgheight=this.height;   
                 if (imgheight >230 || imgwidth >200) 
                  {
                    var imgDimen = 1;
                    alert("Image Height and Width must not exceed 200*230px");
                   
                    $("#profile_img").val('');
                     return false;
                  }        

                _URL.revokeObjectURL(objectUrl);
            };
            img.src = objectUrl;   
        }
     
            if (size > 5) { 
                alert(size+"Image Size or Format Mismatches!"); 
                 // this.files[0].name=" ";
                 $("#profile_img").val('');
                return false;
            }            
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1)
             {

                alert("Only formats are allowed : "+fileExtension.join(', '));
                $("#profile_img").val('');
                return false;
              }
             // else { 

             //    $("#imgerr").html('<b>' + 
             //       'This file size is: ' + size + " MB" + '</b>'); 
             //    return true;
            //} 
        }); 

    </script>

<script type="text/javascript">
$(document).ready(function()
  {
    var tbody = $('#myTable').children('tbody');

   var table = tbody.length ? tbody : $('#myTable');

   $(".row-periord:last").find(".company");

   $('.add-btn').on('click', function()
   {
       
    var rows=$('#myTable tr').length;
    if(rows>1)
    {
        document.getElementById("row_no").value =rows;
        $("#vk").show();
    }
    table.append('<tr  class="row-periord" id="1"><td><input type="text" class="form-control company" placeholder="Enter Company Name" name="company_name[]"></td><td><input type="text" class="form-control position" placeholder="Enter Position" name="job_position[]"></td><td><input type="text" class="form-control duration" placeholder="Enter Duration" name="duration[]"></td></tr>');
    $(".t-body tr:last").find(".company");

   });

   });

</script>
<script type="text/javascript">
  $('.del-btn').click(function(){
      
       $( ".row-periord:last").remove();
       
       var rows=$('#myTable tr').length;
       document.getElementById("row_no").value =rows - 1;
       
       if(rows <= 2)
        {
            $("#vk").hide();
        }
  });
</script>
<script>
    function getState(val) 
 {

	$.ajax({
        	type: "POST",
	        url: "<?php echo base_url();?>jobfair_controller/getState",
	        data:'country_id='+val,
	        success: function(data)
	        {
		        $("#state").html(data);
		        getCity();
	        }
	});
 }
 function getCity(val) 
 {
	$.ajax({
        	type: "POST",
	        url: "<?php echo base_url();?>jobfair_controller/getCity",
	        data:'state_id='+val,
	        success: function(data)
	        {
		        $("#city").html(data);
	        }
	    });
 }
</script>
<?php } else{ redirect('logout');}?>
