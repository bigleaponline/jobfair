<style type="text/css">
    .col-md-2.new, .col-md-5.new {

        
        margin-bottom: 20px;
        margin-left: 25px;
    }
body{

  font-family: Arial, Helvetica, sans-serif;
}
.head_tr{
}
.head_td{
}
}
.head_address{
  padding-left:20px;
  font-size: 12px;
 
  padding-top: 50px;
  width:250px;
  text-align: center;
}
.skills_td{
}
.progress-bar.new {
    background-color: #990099;
}

.progress-bar.new1{

  background-color: #428bca;
}

.progress-bar.new2{

  background-color: #d9534f;
}

.progress-bar.new3{

  background-color: #f0ad4e;
}

.progress-bar.new4{

  background-color: #5cb85c;
}


.col-md-6.skill_title {
    padding: 2% 0 2% 1%;
}

.col-md-6.skill_chart {
    padding: 2% 0 2% 0;
}
</style>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="container-fluid mt">
  <div class="col-md-9 col-sm-8 site">
 <?php include('include/menubar.php');?>
    <div class="container-fluid">
      <div class="titles" style="border-top:none;">
        <h2><b>Assessment Report</b></h2>
      </div>
    </div>
    <style>
    .assmnt-clm label { font-weight:normal; }
    </style>
    <?php

      foreach($qualification as $qualification)
      {
        $qualification = $qualification['qualification'];
      }

      foreach ($quantitative_score as $quantitative_score) {

        $q_score = $quantitative_score['total_score'];
        $q_percent = $q_score * 5;

      }
      foreach ($reasoning_score as $reasoning_score) {

        $r_score = $reasoning_score['total_score'];
        $r_percent = $r_score * 5;

      }
      foreach ($verbal_score as $verbal_score) {

        $v_score = $verbal_score['total_score'];
        $v_percent = $v_score * 5;

      }
    ?>
    <div class="assmnt-clm">
    <?php 

      $attributes  = array('id' => 'myform'); 

      echo form_open('thrissur/technical_report_pdf',$attributes) ?>
    <div align="center" class="pdf_btn" style="padding: 2%;">
      <input type="submit" name="cmd" id="cmd" value="Download Report">
      <a href="<?php echo site_url("thrissur/technical_report_print"); ?>" target="_blank" class="btn btn-info btn-xs" tooltip="Print" title="Print" data-toggle="modal"><i class="fa fa-print"></i></a>
    </div>
     <div class="col-md-12 col-xs-12">
      <div class="row">
       <div class="col-md-4 col-xs-4">
         <label><b>Candidate Name :</b> <?php echo $name;?></label>
       </div>
       <div class="col-md-4 col-xs-4">
         <label><b>Candidate Id :</b> <?php echo $cand_id;?></label>
       </div>
        <div class="col-md-4 col-xs-4">
         <label><b>Qualification :</b> <?php echo $qualification;?></label>
       </div>
       </div>
       <div class="row">
         <div class="col-md-6 col-xs-6 skill_title">
           <h4><b>Skill Distribution</b></h4>
         </div>
       </div>
       <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Quantitative Aptitude : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $q_percent;?>%;height: 15px;" class="progress-bar new1" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
         <span><?php echo $q_score;?>/20</span>
       </div>

       <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Reasoning : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $r_percent;?>%;height: 15px;" class="progress-bar new2" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
         <span><?php echo $r_score;?>/20</span>
       </div>

       <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Verbal Aptitude : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $v_percent;?>%;height: 15px;" class="progress-bar new3" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
          <span><?php echo $v_score;?>/20</span>
       </div>
       <div class="row">
         <div class="col-md-6 col-xs-6 skill_chart">
            <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
            <input type="hidden" name="chrt_img" id="chrt_img">
         </div>
       </div>
     </div>
     <table class="header_tb" width="100%" style="height:auto;" ></table>
      <?php echo form_close(); ?>
    </div>
<!--      <div class="fter asft">
<div class="row">
<div class="col-xs-4 footer-menu">
<ul class="footer-menu">
<li><a href="#">Home</a></li>        
<li><a href="#">About Us</a></li>        
<li><a href="#">Contact Us</a></li>        
</ul>        
</div>
<div class="col-xs-4 footer-address">
<p>S I G N (Society for Integrated Growth of the Nation)
Nakshatra Garden, Cheranallor P O, Cochin, Kerala - 683544</p>
</div>
<div class="col-xs-4 footer-address">
<p>BigLeap Solutions Pvt Ltd.<br>
4th Floor, Markaz Complex, Calicut, Kerala - 673004</p>
</div>
<div class="col-xs-12 copyright">
<p class="copy-txt">Copyright © 2019 All Rights Reserved.</p> 
<ul class="social-media">
<li><a href="https://www.facebook.com/indiajobfairs/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
<li><a href="https://www.instagram.com/india_mega_job_fairs/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
</ul>
</div>
</div>
</div>
  </div> -->
  <script>
$(document).ready(function(){
$(".vk2").click(function(){
  $(".vk1").removeClass("actived");
});
    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        if(value == "all")
        { 
            $('.filter').show('1000');
        }
        else
        {
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");
});
</script>
<script type="text/javascript">
  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);

  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Assessment', 'Marks per Topic'],
      ['Quantitative Aptitude',     <?php echo $q_score;?>],
      ['Reasoning',      <?php echo $r_score;?>],
      ['Verbal Aptitude',  <?php echo $v_score;?>]
    ]);

    var options = {
      title: 'My Mark Distribution',
      is3D: true,
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));

    chart.draw(data, options);
  }
</script>