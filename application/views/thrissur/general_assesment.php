<?php 
if($this->session->userdata('cand_data'))
{
    $cand_id=$this->session->userdata['cand_data']['cand_id'];
?>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
var submitted=false;
    $(document).ready(function(){  
        var user = '<?php echo $cand_id; ?>';
        if (typeof(Storage) !== "undefined") 
        {
            if (localStorage.user) 
            {
                users = localStorage.user;
            } 
            else 
            {
                localStorage.setItem("user", user);
            }
        }
        if (typeof(Storage) !== "undefined") 
        {
            if (localStorage.user) 
            {
                users = localStorage.user;
                console.log(users);
            }
        }
        if(users == user)
        {
        }
        else 
        {
            localStorage.removeItem("seconds");
            if (typeof(Storage) !== "undefined") 
            {
                localStorage.setItem("user", user);
            }
        }
        var seconds = 1201; 
        if (typeof(Storage) !== "undefined") 
        {
            if (localStorage.seconds) 
            {
                seconds = localStorage.seconds;
            }
        }
        function secondPassed() 
        {
            seconds--;
            var hours = Math.floor(seconds / 3600);
            var minutes = Math.floor((seconds - (hours*3600)) / 60);
            var remainingSeconds = Math.floor(seconds % 60);
            if (remainingSeconds < 10) 
            {
                remainingSeconds = "0" + remainingSeconds;
            }
            if (typeof(Storage) !== "undefined") 
            {
                localStorage.setItem("seconds", seconds);
            }
            document.getElementById('countdown').innerHTML = hours + ":" + minutes + ":" + remainingSeconds;
            if (seconds == 0) 
            {
                submitted = true;
                clearInterval(myVar);
                document.getElementById('countdown').innerHTML = "Time Out";
                if (typeof(Storage) !== "undefined") 
                {
                    localStorage.removeItem("seconds");
                }
                alert("Time Out....")
                document.getElementById("my_formm").submit();
            }
            else 
            {
                console.log(seconds);
            }
        }
    
    var myVar = setInterval(secondPassed, 1000);
     $("form").submit(function() { 
            submitted = true;
        }); 
        window.onbeforeunload = function() {
            if (!submitted)
            {
                return "Dude, are you sure you want to leave? Think of the kittens!";
             }
        }
  });
</script>
<div class="container-fluid mt report-page">
  <div class="col-md-9 col-sm-8 site">
   <?php include('include/menubar.php');?>
    <div class="container-fluid">
        <div class="timer_div">
       Time Left :  <span id='countdown'></span>
      </div>
      <div class="titles asesmnt-title" style="border-top:none;">
        <h2>Assessment</h2>
      </div>
    </div>
    <style>
    .assmnt-clm label { font-weight:normal; }
    </style>
    <div class="assmnt-clm">
    <form action="<?php echo base_url() .'calicut_jobfair_controller/assesment_test_submission/'.$topic_id?>" method="post" id="my_formm" name="my_formm">
       <?php 
         $count=1;
         $ccount = count($question_list);?>
            <input type="hidden" name="ccount" value="<?php echo $ccount;?>" />
            <input type="hidden" name="cand_id" value="<?php echo $cand_id;?>" />
            <input type="hidden" name="topic_id" value="<?php echo $topic_id;?>" />
            <?php foreach($question_list as $question_list){ ?>
                <?php
                $id = $question_list['id'];

                $option_view = get_question_option($id,$count);
                foreach ($option_view as $option_view) {
                $op1=$option_view['op1'];
                $op2=$option_view['op2'];
                $op3=$option_view['op3'];
                $op4=$option_view['op4'];
                }
            ?>
      <div class="form-group f-r">
        <label class="q_no" style="float: left; padding: 1%;"><span><?php echo $count;?>.</span></label>   <?Php echo $question_list['ques_name'];?>
        <input type="hidden" name="question_id[]" id="question_id" value="<?php echo $question_list['id'];?>">
        <input type="hidden" name="question_type[]" id="question_type" value="<?php echo $question_list['question_type'];?>">
        <p class="radio-items">
          <label class="radio-inline">
            <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op1;?>" />
            <?php echo $op1;?> </label>
          <label class="radio-inline">
            <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op2;?>" >
            <?php echo $op2;?> </label>
          <label class="radio-inline">
            <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op3;?>" >
            <?php echo $op3;?> </label>
          <label class="radio-inline">
            <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op4;?>" >
            <?php echo $op4;?> </label>
        </p>
      </div>
      <?php
       $count++; 
      }
      ?>
      <div class="form-group sbmt_test" style="text-align: right;padding: 30px;">
        <button type="submit" name="submit_test"  class="btn btn-info" id="submitBtn" style="width:150px;font-weight: bold;font-family:Times New Roman;font-size: 18px;" >Submit</button>
      </div>
     </form>
    </div>
  <?php }else { redirect('logout');}?> 