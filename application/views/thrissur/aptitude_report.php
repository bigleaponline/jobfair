<style type="text/css">
.progress-bar.new1{
  background-color: #428bca;
}
.progress-bar.new2{
  background-color: #d9534f;
}
.col-md-6.skill_title {
    padding: 2% 0 2% 1%;
}
.col-md-6.skill_chart {
    padding: 2% 0 2% 0;
}
</style>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<div class="container-fluid mt  report-page">
  <div class="col-md-9 col-sm-8 site">
   <?php include('include/menubar.php');?>
    <div class="container-fluid">
      <div class="titles" style="border-top:none;">
        <h2>Report</h2>
      </div>
    </div>
    <style>
    .assmnt-clm label { font-weight:normal; }
    </style>
    <?php
      foreach ($result as $result) {
        
        $score = $result['total_score'];
        $score_percent = $score * 5;
        $wrong = 20 - $score;
        $wrong_percent = $wrong * 5;
      }
    ?>
    <div class="assmnt-clm">
      
      <?php 

      $attributes  = array('id' => 'myform'); 
      echo form_open('thrissur/aptitude_report_pdf',$attributes) ?>
      <div align="center" class="pdf_btn" style="padding: 2%;">
        <input type="submit" name="cmd" id="cmd" value="Download Report">
        <!-- <a href="<?php echo site_url("thrissur/technical_report_print"); ?>" target="_blank" class="btn btn-info btn-xs" tooltip="Print" title="Print" data-toggle="modal"><i class="fa fa-print"></i></a> -->
      </div>
      <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Correct Answers : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $score_percent;?>%;height: 15px;" class="progress-bar new1" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
         <span><?php echo $score;?>/20</span>
       </div>
       <div class="row">
         <div class="col-md-3 col-xs-3">
          <label>Wrong Answers : </label>
         </div>
         <div class="col-md-6 col-xs-6">
           <div data-percentage="0%" style="width: <?php echo $wrong_percent;?>%;height: 15px;" class="progress-bar new2" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
         </div>
         <span><?php echo $wrong;?>/20</span>
       </div>
        <div class="row">
           <div class="col-xs-12 skill_chart">
              <div id="piechart_3d"></div>
              <input type="hidden" name="chrt_img" id="chrt_img">
           </div>
        </div>
    </div>

<script type="text/javascript">
google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
var data = google.visualization.arrayToDataTable([
['Assessment', 'Marks per Topic'],
['Correct Answers',     <?php echo $score;?>],
['Wrong Answers',      <?php echo $wrong;?>]
]);
var options = {
  title: 'My Mark Distribution',
  width: 400,
  height: 200,
  is3D: true,
};
var chart_area = document.getElementById('piechart_3d');
var chart = new google.visualization.PieChart(chart_area);
google.visualization.events.addListener(chart, 'ready', function(){
chart_area.innerHTML = '<img src="' + chart.getImageURI() + '" class="img-responsive">';
document.getElementById("chrt_img").value =chart.getImageURI();
}); 
chart.draw(data, options);
}
</script>