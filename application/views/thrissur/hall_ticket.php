    <style>
        .container {

            width: 100%;
            max-width: 1000px;
            margin: 0 auto;
            border: 2px solid #000;
            padding: 10px;
            margin-bottom: 4%;

        }

        .container tr {

            border: 2px solid #000;
        }

        .txt-large {
            font-size: 30px;
            font-weight: 800;

        }

        .txt-center {

            text-align: center;
        }

        .heading {
            border: 2px solid #000;
            padding: 10px;
            margin-bottom: 10px;

        }

/*        .logo {

            width: 160px;
            -webkit-filter: grayscale(100%);
            /* Safari 6.0 - 9.0 */
            filter: grayscale(100%);
        }

        img {

            width: 100%;
        }

        .border-1,
        .border-1 td {

            border: 1px solid #000;
        }

        .border-1 td {
            padding: 10px;


        }

        .m-b-10 {

            margin-bottom: 10px;
        }

        .avatar {}
        img.profile.img-responsive {
    width: 100%;
    height: auto;
    display: block;
}

    </style>
<div class="container-fluid mt hall-page">
  <div class="col-md-9 col-sm-8 site">
    <?php include('include/menubar.php');?>
    <div class="container-fluid">
      <div class="titles" style="border-top:none;">
        <!-- <h2>Hall Ticket</h2> -->
      </div>
    </div>
    <style>
    .assmnt-clm label { font-weight:normal; }
    </style>
    <?php
    $cand_test_status   =$this->session->userdata['cand_data']['cand_test_status'];
    $normal_cand_image  = "";
    $cand_img = "";
        
        foreach ($candidate_details as $candidate_details) 
        {
            $name = $candidate_details['cand_name'];
            $dob = $candidate_details['cand_dob'];
            $mark = $candidate_details['cand_marks'];
            $course = $candidate_details['qualification'];
            $branch = $candidate_details['stream_name'];
            $category = $candidate_details['cand_category'];
            $level = $candidate_details['cand_level'];
            $cand_img1=$candidate_details['cand_image'];
            $test_status = $candidate_details['cand_test_status'];
            $hallticket_category = $candidate_details['hallticket_category'];
            $cand_passed = $candidate_details['cand_passed'];
            $cand_backpapers = $candidate_details['cand_backpapers'];
        }
        if($level!=0)
        {
        foreach ($cand_image as $cand_image) 
        {
            $cand_img = $cand_image['cand_img'];
        }
         }//else{
        // foreach ($normal_cand_image as $normal_cand_image) 
        // {
        //     $normal_cand_image = $normal_cand_image['cand_image'];
        // }
        // }
        if($level==0)
        {
            $score="";
            $cand_img=$cand_img1;
        }
        if($test_status == 1)
        { 
            // echo $test_status;exit();
            foreach ($general_score as $general_score) 
            {
                $gen_score = $general_score['total_score'];
            }
        }
        if($test_status == 2)
        {
            if($quantitative_score != ' ')
            {
                foreach ($quantitative_score as $quantitative_score) 
                {
                    $q_score = $quantitative_score['total_score'];
                }
            }
            if($reasoning_score != ' ')
            {
                foreach ($reasoning_score as $reasoning_score) 
                {
                    $r_score = $reasoning_score['total_score'];
                }
            }
            if($verbal_score != ' ')
            {
                foreach ($verbal_score as $verbal_score) 
                {
                    $v_score = $verbal_score['total_score'];
                }
            }
            $tech_score = $q_score + $r_score + $v_score;
        }
    ?>
    <div class="assmnt-clm hall-ticket">
       <?php 

      $attributes  = array('id' => 'myform'); 

      echo form_open('thrissur/hall_ticket_pdf',$attributes) ?>
    <div align="center" class="pdf_btn" style="padding: 2%;">
      <input type="submit" name="cmd" id="cmd" value="Download Hall Ticket">
      <a href="<?php echo site_url("thrissur/hall_ticket_print"); ?>" target="_blank" class="btn btn-info btn-xs" tooltip="Print" title="Print" data-toggle="modal"><i class="fa fa-print"></i></a>
    </div>
<table class="container">
        <tbody>
            <tr>
                <td>
                    <table width="100%" class="heading">
                        <tbody>
                            <tr>
                                <td class="logo"><img src="<?php echo base_url(); ?>assets/images/hall_ticket/logo.png" alt="" style="width: 160px;"/></td>
                                <td class="txt-large">Hall Ticket</td>
                                <td width="100" class="txt-center" style="border: 1px solid #000;"><span>Category</span>
                                    <br><br>
                                    <span style="margin-top: 25px; font-size: 50px;"><?php echo $hallticket_category;?></span>
                                </td>
                            </tr>
                        </tbody>
                    </table>


                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="border-1 m-b-10" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td style="width: 155px">Registration Number</td>
                                <td><strong><?php echo $cand_id;?></strong></td>
                                <td style="width: 130px"><?php if($level == 0){ echo 'Assessment:'; } else {echo 'Assessment Score:';} ?></td>
                                <td width="100px"><strong><?php if($test_status == 1){ echo $gen_score; } elseif($test_status == 0){echo "Not Attended";}else { echo $tech_score;}?></strong></td>
                            </tr>
                            <tr>
                                <td>Venue:</td>
                                <td><strong>Holy Grace Engineering Campus Mala, Thrissur</strong></td>
                                <td>Date:</td>
                                <td>02/02/2020</td>
                            </tr>
                        </tbody>
                    </table>


                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="border-1 m-b-10" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td style="width: 200px;">Name of the Candidate:</td>
                                <td style="text-transform:Capitalize;"><strong><?php echo $name;?></strong></td>
                                <!--<td rowspan="6" style="width:150px;height:200px"><?php if($level == 0){?><img src="<?php echo base_url().'upload/profile_images/'.$normal_cand_image?>" class="img-responsive" alt="" /><input type="hidden" name="cand_img" id="cand_img"><?php } else {?><img src="<?php echo base_url().'upload/profile_images/'.$cand_img?>" class="img-responsive" alt="" /><input type="hidden" name="cand_img" id="cand_img"><?php } ?></td>-->
                                <td rowspan="5" align="center" style="text-align: center;" ><?php if($normal_cand_image !="" || $cand_img != ""){ if($level == 0){?><img src="<?php echo base_url().'upload/profile_images/'.$cand_img?>" class="profile img-responsive" alt="" /><?php } else {?><img src="<?php echo base_url().'upload/profile_images/'.$cand_img?>" class="profile img-responsive" alt="" /><?php }}else{ echo " Attach Photograph"; } ?></td>
                            </tr>
                            <tr>
                                <td>Date of Birth:</td>
                                <td><strong><?php echo $dob;?></strong></td>

                            </tr>
                            <tr>
                                <td>Course:</td>
                                <td><strong><?php echo $course;?></strong></td>

                            </tr>
                            <tr>
                                <?php if($cand_passed == 1){ ?>
                                <td>Mark Percentage:</td>
                                <td><strong><?php if($cand_passed == 1){echo $mark;}?>%</strong></td>
                                <?php } elseif($cand_passed == 2 || $cand_passed == 3){ ?>
                                <td>Course Status:</td>
                                <td><strong>
                                    <?php if($cand_passed == 2 && $cand_backpapers != '' || $cand_passed == 2 && $cand_backpapers != ''){echo "Not Completed";} elseif($cand_passed == 3 && $cand_backpapers == '' || $cand_passed == 3 && $cand_backpapers != ''){echo "Ongoing";} } ?></strong></td>
                            </tr>
                            <tr>
                                <td>Branch:</td>
                                <td><strong><?php echo $branch;?></strong></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="border-1 m-b-10" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td style="line-height: 24px;">I, declare that I have registered myself on the India Mega Job Fairs site and all the information given above are true in my sense and knowledge</td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="border-1 m-b-10" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td><span>Signature of the student:</span>
                                    <br><br>
                                    <span>Date:</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="border-1 m-b-10" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>Bring following to the venue:
                                </td>

                                <td><span style="line-height: 24px;">10 copies of Resume<br>
                                        10 set of photocopies of your mark lists<br>
                                        10 copies of passport size photographs<br>
                                        Voters ID/ Any other Photo ID<br>
                                        Pen</span>

                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%" class="border-1" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td>
                                    <span style="line-height: 24px;">
                                        Note: Please carry the Hall Ticket, ID-Card or any identity card on the date of job fair.<br>
                                        Copyright www.indiamegajobfairs.com. All Rights Reserved.
                                    </span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
    <?php echo form_close(); ?>
    </div>
