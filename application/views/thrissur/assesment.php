<?php 
if($this->session->userdata('cand_data'))
{
    $cand_id=$this->session->userdata['cand_data']['cand_id'];
?>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script>
$(document).ready(function(){  
        var user = '<?php echo $cand_id; ?>';
        if (typeof(Storage) !== "undefined") 
        {
            if (localStorage.user) 
            {
                users = localStorage.user;
            } 
            else 
            {
                localStorage.setItem("user", user);
            }
        }
        if (typeof(Storage) !== "undefined") 
        {
            if (localStorage.user) 
            {
                users = localStorage.user;
                console.log(users);
            }
        }
        if(users == user)
        {
        }
        else 
        {
            localStorage.removeItem("seconds");
            if (typeof(Storage) !== "undefined") 
            {
                localStorage.setItem("user", user);
            }
        }
        var seconds = 3601; 
        if (typeof(Storage) !== "undefined") 
        {
            if (localStorage.seconds) 
            {
                seconds = localStorage.seconds;
            }
        }
        function secondPassed() 
        {
            seconds--;
            var hours = Math.floor(seconds / 3600);
            var minutes = Math.floor((seconds - (hours*3600)) / 60);
            var remainingSeconds = Math.floor(seconds % 60);
            if (remainingSeconds < 10) 
            {
                remainingSeconds = "0" + remainingSeconds;
            }
            if (typeof(Storage) !== "undefined") 
            {
                localStorage.setItem("seconds", seconds);
            }
            document.getElementById('countdown').innerHTML = hours + ":" + minutes + ":" + remainingSeconds;
            if (seconds == 0) 
            {
                clearInterval(myVar);
                document.getElementById('countdown').innerHTML = "Time Out";
                if (typeof(Storage) !== "undefined") 
                {
                    localStorage.removeItem("seconds");
                    var base_url = window.location.origin;
                }
            }
            else 
            {
                console.log(seconds);
            }
        }
    
    var myVar = setInterval(secondPassed, 1000);
  });
    </script>
<div class="container-fluid mt">
  <div class="col-md-9 col-sm-8 site">
      
  <?php include('include/menubar.php');?>
    <div class="container-fluid">
      <div class="timer_div" style=" text-align: right; color: #104fa2ed; padding: 10px 20px 10px 10px; font-weight: bold;font-size: 18px; border-top :20px solid lightgrey ; border-bottom: 1px solid lightgrey;">
       Time Left :  <span id='countdown'></span>
      </div>
      <div class="titles assmnt-title" style="border-top:none;">
        <h2 align="center"><?php if($topic_id == 2) echo 'Quantitative';if($topic_id == 3) echo 'Reasoning';if($topic_id == 4) echo 'Verbal'; {
          # code...
        } ?></h2>
      </div>
    </div>
    <style>
    .assmnt-clm label { font-weight:normal; }
    </style>
    
    <div class="assmnt-clm">
    <form action="<?php echo base_url() .'calicut_jobfair_controller/assesment_test_submission/'.$topic_id?>" method="post">
     
      <input type="hidden" name="cand_id" value="<?php echo $cand_id;?>" />
      <?php 
        $count=1;
        $ccount = count($question_list);
      ?>
      <input type="hidden" name="ccount" value="<?php echo $ccount;?>" />
      <input type="hidden" name="topic_id" value="<?php echo $topic_id;?>" />

      <?php foreach($question_list as $question_list){ 

            $id = $question_list['id'];
            $question_type = $question_list['question_type'];
      ?>
      <div class="form-group f-r">
        <label class="q_no"><?php if ($question_type == 'general') {
          echo $count .'.' ; }?> </label>  <?Php echo $question_list['ques_name'];?>
        <input type="hidden" name="question_id[]" id="question_id" value="<?php echo $question_list['id'];?>">
         <input type="hidden" name="question_type[]" id="question_type" value="<?php echo $question_list['question_type'];?>">
        <p>
          <?php
           if($question_list['question_type'] == 'general')
           {

            $option_view = get_question_option($id,$count);
            foreach ($option_view as $option_view) {
              $op1=$option_view['op1'];
              $op2=$option_view['op2'];
              $op3=$option_view['op3'];
              $op4=$option_view['op4'];
              }
              ?> 
          
          <label class="radio-inline">
             <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op1;?>"> <?php echo $op1;?></label> 
          <label class="radio-inline">
             <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op2;?>"> <?php echo $op2;?></label>
          <label class="radio-inline">
            <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op3;?>"> <?php echo $op3;?> </label>
          <label class="radio-inline">
            <input type="radio" name="quest_optn_<?php echo  $count;?>" value="<?php echo $op4;?>"> <?php echo $op4;?></label></p>
            <?php
            }
            if($question_list['question_type'] == 'passage')
            {

              $subquestions = get_sub_questions($id);

            ?>   
              <?php 
                  $qno=1;
                  $subq_count = count($subquestions);
                ?>
                <input type="hidden" name="subq_count" value="<?php echo $subq_count;?>" />
                <?php foreach($subquestions as $subquestions){ 

                      $passage_id = $subquestions['passage_id'];
                      $sub_option = get_subquestion_option($passage_id,$count);
                      foreach ($sub_option as $sub_option) {
                        $sub_op1=$sub_option['op1'];
                        $sub_op2=$sub_option['op2'];
                        $sub_op3=$sub_option['op3'];
                        $sub_op4=$sub_option['op4'];
                        }


                ?>
                <div class="form-group f-r">
                <label><?php echo $count ;?>. <?Php echo $subquestions['question'];?></label>
                <input type="hidden" name="passage_id[]" id="passage_id" value="<?php echo $subquestions['passage_id'];?>">

         <p>      
          
          <label class="radio-inline">
             <input type="radio" name="subquest_optn_<?php echo  $qno;?>" value="<?php echo $sub_op1;?>"> <?php echo $sub_op1;?></label> 
          <label class="radio-inline">
             <input type="radio" name="subquest_optn_<?php echo  $qno;?>" value="<?php echo $sub_op2;?>"> <?php echo $sub_op2;?></label>
          <label class="radio-inline">
            <input type="radio" name="subquest_optn_<?php echo  $qno;?>" value="<?php echo $sub_op3;?>"> <?php echo $sub_op3;?> </label>
          <label class="radio-inline">
            <input type="radio" name="subquest_optn_<?php echo  $qno;?>" value="<?php echo $sub_op4;?>"> <?php echo $sub_op4;?></label> </p>

                <?php
                $qno++;
                if($qno <= $subq_count)
                $count++;
                if($count > 20)
                  goto a;

              }
              ?>
               

            </div>
             
         
       
            <?php
          }
          ?>
       
      </div>
      <?php

        $count++; 
        }
        a:
      ?>
      <div align="right" style="padding: 30px;" >
      <input style="font-weight: bold;width: 150px;"  class="btn btn-info" type="submit" name="submit" value="Submit">
    </div>
    </form>
    
    </div>
  <script>
 
  var submitted = false;
  
$(document).ready(function(){
    $("form").submit(function() {
        submitted = true;
     });
     window.onbeforeunload = function() {
         if (!submitted) {
        return "Dude, are you sure you want to leave? Think of the kittens!";
         }
    }
$(".vk2").click(function(){
	$(".vk1").removeClass("actived");
});
    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        if(value == "all")
        {
            $('.filter').show('1000');
        }
        else
        {
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});
</script>
<?php }else { redirect('logout');}?> 
