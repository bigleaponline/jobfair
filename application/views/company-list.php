<?php include('include/modi.php');?>
<div class="container-fluid mt right-content">
<div class="col-md-9 col-sm-8 site">
<div id="height-full">
<div class="container-fluid">
    <?php
     if($this->session->flashdata('employer'))
        {
          echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('employer').'</div>';   
        }
        if($this->session->flashdata('jd'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('jd').'</div>';   
        }
        ?>
        <?php include('include/main-sponsor-slider.php');?>
        <marquee class="marque-one" direction=”right” onmouseover="stop()" onmouseout="start()">Upcoming Event: ★ Mega Job fair at Holy Grace Academy, mala,thrissur on 02-feb-2020 ★</marquee>

  <div class="titles" style="border-top:none;">
  <h2>Company Details</h2>
  </div>
  <div class="e-r-f c-name">
    <div class="table-responsive">
  <table class="table" width="100%">
    <thead>
      <tr>
      <th scope="row">Sl.No.</th>
        <th scope="col">Company Name</th>
        <th scope="col">REQUIREMENT</th>
        <th scope="col">POSITION</th>
        <th scope="col">QUALIFICATION</th>
        <th scope="col">SALARY</th>
        <th scope="col">LOCATION</th>
       
      </tr>
    </thead>
    <tbody>
     <?php
        $i=0;
        foreach($company as $c)
        // {
        //     $i=$i+1;
        ?>
      <!-- <tr>
        <th scope="row" style="width:5%;"><?php echo $i;?></th>
        <a href="<?php echo base_url();?>jd/<?php echo $c->emp_id;?>"><td style="width:10%;"><?php echo $c->company_name;?></td></a>
        <td style="width:10%;"><?php echo $c->industry;?></td>
       
      </tr> -->
      <?php// }?>
      
        <tr>
           <td>01</td>
           <!-- <td><a href="#" target="_blank"><img src="http://www.indiamegajobfairs.com/assets/images/company-logos/tech-mahi.png" class="img-responsive center-block"></a></td>-->
           <td>TECH MAHINDRA</td>
      <td>IT & NON IT</td>
      <td>PROCESS ASSOCIATE ,CUSTOMER SUPPORT SERVICE</td>
      <td>ANY DEGREE/BTECH</td>
      <td>2.8-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      
      <tr>
           <td>02</td>
           <td>ARTECH ( FOR IBM & HP PROCESS)</td>
      <td>IT & NON IT</td>
      <td>SOFTWARE DEVELOPER,TECHNICAL SUPPORT</td>
      <td>BTECH</td>
      <td>3.6 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
       <tr>
           <td>03</td>
           <td>GENPACT</td>
      <td>IT & NON IT</td>
      <td>CUSTOMER CARE EXCECUTIVE</td>
      <td>ANY DEGREE/BTECH</td>
      <td>2.8-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
       <tr>
           <td>04</td>
           <td>EUREKA FORBES</td>
      <td>IT & NON IT</td>
      <td>MARKETING EXECUTIVE, CSS</td>
      <td>ANY DEGREE/BTECH</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      
       <tr>
           <td>05</td>
           <td>AXIS BANK</td>
      <td>NON IT</td>
      <td>BUSINESS DEVELOPMENT EXECUTIVE</td>
      <td>GRADUATE / MBA(MARKETING)</td>
      <td>1.8LPA-2LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
       <tr>
           <td></td>
           <td></td>
      <td>NON IT</td>
      <td>OFFICER-SALES</td>
      <td>GRADUATE / MBA(MARKETING)</td>
      <td>1.91LPA-2.2LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
 <tr>
           <td>06</td>
           <td>VOLKSOFT TECHNOLOGIES</td>
      <td>IT</td>
      <td>JAVA, DOT NET</td>
      <td>BSC.(CS,IT), BTECH (CS,IT), BCA, MCA, MSC.(CS,IT)</td>
      <td>-</td>
      <td>BANGALORE TELANGANA</td>
      </tr>
      <tr>
           <td>07</td>
           <td>CODILAR</td>
      <td>IT</td>
      <td>ASSOCIATE SOFTWARE DEVELOPERS</td>
      <td>BSC,BCA,MCA,BE-(CS/IT)</td>
      <td>2 LPA -3 LPA</td>
      <td>BANGLORE</td>
      </tr>
      <tr>
           <td>08</td>
           <td>POPCORN APPS</td>
      <td>IT & NON IT</td>
      <td>JAVA, SALESFORCE</td>
      <td>ANY DEGREE</td>
      <td>-</td>
      <td>HYDERABAD</td>
      </tr>

       <tr>
           <td>09</td>
           <td>UDHAYAM CONSTRUCTION</td>
      <td>CORE</td>
      <td>SITE ENGINEER</td>
      <td>ITI, DIPLOMA, BTECH</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      
       <tr>
           <td>10</td>
           <td>TOPPER</td>
      <td>NON IT</td>
      <td>BDE</td>
      <td>ANY DEGREE/BTECH</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
         <tr>
      <td>11</td>
           <td>JUST DIAL</td>
      <td>NON IT</td>
      <td>BDE</td>
      <td>ANY DEGREE/BTECH</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
       <tr>
      <td>12</td>
           <td>KARVY</td>
      <td>NON IT</td>
      <td>NON VOICE PROCESS, VOICE PROCESS, DATA ENTRY PROCESS</td>
      <td>ANY DEGREE/BTECH</td>
      <td>1.4-1.8 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      <tr>
      <td>13</td>
           <td>SECURE VALUE</td>
      <td>NON IT</td>
      <td>SECUIRTY,PICK UP BOY</td>
      <td>ANY DEGREE/BTECH</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      <tr>
      <td>15</td>
           <td><td>SE SAME SOFTWARE SOLUTIONS</td></td>
      <td>IT & NON IT</td>
      <td>BUSINESS ANALYST</td>
      <td>MBA
PG DIPLOMA IN BANKING OR BANKING RELATED SUBJECTS OR COMPUTER APPLICATION PREFERRED
</td>
      <td>25000-35000 PER MONTH</td>
      <td>CALICUT</td>
      </tr>
      <tr>
      <td></td>
           <td></td>
      <td>IT</td>
      <td>SOFTWARE ENGINEER TRAINEES (M)</td>
      <td>DIPLOMA IN COMPUTER SCIENCE/DEGREE IN COMPUTER SCIENCE OR ABOVE</td>
      <td>12000/</td>
      <td>CALICUT</td>
      </tr>
      <tr>
      <td></td>
           <td></td>
      <td>IT</td>
      <td>DOTNET SE –FULLSTACK</td>
      <td>MASTERS DEGREE IN COMPUTER SCIENCE/MCA/ B TECH</td>
      <td>15-30K INR PER MONTH</td>
      <td>CALICUT</td>
      </tr>
      <tr>
      <td></td>
            <td></td>
      <td>IT & NON IT</td>
      <td>TECHNICAL WRITER</td>
      <td>B.TECH/MCA/M.TECH/MSC COMPUTER SCIENCE (2 YEARS EXPERIENCE )</td>
      <td></td>
      <td>CALICUT</td>
      </tr>
      <tr>
      <td>16</td>
           <td>GRAPES INNOVATIVE SOLUTIONS</td>
      <td>IT</td>
      <td>SYSTEM DEVELOPER, FLUTTER DEVELOPER, DOT NET DEVELOPER</td>
      <td>ANY DEGREE/BTECH</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      <tr>
      <td>17</td>
           <td>TECHNAUREAS INFO SOLUTIONS</td>
      <td>IT</td>
      <td>BUSINESS DEVELOPMENT EXECUTIVES, DIGITAL MARKETING EXECUTIVES, PYTHON TRAINER, FREE ODOO INTERNSHIP (PYTHON)</td>
      <td>ANY DEGREE/BTECH</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      <tr>
      <td>18</td>
           <td>CODE SAP TECHNOLOGIES</td>
      <td>IT</td>
      <td>SAP ABAP</td>
      <td>BTECH</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
       <tr>
      <td>19</td>
           <td>FOXY FRAMES</td>
      <td>IT </td>
      <td>PRODUCTION</td>
      <td>GRADUATE</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      <tr>
      <td>20</td>
           <td>SPINOX INNOVATIONS</td>
      <td>IT</td>
      <td>PHP,DOTNET,ANDRIOD</td>
      <td>BTECH</td>
      <td>2.4 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      <tr>
      <td>21</td>
           <td>CREOPDIA BUSINESS</td>
      <td>IT </td>
      <td>SOFTWARE TESTING</td>
      <td>BTECH</td>
      <td>2.2-3.2 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
       <tr>
      <td>22</td>
           <td>INOX LEISURE LTD</td>
      <td>IT</td>
      <td>MARKETING EXCECUTIVE</td>
      <td>ANY DEGREE/BTECH</td>
      <td>2.4 LPA</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      <tr>
      <td>23</td>
           <td>HOME CREDIT</td>
      <td>NON IT</td>
      <td>SALES ASSOCIATE/AND TELE COLLECTIONS</td>
      <td>ANY DEGREE</td>
      <td>10000-16000 INR</td>
      <td>KERALA, BANGALORE</td>
      </tr>
       <tr>
      <td>24</td>
           <td>TATASKY </td>
      <td>NON IT</td>
      <td>RETAIL SALES PROMOTER </td>
      <td>ANY DEGREE/BTECH</td>
      <td>10000-16000 INR</td>
      <td>BANGALORE, KERALA</td>
      </tr>
       <tr>
      <td>25</td>
           <td>MULTIPLIER BRAND SOL. PVT. LTD. </td>
      <td>NON IT</td>
      <td>FSE-QR (Sales Executive)</td>
      <td>ANY DEGREE</td>
      <td>2.4 LPA</td>
      <td> KERALA</td>
      </tr>
      <tr>
      <td></td>
           <td></td>
      <td>NON IT</td>
      <td>FSE-POS (Senior Sales Executive)</td>
      <td>ANY DEGREE</td>
      <td>2.4 LPA</td>
      <td> KERALA</td>
      </tr>
      <tr>
      <td>26</td>
           <td>Future Group</td>
      <td>NON IT</td>
      <td>CUSTOMER SERVICE ASSOCIATE / TEAM MEMBER (FRESHER)</td>
      <td>12TH PASS & ABOVE</td>
      <td>-</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      <tr>
      <td></td>
           <td></td>
      <td>NON IT</td>
      <td>STORE EXECUTIVE / ASST TEAM LEADER(FRESHER / 0-1 YRS EXP IN FMCG RETAIL)</td>
      <td>GRADUATE</td>
      <td>-</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
      <tr>
      <td></td>
           <td></td>
      <td>NON IT</td>
      <td>STORE MANAGER(3 + YEARS IN HANDLING FMCG RETAIL STORE MBA 2020 PASSOUTS CAN BE CONSIDERED FOR MANAGEMENT TRAINEE - OPERATIONS)</td>
      <td>GRADUATE & ABOVE</td>
      <td>-</td>
      <td>KERALA, BANGALORE, HYDERABAD</td>
      </tr>
       <tr>
      <td>27</td>
           <td>M-SWIPE</td>
      <td>IT & NON IT</td>
      <td>AREA SALES OFFICER</td>
      <td>ANY DEGREE/BTECH</td>
        <td>10000-16000 INR</td>
      <td>KERALA, BANGALORE</td>
      </tr>
      <tr>
      <td>28</td>
           <td>JUST DIAL</td>
      <td>NON IT</td>
      <td>AMBASSADOR AND BUSINESS EXECUTIVE</td>
      <td>GRADUATE</td>
          <td>10000-16000 INR</td>
      <td>KERALA, BANGALORE</td>
      </tr>
      <tr>
      <td>29</td>
           <td>HGS</td>
      <td>NON IT</td>
      <td>CUSTOMER CARE EXECUTIVE</td>
      <td>ANY DEGREE/BTECH</td>
         <td>10000-16000 INR</td>
      <td>KERALA, BANGALORE</td>
      </tr>
      <tr>
      <td>30</td>
           <td>ALLSEC</td>
      <td>NON IT</td>
      <td>CUSTOMER CARE ASSOCIATE</td>
      <td>ANY DEGREE/BTECH</td>
        <td>10000-16000 INR</td>
      <td>KERALA, BANGALORE</td>
      </tr>
      <tr>
      <td>31</td>
           <td>INTELENET</td>
      <td>NON IT</td>
      <td>CUSTOMER CARE EXECUTIVE</td>
      <td>ANY DEGREE/BTECH</td>
        <td>10000-16000 INR</td>
      <td>KERALA, BANGALORE</td>
      </tr>
      <tr>
      <td>32</td>
           <td>BHARATH MATRIMONY</td>
      <td>NON IT</td>
      <td>CUSTOMER CARE EXECUTIVE</td>
      <td>ANY DEGREE/BTECH</td>
        <td>10000-16000 INR</td>
      <td>KERALA, BANGALORE</td>
      </tr>
      <tr>
      <td>33</td>
           <td>IKYA</td>
      <td>NON IT</td>
      <td>CUSTOMER CARE EXECUTIVE</td>
      <td>ANY DEGREE/BTECH</td>
        <td>10000-16000 INR</td>
      <td>KERALA, BANGALORE</td>
      </tr>
      <tr>
      <td>34</td>
           <td>SERCO</td>
      <td>NON IT</td>
      <td>BPO</td>
      <td>ANY DEGREE/BTECH</td>
        <td>14000-20000 INR</td>
      <td>KERALA, BANGALORE</td>
      </tr>
      <tr>
      <td>35</td>
           <td>LIMENZY TECHNOLOGIES</td>
      <td>IT & NON IT</td>
      <td>PHP DEVELOPERS</td>
      <td>ANY DEGREE(BOTH FRESHERS OR EXPERIENCED)</td>
        <td>-</td>
      <td>CALICUT</td>
      </tr>
        <tr>
      <td></td>
           <td></td>
      <td>IT & NON IT</td>
      <td>SEO</td>
      <td>ANY DEGREE(BOTH FRESHERS OR EXPERIENCED</td>
        <td>-</td>
      <td>CALICUT</td>
      </tr>
        <tr>
      <td></td>
           <td></td>
      <td>IT & NON IT</td>
      <td>CONTENT  WRITERS</td>
      <td>ANY DEGREE (BOTH FRESHERS OR EXPERIENCED, MUST HAVE EXCELLENT COMMUNICATION SKILL)</td>
        <td>-</td>
      <td>CALICUT</td>
      </tr>
        <tr>
      <td></td>
           <td></td>
      <td>IT & NON IT</td>
      <td>BDE</td>
      <td>ANY DEGREE(BOTH FRESHERS OR EXPERIENCED, MUST HAVE EXCELLENT COMMUNICATION SKILL)</td>
        <td>-</td>
      <td>CALICUT</td>
      </tr>
        <tr>
      <td></td>
           <td></td>
      <td>IT & NON IT</td>
      <td>WORDPRESS DEVELOPERS</td>
      <td>ANY DEGREE/BTECH</td>
        <td>-</td>
      <td>CALICUT</td>
      </tr>
      <tr>
      <td>36</td>
           <td>NIER</td>
      <td>NON IT</td>
      <td>FACULTY -PART TIME/ FULL TIME</td>
      <td>PG IN SCIENCE /MATHS/ ANY OTHER SUBJECTS (1 YEAR EXPERIENCE)</td>
        <td>10000-12000 INR</td>
      <td>ALL KERALA</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>NON IT</td>
      <td>TEACHERS -PART TIME /FULL TIME</td>
      <td>ANY UG/PG</td>
        <td>7000-10000 INR</td>
      <td>ALL KERALA</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>NON IT</td>
      <td>DISTRICT COORDINATORS- PART TIME /FULL TIME</td>
      <td>ANY UG/PG</td>
        <td>7000-14000 INR</td>
      <td>ALL KERALA</td>
      </tr>
      <tr>
      <td>37</td>
           <td>IGUARD</td>
      <td>IT & NON IT</td>
      <td>TECHNICAL TRAINEE</td>
      <td>EEE,ECE,POLY,ITI (EXP: FRESHER)</td>
        <td>-</td>
      <td>CALICUT</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>IT & NON IT</td>
      <td>TECHNICAL EXECUTIVE</td>
      <td>EEE, ECE,POLY,ITI
(EXP: 1- 4 YEAR)</td>
        <td>-</td>
      <td>ALL KERALA</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>IT</td>
      <td>SOFTWARE TRAINEE</td>
      <td>BCA, MCA, MSC.CS, BSC.CS, B.TECH CS
(EXP: 0 – 1 YEAR)</td>
        <td>-</td>
      <td>CALICUT</td>
      </tr>
        <tr>
      <td></td>
           <td></td>
      <td>NON IT</td>
      <td>HR ADMINISTATOR (F)</td>
      <td>MBA SPECIALISED IN HR
EXP:0 -1 YEAR</td>
        <td>-</td>
      <td>CALICUT</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>BDO (M&F)</td>
      <td>ANY DEGREE /MBA
(EXP:1 – 2 YEAR)</td>
      <td>EEE,ECE,POLY,ITI (EXP: FRESHER)</td>
        <td>-</td>
      <td>CALICUT</td>
      </tr>
       <tr>
      <td>38</td>
           <td>INMAKES SOFTWARE SOLUTIONS</td>
      <td>IT & NON IT</td>
      <td>PHP FRESHER</td>
      <td>BSC.(CS,IT), B-TECH (CS,IT), BCA, MCA, MSC.(CS,IT)</td>
        <td>8,000 -14,000/-</td>
      <td>TRIVANDRUM</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>IT</td>
      <td>ANDROID FRESHER</td>
      <td>BSC.(CS,IT), BTECH (CS,IT), BCA, MCA, MSC.(CS,IT)</td>
        <td>10,000 -14,000/-</td>
      <td>TRIVANDRUM</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>IT</td>
      <td>PYTHON FRESHER</td>
      <td>BSC.(CS,IT), BTECH (CS,IT), BCA, MCA, MSC.(CS,IT)</td>
        <td>8,000 -14,000/-</td>
      <td>TRIVANDRUM</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>IT & NON IT</td>
      <td>SOFTWARE MARKETING EXECUTIVE (FRESHER)</td>
      <td>BSC.(CS,IT), BTECH (CS,IT),
BCA, MCA, MSC.(CS,IT)</td>
        <td>16,000 -20,000/-</td>
      <td>ERNAKULAM, THRISSUR, KOTTAYAM, KOLLAM, ALAPPUZHA, WAYANAD, PALAKKAD, MALAPPURAM</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>IT & NON IT</td>
      <td>SOFTWARE MARKETING EXECUTIVE (1+ YEAR EXPERIENCE )</td>
      <td>BBA,MBA, BSC.(CS,IT), BTECH (CS,IT), BCA, MCA, MSC.(CS,IT)</td>
        <td>20,000 -42,000/-</td>
      <td>ERNAKULAM, THRISSUR, KOTTAYAM, KOLLAM, ALAPPUZHA, WAYANAD, PALAKKAD, MALAPPURAM,</td>
      </tr>
       <tr>
      <td></td>
           <td></td>
      <td>IT & NON IT</td>
      <td>JAVASCRIPT DEVELOPER (1+EXPERIENCE)</td>
      <td>BSC.(CS,IT), BTECH (CS,IT), BCA, MCA, MSC.(CS,IT)</td>
        <td>20,000 -40,000/-</td>
      <td>TRIVANDRUM</td>
      </tr>
      
      
      
  
      
 <!--    <tr>-->
 <!--       <th scope="row">3</th>-->
 <!--       <td>Malabar Group(Eham Digitals)-->
 <!--</td>-->
 <!--       <td>tt</td>-->
 <!--     </tr>-->
 <!--     <tr>-->
 <!--       <th scope="row">4</th>-->
 <!--       <td>Softfruit Solution-->
 <!--</td>-->
 <!--       <td>tt</td>-->
 <!--     </tr>-->
 <!--     <tr>-->
 <!--       <th scope="row">5</th>-->
 <!--       <td>Veuz Concept-->
 <!--</td>-->
 <!--       <td>tt</td>-->
 <!--     </tr>-->
 <!--     <tr>-->
 <!--       <th scope="row">6</th>-->
 <!--       <td>Shobika Wedding Mall-->
 <!--</td>-->
 <!--       <td>tt</td>-->
 <!--     </tr>-->
 <!--     <tr>-->
 <!--       <th scope="row">7</th>-->
 <!--       <td>Focus Infotech-->
 <!--</td>-->
 <!--       <td>tt</td>-->
 <!--     </tr>-->
 <!--     <tr>-->
 <!--       <th scope="row">8</th>-->
 <!--       <td>Star Health Insurance-->
 <!--</td>-->
 <!--       <td>tt</td>-->
 <!--     </tr>-->
 <!--     <tr>-->
 <!--       <th scope="row">9</th>-->
 <!--       <td>Proaims iot solution-->
 <!--</td>-->
 <!--       <td>tt</td>-->
 <!--     </tr>-->
 <!--     <tr>-->
 <!--       <th scope="row">10</th>-->
 <!--       <td>Spark support infotech-->
 <!--</td>-->
 <!--       <td>tt</td>-->
 <!--     </tr>-->
    </tbody>
  </table>
</div>
  </div>
  
</div>
</div>
   <div class="buttons">

    <div class="d-flex">
      <div class="path-tosignin path-tosignin2">
          <a href="http://www.indiamegajobfairs.com/" class="back-link"> <i class="fa fa-home animated flash infinite" title="Back" aria-hidden="true"></i></a> 
      <a href="http://www.indiamegajobfairs.com/sign-in">
          Candidate Registration <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i>
      </a>
      <a href="http://www.indiamegajobfairs.com/" class="back-linktwo"> <i class="fa fa-undo animated flash infinite" title="Back" aria-hidden="true" action="action" onclick="window.history.go(-1); return false;" value="Cancel"></i></a> 
    

    </div>
    </div>    
</div>
<?php include('include/main-sponsor-slider2.php');?>
         <?php include('include/co-sponsors.php');?>
          <?php include('include/local-sponser.php');?>
<?php include('include/microsite.php');?>
