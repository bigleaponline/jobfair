<?php include('include/modi.php');?>
<marquee class="marque-one" direction=”right” onmouseover="stop()" onmouseout="start()">Upcoming Event: ★ Mega Job fair at Holy Grace Academy, mala,thrissur on 02-feb-2020 ★</marquee>

<div class="container-fluid mt right-content clt-page">
  <?php
    if($this->session->flashdata('sponsor'))
        {
          echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('sponsor').'</div>';   
        }
    ?>
  <div class="col-md-9 col-sm-8 col-xs-12 site">
    <style>
    .d-flex { display:flex; }
</style>
    <div id="carousel-example-generic" class="carousel slide home-sliders" data-ride="carousel"> 
      <ol class="carousel-indicators">
        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
        <li data-target="#carousel-example-generic" data-slide-to="3"></li>
        <li data-target="#carousel-example-generic" data-slide-to="4"></li>
      </ol>
      <div class="carousel-inner" role="listbox"> 
        <div class="item b1 active">
          <h1>1500 New jobs for you</h1>
          <h5>Thrissur, Chennai, Bangalore, Coimbatore, Cochin, Trivandrum, Calicut</h5>
          <div class="row j-br-coutr">
            <div class="ctr-clum">
              <ul>
                <li> <img src="<?php echo base_url();?>assets/images/b1.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="6" data-speed="1500"></h2>
                  <p class="count-text ">Job Locations</p>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b2.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="1500" data-speed="1500"></h2>
                  <p class="count-text ">Jobs on Offer</p>
                </li>
                <a href="<?php echo base_url('thrissur/company-list');?>">
                <li> <img src="<?php echo base_url();?>assets/images/b3.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="50" data-speed="1500"></h2>
                  <p class="count-text ">Companies</p>
                </li>
                </a>
                <li class="loc-map-pop">
                    <div class="" data-js="open">
                     <img src="<?php echo base_url();?>assets/images/b4.png" class="animated jello  infinite">
                  <h2 data-speed="1500"><a href="#popup1" class="btnclick"> Thrissur</a></h2>
                  <p class="count-text ">Venue</p>
                  </div>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b10.png" class="animated jello  infinite">
                  <h2> Feb <span class="timer"  data-speed="1500" data-to="02"> </span></h2>
                  <p class="count-text ">02-Feb-2020</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="item b1 b2">
          <h1>1500 New jobs for you</h1>
          <h5>Thrissur, Chennai, Bangalore, Coimbatore, Cochin, Trivandrum, Calicut</h5>
          <div class="row j-br-coutr">
            <div class="ctr-clum">
              <ul>
                <li> <img src="<?php echo base_url();?>assets/images/b1.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="6" data-speed="1500"></h2>
                  <p class="count-text ">Job Locations</p>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b2.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="1500" data-speed="1500"></h2>
                  <p class="count-text ">Jobs on Offer</p>
                </li>
                <a href="<?php echo base_url('thrissur/company-list');?>">
                <li> <img src="<?php echo base_url();?>assets/images/b3.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="50" data-speed="1500"></h2>
                  <p class="count-text ">Companies</p>
                </li>
                </a>
                <li> <img src="<?php echo base_url();?>assets/images/b4.png" class="animated jello  infinite">
                  <h2 data-speed="1500"><a href="#popup1" class="btnclick"> Thrissur</a></h2>
                  <p class="count-text ">Venue</p>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b10.png" class="animated jello  infinite">
                 <h2> Feb <span class="timer"  data-speed="1500" data-to="02"></span></h2>
                  <p class="count-text ">02-Feb-2020</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="item b1 b3">
          <h1>1500 New jobs for you</h1>
          <h5>Thrissur, Chennai, Bangalore, Coimbatore, Cochin, Trivandrum, Calicut</h5>
          <div class="row j-br-coutr">
            <div class="ctr-clum">
              <ul>
                <li> <img src="<?php echo base_url();?>assets/images/b1.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="6" data-speed="1500"></h2>
                  <p class="count-text ">Job Locations</p>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b2.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="1500" data-speed="1500"></h2>
                  <p class="count-text ">Jobs on Offer</p>
                </li>
                <a href="<?php echo base_url('thrissur/company-list');?>">
                <li> <img src="<?php echo base_url();?>assets/images/b3.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="50" data-speed="1500"></h2>
                  <p class="count-text ">Companies</p>
                </li>
                </a>
                <li> <img src="<?php echo base_url();?>assets/images/b4.png" class="animated jello  infinite">
                  <h2 data-speed="1500"> <a href="#popup1" class="btnclick"> Thrissur</a></h2>
                  <p class="count-text ">Venue</p>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b10.png" class="animated jello  infinite">
                 <h2> Feb <span class="timer"  data-speed="1500" data-to="02"> </span></h2>
                  <p class="count-text ">02-Feb-2020</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="item b1 b4">
          <h1>1500 New jobs for you</h1>
          <h5>Thrissur, Chennai, Bangalore, Coimbatore, Cochin, Trivandrum, Calicut</h5>
          <div class="row j-br-coutr">
            <div class="ctr-clum">
              <ul>
                <li> <img src="<?php echo base_url();?>assets/images/b1.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="6" data-speed="1500"></h2>
                  <p class="count-text ">Job Locations</p>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b2.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="1500" data-speed="1500"></h2>
                  <p class="count-text ">Jobs on Offer</p>
                </li>
                <a href="<?php echo base_url('thrissur/company-list');?>">
                <li> <img src="<?php echo base_url();?>assets/images/b3.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="50" data-speed="1500"></h2>
                  <p class="count-text ">Companies</p>
                </li>
                </a>
                <li> <img src="<?php echo base_url();?>assets/images/b4.png" class="animated jello  infinite">
                  <h2 data-speed="1500"> <a href="#popup1" class="btnclick"> Thrissur</a></h2>
                  <p class="count-text ">Venue</p>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b10.png" class="animated jello  infinite">
                 <h2> Feb <span class="timer"  data-speed="1500" data-to="02"> </span></h2>
                  <p class="count-text ">02-Feb-2020</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div class="item b1 b5">
          <h1>1500 New jobs for you</h1>
          <h5>Thrissur, Chennai, Bangalore, Coimbatore, Cochin, Trivandrum, Calicut</h5>
          <div class="row j-br-coutr">
            <div class="ctr-clum">
              <ul>
                <li> <img src="<?php echo base_url();?>assets/images/b1.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="6" data-speed="1500"></h2>
                  <p class="count-text ">Job Locations</p>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b2.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="1500" data-speed="1500"></h2>
                  <p class="count-text ">Jobs on Offer</p>
                </li>
                <a href="<?php echo base_url('thrissur/company-list');?>">
                <li> <img src="<?php echo base_url();?>assets/images/b3.png" class="animated jello  infinite">
                  <h2 class="timer" data-to="50" data-speed="1500"></h2>
                  <p class="count-text ">Companies</p>
                </li>
                </a>
                <li> <img src="<?php echo base_url();?>assets/images/b4.png" class="animated jello  infinite">
                  <h2 data-speed="1500"> <a href="#popup1" class="btnclick"> Thrissur</a></h2>
                  <p class="count-text ">Venue</p>
                </li>
                <li> <img src="<?php echo base_url();?>assets/images/b10.png" class="animated jello  infinite">
                  <h2> Feb <span class="timer"  data-speed="1500" data-to="02"> </span></h2>
                  <p class="count-text ">02-Feb-2020</p>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> <span class="sr-only">Next</span> </a> </div>
    <?php //include('include/main-sponsor-slider.php');?>
     <?php include('include/co-sponsors.php');?>
    <div class="j-three-clms">
      <div class="col-md-4 col-sm-6">
        <div class="j-4clms">
          <div class="j-4clms-img-clm"> <img src="<?php echo base_url();?>assets/images/clt-1.jpg" class="img-responsive"> </div>
          <div class="j-4clms-itxt-clm">
            <h3>Who should attend the job fair?</h3>
            <div class="r-more"><a href="<?php echo base_url('thrissur/job1');?>">
              <p>Read more</p>
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6">
        <div class="j-4clms">
          <div class="j-4clms-img-clm"> <img src="<?php echo base_url();?>assets/images/clt-2.jpg" class="img-responsive"> </div>
          <div class="j-4clms-itxt-clm">
            <h3>How you should attend the job fair?</h3>
            <div class="r-more"><a href="<?php echo base_url('thrissur/job2');?>">
              <p>Read more</p>
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> </div>
          </div>
        </div>
      </div>
      <div class="col-md-4 col-sm-6">
        <div class="j-4clms">
          <div class="j-4clms-img-clm"> <img src="<?php echo base_url();?>assets/images/clt-3.jpg" class="img-responsive"> </div>
          <div class="j-4clms-itxt-clm">
            <h3>FAQ</h3>
            <div class="r-more"><a href="<?php echo base_url('thrissur/faq');?>">
              <p>Read more</p>
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> </div>
          </div>
        </div>
      </div>
    </div>
          <div class="home-circle hidden-xs">
      <div class="titles">
        <h2>Why should you attend the job fair?</h2>
      </div>
      <div class="holderCircle">
        <div class="round"></div>
        <div class="dotCircle"> <span class="itemDot active itemDot1" data-tab="1"> <i class="fa fa-search"></i> <span class="forActive"></span> </span> <span class="itemDot itemDot2" data-tab="2"> <i class="fa fa-id-card-o"></i> <span class="forActive"></span> </span> <span class="itemDot itemDot3" data-tab="3"> <i class="fa fa-handshake-o"></i> <span class="forActive"></span> </span> <span class="itemDot itemDot4" data-tab="4"> <i class="fa fa-file-text-o"></i> <span class="forActive"></span> </span> <span class="itemDot itemDot5" data-tab="5"> <i class="fa fa-calendar"></i> <span class="forActive"></span> </span> </div>
        <div class="contentCircle">
          <div class="CirItem title-box active CirItem1">
            <h2 class="title"><span>Job seeker vs Employer</span></h2>
            <p>Find your dream job ! Get hired faster ! We connect job seeker with employer needs.</p>
            <i class="fa fa-search"></i> </div>
          <div class="CirItem title-box CirItem2">
            <h2 class="title"><span>Resume distribution </span></h2>
            <p>We help right aspirant to reach the right employer.</p>
            <i class="fa fa-id-card-o"></i> </div>
          <div class="CirItem title-box CirItem3">
            <h2 class="title"><span>Best employers @ Calicut</span></h2>
            <p>Best employers offer Best careers ; 50 top companies from PAN India are coming to meet you.</p>
            <i class="fa fa-handshake-o"></i> </div>
          <div class="CirItem title-box CirItem4">
            <h2 class="title"><span>Assessments</span></h2>
            <p>Clearing 3 types of assessments win your dream job.</p>
            <i class="fa fa-file-text-o"></i> </div>
          <div class="CirItem title-box CirItem5">
            <h2 class="title"><span>Quality event</span></h2>
            <p>The first of its kind in Kerala for right opportunities to technical and non technical aspirants.</p>
            <i class="fa fa-calendar"></i> </div>
        </div>
      </div>
    </div>
    <div class="home-circle hidden-lg hidden-sm hidden-md">
      <div class="titles">
        <h2>Who should attend the job fair?</h2>
      </div>
      <div class="b-r-clm">
        <div class="ba-round-icon"><i class="fa fa-search"></i> </div>
        <h2>Job seeker vs Employer</h2>
        <p>Find your dream job ! Get hired faster ! We connect job seeker with employer needs.</p>
      </div>
      <div class="b-r-clm">
        <div class="ba-round-icon"><i class="fa fa-id-card-o"></i> </div>
        <h2>Resume distribution</h2>
        <p>We help right aspirant to reach the right employer.</p>
      </div>
      <div class="b-r-clm">
        <div class="ba-round-icon"><i class="fa fa-handshake-o"></i> </div>
        <h2>Best employers @ Calicut</h2>
        <p>Best employers offer Best careers ; 50 top companies from PAN India are coming to meet you.</p>
      </div>
      <div class="b-r-clm">
        <div class="ba-round-icon"><i class="fa fa-file-text-o"></i> </div>
        <h2>Assessments</h2>
        <p>Clearing 3 types of assessments win your dream job.</p>
      </div>
      <div class="b-r-clm">
        <div class="ba-round-icon"><i class="fa fa-calendar"></i> </div>
        <h2>Quality event</h2>
        <p>The first of its kind in Kerala for right opportunities to technical and non technical aspirants.</p>
      </div>
    </div>
    
    <div class="d-flex">
        
      <div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url('thrissur/employ-registration');?>" >Employer Registration <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
      <div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url('thrissur/sign-in');?>" >Candidate Registration <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
      <div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url('thrissur/sponsor-registration');?>" >Sponsors Registration <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
    </div>
    <?php include('include/main-sponsor-slider2.php');?>
    <div class="container-fluid">
      <div class="titles">
        <h2>Date Of Events</h2>
      </div>
      <table class="copmy-list">
  <thead>
    <tr>
      <th>Events</th>
      <th>Date</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td data-column="First Name">Basic Qualification Test dates:</td>
      <td data-column="Last Name">Monday Onwards</td>
    </tr>
    <!--<tr>
      <td data-column="First Name">50 Companies Announcement dates:</td>
      <td data-column="Last Name"></td>
    </tr>
    <tr>
      <td data-column="First Name">Technical Assessment dates:</td>
      <td data-column="Last Name"></td>
    </tr>-->
    <tr>
      <td data-column="First Name">Main Assessment dates ( Individual Companies): </td>
      <td data-column="Last Name">02-02-2020</td>
    </tr>
  </tbody>
 <a href="http://www.indiamegajobfairs.com/" class="back-link back-linkfull"> <i class="fa fa-home animated flash infinite" aria-hidden="true"></i></a>
</table>
      
          
   </div>

<div id="popup1" class="popup">
  <a href="#" class="close">&times;</a>
  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3926.3732334028564!2d76.27427651428341!3d10.231451671814684!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3b0804a97756143f%3A0x90b4944b9b43980f!2sHoly%20Grace%20Academy%20of%20Engineering!5e0!3m2!1sen!2sin!4v1579847065852!5m2!1sen!2sin" width="100%" height="300" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
</div>
<a href="#" class="close-popup"></a>

      
      <?php //include('include/local-sponser.php');?>
<!--       <div class="fter asft">-->
<!--<div class="row">-->
<!--<div class="col-xs-4 footer-menu">-->
<!--<ul class="footer-menu">-->
<!--<li><a href="#">Home</a></li>        -->
<!--<li><a href="#">About Us</a></li>        -->
<!--<li><a href="#">Contact Us</a></li>        -->
<!--</ul>        -->

<!--</div>-->

<!--<div class="col-xs-4 footer-address">-->
<!--<p>S I G N (Society for Integrated Growth of the Nation)-->
<!--Nakshatra Garden, Cheranallor P O, Cochin, Kerala - 683544</p>-->
<!--</div>-->

<!--<div class="col-xs-4 footer-address">-->
<!--<p>BigLeap Solutions Pvt Ltd.<br>-->
<!--4th Floor, Markaz Complex, Calicut, Kerala - 673004</p>-->
<!--</div>-->

<!--<div class="col-xs-12 copyright">-->
<!--<p class="copy-txt">Copyright © 2019 All Rights Reserved.</p> -->
<!--<ul class="social-media">-->
<!--<li><a href="https://www.facebook.com/indiajobfairs/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>-->
<!--<li><a href="https://www.instagram.com/india_mega_job_fairs/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--    </div>-->
<!--  </div>-->
<!--  <?php include('include/microsite.php');?>-->
  
