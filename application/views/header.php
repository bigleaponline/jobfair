<?php
if($this->session->userdata('cand_data'))
{
    $cand_data=$this->session->userdata('cand_data');
    $loginid=$this->session->userdata['cand_data']['cand_id'];
    $username=$this->session->userdata['cand_data']['cand_username'];
    $password=base64_decode($this->session->userdata['cand_data']['cand_password']);
}
?>
<!DOCTYPE html>
<html lang="en">
<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php echo $page_title;?></title>
<meta name="description" content="Opportunities await you at Kerala’s Biggest Job Fair ever. Look for prospects, which will help play an influential part in this mega government event destined to change the lives of the young bloods. The event aims to mark a potential difference in the employment rate of the state and will be addressing the long unaddressed grievance of unemployed and under-employed youth in and around Kerala and its neighbourhood. Better employment will lead to better livelihood and better lifestyle.">
<meta name="keywords" content="govtjobfair, Jobfairindia, Free Registration, Assessment test, Mega jobfair, MNC jobs, IT jobs, Fresher jobs, Online test, india jobs, Vacancies, jobfair, jobfest, careerfest, career, job, employment, fresher vacancy, recruitment, hr jobs, mba jobs, btech jobs, btech freshers, IT vacancies, software jobs, software vacancies, kerala jobs, core company jobs, bpo jobs, Mega jobfest" />
<meta name="indiamegajobfairs" content="1500 New jobs for you" />
<link href="<?php echo base_url(); ?>/assets/css/docs.theme.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/assets/css/owl.carousel.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/assets/css/owl.theme.default.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/assets/css/ciecle.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/assets/css/ani.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<!--<link rel="stylesheet" href="<?php echo base_url();?>assets/css/font-awesome-4.7.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600,700,800&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
<link href="<?php echo base_url(); ?>/assets/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/assets/css/rs.css" rel="stylesheet" type="text/css">

<!--local-->
<script src="<?php echo base_url(); ?>/assets/js/jquery-1.12.4.js"></script>
<script src="<?php echo base_url(); ?>/assets/js/jquery-ui.js"></script>
<!--<script src="<?php echo base_url(); ?>/assets/js/ej2.min.js"></script>-->
<link href="<?php echo base_url(); ?>/assets/css/material1.css" rel="stylesheet">

<link href="<?php echo base_url(); ?>/assets/css/dcalendar.picker.css" rel="stylesheet" type="text/css">
<!--end-->

<!--online-->
<!--<script src="https://code.jquery.com/jquery-1.12.4.js"></script>-->
<!--<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>-->

<!--<script src="https://cdn.syncfusion.com/ej2/dist/ej2.min.js"></script>-->
<!--<link href="https://cdn.syncfusion.com/ej2/material.css" rel="stylesheet">-->
<!--end-->

<link rel="apple-touch-icon" sizes="57x57" href="<?php echo base_url(); ?>assets/images/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?php echo base_url(); ?>assets/images/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?php echo base_url(); ?>assets/images/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url(); ?>assets/images/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?php echo base_url(); ?>assets/images/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url(); ?>assets/images/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?php echo base_url(); ?>assets/images/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url(); ?>assets/images/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/images/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?php echo base_url(); ?>assets/images/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/images/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?php echo base_url(); ?>assets/images/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>assets/images/favicon/favicon-16x16.png">
<link rel="manifest" href="<?php echo base_url(); ?>assets/images/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?php echo base_url(); ?>assets/images//ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
</head>
<body>
<style>
 .image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
 .image2 {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle2 {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
 .image3 {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}
.dis-flex { display:flex;}
.middle3 {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
.poli-img-1:hover .image {
  opacity: 1;
}
.poli-img-2:hover .image2 {
  opacity: 1;
}
.poli-img-3:hover .image3 {
  opacity: 1;
}
.poli-img-1:hover .middle {
  opacity: 1;
}
.poli-img-2:hover .middle2 {
  opacity: 1;
}
.poli-img-3:hover .middle3 {
  opacity: 1;
}

.poli-img-1 {
  position: relative;
}
.poli-img-2 {
  position: relative;
}
.poli-img-3 {
  position: relative;
}
.text { color:#fff; font-size:10px; }
.tooltip > .tooltip-inner { background-color: #73AD21!important; color: #fff!important; border: 1px solid green!important; padding: 10px!important; font-size: 14px!important; font-weight:bold; }
</style>
<div class="container-fluid navbar-new navbar-fixed-top hidden-xs">
  <div class="col-md-2 col-sm-2 col-xs-2">
    <div class="logo-clm"><a href="http://www.indiamegajobfairs.com/"><img src="<?php echo base_url();?>assets/images/logo-blue.png" alt="" class=""/></a></div>
  </div>
  <div class="col-md-7 col-sm-6 nav-clm-4 col-xs-7">
       <span class="beta"> Beta version..To be launched on 20th October 2019</span>
    <div class="col-md-8 col-sm-8">
                    <!-- <a href="http://www.indiamegajobfairs.com/"><img src="<?php echo base_url();?>assets/images/logo-blue.png" alt="" class="logo-blue"/></a>-->
    
         <!--<div class="main-c-img-clm"> <a href="http://bigleaponline.com/" > <img src="<?php echo base_url();?>assets/images/Logo/logo/gf 3.png"></a>
        <h7></h7>
      </div>-->
      
      <h2>India Mega Job fairs</h2>
      <a href="<?php echo base_url('sign-in');?>" ><h3 class="animated pulse infinite ani-one" style="display: inline-block; float: left; margin-right:15px;" title="Registration">Free Registration</h3></a>
      <a href="<?php echo base_url('jobfair/company-list');?>" ><h3 class="animated pulse infinite ani-two" style="float: right; ">Participating Companies</h3></a>
    </div>
<div class="col-md-4 nav-clm-4 col-sm-4 ">
<div class="c-clm-left row">
<div class="owl-carousel header-slide">
<div class="item">
<div class="c-clm-right">
<h5>Organisers</h5>
<div class="orga-image image-fst"><img src="<?php echo base_url();?>assets/images/menu/c4.jpg"></div>
<div class="orga-image"><img src="<?php echo base_url();?>assets/images/menu/c5.jpg"></div>
<!--<div class="orga-image image-lst"><img src="<?php echo base_url();?>assets/images/menu/c6.jpg"></div>
<div class="orga-image image-lst"><img src="<?php echo base_url();?>assets/images/menu/nc66.png"></div>-->
</div>
</div>
<div class="item">
<div class="c-clm-right">
<h5>Host</h5>
<div class="orga-image image-lst orga-image-change"><img src="<?php echo base_url();?>assets/images/menu/nc66.png"></div>
</div>
</div>
<div class="item">
<div class="c-clm-right">
<h5>Govt Partners</h5>
<div class="orga-image"><img src="<?php echo base_url();?>assets/images/menu/c1.jpg"></div>
<div class="orga-image"><img src="<?php echo base_url();?>assets/images/menu/c2.jpg"></div>
<div class="orga-image"><img src="<?php echo base_url();?>assets/images/menu/c3.jpg"></div>
</div>
</div>
</div>
</div>
</div>
  </div>
  <div class="col-md-3 col-sm-4 modi-logo-r  col-xs-3">
    <div class="col-md-4s">
      <div class="polition-img polition-img2 poli-img-1">
          <img src="<?php echo base_url();?>assets/images/menu/modi.jpg" class="menu-call"" class="image3"" class="image">
        <div class="rs-tooltip">
        <span>Shri.Narendra Modi <br>Prime Minister of India</span>  
        </div>
      </div>
    </div>
    <div class="col-md-8s">
      <div class="modi-logo-rig">
<div class="main-menu-holder">
<div class="main-menu-toggle">
<button class="menu-toggle">
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="main-menu-items">
<ul>
<li><a href="<?php echo base_url();?>">Home</a></li>
<?php if($this->session->userdata('cand_data'))
{?>
<form action="<?php echo base_url();?>jobfair_controller/login" method="post" id="myform">
    <input type="hidden" name="username" value="<?php echo $username;?>">
    <input type="hidden" name="password" value="<?php echo $password;?>">
    <li><a href="#" onclick="myhome()">My Home</a></li>
</form>
<li><a href="<?php echo base_url('logout');?>">Logout</a></li>
<?php } else{?>
<li><a href="<?php echo base_url('login');?>">Login</a></li>
<?php } ?>
<li><a href="<?php echo base_url('sign-in');?>">Sign Up</a></li>
</ul>
</div>
</div>
      </div>
      <div class="col-md-12 col-sm-12 dis-flex small-images ">
        <div class="polition-img poli-img-2" class="menu-call"><img src="<?php echo base_url();?>assets/images/menu/modi3.jpg" class="image2" >
            
        <div class="rs-tooltip">
        <span>Shri.Babul Supriyo<br>
            	Ministry of Environment, Forest and Climate Change</span>  
        </div>
        </div>
        <script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<script>
    function myhome()
    {
        document.getElementById("myform").submit();
    }
</script>

        <div class="polition-img poli-img-3"><img src="<?php echo base_url();?>assets/images/menu/modi2.jpg" class="menu-call" class="image3">
                    <div class="rs-tooltip">
        <span>Shri.A.N.Radhakrishnan <br>Chairman, SIGN</span>  
        </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!----------------------------------------------------------------------------------------------------------mobi-header---------------------------------------------------->
<div class="container-fluid hidden-md hidden-lg hidden-sm mob-nav navbar-fixed-top">
        <span class="beta">Beta version. To be launched on 20th October 2019</span>
<div class="logo-clm">
   <img src="<?php echo base_url();?>assets/images/logo.jpg" >
</div>    
  <div class="nav-text-clm">
      <img src="<?php echo base_url();?>assets/images/logo-blue.png" alt="" class="logo-blue two"/>
     <!--<div class="title-sponser"><a href="http://bigleaponline.com/" > <img src="<?php echo base_url();?>assets/images/Logo/logo/gf 3.png"></a>
      </div>-->
      <h2>India Mega Job fair</h2>
      <a href="<?php echo base_url('sign-in');?>" ><h3 class="animated pulse infinite">Free Registration</h3></a>
      <a href="<?php echo base_url('jobfair/company-list');?>" ><h3 class="animated pulse infinite">Participating Companies</h3></a>
  </div>
<div class="main-menu-holder">
<div class="main-menu-toggle">
<button>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
</div>
<div class="main-menu-items">
<ul>
<li><a href="<?php echo base_url();?>">Home</a></li>
<?php if($this->session->userdata('cand_data'))
{?>
<form action="<?php echo base_url();?>jobfair_controller/login" method="post" id="myform">
    <input type="hidden" name="username" value="<?php echo $username;?>">
    <input type="hidden" name="password" value="<?php echo $password;?>">
    <li><a href="#" onclick="myhome()">My Home</a></li>
</form>
<li><a href="<?php echo base_url('logout');?>">Logout</a></li>
<?php } else{?>
<li><a href="<?php echo base_url('login');?>">Login</a></li>
<?php } ?>
<li><a href="<?php echo base_url('sign-in');?>">Sign Up</a></li>
</ul>
</div>
</div>
    <?php include('include/modi.php');?>  
</div>