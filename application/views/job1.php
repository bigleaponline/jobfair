<?php include('include/modi.php');?>
<marquee class="marque-one" direction=”right” onmouseover="stop()" onmouseout="start()">★ Mega Job fair at holy grace engineering campus mala,thrissur on 02-feb-2020 ★</marquee>

<div class="container-fluid mt right-content">
  <div class="col-md-9 col-sm-8 site">
    <div class="container-fluid">
    <?php include('include/main-sponsor-slider.php');?>
      <div class="tp-clm">
        <div class="titles til-2"> 
          <h2>50 Companies 6 Sectors </h2>
        </div>
        <div class="full row">
          <div class="col-md-9">
            <div class="n-clms">
              <div class="col-sm-4 col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n2.png"></div>
                  <h2>Software<br>
                    Engineering</h2>
                  <p>10 companies</p>
                </div>
              </div>
              <div class="col-sm-4 col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n4.png"></div>
                  <h2>Marketing, HR & Finance Management </h2>
                  <p>10 companies</p>
                </div>
              </div>
              
              <div class="col-sm-4 col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n1.png"></div>
                  <h2>Mech, Electrical & Civil Engineering</h2>
                  <p>10 companies</p>
                </div>
              </div>
              <div class="col-sm-4 col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n3.png"></div>
                  <h2>Pharmacy & <br>Bio-tech</h2>
                  <p>5 companies</p>
                </div>
              </div>
              <div class="col-sm-4 col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n5.png"></div>
                  <h2>Accounting & Finance</h2>
                  <p>5 companies</p>
                </div>
              </div>
              <div class="col-sm-4 col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n6.png"></div>
                  <h2>BPO & others</h2>
                  <p>10 companies</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="n2-img"><img src="<?php echo base_url();?>assets/images/n-big.png" class="img-responsive"></div>
          </div>
        </div>
      </div>
      <div class="bt-clm">
      <div class="titles til-2" style="border-top:none;">
        <h2>Who should attend the Job Fair? </h2>
        <h5>Eligibility Criteria of the Candidates</h5>
        <hr>
      </div>
      <div class="q1"> <span> 
        <h6>Normal Category</h6>
        </span>
        <li>Higher Secondary Fail and Below Qualification</li>
      </div>
      <div class="q1"> <span> 
        <h6>Reserved Category </h6>
        </span> <br>
                    <strong>Category A:</strong><br>
          <span>•	10+2 <br>
•	Diploma less than 3 years
</span>
          
          <br>
          <br>
          <strong>Category B(MID TIER JOBS):</strong><br>
          <span>•	 Diploma(10+2+3)<br>
•	Any Graduates<br>
•	Any Post Graduates

</span>
          
                    <br> <br>
          <strong>Category C (CORE COMPANIES):</strong><br>
          <span>•	BTech/MTech- Civil/Mechanical/Electrical/Electronics

</span>
<br> <br>

          <strong>Category D (TOP TIER JOBS):</strong><br>
          <span>•	All 2020,2019 and 2018  pass out  candidates of below criteria:<br> 
BTech/M.tech-  Above 60% marks at all academic levels Electrical/Electronics/Mechanical/Civil [ For Core Jobs]<br>
Computer Science(CS)/Information Technology(IT) Electrical/Electronics/Mechanical [For IT Jobs]<br>
MCA/MSc above 60% ( IT,CS, Electronics, Electrical, Mechanical ( optional) - [ For IT Jobs]<br>
MBA -with 55% & above marks<br>
BTech with MBA 
</span>
          <br><br>
          
          <span> A one stop solution for every job aspirant of Kerala from every eligibility criteria to bag the job offer and build a career in one’s domain interest .</span>
          </div>
</div>
        
        <div class="buttons">

    <div class="d-flex">
      <div class="path-tosignin path-tosignin2">
          <a href="http://www.indiamegajobfairs.com/" class="back-link"> <i class="fa fa-home animated flash infinite" title="Back" aria-hidden="true"  title="Home"></i></a> 
      <a href="<?php if($this->uri->segment('1')=='calicut'){ echo base_url('calicut/sign-in');}else {echo base_url('sign-in');}?>" >
          Candidate Registration <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i>
      </a>
      <a href="http://www.indiamegajobfairs.com/" class="back-linktwo"> <i class="fa fa-undo animated flash infinite" title="Back" aria-hidden="true" action="action" onclick="window.history.go(-1); return false;" value="Cancel"></i></a> 
    

    </div>
    </div>    
</div>
    </div>
    <?php include('include/main-sponsor-slider2.php');?>
 <?php include('include/co-sponsors.php');?>
  <?php include('include/local-sponser.php');?>
