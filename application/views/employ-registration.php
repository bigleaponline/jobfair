<style type="text/css">
  .err{color:red;}
#errmsg0
{
color: red;
}
</style>
<!--nav-->
<?php include('include/modi.php');?>
<div class="container-fluid mt right-content">
     <?php 
        if($this->session->flashdata('employererror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('employererror').'</div>';   
        }
    ?>
  <div class="col-md-9 col-sm-8 site">
    <?php include('include/main-sponsor-slider.php');?>
      <div class="titles til-2" style="border-top:none;">
        <h2>Employer Registration</h2>
      </div>
      <div class="container-fluid jd-right">
      <div class="e-r-f">
        <form action="<?php echo base_url();?>jobfair_controller/employer_registration" method="post" enctype="multipart/form-data">
          <div class="row row-full">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Company Name: </label>
                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Enter Company Name" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Contact No: </label>
                <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Enter Contact No" required  onchange="profile(this.value)" maxlength="10">
                <span id="errmsg0"></span>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email ID: </label>
                <input type="email" class="form-control" name="email_id" id="email_id" placeholder="Enter Email ID" required onkeyup="validate(this.value);">
                <span  id="error_msg"></span>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Type of Industry: </label>
                <input type="text" class="form-control" name="industry" id="industry" placeholder="Enter Industry Type" required>
              </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                <label for="exampleInputEmail1"> Job Fair Venue : </label>
                <select class="form-control" name="venue"  id="venue" required>
                    <option value="" selected disabled>Select Venue</option>
                    <option <?php if($this->uri->segment('1')=='thrissur'){echo "selected='selected'";} ?> value="1">Thrissur</option>
                    <option <?php if($this->uri->segment('1')=='kochi'){echo "selected='selected'";} ?> value="2" disabled>Kochi</option>
                    <option value="3" disabled>Calicut & Kochi</option>
                    <option <?php if($this->uri->segment('1')=='northbanglore'){echo "selected='selected'";} ?> value="4" disabled>North Banglore</option>
                    <option <?php if($this->uri->segment('1')=='hyderbad'){echo "selected='selected'";} ?> value="5" disabled>Hyderabad</option>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Contact Person: </label>
                <input type="text" class="form-control" name="contact_person" id="contact_person" placeholder="Enter Contact Name" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Designation: </label>
                <input type="text" class="form-control" name="designation" id="designation" placeholder="Enter Designation" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Address: </label>
                <textarea class="form-control" name="address" id="address" placeholder="Enter Address" rows="4" required></textarea>
              </div>
            </div>
          </div>
          <div class="row row-full">
            <div class="titles t2 til-2" style="border-top:none;">
              <h2>Job Details</h2>
            </div>
            <div class="col-md-12 hidden-div jd" >
              <div class="col-md-11 col-xs-11">
                <input type="hidden" name="row_no" value="1" id="row_no">
               <div class="table-responsive">
                <table class="table table-bordered" id="myTable">
                  <thead>
                    <tr>
                      <th scope="col">Job Title</th>
                      <th scope="col">Experience</th>
                      <th scope="col">Salary</th>
                      <th scope="col">Job Description</th>
                    </tr>
                  </thead>
                  <tbody class="t-body">
                    <tr  class="row-periord" id="row1">
                      <td><input type="text" class="form-control title" id="job_title" name="job_title[]" placeholder="Enter Job Title"></td>
                      <td><input type="text" class="form-control experience" id="eperience" name="eperience[]" placeholder="Enter Experience"></td>
                      <td><input type="text" class="form-control salary" id="salary" name="salary[]" placeholder="Enter Salary"></td>
                      <td class="jb-txtarea"><textarea class="form-control description" id="job_description" name="job_description[]" placeholder="Enter Job Description"></textarea></td>
                    </tr>
                  </tbody>
                </table></div>
              </div>
              <div class="col-md-1 add-del-btn add-del-btn-mobi">
                <div class="add-btn"><i class="fa fa-plus" aria-hidden="true"></i></div>
                <div class="del-btn" style="display:none;" id="vk"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
              </div>
            </div>
          </div>
           <div class="col-md-12 bbb container em-file" style="margin-top: 17px;margin-bottom: 22px;">
               <div class="col-md-6 element" id="div_1">
              <label>Upload your JD</label><span class="imp" style="color: #d0242a;"> *</span>(txt or pdf file.)
              <!--<span class="btns  btn-file" style="margin-left: 20px;">Browse file-->
              <!--  <input type="file" name="jd[]">-->
              <!--  </span>-->
              <input type="file" name="jd[]">
               </div>
                <i class="fa fa-plus add" aria-hidden="true" onclick="addFile()"></i>
            </div> 
            <p>
          <button type="submit" class="sb-btn">Submit</button></p>
        </form>
      </div>
 </div>
 <?php include('include/main-sponsor-slider2.php');?>
 <?php include('include/co-sponsors.php');?>
  <?php include('include/local-sponser.php');?>
<!--     <div class="fter asft">-->
<!--<div class="row">-->
<!--<div class="col-xs-4 footer-menu">-->
<!--<ul class="footer-menu">-->
<!--<li><a href="#">Home</a></li>        -->
<!--<li><a href="#">About Us</a></li>        -->
<!--<li><a href="#">Contact Us</a></li>        -->
<!--</ul>        -->
<!--</div>-->
<!--<div class="col-xs-4 footer-address">-->
<!--<p>S I G N (Society for Integrated Growth of the Nation)-->
<!--Nakshatra Garden, Cheranallor P O, Cochin, Kerala - 683544</p>-->
<!--</div>-->
<!--<div class="col-xs-4 footer-address">-->
<!--<p>BigLeap Solutions Pvt Ltd.<br>-->
<!--4th Floor, Markaz Complex, Calicut, Kerala - 673004</p>-->
<!--</div>-->
<!--<div class="col-xs-12 copyright">-->
<!--<p class="copy-txt">Copyright © 2019 All Rights Reserved.</p> -->
<!--<ul class="social-media">-->
<!--<li><a href="https://www.facebook.com/indiajobfairs/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>-->
<!--<li><a href="https://www.instagram.com/india_mega_job_fairs/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>-->
<!--</ul>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--  </div>-->
<?php include('include/microsite.php');?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script>
    $(document).ready(function () {
    $("#contact_no").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
        {
            return false;
        }
        return true;
    });
});
</script>
<script type="text/javascript">
$(document).ready(function()
  {
    var tbody = $('#myTable').children('tbody');
   var table = tbody.length ? tbody : $('#myTable');
   $(".row-periord:last").find(".title");
   $('.add-btn').on('click', function()
   {
    var row_no = $('#row_no').val();
    var rows=$('#myTable tr').length;
    var $sr = ($(".row-periord").length + 1);
    var rowid = Math.random();
    if(rows>1)
    {
        $("#vk").show();
    }
    var $html = '<tr class="row-periord" id="' + rowid + '">' +
     
        '<td><input class="form-control"  type="input" name="job_title[]" id="job_title'+$sr+'" value=""/></td>' +
        '<td><input class="form-control"  type="input" name="eperience[]" id="eperience'+$sr+'" value=""/></td>' +
        '<td><input class="form-control"  type="input" name="salary[]" id="salary'+$sr+'" value=""/></td>' +
        '<td><textarea class="form-control"  name="job_description[]" id="job_description'+$sr+'"></textarea></td>' +
        '</tr>';
    $("#myTable").append($html);
    $("#row_no").val($sr);
    return false;
   });
   });
</script>
<script type="text/javascript">
  $('.del-btn').click(function(){
       $( ".row-periord:last").remove();
       var rows=$('#myTable tr').length;
       document.getElementById("row_no").value =rows - 1;
        if(rows <= 2)
        {
            $("#vk").hide();
        }
});
</script>
<script>
    $(document).ready(function(){
        $(".add").click(function(){
            var total_element = $(".element").length;
            var lastid = $(".element:last").attr("id");
            var split_id = lastid.split("_");
            var nextindex = Number(split_id[1]) + 1;
            var max = 5;
            $(".element:last").after("<div class='element' id='div_"+ nextindex +"'></div>");
            $("#div_" + nextindex).append('<label>Upload your JD</label><span class="" style="margin-left: 20px;"><input type="file" name="jd[]"></span>&nbsp;<span id="remove_' + nextindex + '" class="remove">X</span>');
        });
        $('.container').on('click','.remove',function(){
            var id = this.id;
            var split_id = id.split("_");
            var deleteindex = split_id[1];
            $("#div_" + deleteindex).remove();
        }); 
    });
</script>
<script language="JavaScript">
    var ev = /^([_a-zA-Z0-9-]+)(\.[_a-zA-Z0-9-]+)*@([a-zA-Z0-9-]+\.)+([a-zA-Z]{2,3})$/;
    var x = document.getElementById("error_msg");
    function validate(email_id){
    if(!ev.test(email_id))
        {
            x.innerHTML	= "Not a valid email";
            x.style.color = "red"
        }
    else
        {
            x.innerHTML	= "Ok!";
            x.style.color = "green"
        }
    }
</script>
<script type='text/javascript'>
$(document).ready(function() {
    $('#contact_no').keyup(function(e) {
        if (validatePhone('contact_no')) {
            $('#errmsg0').html('Valid Mobile Number');
            $('#errmsg0').css('color', 'green');
        }
        else {
            $('#errmsg0').html('Invalid Mobile Number');
            $('#errmsg0').css('color', 'red');
        }
    });
});
function validatePhone(contact_no) {
    var a = document.getElementById(contact_no).value;
    var filter = /[1-9]{1}[0-9]{9}/;
    if (filter.test(a)) {
        return true;
    }
    else {
        return false;
    }
}
</script>