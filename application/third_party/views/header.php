<?php
$cand_data=$this->session->userdata('cand_data');

?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Job Fair</title>
<link href="<?php echo base_url(); ?>/assets/css/ciecle.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/assets/css/ani.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600,700,800&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700,700i,900,900i&display=swap" rel="stylesheet">
<link href="<?php echo base_url(); ?>/assets/css/style.css" rel="stylesheet" type="text/css">
<link href="<?php echo base_url(); ?>/assets/css/rs.css" rel="stylesheet" type="text/css">
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>

 <style>


</style>
<!--<div class="container-fluid navbar-new navbar-fixed-top">-->
<!--  <div class="col-md-2 col-sm-2">-->
<!--    <div class="logo-clm"><img src="<?php echo base_url(); ?>assets/images/logo.jpg" class="img-responsive"></div>-->
<!--  </div>-->
<!--  <div class="col-md-7 col-sm-6">-->
<!--    <div class="col-md-12">-->
<!--      <div class="main-c-img-clm"> <img src="<?php echo base_url(); ?>assets/images/menu/oppo.png">-->
<!--        <h6>OPPO</h6>-->
<!--      </div>-->
<!--      <h2>Mega Job fair</h2>-->
<!--    </div>-->
<!--    <div class="col-md-12">-->
<!--      <div class="c-img-clm"><img src="<?php echo base_url(); ?>assets/images/menu/c1.jpg"></div>-->
<!--      <div class="c-img-clm"><img src="<?php echo base_url(); ?>assets/images/menu/c2.jpg"></div>-->
<!--      <div class="c-img-clm"><img src="<?php echo base_url(); ?>assets/images/menu/c3.jpg"></div>-->
<!--      <div class="c-img-clm"><img src="<?php echo base_url(); ?>assets/images/menu/c4.jpg"></div>-->
<!--      <div class="c-img-clm"><img src="<?php echo base_url(); ?>assets/images/menu/c5.jpg"></div>-->
<!--      <div class="c-img-clm"><img src="<?php echo base_url(); ?>assets/images/menu/c6.jpg"></div>-->
<!--    </div>-->
<!--  </div>-->
  
  
<!--  <div class="col-md-3 col-sm-4 modi-logo-r ">-->
<!--   <div class="col-md-4s">-->
<!--   <div class="polition-img polition-img2"><img src="<?php echo base_url(); ?>assets/images/menu/modi.jpg"></div>-->
<!--   </div>-->
<!--  <div class="col-md-8s">-->
<!--  <div class="col-md-12 modi-logo-rig">-->
<!--      <div class="polition-log"><a href="<?php echo base_url(); ?>logout">Log Out</a></div>-->
<!--      <div class="polition-log"><a href="<?php echo base_url(); ?>login">Login</a></div>-->
<!--      <div class="polition-log"><a href="<?php echo base_url(); ?>">Home</a></div>-->
<!--    </div>-->
<!--    <div class="col-md-12">-->
<!--        <div class="polition-img"><img src="<?php echo base_url(); ?>/assets/images/menu/modi3.jpg"></div>-->
<!--      <div class="polition-img"><img src="<?php echo base_url(); ?>/assets/images/menu/modi2.jpg"></div>-->
      
<!--    </div>-->
<!--  </div>-->
  
<!--  </div>-->
  
<!--</div>-->
<style>
 .image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
 .image2 {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle2 {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
 .image3 {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}
.dis-flex { display:flex;}
.middle3 {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
.poli-img-1:hover .image {
  opacity: 0.2;
}
.poli-img-2:hover .image2 {
  opacity: 0.2;
}
.poli-img-3:hover .image3 {
  opacity: 0.2;
}
.poli-img-1:hover .middle {
  opacity: 1;
}
.poli-img-2:hover .middle2 {
  opacity: 1;
}
.poli-img-3:hover .middle3 {
  opacity: 1;
}

.poli-img-1 {
  position: relative;
}
.poli-img-2 {
  position: relative;
}
.poli-img-3 {
  position: relative;
}
.text { color:#000; font-size:10px; }
</style>
<div class="container-fluid navbar-new navbar-fixed-top hidden-xs">
  <div class="col-md-2 col-sm-2 col-xs-2">
    <div class="logo-clm"><img src="<?php echo base_url();?>assets/images/logo.jpg" class="img-responsive" ></div>
  </div>
  <div class="col-md-7 col-sm-6 nav-clm-4 col-xs-7">
    <div class="col-md-8 ">
     <a href="http://bigleaponline.com/" >
         <div class="main-c-img-clm"> <img src="<?php echo base_url();?>assets/images/menu/oppo.png">
        <h6>OPPO</h6>
      </div>
      </a>
      <h2>India Mega Job fair</h2>
      <h3 class="animated pulse infinite">Free Registration</h3>
    </div>
    
    
    <div class="col-md-4 nav-clm-4 ">
        <div class="c-clm-left row">
      <div class="c-img-clm c-img-clm-img-1"><img src="<?php echo base_url();?>assets/images/menu/c1.jpg"></div>
      <div class="c-img-clm c-img-clm-img-2"><img src="<?php echo base_url();?>assets/images/menu/c2.jpg"></div>
      <div class="c-img-clm c-img-clm-img-3"><img src="<?php echo base_url();?>assets/images/menu/c3.jpg"></div>
      </div>
      <div class="c-clm-right row">
          <h5>Organisers</h5>
      <div class="c-img-clm c-img-clm-img-4"><img src="<?php echo base_url();?>assets/images/menu/c4.jpg"></div>
      <div class="c-img-clm c-img-clm-img-5"><img src="<?php echo base_url();?>assets/images/menu/c5.jpg"></div>
      <div class="c-img-clm c-img-clm-img-6"><img src="<?php echo base_url();?>assets/images/menu/c6.jpg"></div>
      </div>
    </div>
    
    
    
  </div>
  <div class="col-md-3 col-sm-4 modi-logo-r  col-xs-3">
    <div class="col-md-4s">
      <div class="polition-img polition-img2 poli-img-1"><img src="<?php echo base_url();?>assets/images/menu/modi.jpg" class="image">
        <div class="middle">
          <div class="text">Shri.Narendra Modi Prime Minister of India</div>
        </div>
      </div>
    </div>
    <div class="col-md-8s">
      <div class="col-md-12 modi-logo-rig">
        <div class="polition-log"><a href="<?php echo base_url('sign-in');?>">Sign Up</a></div>
        <div class="polition-log"><a href="<?php echo base_url('login');?>">Login</a></div>
        <div class="polition-log"><a href="<?php echo base_url();?>">Home</a></div>
      
        
      </div>
      <div class="col-md-12 dis-flex ">
        <div class="polition-img poli-img-2"><img src="<?php echo base_url();?>assets/images/menu/modi3.jpg" class="image2">
          <div class="middle2">
            <div class="text">Shri.Santhosh Gangwar Minister of State for Labour and Employment</div>
          </div>
        </div>
        <div class="polition-img poli-img-3"><img src="<?php echo base_url();?>assets/images/menu/modi2.jpg" class="image3">
          <div class="middle3">
            <div class="text">Shri.A.N.Radhakrishnan Chairman, SIGN</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="container-fluid hidden-md hidden-lg hidden-sm mob-nav">
    
<div class="logo-clm">
    
   <img src="<?php echo base_url();?>assets/images/logo.jpg" >
</div>    
  
  
  <div class="nav-text-clm">
     <div class="title-sponser"> <img src="<?php echo base_url();?>assets/images/menu/oppo.png">
        <h6>OPPO</h6>
      </div>
      
      <h2>India Mega Job fair</h2>
      <h3 class="animated pulse infinite">Free Registration</h3>
      
  </div>
  
  <div class="toggle-clm">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      </div>
      <div class="collapse navbar-collapse mob-nav-res" id="bs-example-navbar-collapse-1">
          
          <ul class="nav navbar-nav">
        <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Login</a></li>
        <li><a href="#">Sign Up</a></li>
        </ul>
      </div>
  
</div>

<!-- <div class="container-fluid mt">
<div class="col-md-9 col-sm-8 col-xs-12 site"> -->