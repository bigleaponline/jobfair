
<div class="container-fluid mt">
  <div class="col-md-9 col-sm-8 site"> 
    <div class="container-fluid">
    <?php 
        if($this->session->flashdata('registration'))
        {
          echo '<div class="alert alert-success" role="alert">'.$this->session->flashdata('registration').'</div>';   
        }
         if($this->session->flashdata('loginerror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('loginerror').'</div>';   
        }
    ?>
      <div class="titles" style="border-top:none;">
        <h2>Candidate Log In</h2>
        <!--<h5>Candidate Profile Creation</h5>-->
      </div>
      <div class="e-r-f e-r-f2 sign-in" id="forgot">
        <form action="<?php echo base_url();?>jobfair_controller/login" method="post">
          <div class="row" >
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">User Name: </label>
                <input type="text" class="form-control" placeholder="Enter Name" name="username">
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Password: </label>
                <input type="password" class="form-control" placeholder="Enter Password" name="password">
              </div>
            </div>
            <h6><a href="#" onclick="forgot()">Forget Password ?</a></h6>
          </div>
          <div class="s-btns">
            <button type="submit" class="sb-btn">Login</button>
            <button type="reset" class="sb-btn sb-btn2">Reset</button>
          </div>
        </form>
        <div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url('sign-in');?>">Click here to sign up <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
      </div>
      
      <div class="e-r-f e-r-f2 sign-in" id="login" style="display:none;">
        <form action="<?php echo base_url();?>jobfair_controller/forgotPassword" method="post">
          <div class="row" >
            <div class="col-md-12">
              <div class="form-group">
                <label for="exampleInputEmail1">Your Email ID: </label>
                <input type="text" class="form-control" placeholder="Enter Your Email ID" name="email">
              </div>
            </div>
            
            <h6><a href="#" onclick="login()">Go Back</a></h6>
          </div>
          <div class="s-btns">
            <button type="submit" class="sb-btn">Login</button>
            <button type="reset" class="sb-btn sb-btn2">Reset</button>
          </div>
        </form>
        <div class="path-tosignin path-tosignin2"> <a href="sign-in.php">Click here to sign in<i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
      </div>
    </div>
    <div class="fter"> Copyright © 2019 All Rights Reserved. </div>
  </div>

<!--web-area-end-->
  

  
<!-- js) --> 
<script>
    function forgot()
    {
        $("#login").show();
        $("#forgot").hide();
    }
    function login()
    {
        $("#login").hide();
        $("#forgot").show();
    }
</script>

