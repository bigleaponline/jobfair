....<body>

<style>
 .image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
 .image2 {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle2 {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
 .image3 {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}
.dis-flex { display:flex;}
.middle3 {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}
.poli-img-1:hover .image {
  opacity: 0.2;
}
.poli-img-2:hover .image2 {
  opacity: 0.2;
}
.poli-img-3:hover .image3 {
  opacity: 0.2;
}
.poli-img-1:hover .middle {
  opacity: 1;
}
.poli-img-2:hover .middle2 {
  opacity: 1;
}
.poli-img-3:hover .middle3 {
  opacity: 1;
}

.poli-img-1 {
  position: relative;
}
.poli-img-2 {
  position: relative;
}
.poli-img-3 {
  position: relative;
}
.text { color:#000; font-size:10px; }
</style>
<div class="container-fluid navbar-new navbar-fixed-top">
  <div class="col-md-2 col-sm-2 col-xs-2">
    <div class="logo-clm"><img src="<?php echo base_url();?>assets/images/logo.jpg" class="img-responsive" ></div>
  </div>
  <div class="col-md-7 col-sm-6 nav-clm-4 col-xs-7">
    <div class="col-md-8 ">
      <a href="http://www.bigleaponline.com"?><div class="main-c-img-clm"> <img src="<?php echo base_url();?>assets/images/menu/oppo.png">
        <h6>OPPO</h6>
      </div></a>
      <h2>Mega Job fair</h2>
      <h3 class="animated pulse infinite">Free Registration</h3>
    </div>
    
    
    <div class="col-md-4 col-xs-4 nav-clm-4">
        <div class="c-clm-left row">
      <div class="c-img-clm c-img-clm-img-1"><img src="<?php echo base_url();?>assets/images/menu/c1.jpg"></div>
      <div class="c-img-clm c-img-clm-img-2"><img src="<?php echo base_url();?>assets/images/menu/c2.jpg"></div>
      <div class="c-img-clm c-img-clm-img-3"><img src="<?php echo base_url();?>assets/images/menu/c3.jpg"></div>
      </div>
      <div class="c-clm-right row">
          <h5>Organisers</h5>
      <div class="c-img-clm c-img-clm-img-4"><img src="<?php echo base_url();?>assets/images/menu/c4.jpg"></div>
      <div class="c-img-clm c-img-clm-img-5"><img src="<?php echo base_url();?>assets/images/menu/c5.jpg"></div>
      <div class="c-img-clm c-img-clm-img-6"><img src="<?php echo base_url();?>assets/images/menu/c6.jpg"></div>
      </div>
    </div>
    
    
    
  </div>
  <div class="col-md-3 col-sm-4 col-xs-3 modi-logo-r ">
    <div class="col-md-4s">
      <div class="polition-img polition-img2 poli-img-1"><img src="<?php echo base_url();?>assets/images/menu/modi.jpg" class="image">
        <div class="middle">
          <div class="text">Shri.Narendra Modi Prime Minister of India</div>
        </div>
      </div>
    </div>
    <div class="col-md-8s">
      <div class="col-md-12 modi-logo-rig">
        <div class="polition-log"><a href="<?php echo base_url('logout');?>">Log Out</a></div>
        <div class="polition-log"><a href="<?php echo base_url('login');?>">Login</a></div>
        <div class="polition-log"><a href="<?php echo base_url();?>">Home</a></div>
      </div>
      <div class="col-md-12 dis-flex ">
        <div class="polition-img poli-img-2"><img src="<?php echo base_url();?>assets/images/menu/modi3.jpg" class="image2">
          <div class="middle2">
            <div class="text">Shri.Santhosh Gangwar Minister of State for Labour and Employment</div>
          </div>
        </div>
        <div class="polition-img poli-img-3"><img src="<?php echo base_url();?>assets/images/menu/modi2.jpg" class="image3">
          <div class="middle3">
            <div class="text">Shri.A.N.Radhakrishnan Chairman, SIGN</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
