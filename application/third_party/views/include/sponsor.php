<!--web-area-end-->
  <div class="col-md-3 col-sm-4 col-xs-12 adds hidden-xs">
    <div class="main-spo hidden-xs">
      <h2>main sponsors</h2>
      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="30000">
        <div class="carousel-inner">
          <div class="item mai-spo-item active">
            <div class="main-spo-clm-1 hidden-xs">
              <div class="m-logos-left">
                <div class="m-logos" data-toggle="modal" data-target="#myModal"> <img src="<?php echo base_url();?>assets/images/main-sponsers/oppo.png"> </div>
                <div class="m-logos" > <img src="<?php echo base_url();?>assets/images/main-sponsers/vi.jpg"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/lg.jpg"> </div>
              </div>
              
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/sam.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/ip.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/voda.png"> </div>
              </div>
            </div>
          </div>
          <div class="item mai-spo-item">
            <div class="main-spo-clm-1 hidden-xs">
              <div class="m-logos-left">
                <div class="m-logos" data-toggle="modal" data-target="#myModal"> <img src="<?php echo base_url();?>assets/images/main-sponsers/oppo.png"> </div>
                <div class="m-logos" > <img src="<?php echo base_url();?>assets/images/main-sponsers/vi.jpg"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/lg.jpg"> </div>
              </div>
              
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/sam.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/ip.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/voda.png"> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="co-spo hidden-xs">
      <h2>co sponsors</h2>
      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">
        <div class="carousel-inner">
          <div class="item co-spo-item active">
            <div class="main-spo-clm-1">
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/1.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/2.png"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/1.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/2.png"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/4.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/5.jpg"> </div>
              </div>
            </div>
          </div>
          <div class="item co-spo-item">
            <div class="main-spo-clm-1">
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/oppo.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/vi.jpg"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/oppo.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/vi.jpg"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/sam.png"> </div>
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/ip.png"> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="co-spo spo hidden-xs">
      <h2>sponsors</h2>
      <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="10000">
        <div class="carousel-inner">
          <div class="item co-spo-item active">
            <div class="main-spo-clm-1">
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/1.png"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/1.png"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/4.png"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/4.png"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/4.png"> </div>
              </div>
            </div>
          </div>
          <div class="item co-spo-item">
            <div class="main-spo-clm-1">
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/oppo.png"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/vi.jpg"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/main-sponsers/ip.png"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/4.png"> </div>
              </div>
              <div class="m-logos-left">
                <div class="m-logos"> <img src="<?php echo base_url();?>assets/images/co-sponsers/4.png"> </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <!--Pc-view-end-->
    
    <!--main-sponsers-end-->
    

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog sr-model-width" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="<?php echo base_url();?>assets/images/microsite/microsite.jpg" class="img-responsive">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">View Site</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog sr-model-width" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <img src="images/microsite/microsite2.jpg" class="img-responsive">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">View Site</button>
      </div>
    </div>
  </div>
</div>
<!--model-end-->