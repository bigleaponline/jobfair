
<!--nav-->
<style>
#errmsg0
{
color: red;
}
</style>
<div class="container-fluid mt">imageerror
<div class="col-md-9 col-sm-8 site">

<!--1e-->
<div class="container-fluid">
    <?php 
        if($this->session->flashdata('imageerror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('imageerror').'</div>';   
        }
        if($this->session->flashdata('sponsorererror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('sponsorererror').'</div>';   
        }
    ?>
  <div class="titles" style="border-top:none;">
    <h2>Sponser Registration</h2>
    <h5>Sponsor Details</h5>
  </div>
  <div class="e-r-f">
    <!--<form action="<?php echo base_url();?>jobfair_controller/sponsor_registration" method="post" name="myForm" enctype="multipart/form-data">-->
     <?php echo form_open_multipart('jobfair_controller/sponsor_registration/','id="my_formm"'); ?>
      <div class="row">
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Brand Name : </label>
            <input type="text" class="form-control" placeholder="Enter Company Name" name="brand_name" id="brand_name">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Email ID : </label>
            <input type="email" class="form-control" placeholder="Enter Email ID" name="email_id" id="email_id">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Address: </label>
            <textarea class="form-control" placeholder="Enter Address" rows="4" name="address" id="address"></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Which Sponsership Opportunity are you interested in ? : </label>
            <input type="text" class="form-control" placeholder="Enter Interest" name="interest" id="interest">
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label for="exampleInputEmail1">Contact No : </label>
            <input type="text" class="form-control" placeholder="Enter Contact No" name="contact_no" id="contact_no" autocomplete="off">
            <span id="errmsg0"></span>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Contact Person : </label>
            <input type="text" class="form-control" placeholder="Enter Contact Person" name="contact_person" id="contact_person">
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Comments or Question: </label>
            <textarea class="form-control"  rows="4" name="comments" id="comments"></textarea>
          </div>
          <div class="form-group bbb">
            <label>Upload your Logo</label>
            <!--<p><span class="btns  btn-file">Browse file-->
            <!--  <input type="file">-->
            <!--  </span></p>-->
            <input type="file" id="logo" name="logo" size="20" />
            <input type="hidden" id="src" name="src" value="" />
          </div>
        </div>
      </div>
      <div class="s-btns">
        <!--<button type="submit" class="sb-btn">-->
        <!--<a href="welcome.php" style="color:#fff;">Submit</a>-->
        <button type="submit" class="sb-btn">Submit</button>
        </button>
        <button type="reset" class="sb-btn sb-btn2">Reset</button>
      </div>
    <!--</form>-->
    <?php echo form_close(); ?>
  </div>
</div>
<div class="fter"> Copyright © 2019 All Rights Reserved. </div>
</div>
<!--web-area-end-->



<!-- js) -->


<script>
$(document).ready(function()
  {
    $("#contact_no").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
        {
            $("#errmsg0").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
});
    </script>
