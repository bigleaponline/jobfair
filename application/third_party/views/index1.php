
<div class="container-fluid mt">
<div class="col-md-9 col-sm-8 col-xs-12 site">

<div class="j-banner row">
  <h1>Get 1500 New Latest Jobs</h1>
  <h5>Chennai, Bangalore, Coimbatore, Cochin, Trivandrum, Calicut</h5>
  <div class="row j-br-coutr">
    <div class="ctr-clum">
      <ul>
        <li> <img src="./assets/images/b1.png" class="animated jello  infinite">
          <h2 class="timer" data-to="6" data-speed="1500"></h2>
          <p class="count-text ">Job Location</p>
        </li>
        <li> <img src="./assets/images/b2.png" class="animated jello  infinite">
          <h2 class="timer" data-to="1500" data-speed="1500"></h2>
          <p class="count-text ">Jobs on Offer</p>
        </li>
        <li> <img src="./assets/images/b3.png" class="animated jello  infinite">
          <h2 class="timer" data-to="50" data-speed="1500"></h2>
          <p class="count-text "><a href="<?php echo base_url('company-list');?>">Companies</a></p>
        </li>
        <li> <img src="./assets/images/b4.png" class="animated jello  infinite">
          <h2 data-speed="1500"> Calicut</h2>
          <p class="count-text ">Venue</p>
        </li>
      </ul>
    </div>
  </div>
</div>
<!--banner-end-->
<div class="home-circle hidden-xs">
  <div class="titles">
    <h2>Why should you attend the job fair?</h2>
  </div>
  <div class="holderCircle">
    <div class="round"></div>
    <div class="dotCircle"> <span class="itemDot active itemDot1" data-tab="1"> <i class="fa fa-search"></i> <span class="forActive"></span> </span> <span class="itemDot itemDot2" data-tab="2"> <i class="fa fa-id-card-o"></i> <span class="forActive"></span> </span> <span class="itemDot itemDot3" data-tab="3"> <i class="fa fa-handshake-o"></i> <span class="forActive"></span> </span> <span class="itemDot itemDot4" data-tab="4"> <i class="fa fa-file-text-o"></i> <span class="forActive"></span> </span> <span class="itemDot itemDot5" data-tab="5"> <i class="fa fa-calendar"></i> <span class="forActive"></span> </span> </div>
    <div class="contentCircle">
      <div class="CirItem title-box active CirItem1">
        <h2 class="title"><span>Job seeker vs Employer</span></h2>
        <p>Find your dream job ! Get hired faster ! We connect job seeker with employer needs.</p>
        <i class="fa fa-search"></i> </div>
      <div class="CirItem title-box CirItem2">
        <h2 class="title"><span>Resume distribution </span></h2>
        <p>We help right aspirant to reach the right employer.</p>
        <i class="fa fa-id-card-o"></i> </div>
      <div class="CirItem title-box CirItem3">
        <h2 class="title"><span>Best employers @ Calicut</span></h2>
        <p>Best employers offer Best careers ; 50 top companies from PAN India are coming to meet you.</p>
        <i class="fa fa-handshake-o"></i> </div>
      <div class="CirItem title-box CirItem4">
        <h2 class="title"><span>Assessments</span></h2>
        <p>Clearing 3 types of assessments win your dream job.</p>
        <i class="fa fa-file-text-o"></i> </div>
      <div class="CirItem title-box CirItem5">
        <h2 class="title"><span>Quality event</span></h2>
        <p>The first of its kind in Kerala for right opportunities to technical and non technical aspirants.</p>
        <i class="fa fa-calendar"></i> </div>
    </div>
  </div>
</div>
<!--pc-view-end-->
<div class="home-circle hidden-lg hidden-sm hidden-md">
  <div class="titles">
    <h2>Who should attend job fair?</h2>
  </div>
  <div class="b-r-clm">
    <div class="ba-round-icon"><i class="fa fa-search"></i> </div>
    <h2>Job seeker vs Employer</h2>
    <p>Find your dream job ! Get hired faster ! We connect job seeker with employer needs.</p>
  </div>
  <div class="b-r-clm">
    <div class="ba-round-icon"><i class="fa fa-id-card-o"></i> </div>
    <h2>Resume distribution</h2>
    <p>We help right aspirant to reach the right employer.</p>
  </div>
  <div class="b-r-clm">
    <div class="ba-round-icon"><i class="fa fa-handshake-o"></i> </div>
    <h2>Best employers @ Calicut</h2>
    <p>Best employers offer Best careers ; 50 top companies from PAN India are coming to meet you.</p>
  </div>
  <div class="b-r-clm">
    <div class="ba-round-icon"><i class="fa fa-file-text-o"></i> </div>
    <h2>Assessments</h2>
    <p>Clearing 3 types of assessments win your dream job.</p>
  </div>
  <div class="b-r-clm">
    <div class="ba-round-icon"><i class="fa fa-calendar"></i> </div>
    <h2>Quality event</h2>
    <p>The first of its kind in Kerala for right opportunities to technical and non technical aspirants.</p>
  </div>
</div>
<!--mobile-view-end-->
<div class="j-three-clms">
  <div class="col-md-4 col-sm-6">
    <div class="j-4clms">
      <div class="j-4clms-img-clm"> <img src="./assets/images/w1.jpg" class="img-responsive"> </div>
      <div class="j-4clms-itxt-clm">
        <h3>Who should attend job fair?</h3>
        <div class="r-more"><a href="<?php echo base_url('job1');?>">
          <p>Read more</p>
          <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-6">
    <div class="j-4clms">
      <div class="j-4clms-img-clm"> <img src="./assets/images/w2.jpg" class="img-responsive"> </div>
      <div class="j-4clms-itxt-clm">
        <h3>How you should attend the job fair?</h3>
        <div class="r-more"><a href="<?php echo base_url('job2');?>">
          <p>Read more</p>
          <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-6">
    <div class="j-4clms">
      <div class="j-4clms-img-clm"> <img src="./assets/images/w3.jpg" class="img-responsive"> </div>
      <div class="j-4clms-itxt-clm">
        <h3>FAQ</h3>
        <div class="r-more"><a href="job3.php">
          <p>Read more</p>
          <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a> </div>
      </div>
    </div>
  </div>
</div>
<!--why-how-assment-clms-end-->

<div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url('employ-registration'); ?>" >Employer Registration <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
<div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url('sponsor-registration'); ?>" >Sponsor Registration <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
<div class="container-fluid">
  <div class="titles">
    <h2>Date Of Events</h2>
  </div>
  <div class="j-dates">
    <div class="row1 row">
      <div class="j-e"> Events </div>
      <div class="j-d">Date </div>
    </div>
    <div class="row2 row">
      <div class="j-e"> Preliminary Assessements dates: </div>
      <div class="j-d">Date </div>
    </div>
    <div class="row2 row">
      <div class="j-e"> 50 Companies Announcement dates: </div>
      <div class="j-d">Date </div>
    </div>
    <div class="row2 row">
      <div class="j-e"> Main Assssments dates (Co-Cubes): </div>
      <div class="j-d">Date </div>
    </div>
    <div class="row2 row">
      <div class="j-e"> Main Assessments dates ( Individual Companies): </div>
      <div class="j-d">Date </div>
    </div>
  </div>
  <div class="fter"> Copyright © 2019 All Rights Reserved.</div>
</div>
<!--event-date-end-->
</div>
<!-- <?php //include 'assets/include/sponsor.php'; ?>

js)

<?php //include 'assets/include/script.php'; ?>
 -->