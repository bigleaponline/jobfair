<style>
  .field-icon {
  float: right;
  margin-left: -25px;
  margin-top: -25px;
  position: relative;
  z-index: 2;
}

.container{
  padding-top:50px;
  margin: auto;
}
#errmsg0,#errmsg1
{
color: red;
}
</style>

<div class="container-fluid mt">
  <div class="col-md-9 col-sm-8 site">
   
    <div class="container-fluid">
      <?php 
        if($this->session->flashdata('errorregistration'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('errorregistration').'</div>';   
        }
        if($this->session->flashdata('password'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('password').'</div>';   
        }
    ?>
      <div class="titles" style="border-top:none;">
        <h2>Candidate Registration</h2>
        <h5>Candidate Profile Creation</h5>
      </div>
       <?php $length = 6;
        $randomString = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
        ?>
      <div class="e-r-f">
        <form action="<?php echo base_url();?>jobfair_controller/candidateRegistration" method="post" name="myForm">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Name : </label>
                <input type="text" class="form-control" placeholder="Enter Name" name="name" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Date Of Birth : </label>
                <input type="text" class="form-control" placeholder="Enter Date Of Birth" name="dob" id="datepicker" required>
              </div>
              <div class="form-group">
                <label for="exampleFormControlSelect1">Gender : </label>
                <select class="form-control" id="exampleFormControlSelect1" name="gender" required>
                  <option value="" selected disabled>Select Gender</option>
                  <option value="male">Male</option>
                  <option value="female">Female</option>
                </select>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Phone No : </label>
                <input type="text" class="form-control" placeholder="Enter Phone No" name="mobile" id="mobile" required autocomplete="off">&nbsp;
                <span id="errmsg0"></span>
              </div>
              
              <div class="form-group">
                <label for="exampleInputEmail1">Whats App No : </label>
                <input type="text" class="form-control" placeholder="Enter Whats App No" name="whatsapp" id="whatsapp"  autocomplete="off">
                 <span id="errmsg1"></span>
              </div>
               <div class="form-group">
                <label for="exampleInputEmail1">Username : </label>
                <input type="text" class="form-control" placeholder="Enter Password" name="username" value="<?php echo $randomString;?>" readonly >
              </div>
           
           
              <div class="form-group">
                <label for="exampleInputEmail1">Email ID : </label>
                <input type="email" class="form-control" placeholder="Enter Email ID" name="email" required>
              </div>
               </div>
               <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">highst academic Qualification : </label>
                <select class="form-control" name="qualification" required onchange="getStream(this.value)">
                  <option value="" selected disabled>Selelct Higher Education</option>
                  <?php foreach($qualification as $q)
                  {?>
                    <option value="<?php echo $q->qualification_id;?>"><?php echo $q->qualification;?></option>
                  <?php } ?>
                </select>
              </div>
               <div class="form-group">
                <label for="exampleInputEmail1">Stream : </label>
                <select class="form-control" name="stream"  id="stream">
                  <option value="" selected disabled>Selelct Stream</option>
                 
                </select>
              </div>
               <div class="form-group">
                <label for="exampleInputEmail1">Marks in %</label>
                <input type="text" class="form-control" placeholder="Enter Marks in %" name="marks" required>
              </div>
               <div class="form-group">
                <label for="exampleInputEmail1">year of Passout : </label>
                <input type="text" class="form-control" placeholder="Enter Year" name="passoutyear" required>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Password : </label>
                <input type="password" class="form-control" placeholder="Enter Password" name="password" id="password" required>
                 <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="myFunction()"></span>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Conform Password : </label>
                <input type="password" class="form-control" placeholder="Enter Password" name="confirmpassword" id="password1" required>
                <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password" onclick="myFunction1()"></span>
              </div>
            </div>
          </div>
          <div class="s-btns">
            <button type="submit" class="sb-btn">Submit</button>
            <button type="reset" class="sb-btn sb-btn2">Reset</button>
          </div>
        </form>
        <div class="path-tosignin path-tosignin2"> <a href="<?php echo base_url(); ?>login" >Click here to Log in <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
      </div>
    </div>
<div class="fter"> Copyright © 2019 All Rights Reserved. </div>
  </div>
  <script>
  $(document).ready(function () {
    $("#mobile").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
        {
            $("#errmsg0").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
     $("#whatsapp").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) 
        {
            $("#errmsg1").html("Digits Only").show().fadeOut("slow");
            return false;
        }
    });
});
    function getStream(val) 
    {
        $.ajax({
              type: "POST",
              url: "<?php echo base_url();?>jobfair_controller/getStream",
              data:'data='+val,
              success: function(data)
              {
                $("#stream").html(data);
                //getFee();
              }
        });
    }
    function myFunction() 
    {
      var x = document.getElementById("password");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
    function myFunction1() 
    {
      var x = document.getElementById("password1");
      if (x.type === "password") {
        x.type = "text";
      } else {
        x.type = "password";
      }
    }
</script>
<script type="text/javascript" charset="utf-8">
$(function() {
    $( "#dob" ).datepicker({
        showButtonPanel: true,
        dateFormat: "dd-mm-yy",
        changeMonth: true,
        changeYear: true
    });
});

</script>
