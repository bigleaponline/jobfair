


<!--nav-->
<div class="container-fluid mt">
  <div class="col-md-9 col-sm-8 site">
   
    <!--1e-->
    <div class="diagram-path">
      <ol>
        <li class="active"><a href="<?php echo base_url();?>other-details">Registration</a></li>
        <li><a href="#">Assessment</a></li>
        <li><a href="#">Report</a></li>
        <li><a href="#">Hall Ticket</a></li>
      </ol>
    </div>
    <div class="container-fluid">
      <div class="titles" style="border-top:none;">
        <h2>Other Details</h2>
      </div>
      <div class="e-r-f">
        <form>
          <div class="row ">
            <div class="frm-sb-sections col-md-12">
              <table class="table table-bordered table-responsive">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">Qualification</th>
                    <th scope="col">Course</th>
                    <th scope="col">Stream </th>
                    <th scope="col">Board/ University</th>
                    <th scope="col">Percentage </th>
                    <th scope="col">Year of Passout</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <th class="int"><input type="text" class="form-control" placeholder="Post Graduation" disabled=""></th>
                    <td><input type="text" class="form-control" placeholder="Enter Course Name"></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder="">
                      </th>
                    <td><input type="text" class="form-control" placeholder=""></td>
                  </tr>
                  <tr>
                    <th class="int"><input type="text" class="form-control" placeholder="Graduation" disabled=""></th>
                    <td><input type="text" class="form-control" placeholder="Enter Course Name"></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder="">
                      </th>
                    <td><input type="text" class="form-control" placeholder=""></td>
                  </tr>
                  <tr>
                    <th class="int"><input type="text" class="form-control" placeholder="Diploma/ITI" disabled=""></th>
                    <td><input type="text" class="form-control" placeholder="Enter Course Name"></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder="">
                      </th>
                    <td><input type="text" class="form-control" placeholder=""></td>
                  </tr>
                  <tr>
                    <th class="int"><input type="text" class="form-control" placeholder="Plus Two" disabled=""></th>
                    <td><input type="text" class="form-control" placeholder="Enter Course Name"></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder="">
                      </th>
                    <td><input type="text" class="form-control" placeholder=""></td>
                  </tr>
                  <tr>
                    <th class="int"><input type="text" class="form-control" placeholder="SSLC/Board Exam" disabled=""></th>
                    <td><input type="text" class="form-control" placeholder="Enter Course Name"></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder=""></td>
                    <td><input type="text" class="form-control" placeholder="">
                      </th>
                    <td><input type="text" class="form-control" placeholder=""></td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div class="col-md-12">
              <div class="form-group">
                <label>Address : </label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
            </div>
            <div class="frm-sb-sections col-md-12">
              <div class="row-class">
                <h4>Your area of interest to be Upskilled</h4>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Category :</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>Dropdown</option>
                    <option>----</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>List :</label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>Dropdown</option>
                    <option>---</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group f-r">
                <label>Do you have any experience :</label>
                <p>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" value="yes" onclick="exp(this.value)">
                    Yes </label>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" value="no" onclick="exp(this.value)">
                    No </label>
                </p>
              </div>
            </div>
            <div class="col-md-12 hidden-div" id="experience" style="display:none;">
              <div class="col-md-11">
                <table class="table table-bordered table-responsive" id="myTable">
                  <thead>
                    <tr>
                      <th scope="col">Company Name</th>
                      <th scope="col">Position</th>
                      <th scope="col">Duration</th>
                    </tr>
                  </thead>
                  <tbody class="t-body">
                    <tr  class="row-periord" id="1">
                      <td><input type="text" class="form-control company" placeholder="Enter Company Name"></td>
                      <td><input type="text" class="form-control position" placeholder="Enter Position"></td>
                      <td><input type="text" class="form-control duration" placeholder="Enter Duration"></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-1 add-del-btn">
                <div class="add-btn"><i class="fa fa-plus" aria-hidden="true"></i></div>
                <div class="del-btn" style="display:none;" id ="vk"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group f-r">
                <label>Have you done Any Internship :</label>
                <p>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" value="yes" onclick="intern(this.value)">
                    Yes </label>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" value="no" onclick="intern(this.value)">
                    No </label>
                </p>
              </div>
            </div>
            <div class="col-md-12 hidden-div">
              <div class="col-md-6" id="intern" style="display:none;">
                <div class="form-group">
                  <label>Area : </label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>Dropdown</option>
                    <option>----</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group f-r">
                <label> Are you interested to do in Internship :</label>
                <p>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" value="yes" onclick="intrest(this.value)">
                    Yes </label>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" value="no" onclick="intrest(this.value)">
                    No </label>
                </p>
              </div>
            </div>
            <div class="col-md-12 hidden-div">
              <div class="col-md-6" id="intrest"  style="display:none;">
                <div class="form-group">
                  <label> </label>
                  <select class="form-control" id="exampleFormControlSelect1">
                    <option>Dropdown</option>
                    <option>----</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="form-group f-r">
                <label>For an overseas career in future Which is your perferred country  :</label>
                <p>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="" >
                    Ireland </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="">
                    Canada </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="">
                    Sweeden </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="">
                    Germany </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="">
                    Australia </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="">
                    Azerbaijan </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="">
                    Not Intersted</label>
                </p>
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group f-r">
                <label>Do you wish to become enterpreneur in the future ?</label>
                <p>
                  <label class="radio-inline">
                    <input type="radio" name="optradio" >
                    Yes </label>
                  <label class="radio-inline">
                    <input type="radio" name="optradio">
                    No </label>
                </p>
              </div>
            </div>
            <div class="col-md-12 bbb">
              <label>Upload your resume</label>
              <p><span class="btns  btn-file">Browse file
                <input type="file" >
                </span></p>
            </div>
            <div class="col-md-12 frm-sb-sections">
              <div class="row-class">
                <h4>Sample Videos ( to be Added )</h4>
              </div>
              <div class="col-md-12 bbb">
                <label>Tell about your self (If Video 45 Sec)</label>
                <p><span class="btns  btn-file">Browse file
                  <input type="file" >
                  </span></p>
              </div>
              <div class="col-md-12 bbb">
                <label>Why Should we hire you (30 Sec)</label>
                <p><span class="btns  btn-file">Browse file
                  <input type="file" >
                  </span></p>
              </div>
              <div class="col-md-12 bbb">
                <label> your Passport Size Photo</label>
                <p><span class="btns  btn-file">Browse file
                  <input type="file" >
                  </span></p>
              </div>
            </div>
          </div>
          <button type="submit" class="sb-btn">Submit</button>
        </form>
      </div>
    </div>
    <div class="fter"> Copyright © 2019 All Rights Reserved. </div>
  </div>
  <!--web-area-end-->


<!-- js) --> 
<script>
function exp(val)
{
	if(val=="yes")
	{
		$("#experience").show();
	}
	else if(val=="no")
	{
		$("#experience").hide();
	}
}
function intern(val)
{
	if(val=="yes")
	{
		$("#intern").show();
	}
	else if(val=="no")
	{
		$("#intern").hide();
	}
}
function intrest(val)
{
	if(val=="yes")
	{
		$("#intrest").show();
	}
	else if(val=="no")
	{
		$("#intrest").hide();
	}
}
</script>

<script type="text/javascript">
$(document).ready(function()
  {
    var tbody = $('#myTable').children('tbody');

   var table = tbody.length ? tbody : $('#myTable');

   $(".row-periord:last").find(".company");

   $('.add-btn').on('click', function()
   {
       
    var rows=$('#myTable tr').length;
    if(rows>1)
    {
        $("#vk").show();
    }
    table.append('<tr  class="row-periord" id="1"><td><input type="text" class="form-control company" placeholder="Enter Company Name"></td><td><input type="text" class="form-control position" placeholder="Enter Position"></td><td><input type="text" class="form-control duration" placeholder="Enter Duration"></td></tr>');
    $(".t-body tr:last").find(".company");

   });

   });

</script>
<script type="text/javascript">
  $('.del-btn').click(function(){
       var rows=$('#myTable tr').length;
        if(rows <= 3)
        {
            $("#vk").hide();
        }
  $( ".row-periord:last").remove();
});
</script>
