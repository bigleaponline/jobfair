
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

<!--nav-->

<div class="container-fluid mt">
  <div class="col-md-9 col-sm-8 site"> 
    <!--1e--->
   
    <div class="container-fluid">
      <div class="titles" style="border-top:none;">
        <h2>How you should attend the job fair</h2>
        <h5>Road map to job fair</h5>
      </div>
      <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3912.733158185757!2d75.78265531429061!3d11.281015252762858!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba65eb819c00de7%3A0x52fa4cc9e14a9ec6!2sUniware+Solutions+P.+Ltd.!5e0!3m2!1sen!2sin!4v1564571116272!5m2!1sen!2sin" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
    <div class="assmnt-clm">
      <h2> What is Assessment</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
      <button class="btn btn-default filter-button actived vk1" data-filter="assessment1" >General Assessment</button>
      <button class="btn btn-default filter-button vk2" data-filter="assessment2" >Technical Assessment</button>
    </div>
    <div class="container-fluid">
      <div class="filter  assessment1">
        <div class="asmnt">
          <h1>General Assessment</h1>
          <div class="d-ar"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
            <h2>Eligibility Criteria of the Candidates </h2>
          </div>
        </div>
        <div class="q1">
          <div class="q-sub-title">
            <h2>Category A</h2>
          </div>
          <span>
          <h6> Plus Two ( Passed )</h6>
          </span> </div>
        <div class="q1">
          <div class="q-sub-title q-sub-title-border-top">
            <h2>Category b</h2>
          </div>
          <span>
          <h6> ITI/Diploma </h6>
          </span> </div>
        <div class="q1"> <span>
          <h6> BSc ( CS, IT, Maths, Electronics, Statistics ) </h6>
          </span> </div>
        <div class="q1"> <span>
          <h6>BCA </h6>
          </span>
          <li>with 50% & above marks</li>
        </div>
        <div class="q1"> <span>
          <h6>BCom/ MCom/ BBA ( Finance )</h6>
          </span>
          <li>55% & above marks</li>
        </div>
        <div class="q1"> <span>
          <h6> BTech - Electrical, Electronics, Mechanical, Civil  ( For Core Jobs ) </h6>
          </span>
          <li>Below 60% marks</li>
        </div>
        <div class="q1"> <span>
          <h6> B.Tech/ MCA/ MSc - IT, CS, Electronics, Electrical, Mechanical ( optional ) ( For IT Jobs )</h6>
          </span>
          <li>Below 60% marks</li>
        </div>
        <div class="q1"> <span>
          <h6>Other Graduates</h6>
          </span> </div>
        <div class="q1 qmy"> <i class="fa fa-star" style="color:#2fbb57; font-size:14px; " aria-hidden="true"></i>
          <h5>Some companies consider 55% marks as eligibility for technical jobs, in that case
            the candidates are considered under category C . In this regard the candidates will
            be intimated by SMS/Mail/Whatsapp to opt for nearby centres to attend Preliminary
            exam under Category C of Technical Assessment.</h5>
        </div>
        <div class="q1 q2 qli1">
          <li>All Eligible Candidates of above criteria undergoes Preliminary Online
            Assessment from ( 20 h August 2019 till 21 st September 2019 ) based on login
            generated after registration.</li>
          <li>Based on the performance of candidates in the General Assessment
            Candidates will be called by different companies to participate in the final
            round at the Job Fair </li>
          <li>General Assessment has NO REJECTION of candidate but to identify
            candidates competencies to suit for different job roles and to segregate
            them to the various screening processes of the companies visiting at the Job
            Fair </li>
        </div>
      </div>
      
      <!--second-->
      <div class="filter  assessment2" style="display:none;">
        <div class="asmnt">
          <h1>Technical Assessment</h1>
          <div class="d-ar"><i class="fa fa-angle-double-right" aria-hidden="true"></i>
            <h2>Eligibility Criteria of the Candidates </h2>
          </div>
        </div>
        <div class="q1">
          <div class="q-sub-title">
            <h2>Category c</h2>
          </div>
          <span>
          <h6> BTech/M.Tech - Electrical, Electronics, Mechanical, Civil ( For Core Job )</h6>
          </span>
          <li>Above 60% marks</li>
        </div>
        <div class="q1"> <span>
          <h6> BTech/MTech - IT,CS, Electronics, Electrical, Mechanical ( optional ) ( For IT Jobs )</h6>
          </span>
          <li>MCA/ MSc ( IT, CS, Electronics, Telecommunications, Statistics, Maths- Above 60% marks) </li>
        </div>
        <div class="q1"> <span>
          <h6> MBA </h6>
          </span>
          <li>55% & above marks</li>
        </div>
        <div class="q1"> <span>
          <h6>MA English </h6>
          </span> </div>
        <div class="q1"> <span>
          <h6>MCJ</h6>
          </span> </div>
        <div class="q1"> <span>
          <h6>BTech with MBA </h6>
          </span>
          <li>aboveBelow 60% marks</li>
        </div>
        <div class="q1 q2 qli">
          <li>All eligible candidates of below criteria undergoes Preliminary Online Assessment
            from (1 th Sept 2019 till 15 th Sept 2019) based on login generated after registration at
            selected centers around Kerala (The details of Centres will be published over the
            website on( 30 th August 2019) Candidate has to select from the list of centres and opt
            3 centres as per her/his choice based on nearness and travelling to the centre.The
            selected centre representatives will schedule date and time of assessment and
            intimate by sms/mail/Whatsapp about your test at the centre )</li>
          <li>After Preliminary Exam, the Candidates fall under three categories C1, C2 and C3 </li>
          <li>C1-The top performers in the preliminary assessment including B.Tech/MBA will be called
            for a second round of online assessments 10 days prior the event to a leading engineering
            college. </li>
          <li>The shortlisted candidates are called for the final round of interview at Job Fair to participate
            for the final round of interview with companies that are in live with India’s one of the Top
            Assessment Company.</li>
          <li>C2-All the eligible students of these category irrespective of their score in the Preliminary
            exam will be called by some recruiters based on their academic scores and profile to conduct
            online exam at recruiters identified venues in North Kerala and South Kerala to be followed
            by final round at the Job Fair</li>
          <li>C3-All the eligible students irrespective of their score in the Preliminary exam also can
            directly attend those companies screening processes where all rounds of the process is
            conducted on the same date of Job fair</li>
          <li>Technical Assessment has NO REJECTION of candidates but to identify candidates
            competencies to suit for different job roles and to segregate them to the various
            screening processes of the companies visiting at the Job Fair</li>
          <li>All the eligible students attending preliminary test have to be ready to follow the different
            processes to align with the procedures and recruitment policy of different recruiters and
            assessment companies associated with Job Fair.</li>
        </div>
      </div>
    </div>
    <div class="path-tosignin path-tosignin2 path-tosignin-job2"> <a href="<?php echo base_url('sign-in');?>">Click here to Register <i class="fa fa-hand-o-right animated flash infinite" aria-hidden="true"></i></a></div>
    <div class="fter"> Copyright © 2019 All Rights Reserved. </div>
  </div>
  
  <!--web-area-end--> 
  <!--tb-js--> 
  <script>
 
  
  
$(document).ready(function(){
$(".vk2").click(function(){
	$(".vk1").removeClass("actived");
});
    $(".filter-button").click(function(){
        var value = $(this).attr('data-filter');
        if(value == "all")
        {
            //$('.filter').removeClass('hidden');
            $('.filter').show('1000');
        }
        else
        {
//            $('.filter[filter-item="'+value+'"]').removeClass('hidden');
//            $(".filter").not('.filter[filter-item="'+value+'"]').addClass('hidden');
            $(".filter").not('.'+value).hide('3000');
            $('.filter').filter('.'+value).show('3000');
            
        }
    });
    
    if ($(".filter-button").removeClass("active")) {
$(this).removeClass("active");
}
$(this).addClass("active");

});
</script>
