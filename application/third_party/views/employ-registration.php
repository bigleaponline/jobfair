
<style type="text/css">
  .err{color:red;}
#errmsg0
{
color: red;
}
</style>
<!--nav-->
<div class="container-fluid mt">
  <div class="col-md-9 col-sm-8 site">
    <!--1e--->
    <div class="container-fluid">
       <?php 
        if($this->session->flashdata('employererror'))
        {
          echo '<div class="alert alert-danger" role="alert">'.$this->session->flashdata('employererror').'</div>';   
        }
    ?>
      <div class="titles" style="border-top:none;">
        <h2>Employer Registration</h2>
      </div>
      <div class="e-r-f">
        <form action="<?php echo base_url();?>jobfair_controller/employer_registration" method="post">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Company Name: </label>
                <input type="text" class="form-control" name="company_name" id="company_name" placeholder="Enter Company Name" required>
        
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Contact No: </label>
                <input type="text" class="form-control" name="contact_no" id="contact_no" placeholder="Enter Contact No" required autocomplete="off">
               <span id="errmsg0"></span>
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Email ID: </label>
                <input type="email" class="form-control" name="email_id" id="email_id" placeholder="Enter Email ID" required>
             
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Type of Industry: </label>
                <input type="text" class="form-control" name="industry" id="industry" placeholder="Enter Industry Type" required>
               
              </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Contact Person: </label>
                <input type="text" class="form-control" name="contact_person" id="contact_person" placeholder="Enter Contact Details" required>
             
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Designation: </label>
                <input type="text" class="form-control" name="designation" id="designation" placeholder="Enter Designation" required>
               
              </div>
              <div class="form-group">
                <label for="exampleInputEmail1">Address: </label>
                <textarea class="form-control" name="address" id="address" placeholder="Enter Address" rows="4" required></textarea>
                
              </div>
            </div>
          </div>
          <div class="row">
            <div class="titles t2" style="border-top:none;">
              <h2>Job Details</h2>
            </div>
            <div class="col-md-12 hidden-div" >
              <div class="col-md-11">
                <input type="hidden" name="row_no" value="1" id="row_no">
                <table class="table table-bordered table-responsive" id="myTable">
                  <thead>
                    <tr>
                      <th scope="col">Job Title</th>
                      <th scope="col">Experience</th>
                      <th scope="col">Salary</th>
                      <th scope="col">Job Description</th>
                    </tr>
                  </thead>
                  <tbody class="t-body">
                    <tr  class="row-periord" id="row1">
                      <td><input type="text" class="form-control title" id="job_title" name="job_title[]" placeholder="Enter Job Title"></td>
                      <td><input type="text" class="form-control experience" id="eperience" name="eperience[]" placeholder="Enter Experience"></td>
                      <td><input type="text" class="form-control salary" id="salary" name="salary[]" placeholder="Enter Salary"></td>
                      <td class="jb-txtarea"><textarea class="form-control description" id="job_description" name="job_description[]" placeholder="Enter Job Description"></textarea></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <div class="col-md-1 add-del-btn">
                <div class="add-btn"><i class="fa fa-plus" aria-hidden="true"></i></div>
                <div class="del-btn" style="display:none;" id="vk"><i class="fa fa-trash-o" aria-hidden="true"></i></div>
              </div>
            </div>
          </div>
          <button type="submit" class="sb-btn">Submit</button>
        </form>
      </div>
    </div>
    <div class="fter"> Copyright © 2019 All Rights Reserved. </div>
  </div>

<!-- js) -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
  {
    var tbody = $('#myTable').children('tbody');

   var table = tbody.length ? tbody : $('#myTable');

   $(".row-periord:last").find(".title");

   $('.add-btn').on('click', function()
   {

    var row_no = $('#row_no').val();
       
    var rows=$('#myTable tr').length;

    // alert(rows);

    var $sr = ($(".row-periord").length + 1);
    
    var rowid = Math.random();

    if(rows>1)
    {
        $("#vk").show();
    }
    // table.append('<tr  class="row-periord" id="1"><td><input type="text" class="form-control title" placeholder="Enter Job Title"></td><td><input type="text" class="form-control experience" placeholder="Enter Experience"></td><td><input type="email" class="form-control salary" placeholder="Enter Salary"></td><td class="jb-txtarea"><textarea class="form-control description" placeholder="Enter Job Description"></textarea></td></tr>');
    // $(".t-body tr:last").find(".title");

    var $html = '<tr class="row-periord" id="' + rowid + '">' +
     
        '<td><input class="form-control"  type="input" name="job_title[]" id="job_title'+$sr+'" value=""/></td>' +
        '<td><input class="form-control"  type="input" name="eperience[]" id="eperience'+$sr+'" value=""/></td>' +
        '<td><input class="form-control"  type="input" name="salary[]" id="salary'+$sr+'" value=""/></td>' +
        '<td><textarea class="form-control"  name="job_description[]" id="job_description'+$sr+'"></textarea></td>' +
       
        '</tr>';

    $("#myTable").append($html);
    $("#row_no").val($sr);

    return false;

   });

   });

</script>

<script type="text/javascript">
  $('.del-btn').click(function(){
       var rows=$('#myTable tr').length;
        if(rows <= 3)
        {
            $("#vk").hide();
        }
  $( ".row-periord:last").remove();
});
</script>
