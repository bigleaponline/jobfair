
<div class="container-fluid mt">
  <div class="col-md-9 col-sm-8 site">

    <div class="container-fluid">
    
      <div class="tp-clm">
        <div class="titles">
          <h2>50 Companies 10 Sectors </h2>
        </div>
        <div class="full row">
          <div class="col-md-7">
            <div class="n-clms">
              <div class="col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n2.png"></div>
                  <h2>Software<br>
                    Engineering</h2>
                  <p>10 companies</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n4.png"></div>
                  <h2>Marketing, HR & Finance Management </h2>
                  <p>10 companies</p>
                </div>
              </div>
              
              <div class="col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n1.png"></div>
                  <h2>Mech, Electrical & Civil Engineering</h2>
                  <p>10 companies</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n3.png"></div>
                  <h2>Pharmacy & <br>Bio-tech</h2>
                  <p>5 companies</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n5.png"></div>
                  <h2>Accounting & Finance</h2>
                  <p>5 companies</p>
                </div>
              </div>
              <div class="col-md-4">
                <div class="no-comp">
                  <div class="n-img"><img src="<?php echo base_url();?>assets/images/n6.png"></div>
                  <h2>BPO & others</h2>
                  <p>10 companies</p>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-5">
            <div class="n2-img"><img src="<?php echo base_url();?>assets/images/n-big.png" class="img-responsive"></div>
          </div>
        </div>
      </div>
      
      
      
      <div class="bt-clm">
      <div class="titles" style="border-top:none;">
        <h2>Who should attend Job Fair </h2>
        <h5>Eligibility Criteria of the Candidates</h5>
        <hr>
      </div>
      <div class="q1"> <span> 
        <h6> Plus Two ( Passed )</h6>
        </span>
        <li>Local Language Skills</li>
        <li>Good English Language Skills</li>
      </div>
      <div class="q1"> <span> 
        <h6> ITI/Diploma </h6>
        </span> </div>
      <div class="q1"> <span> 
        <h6> Any Graduates </h6>
        </span>
        <li>Local Language Skills</li>
        <li>Good English Language Skills</li>
      </div>
      <div class="q1"> <span> 
        <h6> BSc (CS, IT, Maths, Electronics, Statistics) , BCA, BVoC</h6>
        </span>
        <li>50% & above marks</li>
      </div>
      <div class="q1"> <span> 
        <h6> BCom / MCom / BBA(Finance)</h6>
        </span>
        <li>55% & above marks</li>
      </div>
      <div class="q1"> <span> 
        <h6> BTech - Electrical/Electronics/Mechanical/Civil  ( For Core Jobs ) </h6>
        </span>
        <li>Above 60% marks</li>
        <li>Good English Language Skills</li>
      </div>
      <div class="q1"> <span> 
        <h6> B.Tech /MCA/MSc ( IT, CS, Electronics, Electrical, Mechanical ( optional)) ( For IT Jobs )</h6>
        </span>
        <li>Above 60% marks</li>
        <li>Good English Language Skills</li>
      </div>
      <div class="q1"> <span> 
        <h6> MBA </h6>
        </span>
        <li>55%  above marks</li>
      </div>
      <div class="q1"> <span> 
        <h6> BTech with MBA </h6>
        </span> </div></div>
    </div>
    <div class="fter"> Copyright © 2019 All Rights Reserved. </div>
  </div>
 
