<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobfair_controller extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('jobfair_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('download');
        $this->load->helper(array('email'));
        // $this->load->library(array('email'));
    }
	public function index()
	{
		$data['page_title']='Homepage';
		$this->load->view('header',$data);
		$this->load->view('index');
		$this->load->view('footer');
	}
	public function calicut()
	{
		$data['page_title']='Thrissur-Homepage';
		$this->load->view('thrissur/calicut_header',$data);
		$this->load->view('calicut');
		$this->load->view('thrissur/calicut_footer');
	}
	public function admin()
	{
		$this->load->view('admin/login');
	}

	public function loadpage()
	{
		$page=$this->uri->segment('1');
        $data['page_title']=$this->uri->segment('1');
		$this->load->view('header',$data);
		$this->load->view($page);
		$this->load->view('footer');
	}
	public function logout()
	{
	    $this->session->unset_userdata('cand_data');
        $this->session->sess_destroy();
		$data['page_title']=$this->uri->segment('1');
		$this->load->view('header',$data);
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}
	public function login()
	{
		$login=$this->jobfair_model->login();
		if($login)
		{
		    $this->session->set_userdata('cand_data',$login);
		    $venue=$this->session->userdata['cand_data']['cand_venue'];
		    if($venue==1)
       	    {
       	        $venue='thrissur';
       	    }
       	    else if($venue==2)
       	    {
       	        $venue='kochi';
       	    }
       	    else if($venue==3)
       	    {
       	        $venue='northbanglore';
       	    }
       	    else if($venue==4)
       	    {
       	        $venue='hyderbad';
       	    }
		    if($this->session->userdata['cand_data']['cand_level']==0)
		    {
			     redirect($venue.'/hall_ticket');
			}
			if($this->session->userdata['cand_data']['cand_status']=='completed') 
			{
    			if($this->session->userdata['cand_data']['cand_test_status']==0)
    			{
    			    redirect($venue.'/instructions');
    			}
			    if($this->session->userdata['cand_data']['cand_test_status']==1)
			    {
			        redirect($venue.'/report');
			    }
			 //   if($this->session->userdata['cand_data']['cand_test_status']==2)
			 //   {
			 //       redirect($venue.'/technical_report');
			 //   }
			}
			else
			{
			    if($this->session->userdata['cand_data']['cand_test_status']==1)
    			{
    			    redirect($venue.'/report');
    			}
    			else
			    redirect($venue.'/welcome');
			}
		}
		else
		{
			$this->session->set_flashdata('loginerror','Username or Password Is Incorrect');
			redirect('login');
		}
	}
	public function signin()
	{
	    $this->session->unset_userdata('cand_data');
        $this->session->sess_destroy();
		$data['qualification']=$this->jobfair_model->signin();
		$data['year']=$this->jobfair_model->year();
		$data['page_title']=$this->uri->segment('1');
		$data['id']=$this->jobfair_model->get_id();
		$this->load->view('header',$data);
		$this->load->view('sign-in',$data);
		$this->load->view('footer');
	}
	public function candidateRegistration()
	{
		$from = 'info@indiamegajobfairs.com';
		$pass=$this->input->post('password');
		$conpass=$this->input->post('confirmpassword');
		if($pass==$conpass)
		{
    	  // $recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
           
       //    $userIp=$this->input->ip_address();
             
       //    $secret='6LcTUtEUAAAAAHb0fxl2QQNqbSIZyjU7ZkdqIZAh';
           
       //    $credential = array(
       //          'secret' => $secret,
       //          'response' => $this->input->post('g-recaptcha-response')
       //      );
     
       //    $verify = curl_init();
       //    curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
       //    curl_setopt($verify, CURLOPT_POST, true);
       //    curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($credential));
       //    curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
       //    curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
       //    $response = curl_exec($verify);
     
       //    $status= json_decode($response, true);
           
       //    if($status['success'])
		    
			$reg=$this->jobfair_model->candidateRegistration();
			
			if($reg)
			{

        $username=$this->input->post('username');
        $password=$this->input->post('password');
        $email=$this->input->post('email');
        $this->load->model('Mail_model');
        $this->Mail_model->sendRegistrationEmail($username,$password,$email);
        $this->session->set_flashdata('registration','Your Login Id is created Successsfully......');
        $this->session->set_flashdata('message','Password and Username is Send to your mail id !! If not found in inbox please check the spam Folder.');
        $this->session->set_flashdata('message1','Please Login to Complete Your Registration....');
        redirect('login');
		}
		else
		{
			$this->session->set_flashdata('errorregistration','Failed....Please Try Again');
			redirect('sign-in');		
		}
		}
		else
		{
			$this->session->set_flashdata('password',"passwords doesn't match");
			redirect('sign-in');
		}
	
	}
	
	public function profile()
	{
	    $profile=$this->jobfair_model->profile();
	}
	public function getStream()
	{
		$this->jobfair_model->getStream();
	}
	public function getWeightage()
	{
		$this->jobfair_model->getWeightage();
	}
	public function employer_registration()
	{
		$employer=$this->jobfair_model->employer_registration();
       	if($employer)
       	{
       	    $venue=$this->input->post('venue');
       	    if($venue==1)
       	    {
       	        $venue='thrissur';
       	    }
       	    else if($venue==2)
       	    {
       	        $venue='kochi';
       	    }
       	    else if($venue==3)
       	    {
       	        $venue='calicutandkochi';
       	    }
       	    if($venue==4)
       	    {
       	        $venue='northbanglore';
       	    }
       	    if($venue==5)
       	    {
       	        $venue='hederbad';
       	    }
       	    $email=$this->input->post('email_id');
            $company_name  = $this->input->post('company_name');
            $contact_no  = $this->input->post('contact_no');
            $industry  = $this->input->post('industry');
            $address  = $this->input->post('address');
            $contact_person  = $this->input->post('contact_person');
            $designation  = $this->input->post('designation');
       	    $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'ssl://mail.indiamegajobfairs.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'info@indiamegajobfairs.com',
                            'smtp_pass' => 'bglpindiameg@123',
                            'mailtype'  => 'text', 
                            'charset'   => 'iso-8859-1',
                            'wordwrap' => TRUE
                        );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");

       	    $message="Company Name:".$company_name."\r\n Contact Name:".$contact_person."\r\n Contact Number:".$contact_no."\r\n Designation:".$designation."\r\n Address:".$address."\r\n Job Fair Venue:".$venue;
            $this->email->from($email);
            $this->email->to('info@indiamegajobfairs.com'); 
            $this->email->subject('Job Fair Companys');
            $this->email->message($message);  
            $this->email->send();
            
            $message1="Thank you for registering with India Mega Job Fair.We will contact you";
            $this->email->from('info@indiamegajobfairs.com');
            $this->email->to($email); 
            $this->email->subject('India mega Job Fair');
            $this->email->message($message1);  
            $this->email->send();
	       	$this->session->set_flashdata('employer','Registration Successfull');
	       	redirect($venue.'/company-list');
       	}
       	else
       	{
	       	redirect('employ-registration');
       	}

	}
	public function companylist()
	{
	    $data['company']=$this->jobfair_model->companylist();
	    $data['page_title']=$this->uri->segment('2');
	    $this->load->view('header',$data);
	    $this->load->view('company-list',$data);
	    $this->load->view('footer');
	}
	public function sponsor_registration()
	{
	    $venue=$this->input->post('venue');
       	if($venue==1)
       	{
       	    $venue='thrissur';
       	}
       	else if($venue==2)
       	{
       	    $venue='kochi';
       	}
       	else if($venue==3)
       	{
       	    $venue='calicut and kochi';
       	}
       	if($venue==4)
       	{
       	    $venue='northbanglore';
       	}
       	if($venue==5)
       	{
       	    $venue='hederbad';
        }
		$sponsor=$this->jobfair_model->sponsor_registration();
       	if($sponsor==true)
       	{
            $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'ssl://mail.indiamegajobfairs.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'info@indiamegajobfairs.com',
                            'smtp_pass' => 'bglpindiameg@123',
                            'mailtype'  => 'text', 
                            'charset'   => 'iso-8859-1'
                        );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            
            $name=$this->input->post('name');
            $email=$this->input->post('email');
            $contact=$this->input->post('contact');
            $organisation=$this->input->post('organisation');
            $designation=$this->input->post('designation');
            
            $message="Name:".$name."\r\n Contact No:".$contact."\r\n Organisation:".$organisation."\r\n Designation:".$designation."\r\n Job Fair Venue:".$venue;
            $this->email->from($email);
            //$this->email->to('kunnummalvipindas@gmail.com,ann@indiamegajobfairs.com'); 
            $this->email->to('info@indiamegajobfairs.com');
            $this->email->subject('Job Fair Sponsors');
            $this->email->message($message);  
            $this->email->send();
                
            $message1="Thank you for registering with India Mega Job Fair.We will contact you";
            $this->email->from('info@indiamegajobfairs.com');
            $this->email->to($email); 
            $this->email->subject('India mega Job Fair');
            $this->email->message($message1);  
            $this->email->send();
            $this->session->set_flashdata('sponsor','Registration Successfull');
	       	redirect('sponsor-registration');
       	}
       	else
       	{
       		$this->session->set_flashdata('sponsorerror','Failed ..... Please Try Again....');
	       	redirect('sponsor-registration');
       	}
    }
    public function other_details()
	{
	    $data['cand_id'] = $this->session->userdata['cand_data']['cand_id'];
        $cand_id = $this->session->userdata['cand_data']['cand_id'];
        $data['name'] = $this->session->userdata['cand_data']['name'];
        $name = $this->session->userdata['cand_data']['name'];
        $data['qualification'] = $this->session->userdata['cand_data']['qualification'];
        $qualification = $this->session->userdata['cand_data']['qualification'];
        $cand_id=$this->session->userdata['cand_data']['cand_id'];
        $get_cand_profile = $this->jobfair_model->get_cand_profile($cand_id);
        if($get_cand_profile)
        {
            $data['get_cand_qualification'] = $this->jobfair_model->get_cand_qualification($qualification);
            $data['qualification_pg'] = $this->jobfair_model->get_qualification_pg();
            $data['qualification_ug'] = $this->jobfair_model->get_qualification_ug();
            $data['qualification_diploma'] = $this->jobfair_model->get_qualification_diploma();
            $data['qualification_plus_two'] = $this->jobfair_model->get_qualification_plus_two();
            $data['qualification_sslc'] = $this->jobfair_model->get_qualification_sslc();
            $data['passion_list'] = $this->jobfair_model->get_passion();
            $data['internship'] = $this->jobfair_model->get_internship();
            $data['page_title']=$this->uri->segment('1');
        	$this->load->view('header',$data);
        	$this->load->view('other-details',$data);
        	$this->load->view('footer');
        }
        else
        {
            redirect('logout');
        }
	}
	public function get_sub_passion()
	{
		$this->jobfair_model->get_sub_passion();
	}
	public function getState()
	{
	    $this->jobfair_model->getState();
	}
	public function getCity()
	{
	    $this->jobfair_model->getCity();
	}
	public function candidate_profile_registration()
	{
	    $venue=$this->session->userdata['cand_data']['cand_venue'];
		if($venue==1)
       	{
       	    $venue='thrissur';
       	}
       	    else if($venue==2)
       	{
       	    $venue='kochi';
       	}
       	else if($venue==3)
       	{
       	    $venue='northbanglore';
       	}
       	else if($venue==4)
       	{
       	    $venue='hyderbad';
       	}
		$video_name = '';
	    $video_name_1 = '';
	    /*if (isset($_FILES['video']['name']) && $_FILES['video']['name'] != '') 
	    {
	        unset($config);
	        $date = date("d-m-Y");
	        $configVideo['upload_path'] = './upload/video';
	        $configVideo['max_size'] = '40000000';
	        $configVideo['allowed_types'] = 'avi|flv|wmv|mp3|mp4';
	        $configVideo['overwrite'] = FALSE;
	        $configVideo['remove_spaces'] = TRUE;
	        $video_name = $date.$_FILES['video']['name'];
            $configVideo['file_name'] = $video_name;
            $this->load->library('upload', $configVideo);
	        $this->upload->initialize($configVideo);
	        if (!$this->upload->do_upload('video')) 
	        {
	            print_r($this->upload->display_errors());
	            echo "video Not Uploaded";
	        }
	        else 
	        {
	            $videoDetails = $this->upload->data();
	        }
	    }
	    if (isset($_FILES['video_1']['name']) && $_FILES['video_1']['name'] != '') 
	    {
	        unset($config);
	        $date = date("ymd");
	        $configVideo['upload_path'] = './upload/video';
	        $configVideo['max_size'] = '40000000';
	        $configVideo['allowed_types'] = 'avi|flv|wmv|mp3|mp4';
	        $configVideo['overwrite'] = FALSE;
	        $configVideo['remove_spaces'] = TRUE;
	        $video_name_1 = $date.$_FILES['video_1']['name']; 
            $configVideo['file_name'] = $video_name_1;
            $this->load->library('upload', $configVideo);
            $this->upload->initialize($configVideo);
            if (!$this->upload->do_upload('video_1'))
            {
                print_r($this->upload->display_errors());
                echo "video_1 Not Uploaded";
	        }
	        else 
	        {
	            $videoDetails = $this->upload->data();
	        }
	    }*/
	    $resume=$this->do_upload_resume();
	   // if(!$resume)
	   // {
	   //     $this->session->set_flashdata('resumeerr','Failed...Resume Only Support Pdf/doc Format With Maximum Size of 5MB !');
	   //     redirect($venue.'/other-details');
	   // }
	    $image=$this->do_upload_profile_image();
	   // if(!$image)
	   // {
	   //     $this->session->set_flashdata('imageerr','Failed...Image Size or Format Mismatches!');
	   //     redirect($venue.'/other-details');
	   // }
	    $query=$this->jobfair_model->candidate_profile_registration($video_name, $video_name_1,$image,$resume);
	    if($query)
	    {
	        $this->session->set_flashdata('otherdetails','Your Registration Completed Successfully......');
	        redirect($venue.'/instructions');
	    }
	    else
	    {
	        $this->session->set_flashdata('otherdetailserr','Failed...Please Try Again...!');
	        redirect($venue.'/other-details');
	    }
	    
	}
	public function do_upload_profile_image() 
	{
	   if($this->session->userdata('cand_username') != ''){
        $cand_username = $this->session->userdata['cand_data']['cand_username'];
	    }
	   
	    
        $config['upload_path'] = './upload/profile_images';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '300000';
        $config['max_width'] = '200';
        $config['max_height'] = '230';
        $config['overwrite'] = TRUE;
        $profileimage = time().$_FILES['profile_img']['name']; 
        $config['file_name'] = $cand_username;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('profile_img')) 
        {
            $error = array('error' => $this->upload->display_errors()); 
            print_r($error);
            return FALSE;
        } 
        else 
        {
            $data = array('upload_data' => $this->upload->data());
            return $data['upload_data']['file_name'];
        }
    }
    public function do_upload_resume() 
    {
        if (isset($_FILES['userfile']['name']) && $_FILES['userfile']['name'] != '') 
        {
            unset($config);
            $config['upload_path'] = './upload/resume';
            $config['allowed_types'] = 'pdf|doc|docx';
            $config['max_size'] = '5000';
            $config['overwrite'] = TRUE;
            $resume = time().$_FILES['userfile']['name']; 
            $config['file_name'] = $resume;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) 
            {
                $error = array('error' => $this->upload->display_errors());  
                print_r($error);
                return FALSE;
            } 
            else
            {
                $data = array('upload_data' => $this->upload->data());
                return $resume;
            }
        }
    }
    public function get_candidate_details()
    {
        $pro=$this->jobfair_model->get_candidate_details();
    }
    public function jobDetails()
    {
        $data['details']=$this->jobfair_model->employerDetails();
        $data['jobdetails']=$this->jobfair_model->jobDetails();
        $data['jd']=$this->jobfair_model->jd();
        $this->load->view('header');
        $this->load->view('jd',$data);
        $this->load->view('footer');
    }
    public function download($fileName) 
    {  
        
        if ($fileName) 
        { 
            $file ='./upload/jd/'.$fileName;
            if (file_exists ( $file )) 
            {
                $data = file_get_contents ($file);
                force_download ( $fileName, $data );
            } 
            else 
            {
               redirect('thrissur/company_list');
            }
        }
    }

  public function company_list()
  {
      $data['company']=$this->jobfair_model->get_company_list();
      $data['page_title']=$this->uri->segment('2');
      $this->load->view('header',$data);
      $this->load->view('company_list',$data);
      $this->load->view('footer');
  }
}
?>