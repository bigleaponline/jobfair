<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assesment_controller extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('calicut_jobfair_model');
        $this->load->model('jobfair_model');
        $this->load->library('session');
        $this->load->library('form_validation');
        $this->load->library('email');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('jobfair_helper');
        $this->load->library('pdf');
    }
    public function technical_assesment_test()
    {
        $cand_id=$this->session->userdata['cand_data']['cand_id'];
        $cand_category=$this->session->userdata['cand_data']['cand_category'];
        $testmaster=$this->calicut_jobfair_model->get_test_master($cand_id);
        if($testmaster)
        {
            $data['cand_id']    = $this->session->userdata['cand_data']['cand_id'];
            $data['page_title']=$this->uri->segment('2');
            $data['question_list'] = $this->calicut_jobfair_model->get_technical_questions();
            $start_time = date('H:i:s');
            $this->session->set_userdata('start_time',$start_time);
            $this->load->view('thrissur/calicut_header',$data);
            $this->load->view('thrissur/technical_assesment',$data);
            $this->load->view('thrissur/calicut_footer');         
        }
        else
        {
            redirect('logout');
        }
	}
    public function technical_test_submission()
    {
        $cand_id=$this->session->userdata['cand_data']['cand_id'];
        $cand_category=$this->session->userdata['cand_data']['cand_category'];
        $cand_level=$this->session->userdata['cand_data']['cand_level'];
        $end_time = date('H:i:s');
        $this->session->set_userdata('end_time',$end_time);
        $this->calicut_jobfair_model->save_technicaltest_answer();
        redirect('thrissur/aptitude_report');
    }
    public function aptitude_report()
    {
        $cand_id              =$this->session->userdata['cand_data']['cand_id'];
        $report['result']     = $this->calicut_jobfair_model->get_report($cand_id);
        $result=$this->calicut_jobfair_model->get_report($cand_id);
        if($result)
        {
            $report['cand_id']    = $cand_id;
            $report['cand_level'] = $this->session->userdata['cand_data']['cand_level']; 
            $data['page_title']   = $this->uri->segment('2'); 
            $this->load->view('thrissur/calicut_header',$data);
            $this->load->view('thrissur/aptitude_report',$report);
            $this->load->view('thrissur/calicut_footer');
        }
        else
        {
            redirect('logout');
        }
    }

    public function aptitude_report_pdf()
    {
        $cand_id           =$this->session->userdata['cand_data']['cand_id'];
        $report['cand_id'] = $cand_id;
        $cand_name           =$this->session->userdata['cand_data']['name'];
        $report['name'] = $cand_name;
        $report['qualification'] = $this->calicut_jobfair_model->get_cand_qualification($cand_id);
        $report['cand_level'] = $this->session->userdata['cand_data']['cand_level'];
        $report['chrt_img']       = $this->input->post('chrt_img');
        $report['result']     = $this->calicut_jobfair_model->get_report($cand_id);
        $this->pdf->load_view('thrissur/aptitude_report_pdf',$report);
        $this->pdf->render();
        $this->pdf->stream("aptitude_report_pdf.pdf");
    }



    /* New test Module  start */


    public function assessment_test()
    {
        $cand_id=$this->session->userdata['cand_data']['cand_id'];
        $cand_category=$this->session->userdata['cand_data']['cand_category'];
        $testmaster=$this->calicut_jobfair_model->get_test_master($cand_id);
        if($testmaster)
        {
            $data['cand_id']    = $this->session->userdata['cand_data']['cand_id'];
            $data['page_title']=$this->uri->segment('2');
            $data['question_list'] = $this->calicut_jobfair_model->get_test_questions_1();
            $start_time = date('H:i:s');
            $this->session->set_userdata('start_time',$start_time);
            $this->load->view('thrissur/calicut_header',$data);
            $this->load->view('thrissur/test',$data);
            $this->load->view('thrissur/calicut_footer');         
        }
        else
        {
            redirect('logout');
        }
    }

    public function assesment_test_submission()
    {
        $cand_id=$this->session->userdata['cand_data']['cand_id'];
        $cand_category=$this->session->userdata['cand_data']['cand_category'];
        $topic_id=$this->uri->segment('3');
        $cand_level=$this->session->userdata['cand_data']['cand_level'];
        $end_time = date('H:i:s');
        $this->session->set_userdata('end_time',$end_time);
        $this->calicut_jobfair_model->save_test_answer();
        if($topic_id == 2)
        {
            redirect('thrissur/technical_assesment/3');
        }
        elseif ($topic_id == 3) 
        {
          redirect('thrissur/technical_assesment/4');
        }
        else
        { 
            if ($topic_id == 1) 
            {
                redirect("thrissur/report");
            }
            if($topic_id == 4)
            {
                 redirect("thrissur/technical_report");
            }
        }
    }



    /* New test Module ending */

}
?>