<?php
ob_start();
class Jobfair_model extends CI_Model 
{
    public function __construct()
    {
        parent::__construct();
        $this->db->cache_on();
        $this->load->database();
       
    }
    function login()
    {
        $username=$this->input->post('username');
        $password=base64_encode($this->input->post('password'));
        $query=$this->db->get_where('tbl_candidate_registration',array('cand_username'=>$username,'cand_password'=>$password));
        if($query->num_rows()>0)
        {
           foreach($query->result() as $details)
            $cand_data=array(
                            'cand_id'=>$details->cand_id,
                            'name'=>$details->cand_name,
                            'qualification'=>$details->cand_qualification,
                            'cand_level'=>$details->cand_level,
                            'cand_test_status'=>$details->cand_test_status,
                            'cand_status'=>$details->cand_status,
                            'cand_venue'=>$details->cand_venue,
                            'cand_category'=>$details->cand_category,
                            'cand_username'=>$details->cand_username,
                            'cand_password'=>$details->cand_password
                        );
            return $cand_data;
        }
        else
        {
            return false;
        }
    }
    function signin()
    {
        $this->db->order_by("qualification", "asc");
        $qualification=$this->db->get('tbl_qualificatio_master');
        return $qualification->result();
    }
    function year()
    {
        $year=$this->db->get('tbl_year')->result_array();
        return $year;
    }
    function candidateRegistration()
    {
        $qualification=$this->input->post('qualification');
        if($qualification != 26 || $qualification != 39) {
					    
		$mark = $this->input->post('mark');
		}
		else
		{
		$mark=$this->input->post('marks');
		}
		$year=$this->input->post('passoutyear');
		$weightage=$this->db->get_where('tbl_qualificatio_master',array('qualification_id'=>$qualification))->result();
		foreach($weightage as $w)
		$weight=$w->weightage;
		$qtop=$w->top;
		if($this->input->post('stream'))
		{
		    $streamid=$this->input->post('stream');
		    $stream=$this->db->get_where('tbl_stream_master',array('stream_id'=>$streamid))->result();
		    foreach($stream as $s)
	    	$core=$s->core;
		}
		$cand_image="";
		 $image="";
		if($weight=='0')
		{
		    $image=$this->do_upload_profile_image();
	       // if(!$image)
	       // {
    	   //     $this->session->set_flashdata('imageerr','Failed...Image Size or Format Mismatches!');
    	   //     redirect('sign-in');
    	   //     $cand_image="";
	       // }
	       // else
	       // {
                $cand_image=$image;
		        $cand_category='Normal';
		        $status='Completed';
		        $cand_level = 0;
	       // }
		}
		else if($weight=='1')
		{
		    $cand_category='A';
		    $status='pending';
		    $cand_level = 1;
		}
		else if(($qtop=='1') && ($mark >= '60') && ($year=='2018' || $year=='2019'))
		{
		    $cand_category='D';
		    $status='pending';
		    $cand_level = 2;
		}
		else if($qtop=='1' && $core=='1')
		{
		    $cand_category='C';
		    $status='pending';
		    $cand_level = 1;
		}
		else
		{
		    $cand_category='B';
		    $status='pending';
		    $cand_level = 1;
		}
		$data=array(
						'cand_id'=>mt_rand(100000, 999999),
						'cand_name'=>$this->input->post('name'),
						'cand_dob'=>$this->input->post('dob'),
						'cand_gender'=>$this->input->post('gender'),
						'cand_phone'=>$this->input->post('mobile'),
						'cand_whatsapp'=>$this->input->post('whatsapp'),
						'cand_email'=>$this->input->post('email'),
						'cand_qualification'=>$this->input->post('qualification'),
						'cand_stream'=>$this->input->post('stream'),
						'cand_marks'=>$mark,
						'cand_passoutyear'=>$this->input->post('passoutyear'),
						'cand_password'=>base64_encode($this->input->post('password')),
						'cand_username'=>$this->input->post('username'),
						'cand_date'=>date('d-m-y'),
						'cand_status'=>$status,
						'cand_category'=> $cand_category,
						'cand_level'=> $cand_level,
						'cand_venue'=>$this->input->post('venue'),
						'cand_image'=>$cand_image,
						'cand_backpapers'=>$this->input->post('backpapers'),
						'cand_description'=>$this->input->post('description'),
						'cand_passed'=>$this->input->post('passed'),
						'cand_education_type'=>$this->input->post('type'),
						'cand_address'=>$this->input->post('address'),
						'cand_institution'=>$this->input->post('institution')
					);
					
					if($cand_category == 'Normal')
					{
					    $data['hallticket_category'] = "P";
					}
					else{
					    $data['hallticket_category'] = "";
					}
    	$query=$this->db->insert('tbl_candidate_registration',$data);
    // 	print_r($this->db->last_query());die();
    	if($weight==0)
    	{
    	    $id=$this->db->insert_id();
    	    $query=$this->db->get_where('tbl_candidate_registration',array('id'=>$id));
            if($query->num_rows()>0)
            {
               foreach($query->result() as $details)
                $cand_data=array(
                                'cand_id'=>$details->cand_id,
                                'name'=>$details->cand_name,
                                'qualification'=>$details->cand_qualification,
                                'cand_level'=>$details->cand_level,
                                'cand_test_status'=>$details->cand_test_status,
                                'cand_status'=>$details->cand_status,
                                'cand_venue'=>$details->cand_venue,
                                'cand_category'=>$details->cand_category,
                                'cand_username'=>$details->cand_username,
                                'cand_password'=>$details->cand_password
                            );
                $this->session->set_userdata('cand_data',$cand_data);
            }
            $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'ssl://mail.indiamegajobfairs.com',
                            'smtp_port' => 465,
                            'smtp_user' => 'info@indiamegajobfairs.com',
                            'smtp_pass' => 'bglpindiameg@123',
                            'mailtype'  => 'html', 
                            'charset'   => 'iso-8859-1'
                        );
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            
            $username=$this->input->post('username');
            $password=$this->input->post('password');
            $email=$this->input->post('email');
            $message="Thank you for registering with India Mega Job Fairs .Your credentilas are given below\r\n UserName:".$username."\r\n Password:".$password;
            $this->email->from('info@indiamegajobfairs.com');
            $this->email->to($email); 
            $this->email->subject('India Mega Job Fair');
            $this->email->message($message);
            ini_set("SMTP","mail.indiamegajobfairs.com");
            $this->email->send();
           
            $venue=$this->input->post('venue');
            if($venue=='1')
           	{
           	    $venue='thrissur';
           	}
           	else if($venue=='2')
           	{
           	    $venue='kochi';
           	}
           	if($venue=='3')
           	{
           	    $venue='banglore';
           	}
           	if($venue=='4')
           	{
                $venue='hederbad';
           	}
            redirect($venue.'/hall_ticket');
    	}
    	if($query)
    	{
    	    return true;
    	}
    	else
    	{
    		return false;
    	}
    }
    function profile()
    {
        $mobile=$this->input->post('mobile');
        $name=$this->input->post('name');
        $dob=$this->input->post('dob');
        $where= "cand_phone=$mobile AND cand_name='$name' AND cand_dob='$dob'";
        $this->db->where($where);
        $query=$this->db->get('tbl_candidate_registration');
        $row = $query->result();
        $jsonData = json_encode($row);
        echo $jsonData;
    }
    function getWeightage()
    {
        $qid=$this->input->post('id');
        $query=$this->db->get_where('tbl_qualificatio_master',array('qualification_id'=>$qid));
        $row = $query->result();
        $jsonData = json_encode($row);
        echo $jsonData;
    }
    function getStream()
    {
        if(isset($_POST["data"]))
         {  
            $this->db->order_by("stream_name", "asc");
            $query = $this->db->get_where('tbl_stream_master',array('qualification_id'=>$_POST["data"]));
            $stream = $query->result();
            ?>
            <option value disabled selected>Select Stream</option>
            <?php
            foreach ($stream as $s) 
            {
            ?>
                <option value="<?php echo $s->stream_id; ?>"><?php echo $s->stream_name; ?></option>
                <?php
            }
        }
    }
    function employer_registration()
    {
        $row_no = $this->input->post('row_no');
        $data['company_name']  = $this->input->post('company_name');
        $data['contact_no']  = $this->input->post('contact_no');
        $data['email_id']  = $this->input->post('email_id');
        $data['industry']  = $this->input->post('industry');
        $data['address']  = $this->input->post('address');
        $data['contact_person']  = $this->input->post('contact_person');
        $data['designation']  = $this->input->post('designation');
        $data['emp_date']  = date('m-d-Y');
        $data['emp_venue']  = $this->input->post('venue');
        $employer=$this->db->insert('tbl_employer_registration',$data);
        if($employer)
        {
            $emp_id = $this->db->insert_id();
            $job_title = $this->input->post('job_title');
            $eperience = $this->input->post('eperience');
            $salary = $this->input->post('salary');
            $job_description = $this->input->post('job_description');
            for($i=0;$i<$row_no;$i++)
            {
                $data1['emp_id']  = $emp_id;
                $data1['job_title'] = $job_title[$i];
                $data1['eperience'] = $eperience[$i];
                $data1['salary'] = $salary[$i];
                $data1['job_description'] = $job_description[$i];
                $this->db->insert('tbl_emp_job_details',$data1);
            }
            $cpt=count($_FILES['jd']['name']);
            if($cpt>0)
            {
                for($i=0; $i<$cpt; $i++)
                {
                    $jd=$_FILES['jd']['name'][$i];
                    if($jd!="")
                    {
                        $mimetype = mime_content_type($_FILES['jd']['tmp_name'][$i]);
                        if(in_array($mimetype, array('text/plain', 'application/pdf')))
                        {
                            $jd=time().".".$jd;
                                move_uploaded_file(
                                $_FILES['jd']['tmp_name'][$i], 'upload/jd/' . $jd
                                    );
                        }
                        else
                        {
                            $jd="";
                            $this->session->set_flashdata('jd','Invalid JD..Support Only Pdf and txt');
                        }
                        $data2['emp_id']=$emp_id;
                        $data2['emp_jd']  = $jd;
                        $this->db->insert('tbl_jd',$data2);
                    }
                }
            }
            return true;
        }
        else
        {
            $this->session->set_flashdata('employererror','Failed ..... Please Try Again....');
            return false;
        }
    }
    function companylist()
    {
        if($this->uri->segment('1')!="jobfair")
        {
            $venue=$this->uri->segment('1');
            if($venue=='thrissur')
           	{
           	    $venue=1;
           	}
           	else if($venue=='kochi')
           	{
           	    $venue=2;
           	}
           	else if($venue=='calicutandkochi')
           	{
           	    $venue=3;
           	}
           	if($venue=='northbanglore')
           	{
           	    $venue=4;
           	}
           	if($venue=='hederbad')
           	{
                $venue=5;
           	}
    	    $company=$this->db->get_where('tbl_employer_registration',array('emp_venue'=>$venue))->result();
    	    return $company;
        }
        else
        {
            $company=$this->db->get('tbl_employer_registration')->result();
	        return $company;
        }
    }
    function sponsor_registration()
    {
        $data['spo_name']  = $this->input->post('name');
        $data['spo_no']  = $this->input->post('contact');
        $data['organisation']  = $this->input->post('organisation');
        $data['designation']  = $this->input->post('designation');
        $data['spo_date']  = date('m-d-Y');
        $data['spo_venue']  =  $this->input->post('venue');
        $data['email_id']  =  $this->input->post('email');
        $query=$this->db->insert('tbl_sponsor_registration',$data);
        if($query)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    function get_cand_qualification($qualification)
    {
        $this->db->select("qualification");
        $this->db->from("tbl_qualificatio_master");
        $this->db-> where('qualification_id',$qualification);
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_pg()
    {

        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','PG');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_ug()
    {

        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','UG');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_diploma()
    {
        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','diploma');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_plus_two()
    {

        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','plus_two');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_qualification_sslc()
    {
        $this->db->select('*');
        $this->db->from('tbl_qualificatio_master');
        $this->db->where('qualfcn_catgry','SSLC');
        $this->db->order_by('qualification','ASC');
        $query = $this->db->get()->result_array();
        return $query;
    }
    function get_passion()
    {
        $this->db->select('*');
        $this->db->order_by('passion' ,'ASC');
        $query = $this->db->get('tbl_passion_master')->result_array();
        return $query;
    }
    function get_internship()
    {
        $this->db->select('*');
        $this->db->order_by('internship_name' ,'ASC');
        $query = $this->db->get('tbl_internship_master')->result_array();
        return $query;
    }
    function get_sub_passion()
    {
        if(isset($_POST["data"]))
         {
            $query = $this->db->get_where('tbl_passion_sub_category',array('passion_id'=>$_POST["data"]));
            $stream = $query->result();
            ?>
            <option value disabled selected>Select interest</option>
            <?php
            foreach ($stream as $s) 
            {
            ?>
                <option value="<?php echo $s->sub_passion_id; ?>"><?php echo $s->sub_passion_name; ?></option>
                <?php
            }
        }
    }
    function getState()
    {
        if (! empty($_POST["country_id"])) 
	    {
            $query = $this->db->get_where('tbl_states',array('country_id'=>$_POST["country_id"]));
            $results = $query->result();
            ?>
            <option value disabled selected>Select State</option>
            <?php
            foreach ($results as $state) 
            {
            ?>
                <option value="<?php echo $state->id; ?>"><?php echo $state->name; ?></option>
                <?php
            }
        }
    }
    function getCity()
    {
        if (! empty($_POST["state_id"])) 
        {
            $query =$this->db->get_where('tbl_cities',array('state_id'=>$_POST['state_id']));
            $results =$query->result();
            ?>
            <option value disabled selected>Select City</option>
            <?php
            foreach ($results as $city) 
            {
                ?>
                <option value="<?php echo $city->id; ?>"><?php echo $city->name; ?></option>
                <?php
            }
        }
    }
    function candidate_profile_registration($video_name, $video_name_1,$image,$resume)
    { 
        $country = $this->input->post('pref_overseas_country');
        $data = array(
                        'candidate_id' => $this->input->post('cand_id'),
                        'name' => $this->input->post('name'),
                        'high_qualification_id' => $this->input->post('qualification_id'),
                        'have_experience' => $this->input->post('have_experience'),
                        'cand_address' => $this->input->post('cand_address'),
                        'cand_country' => $this->input->post('country'),
                        'cand_state' => $this->input->post('state'),
                        'cand_city' => $this->input->post('city'),
                        'passion_id' => $this->input->post('passion_id'),
                        'interest_id' => $this->input->post('interest_id'),
                        'done_internship' => $this->input->post('done_internship'),
                        'internship_detail' => $this->input->post('internship_detail'),
                        'interest_internship' => $this->input->post('interest_internship'),
                        'pref_intern_id' => $this->input->post('pref_intern_id'),
                        'pref_overseas_country' => implode(",", $country),
                        'makein_india_interest' => $this->input->post('makein_india_interest'),
                        'makein_india_interest' => $this->input->post('makein_india_interest'),
                        'cand_resume' => $resume,
                        'candidate_video'=>$video_name,
                        'candidate_video1'=>$video_name_1,
                        'cand_img'=>$image 
                    );
        $candpro=$this->db->insert('tbl_candidate_profile', $data);
        if($candpro)
        {
            $cand_id = $this->db->insert_id();
            $row_no  = 5;
            $qualfcn_catgry  = $this->input->post('qualfcn_catgry');
            $course_id  = $this->input->post('course_id');
            // $stream_id  = $this->input->post('stream_id');
            $board  = $this->input->post('board');
            $percent  = $this->input->post('percent');
            $pass_out  = $this->input->post('pass_out');
            for($i=0;$i<$row_no;$i++)
            {
                $data1['qualfcn_catgry'] = $qualfcn_catgry[$i];
                $data1['course_id'] = $course_id[$i];
                $data1['stream_id'] = $this->input->post('stream_id_'.$i);
                $data1['board'] = $board[$i];
                $data1['percent'] = $percent[$i];
                $data1['pass_out'] = $pass_out[$i];
                $data1['cand_id'] = $cand_id;
                $candedu=$this->db->insert('tbl_candidate_edu_details',$data1);
            }
            $row_no1  = $this->input->post('row_no');
            $company_name = $this->input->post('company_name');
            $job_position  = $this->input->post('job_position');
            $duration  = $this->input->post('duration');
            for($i=0;$i<$row_no1;$i++)
            {
                $data2['company_name'] = $company_name[$i];
                $data2['job_position'] = $job_position[$i];
                $data2['duration'] = $duration[$i];
                $data2['candidate_id'] = $cand_id;
                $this->db->insert('tbl_candidate_experience',$data2);
            }
            $data3['cand_status']='completed';
            $this->db->where('cand_id',$this->input->post('cand_id'));
            $this->db->update('tbl_candidate_registration',$data3);
            return true;
        }
        else
        {
            return false;
        }
    }
    function get_candidate_details()
    {
        $cand=$this->input->post('cand_id');
        $query=$this->db->get_where('tbl_candidate_profile',array('candidate_id'=>$cand))->result();
        if($query)
        {
            echo $query;
        }
        else
        {
            return false;
        }
    }
    function get_cand_profile($candid)
    {
        $query=$this->db->get_where('tbl_candidate_profile',array('candidate_id'=>$candid));
        if($query->num_rows()>0)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
    function do_upload_profile_image() 
	{
        if($this->session->userdata('cand_username') != ''){
        $cand_username = $this->session->userdata['cand_data']['cand_username'];
	    }
	    else{
	         $cand_username = $this->input->post('username');
	         
	    }
        
        $config['upload_path'] = './upload/profile_images';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = '300000';
        $config['max_width'] = '150';
        $config['max_height'] = '200';
        $config['overwrite'] = TRUE;
        $profileimage = time().$_FILES['profile_img']['name']; 
        $config['file_name'] = $cand_username;
        $this->load->library('upload', $config);
        $this->upload->initialize($config);
       
        if (!$this->upload->do_upload('profile_img')) 
        {
            $error = array('error' => $this->upload->display_errors()); 
           // print_r($error);exit();
            return FALSE;
        } 
        else 
        {
            $data = array('upload_data' => $this->upload->data());
            return $data['upload_data']['file_name'];
        }
	}
	function employerDetails()
	{
	    $id=$this->uri->segment('3');
	    $query=$this->db->get_where('tbl_employer_registration',array('emp_id'=>$id));
	    return $query->result_array();
	}
	function jobDetails()
	{
	    $id=$this->uri->segment('3');
	    $query=$this->db->get_where('tbl_emp_job_details',array('emp_id'=>$id));
	    return $query->result_array();
	}
	function jd()
	{
	    $id=$this->uri->segment('3');
	    $query=$this->db->get_where('tbl_jd',array('emp_id'=>$id));
	    return $query->result_array();
	}
    public function get_id()
    {
        $this->db->select_max("id");
        $this->db->from('tbl_candidate_registration');
        $query = $this->db->get()->result_array();
        $result = $query[0]['id']; 
        //print_r($this->db->last_query());die();
        if($result == '')
        {
            $result = 'INJBFR001';
        }
        else{
            return $result;
        }
    }

    public function get_company_list()
    {
        $this->db->select('A.*,B.*');
        $this->db->FROM('tbl_employer_registration A');
        $this->db->join('tbl_emp_job_details B', 'A.emp_id=B.emp_id','left');
        $this->db->order_by('A.emp_id' ,'ASC');

        $query = $this->db->get()->result_array();
        // print_r($this->db->last_query());die();
        
        return $query;
    }
}
?>