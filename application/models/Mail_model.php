<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once(APPPATH . "third_party/phpmailer/src/Exception.php");
require_once(APPPATH . "third_party/phpmailer/src/PHPMailer.php");
require_once(APPPATH . "third_party/phpmailer/src/SMTP.php");


class Mail_model extends CI_Model

{

    public function __construct() {
        $this->load->database();
    }


    public function sendEmail($subject, $mail_body, $email, $full_name) {

        $message = $mail_body;

        $mail = new PHPMailer();

        $mail->SMTPDebug = 0;                                    // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'mail.indiamegajobfairs.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'admin@indiamegajobfairs.com';                 // SMTP username
        $mail->Password = 'admin@bglp';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 26;                                       // TCP port to connect to

        //Recipients
        $mail->setFrom('info@indiamegajobfairs.com', 'indiamegajobfairs.com');
        $mail->addAddress($email, $full_name);     // Add a recipient

        //Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $subject;
        $mail->Body    = $message;
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();

        return true;
    }

   public function sendRegistrationEmail($username,$password,$email) {

        $subject = "Welcome Mail - indiamegajobfairs.com";
        $register_body = "
           <tr align='center'>
               <td style='font-family: Arial;padding-bottom: 2.1rem'><h3 style='margin: 10px 0;font-size: 1.1rem; font-weight: 500;'>You have been registered to the Indiamegajobfairs.</h3></td>
           </tr>
           <tr align='center'>
               <td style='padding-bottom: 0.5rem'><strong style='font-size: 1.3rem; font-family: Arial, sans-serif'>Your user details are,</strong></td>
           </tr>
           <tr align='center'>
               <td>
                   <ul class='list' style='list-style-type: none; display: inline-block; font-family: Verdana, Geneva, sans-serif; padding: 0;
               margin: 0;'>
                       <li>User name : $username</li>
                   </ul>
               </td>
           </tr>
           <tr align='center'>
               <td style='padding-bottom: 2.1rem'>
                   <ul class='list' style='list-style-type: none; display: inline-block; font-family: Verdana, Geneva, sans-serif; padding: 0;
               margin: 0;'>
                       <li>Password : $password</li>
                   </ul>
               </td>
           </tr>
            ";

        $this->sendEmail($subject, $register_body, $email, $username);

        return true;
    }

   
    

}

/* 

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */



