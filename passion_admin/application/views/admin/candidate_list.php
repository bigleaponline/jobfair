<!DOCTYPE html>
<html dir="ltr" lang="en">

<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
<style>

table th , table td{
    text-align: center;
}

table tr:nth-child(even){
    background-color: #BEF2F5
}

.pagination li:hover{
    cursor: pointer;
}
ul.pagination span {
    outline-color: black;
    padding: 4px;
    border: 1px solid #000;
}
.pagination li.active {
    background-color: aqua;
}
ul.pagination span.active {
    padding: 4px;
    background-color: red;
}
input {
    padding: 10px 8px;
    width: 300px;
}
.search_div{
    padding-bottom: 20px;
    width:100%;
}
.search_width{
    /*margin-right:2px;*/
    /*width: 105px;*/
    width: 14%;
    padding: 1% 1% 1% 3%;
    /*text-align:right;*/
}
.form-control-feedback {
    position: absolute;
    z-index: 2;
    /*display: block;*/
    width: 2.375rem;
    height: 2.375rem;
    line-height: 2.375rem;
    /*text-align: center;*/
    pointer-events: none;
    color: #aaa;
    padding-left: 2px;
}
tfoot input {
        width: 100%;
        padding: 3px;
        box-sizing: border-box;
    }
tfoot tr {
    display: table-row !important;
}
tfoot {
  display: table-header-group;
}


</style>
</head>

<body>
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <div id="main-wrapper">
        <div class="page-wrapper">
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-12 d-flex no-block align-items-center">
                        <h4 class="page-title">Candidate List</h4>
                        <div class="ml-auto text-right">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">candidate List</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">


                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered" id= "table-id">
                                        <thead class="my_head">
                                            <tr>
                                                <th width="5%">Sl.No</th>
                                                <th width="5%">ID Number</th>
                                                <th width="10%">Name</th>
                                                <th width="10%">Email</th>
                                                <th width="10%">Whatsapp Number</th>
                                                <th width="10%">Status</th>
                                                <th width="10%">Options</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>ID Number</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Contact Number</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                            <tbody class="my-list">
                                        <?php
                                        $count=1;
                                       
                                        foreach($candidate_list as $u){
                                        ?>
                                            <tr>
                                                <td><?php echo $count; ?></td>
                                                <td><?php echo $u['cand_id']; ?></td>
                                                <td><?php echo $u['name']; ?></td>
                                                <td><?php echo $u['email']; ?></td>
                                                <td><?php echo $u['contactnumber']; ?></td>
                                                <td align="center"><?php if($u['status'] == '0'){ ?><a class="btn btn-info btn-xs" title="View" href="<?php echo site_url("admin_controller/approve_candidate/" . $u['cand_id']); ?>">Approve</a>
                                            <?php }else { ?>
                                                <a class="btn btn-success btn-xs" title="View" href="<?php //echo site_url("admin_controller/approve_candidate/" . $u['cand_id']); ?>">Approved</a></td>
                                            <?php } ?>
                                                <td align="center"><a class="btn btn-info btn-xs" title="View" href="<?php echo site_url("admin_controller/view_candidate_details/" . $u['cand_id']); ?>"><i class="fa fa-eye"></i></a>
                                                
                                                <a class="btn btn-danger btn-xs confirmDelete" title="Delete" onClick="return confirm('Are you sure you want to delete')" href="<?php echo site_url("admin_controller/delete_candidate_details/" .$u['cand_id']); ?>"><i class="fa fa-times-circle"></i></a>
                                                <!-- <a class="btn btn-danger btn-xs confirmDelete" title="Delete" href="<?php echo site_url('candidate_list/2'); ?>"><i class="fa fa-times-circle"></i></a>-->
                                                </td>
                                            </tr>
                                        <?php $count++;} ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/libs/jquery/dist/jquery.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/popper.js/dist/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/libs/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url(); ?>assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/sparkline/sparkline.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>dist/js/waves.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url(); ?>dist/js/sidebarmenu.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url(); ?>dist/js/custom.min.js"></script>
    <!-- this page js -->
    <script src="<?php echo base_url(); ?>assets/extra-libs/multicheck/datatable-checkbox-init.js"></script>
    <script src="<?php echo base_url(); ?>assets/extra-libs/multicheck/jquery.multicheck.js"></script>
<script>
    $(document).ready(function() {
    $('#table-id').dataTable( {
        "aoColumns": [
            null,
            null,
            null,
            { "sType": "date-uk" },
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null
        ]
    });
    });
</script>
<script>
$(document).ready(function () {
$('#table-id').DataTable();
$('.dataTables_length').addClass('bs-select');
});
</script>
<script>
    $('.dataTables_filter input')
 .off()
 .on('keyup', function() {
    $('#table-id').DataTable().search(this.value.trim(), true, false).draw();
 }); 
</script>
   

<script>
    $("#my-search-input").keyup(function () {
    var search = $(this).val();
    $(".my-list").children().show();
    $('.noresults').remove();
    if (search) {
        $(".my-list").children().not(":containsNoCase(" + search + ")").hide();
        $(".my-list").each(function () {
            if ($(this).children(':visible').length == 0) $(this).append('<tr class="noresults"><td colspan="9"><em>No Results</em></td></tr>');
        });

    }
});

$.expr[":"].containsNoCase = function (el, i, m) {
    var search = m[3];
    if (!search) return false;
    return new RegExp(search, "i").test($(el).text());
};
</script>
 <script>
// function myFunction(arg) {
//   var input, filter, table, tr, td, i,j, txtValue,d1,d2;
//   var id = arg.getAttribute('id');
//   //alert(id);
//     if(id=="by_date")
//         j=3;
//     if(id=="InputGender")
//         j=4;
//     if(id=="InputQlficn")
//         j=7;
//     if(id=="InputStream")
//         j=8;
//     if(id=="InputPassYr")
//         j=11;
//     if(id=="InputMarks")
//         j=12;
//     if(id=="InputDistrict")
//         j=13;
//     if(id=="InputBackPapers")
//         j=10;
    
//   input = document.getElementById(id);
//   d1    =Date.parse(input.value);
//   //alert(d1);
//   filter = input.value.toUpperCase();
  
//   table = document.getElementById("table-id");
//   tr = table.getElementsByTagName("tr");
//   for (i = 0; i < tr.length; i++) {
//     td = tr[i].getElementsByTagName("td")[j];
    
//         if (td) {
//           txtValue = td.textContent || td.innerText;
//           d2    =Date.parse(txtValue);  
//           //alert(d2);
//           if(id !="by_date")
//           {//alert(txtValue);
//               if (txtValue.toUpperCase().indexOf(filter) > -1){
//                 tr[i].style.display = "";
//               } else {
//                 tr[i].style.display = "none";
//               }
              
//           }
//           else
//           {
//               //alert("txtValue");
//               if (d1 == d2){
//                   //alert("jhvj");
//                 tr[i].style.display = "";
//               } else {
//                  // alert(d1+d2);
//                 tr[i].style.display = "none";
//               }
             
//           }
//         }       
//   }
// }
</script>
<script>
    $(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#table-id tfoot th').each( function () {
        var title = $(this).text();
        $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    } );
 
    // DataTable
    var table = $('#table-id').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change clear', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
} );
</script>
</body>

</html>