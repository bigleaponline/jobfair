 <!DOCTYPE html>
 <html>
 <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
     <title></title>
 </head>
 <body>
 
 <table class="table table-striped table-bordered" id= "table-id">
                                        <thead class="my_head">
                                            <tr>
                                                <th width="5%">Sl.No</th>
                                                <th width="5%">ID Number</th>
                                                <th width="10%">Name</th>
                                                <th width="10%">Email</th>
                                                <th width="10%">Whatsapp Number</th>
                                                <th width="10%">Status</th>
                                                <th width="10%">Options</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <th></th>
                                                <th>ID Number</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Whatsapp Number</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                            <tbody class="my-list">
                                        <?php
                                        $count=1;
                                        foreach($candidate_list as $u){
                                        ?>
                                            <tr>
                                                <td><?php echo $count; ?></td>
                                                <td><?php echo $u['cand_id']; ?></td>
                                                <td><?php echo $u['name']; ?></td>
                                                <td><?php echo $u['email']; ?></td>
                                                <td><?php echo $u['whatsappnumber']; ?></td>
                                                <td align="center"><?php if($u['status'] == ''){ ?><a class="btn btn-info btn-xs" title="View" href="<?php echo site_url("admin_controller/approve_candidate/" . $u['cand_id']); ?>">Approve</a>
                                            <?php }else { ?>
                                                <a class="btn btn-success btn-xs" title="View" href="<?php echo site_url("admin_controller/approve_candidate/" . $u['cand_id']); ?>">Approved</a></td>
                                            <?php } ?>
                                                <td align="center"><a class="btn btn-info btn-xs" title="View" href="<?php echo site_url("admin_controller/view_candidate_details/" . $u['cand_id']); ?>"><i class="fa fa-eye"></i></a>
                                                
                                                <a class="btn btn-danger btn-xs confirmDelete" title="Delete" onClick="return confirm('Are you sure you want to delete')" href="<?php echo site_url("admin_controller/delete_candidate_details/" .$u['cand_id']); ?>"><i class="fa fa-times-circle"></i></a>
                                                 <a class="btn btn-danger btn-xs confirmDelete" title="Delete" href="<?php echo site_url('candidate_list/2'); ?>"><i class="fa fa-times-circle"></i></a>
                                                </td>
                                            </tr>
                                        <?php $count++;} ?>
                                        </tbody>
                                    </table>
 
 </body>
 </html>
